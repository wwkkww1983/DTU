#ifndef OS_H
#define OS_H

#include "Hal.h"
#include "os_timer.h"
#include "app.h" //12/19


typedef unsigned char       BOOL;
typedef unsigned char       bool;

// Data
typedef unsigned char       BYTE;
typedef unsigned short      WORD;
typedef unsigned long       DWORD;

// Unsigned numbers
typedef unsigned char       UINT8;
typedef unsigned char       INT8U;
typedef unsigned short      UINT16;
typedef unsigned short      INT16U;
typedef unsigned long       UINT32;
typedef unsigned long       INT32U;

// Signed numbers
typedef signed char         INT8;
typedef signed short        INT16;
typedef signed long         INT32;

/* These types must be 16-bit, 32-bit or larger integer */
typedef int		INT;
typedef unsigned int	UINT;

/* These types must be 8-bit integer */
typedef signed char	CHAR;
typedef unsigned char	UCHAR;

/* These types must be 16-bit integer */
typedef short		SHORT;
typedef unsigned short	USHORT;

/* These types must be 32-bit integer */
typedef long		LONG;
typedef unsigned long	ULONG;
typedef unsigned long	DWORD;


//-----------------------------------------------------------------------------
// Common values
#ifndef FALSE
    #define FALSE 0
#endif

#ifndef false
    #define false 0
#endif

#ifndef TRUE
    #define TRUE 1
#endif

#ifndef true
    #define true 1
#endif

#ifndef NULL
    #define NULL 0
#endif

#ifndef HIGH
    #define HIGH 1
#endif

#ifndef LOW
    #define LOW 0
#endif

#define OS_ENTER_CRITICAL(i_state)  HAL_ENTER_CRITICAL(i_state)
#define OS_EXIT_CRITICAL(i_state)   HAL_EXIT_CRITICAL(i_state)

#define OS_RADIO_DATA_MAXLEN      64
#define OS_UART_DATA_MAXLEN       64
#define OS_MAX_DEVICE_COUNT       64

#define UINT32_IS_LESS(n1, n2) ((n1 - n2) & ((uint32_t)1 << 31))
#define OS_SYSTIME_BEYOND(time) UINT32_IS_LESS(time, os_timer_get_systime())
#define OS_SYSTIME_BEYOND_SPAN(time, span) (os_timer_get_systime() - time > span)

#define os_printf(...) printf(__VA_ARGS__)
#define os_log(...) os_printf("%s-%s[%d]:", __FILE__, __FUNCTION__, __LINE__);os_printf(__VA_ARGS__);os_printf("\r\n")

#define MAIN_MODULE

#endif /* OS_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

