//#include "ctrl.h"
#include "net.h"
#include "hal_wait.h"
#include "userSys.h"
//#include "gateway.h"

#define UPGRADE_PROGRAM_INFO   0x03
#define UPGRADE_PROGRAM_DATA   0x04
#define UPGRADE_PART_SIZE      64

#define UP_FROM_MAIN          1
#define UP_FROM_SUB           0

typedef struct up_frame_header
{
    uint8_t from;
    uint8_t segaddr[NWK_SEGADDR_LEN];
    uint8_t payload[];
} up_frame_header_t;

void ctrl_upgrade_transmit(uint8_t len)
{
    up_frame_header_t *up_frm = phy_get_trans_payload();
    up_frm->from = UP_FROM_MAIN;
    memcpy(up_frm->segaddr, nwk_get_segaddr(), NWK_SEGADDR_LEN);
    //phy_transmit(PHY_TYPE_UPGRADE, len + sizeof(up_frame_header_t));
    phy_transmit_upgrade(len + sizeof(up_frame_header_t));
}

void *ctrl_upgrade_get_trans_payload()
{
    return ((up_frame_header_t *)phy_get_trans_payload())->payload;
}

static void _ctrl_upgrade_deal(up_frame_header_t *up_frm, uint8_t len)
{
#if 0	
#ifdef MAIN_MODULE
//  uint8_t *send_data;

    if(up_frm->from != UP_FROM_SUB
            || (memcmp(up_frm->segaddr, NWK_NULL_SEGADDR, NWK_SEGADDR_LEN) != 0
                && memcmp(up_frm->segaddr, nwk_get_segaddr(), NWK_SEGADDR_LEN) != 0)) //��ַ����
    {
        return;
    }
    //SysLog(SYS_LOG_DEBUG, "");
    //SysDataPrint(up_frm->payload, len - sizeof(up_frame_header_t));
    GatewayDeviceUpgradeRecv(up_frm->payload, len - sizeof(up_frame_header_t));

#endif // MAIN_MODULE
#endif
}

void ctrl_deal(phy_header_t *phy)
{
    uint8_t data_len;
    data_len = phy->len - PHY_HEAD_LEN;

    switch(phy->type)
    {
        case PHY_TYPE_UPGRADE:
            _ctrl_upgrade_deal((up_frame_header_t *)phy->payload, data_len);
            break;
    }
}

//void ctrl_upgrade_start(bool state)
//{
//  phy_rf_start_upgrade(state);
//}

