#include "link.h"
#include "net.h"
#include "mlist.h"
#include "os_timer.h"
#include "hal_rnd.h"

#define LINK_RESPONSE_DEAL_DELAY  1200
#define LINK_BUSY_TIME            500
#define LINK_RECORD_SIZE  OS_MAX_DEVICE_COUNT

enum
{
    LINK_REQUEST,
    LINK_RESPONSE,
    LINK_BUILD,
};

enum link_state
{
    UNLINKED,
    LINKING,
    LINKED,
};

static void _response_link(void *p);
static void _link_response_deal(void);

static uint8_t g_link_busy = 0;
static os_time_t g_link_busy_time = 0;
static uint8_t g_link_hops = 0xff;
static uint8_t g_link_state = UNLINKED;
static os_time_t g_link_deal_time = 0;
static uint8_t g_link_is_stable = 0;
static link_record_t g_records[LINK_RECORD_SIZE];

static void _response_link(void *p)
{
    uint8_t addr = (uint32_t)p;
    link_header_t *link = nwk_get_trans_payload();
    link->type = LINK_RESPONSE;
    link->payload[0] = g_link_hops + 1;
//  os_log("");//+cbl 0207
    nwk_transmit(NWK_FRAME_LINK, addr, 2, NWK_FLAG_SIMPLE);
}

static void _link_response_deal()
{
    link_header_t *link = nwk_get_trans_payload();
    if(g_records[0].dstaddr == 0)
    {
        g_link_state = LINKED;
        link->type = LINK_BUILD;
        g_link_is_stable = 1;
//  os_log("send LINK_BUILD");//+cbl 0207
        nwk_transmit(NWK_FRAME_LINK, 0, 1, 0);
    }
    else
    {
        g_link_state = UNLINKED;
    }
}

void link_init()
{
    memset(g_records, 0xff, sizeof(g_records));
    if(nwk_get_role() == NWK_ROLE_COORDINATOR)
    {
        g_link_is_stable = 1;
        g_link_hops = 0;
        g_link_state = LINKED;
    }
}

void link_request()
{
    link_header_t *link;

    if(g_link_busy)
    {
        return;
    }

    if(g_link_state == LINKING)
    {
        return;
    }

    //os_log("");

    //设置连接中，并在LINK_RESPONSE_DEAL_DELAY + 随机时间后处理连接
    link = (link_header_t *)nwk_get_trans_payload();
    link->type = LINK_REQUEST;
//  os_log("LINK_REQUEST");//+cbl 0207
    if(nwk_transmit(NWK_FRAME_LINK, NWK_BROADCAST_ADDR, sizeof(link_header_t), 0))
    {
        g_link_state = LINKING;
        g_link_deal_time = os_timer_get_systime() + LINK_RESPONSE_DEAL_DELAY + hal_rnd_range(1, 200);
    }
}

void link_disattach()
{
    uint8_t i_state;

    //os_log("");
    HAL_ENTER_CRITICAL(i_state);
    g_records[0].dstaddr = 0xff;
    g_records[0].nextaddr = 0xff;
    g_link_hops = 0xff;
    g_link_state = UNLINKED;
    g_link_is_stable = 0;
    HAL_EXIT_CRITICAL(i_state);
}

void link_add_record(uint8_t dstaddr, uint8_t nextaddr, uint8_t sleepmode)
{
    //os_log(" %d %d %d %d", dstaddr, nextaddr, role, sleepmode);

    if(dstaddr >= OS_MAX_DEVICE_COUNT)
    {
        return;
    }

    if(dstaddr == 0 && nextaddr == 0)
    {
        g_link_hops = 1;
    }

    g_records[dstaddr].dstaddr = dstaddr;
    g_records[dstaddr].nextaddr = nextaddr;
    g_records[dstaddr].sleepmode = sleepmode;
}

link_record_t *link_get_record(uint8_t addr)
{
    if(addr >= OS_MAX_DEVICE_COUNT)
    {
        return NULL;
    }

    if(g_records[addr].dstaddr != addr)
    {
        return NULL;
    }
    return &g_records[addr];
}

uint8_t link_get_father()
{
    return g_records[0].nextaddr;
}

uint8_t link_is_child(uint8_t addr)
{
    if(g_records[addr].dstaddr == 0xff)
    {
        return 0;
    }

    return (g_records[addr].nextaddr == addr && addr != g_records[0].nextaddr);
}

uint8_t link_is_build()
{
    return g_link_state == LINKED;
}

void link_recv_cb(link_header_t *plink, uint8_t srcaddr, uint8_t lastaddr, uint8_t sleepmode, int8_t rssi)
{
    g_link_busy = 1;
    g_link_busy_time = os_timer_get_systime();

    if(plink->type == LINK_RESPONSE)
    {
//      os_log("plink->type == LINK_RESPONSE");
        uint8_t tmp_hops = plink->payload[0];
        if(g_link_hops <= tmp_hops)
        {
            return;
        }

        g_link_hops = tmp_hops;
        g_records[0].dstaddr = 0;
        g_records[0].nextaddr = srcaddr;
        g_records[0].sleepmode = 0;
//    os_log("Link Success.");
        return;
    }

    if(nwk_get_role() == NWK_ROLE_ENDDEVICE || !link_is_build() || !g_link_is_stable)
    {
        return;
    }

    if(plink->type == LINK_REQUEST)
    {
        //协调节点先回复
        if(nwk_get_role() == NWK_ROLE_COORDINATOR)
        {
            os_timer_set(_response_link, 100, (void *)srcaddr, 0);
        }
        else
        {
            os_timer_set(_response_link, hal_rnd_range(20, 40) * 10, (void *)srcaddr, 0);
        }

        return;
    }

    if(plink->type == LINK_BUILD)
    {
        link_add_record(srcaddr, lastaddr, sleepmode);
        return;
    }

    return;
}

void link_set_stable(uint8_t is_stable)
{
    g_link_is_stable = is_stable;
}

void link_poll()
{
    //清除busy
    if(g_link_busy && OS_SYSTIME_BEYOND_SPAN(g_link_busy_time, LINK_BUSY_TIME))
    {
        g_link_busy = 0;
    }

    //处理连接
    if(g_link_state == LINKING && OS_SYSTIME_BEYOND(g_link_deal_time))
    {
        _link_response_deal();
    }

}
