#include "Hal.h"
#include "userSys.h"

static SensorInfo_t g_sensorInfo;

void HalSensorInit(void)
{
	/* PM2.5 CO2 by uart */
	HalPM25CO2Init();  
	/* sht20  io simulate I2C*/
	HalSHT20Init();
}

void HalSensorPoll(void)
{
	HalPM25CO2Poll();
	HalSHT20Poll();
}

SensorInfo_t *HalGetSensorInfo(void)
{
	g_sensorInfo.pm25Co2Info = HalGetPM25CO2Info();
	g_sensorInfo.sht20Info   = HalGetSHT20Info();
	return &g_sensorInfo;
}

