#include "SysTimer.h"

static SysTimer_t g_timers[SYS_TIMER_LIMIT];

void SysTimerInit()
{

}

static void timerExecute(SysTimer_t *timer)
{
    timer->run(timer->arg);

    if(!(timer->type & SYS_TIMER_REPEAT))
    {
        if(!(timer->type & SYS_TIMER_NEED_UNSET))
        {
            timer->alloced = 0;
        }
    }
    else
    {
        timer->startTime = SysTime();
    }
}

void SysTimerUnSet(SysTimer_t *timer)
{
    if(timer)
    {
        timer->alloced = 0;
    }
}

SysTimer_t *SysTimerSet(SysTimerRun_t run, uint32_t span, uint8_t type, void *arg)
{
    int i;
    uint8_t istate;
    SysTimer_t *timer = NULL;

    //SYS_ENTER_CRITICAL(istate);
    HAL_ENTER_CRITICAL(istate);
    for(i = 0; i < SYS_TIMER_LIMIT; i++)
    {
        if(g_timers[i].alloced)
        {
            continue;
        }
        timer = &g_timers[i];
        timer->alloced = 1;
        break;
    }
    //SYS_EXIT_CRITICAL(istate);
    HAL_EXIT_CRITICAL(istate);

    if(!timer)
    {
        return NULL;
    }

    timer->arg = arg;
    timer->run = run;
    timer->span = span;
    timer->startTime = SysTime();
    timer->type = type;

    if(timer->type & SYS_TIMER_IMME)
    {
		hal_wdt_feed();
        timerExecute(timer);
    }

    return timer;
}

void SysTimerPoll()
{
    int i;
    for(i = 0; i < SYS_TIMER_LIMIT; i++)
    {
        if(!g_timers[i].alloced)
        {
            continue;
        }

        if(SysTimeHasPast(g_timers[i].startTime, g_timers[i].span))
        {
            timerExecute(&g_timers[i]);
        }
    }
}

void SysTimerUpdate(SysTimer_t *timer)
{
    timer->startTime = SysTime();
}

