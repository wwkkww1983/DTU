/********************************************************************************
  * @file    Project/STM32F0xx_StdPeriph_Templates/main.c
  * @author  MCD Application Team
  * @version V1.4.0
  * @date    24-July-2014
  * @brief   Main program body
  *******************************************************************************/

#include "Hal.h"
#include "userSys.h"
#include "net.h"
#include "os_timer.h"
#include "app.h"
#include "ModuleTester.h"
#include "SDCardRecord.h"
#include <string.h>

int main(void)
{
	HalInit();
	SysOSInit();
	MenuConfigInit();
	SDCardRecordInit();
	ModuleTesterInit();
    SysDevicesInit();
    while(1)
    {    	
		HalPoll();
        SysPoll();
        net_poll();
        os_timer_poll();
		SDCardRecordPoll();
        ModuleTesterPoll();
		MenuConfigPoll();
    }
}

