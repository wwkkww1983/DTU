#ifndef MENUCONFIG_H
#define MENUCONFIG_H

#include "os.h"
#include "stdarg.h"


typedef struct MenuconfigInfo_st
{
	uint8_t saveFlag;
	uint8_t version[4];
	char id[8];
	char pin[64];
	uint8_t tm;
	uint8_t hum;
	uint8_t pm;
	uint8_t co2;
	uint8_t shtNum;
	uint8_t wmNum;
	uint8_t skNum;
}MenuconfigInfo_t;

void MenuConfigInit(void);
void MenuConfigPoll(void);
uint8_t MenuConfigRecv(const uint8_t *buf, uint32_t len);
void MenuConfigPrintf(char *fmt,...);
uint8_t MenuConfigISStart(void);
MenuconfigInfo_t *GetConfig(void);


#endif

