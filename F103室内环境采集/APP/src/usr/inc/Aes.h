#ifndef AES_H
#define AES_H
#include "userSys.h"

int AesEncrypt(const unsigned char *in, unsigned char *out, int len,
               const unsigned char *key, int keylen);
int AesDecrypt(const unsigned char *in, unsigned char *out, int len,
               const unsigned char *key, int keylen);

#endif // AES_H
