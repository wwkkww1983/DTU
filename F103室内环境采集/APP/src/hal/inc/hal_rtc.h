#ifndef HAL_RTC_H
#define HAL_RTC_H
#include "os.h"

typedef struct rtc_time_st
{
	uint8_t hour;
	uint8_t min;
	uint8_t sec;
	uint16_t year;
	uint8_t  month;
	uint8_t  date;
	uint8_t  week;

	uint32_t secCount;
	
}rtc_time_t;

void RTCInit(void);
uint8_t SetRTCTime(uint32_t secCount);
uint32_t GetRTCTime(void);
rtc_time_t *RTCTransDate(u32 secCount);
u32 RTCTransSecCount(u16 year,u8 mon,u8 day,u8 hour,u8 min,u8 sec);


#endif

