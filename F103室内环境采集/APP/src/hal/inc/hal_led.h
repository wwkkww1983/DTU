#ifndef HAL_LED_H
#define HAL_LED_H
#include "os.h"

//LED
#define LED1_PORT  GPIOD
#define LED1_PIN   GPIO_Pin_6  // 3

#define LED2_PORT  GPIOD
#define LED2_PIN   GPIO_Pin_7 // 2

#define LED3_PORT  GPIOB
#define LED3_PIN   GPIO_Pin_5 // 1

typedef enum
{
	HAL_LED_1,
	HAL_LED_2,
	HAL_LED_3,
	HAL_LED_COUNT
}HAL_LED_TYPE;

void HalLedInit(void);
void HalLedPoll(void);
void HalLedON(uint8_t ledNo);
void HalLedOFF(uint8_t ledNo);
void HalLedToggle(uint8_t ledNo);
void HalLedSetStatus(uint8_t ledNo, uint8_t status);
uint8_t HalLedGetStatus(uint8_t ledNo);
void HalLedStartBlink(uint8_t ledNo, uint8_t freg, uint16_t repatTimes);
void HalLedStopBlink(uint8_t ledNo);
uint8_t HalLedIsBlank(uint8_t ledNo);

#endif

