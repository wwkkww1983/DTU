#ifndef HAL_UART_H
#define HAL_UART_H
#include "stdint.h"
#include <stdio.h>
#include <stm32f10x.h>

/* 串口定义 */
typedef enum
{
    HAL_UART_1,	/* WIFI COM */
	HAL_UART_2,	/* LOG COM */
	HAL_UART_3,	/* MHZ19 COM */
	HAL_UART_4,	/* BLE COM */
	HAL_UART_5,	/* PM2.5 COM */
    HAL_UART_COUNT,
} HalUart_t;

typedef uint8_t(*HalUartRecvCallback_t)(const uint8_t *data, uint32_t len);

void HalUartConfig(HalUart_t uart, uint32_t baudRate, HalUartRecvCallback_t cb);

/**
 *  写串口
 *  @param uart  指定串口
 *  @param data 数据
 *  @param len 数据长度
 *  @return 成功返回数据长度失败返回0
 */
uint16_t HalUartWrite(HalUart_t uart, const uint8_t *data, uint16_t len);
void hal_uart_rcv_poll(void);


#endif // HAL_UART_H
