#ifndef HAL_WDT
#define HAL_WDT
#include "hal.h"
#include "os.h"

void hal_wdt_init(void);
void hal_wdt_feed(void);

#endif // HAL_WDT
