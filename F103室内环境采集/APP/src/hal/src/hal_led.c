#include "Hal.h"
#include "userSys.h"

typedef struct Led_Blink_st
{
	uint8_t ledBlinkStart;
	uint8_t blinkFreg;
	uint16_t blinkTimes;
	uint16_t blinkCount;
	uint16_t repatTimes;
	uint16_t nowRepat;
	SysTime_t ledBlinkTime;
	uint8_t lastLedStatus;
}Led_Blink_t;
static Led_Blink_t g_ledBlink[HAL_LED_COUNT] = {0};

static void HalLedBlinkHandle(void);


void HalLedInit(void)
{

    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;

    GPIO_InitStructure.GPIO_Pin = LED1_PIN;
    GPIO_Init(LED1_PORT, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = LED2_PIN;
	GPIO_Init(LED2_PORT, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = LED3_PIN;
	GPIO_Init(LED3_PORT, &GPIO_InitStructure);

	GPIO_SetBits(LED3_PORT, LED3_PIN);
	GPIO_SetBits(LED2_PORT, LED2_PIN);
	GPIO_SetBits(LED1_PORT, LED1_PIN);

}

void HalLedPoll(void)
{
	HalLedBlinkHandle();
}

void HalLedON(uint8_t ledNo)
{	
	switch(ledNo)
	{
		case HAL_LED_1:
			GPIO_ResetBits(LED1_PORT, LED1_PIN);
			break;
			
		case HAL_LED_2:
			GPIO_ResetBits(LED2_PORT, LED2_PIN);
			break;
			
		case HAL_LED_3:
			GPIO_ResetBits(LED3_PORT, LED3_PIN);
			break;
			
		default:
			return;
	}
}

void HalLedOFF(uint8_t ledNo)
{	
	switch(ledNo)
	{
		case HAL_LED_1:
			GPIO_SetBits(LED1_PORT, LED1_PIN);
			break;
			
		case HAL_LED_2:
			GPIO_SetBits(LED2_PORT, LED2_PIN);
			break;
			
		case HAL_LED_3:
			GPIO_SetBits(LED3_PORT, LED3_PIN);
			break;
			
		default:
			return;
	}
}

void HalLedToggle(uint8_t ledNo)
{
	switch(ledNo)
	{
		case HAL_LED_1:
			if(GPIO_ReadOutputDataBit(LED1_PORT, LED1_PIN))
			{
				GPIO_ResetBits(LED1_PORT, LED1_PIN);
			}
			else
			{
				GPIO_SetBits(LED1_PORT, LED1_PIN);
			}
			break;
			
		case HAL_LED_2:
			if(GPIO_ReadOutputDataBit(LED2_PORT, LED2_PIN))
			{
				GPIO_ResetBits(LED2_PORT, LED2_PIN);
			}
			else
			{
				GPIO_SetBits(LED2_PORT, LED2_PIN);
			}
			break;
			
		case HAL_LED_3:
			if(GPIO_ReadOutputDataBit(LED3_PORT, LED3_PIN))
			{
				GPIO_ResetBits(LED3_PORT, LED3_PIN);
			}
			else
			{
				GPIO_SetBits(LED3_PORT, LED3_PIN);
			}
			break;
			
		default:
			return;
	}
}

void HalLedSetStatus(uint8_t ledNo, uint8_t status)
{	
	HalLedStopBlink(ledNo);
	if(status)
	{
		HalLedON(ledNo);
	}
	else
	{
		HalLedOFF(ledNo);
	}
}

uint8_t HalLedGetStatus(uint8_t ledNo)
{
	switch(ledNo)
	{
		case HAL_LED_1:
			return !GPIO_ReadOutputDataBit(LED1_PORT, LED1_PIN);
			
		case HAL_LED_2:
			return !GPIO_ReadOutputDataBit(LED2_PORT, LED2_PIN);
			
		case HAL_LED_3:
			return !GPIO_ReadOutputDataBit(LED3_PORT, LED3_PIN);
			
		default:
			return 0;
	}
}

uint8_t HalLedIsBlank(uint8_t ledNo)
{
	return g_ledBlink[ledNo].ledBlinkStart;
}

void HalLedStartBlink(uint8_t ledNo, uint8_t freg, uint16_t repatTimes)
{
	if(ledNo < HAL_LED_COUNT)
	{	
		g_ledBlink[ledNo].ledBlinkStart = 1;
		g_ledBlink[ledNo].blinkFreg = freg;        //闪的次数
		g_ledBlink[ledNo].repatTimes = repatTimes; //重复次数，0时无限重复
		g_ledBlink[ledNo].blinkCount = 0;
		g_ledBlink[ledNo].ledBlinkTime = SysTime();
		g_ledBlink[ledNo].lastLedStatus = HalLedGetStatus(ledNo);
	}
	
}

void HalLedStopBlink(uint8_t ledNo)
{
	if(ledNo < HAL_LED_COUNT)
	{
		memset(&g_ledBlink[ledNo], 0, sizeof(Led_Blink_t));
	}
}

static void HalLedBlinkHandle(void)
{
	LOOP(x, HAL_LED_COUNT)
	{
		Led_Blink_t *led = &g_ledBlink[VAR(x)];		
		if(led->ledBlinkStart && led->blinkFreg > 0)
		{
			uint16_t blinkSpan = 500 / led->blinkFreg;
			if(led->blinkTimes < led->blinkFreg)
	        {
	            if(SysTimeHasPast(led->ledBlinkTime, blinkSpan))
	            {
	                led->ledBlinkTime = SysTime();
	                led->blinkTimes++;
					HalLedToggle(VAR(x));
	            }
	        }
	        else
	        {
	            if(SysTimeHasPast(led->ledBlinkTime, 1000))
	            {
	                led->ledBlinkTime = SysTime();
	                led->blinkTimes = 0;					
					led->nowRepat++;
					HalLedOFF(VAR(x));
	            }
	        }

			if(led->repatTimes != 0 && led->nowRepat >= led->repatTimes)
			{
				HalLedSetStatus(VAR(x), led->lastLedStatus);
			}
		}
	}
}


