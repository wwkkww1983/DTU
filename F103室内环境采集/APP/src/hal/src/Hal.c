#include "Hal.h"
#include "userSys.h"

#define HAL_CPU_HZ  (8000000UL)  //8MHZ

void HalInit(void)
{	
	uint8_t i_state;
	HAL_ENTER_CRITICAL(i_state);
	NVIC_SetVectorTable(NVIC_VectTab_FLASH, APP_START_ADDR);
	HAL_EXIT_CRITICAL(i_state);

	SysTick_Config(HAL_CPU_HZ / 1000);
	HalUartConfig(LOG_UART, 115200, MenuConfigRecv);
	hal_wdt_init();
	HalGPIOInitialize();
	RTCInit();
	HalTimerInit();
	HalSensorInit();
	printf("Hal Init Success...\n");
}

void HalPoll(void)
{
    hal_wdt_feed();
	HalGPIOPoll();
	HalSensorPoll();
}

void HalRestart(void)
{
	SysLog("HalRestart!!!!!!!!!!");
	hal_wait_ms(200);
    NVIC_SystemReset();
}


void HalWdtFeed(void)
{
	hal_wdt_feed();
}


