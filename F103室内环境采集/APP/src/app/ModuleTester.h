#ifndef MODULE_TESTER_H
#define MODULE_TESTER_H

#include "userSys.h"
#include "device.h"

	
#define ROM_DOWNLOAD_FLAG       0XAA    //固件下载标志值
#define RESET_TO_DEFAULTS_FLAG  0X5A    //恢复出厂设置标志

//flash中存储标志地址
#define FW_NEED_UPDATE_FLAG     0XB007B007  //固件需要升级
#define FW_UPDATE_DONE          0X13145213  //存在完整固件或者升级完成

#define FLASH_LOCK_TIMEOUT      2000    //2S
#define UPDATE_REQUEST_TIMEOUT  500     //500ms

//电相关信息
typedef struct ModuleInfo_st
{
	
    uint32_t CurTemperature;  //板载内部温度传感器
    uint32_t CurHumidity;     //采集到的当前湿度
    uint32_t PM25;            //PM2.5
    uint32_t CO2;             //CO2

    uint32_t WifiCfgTimeOut;  //wifi配置超时时间
    uint32_t ResetToDefaultsTimeOut;//恢复出厂设置超时
    uint32_t NormalTimeOut;

    uint8_t PreSmartFlag;   //预进入smart模式标志
    uint8_t SendReqFlag;    //发送配网指令标志

    uint8_t ResetToDefaultsFlag;  //恢复出厂设置标志
    uint8_t AppAdjCmd;      //校正Att7053命令值
    uint8_t AppOperate;     //收到操作指令
    uint8_t AppQry;         //APP查询状态
    uint8_t TestStatus;     //测试状态
    uint8_t NormalCount;    //计数

	WM_device_status_t *wm_online_status;
	SHT_device_status_t *sht_online_status;
	SK_device_status_t *sk_online_status;

	uint8_t sensorTHWork;
	uint8_t sensorPM25Work;
	uint8_t sensorCO2Work;

	
} ModuleInfo_t;


void ModuleTesterInit(void);
void ModuleTesterPoll(void);
ModuleInfo_t *GetModuleTesterInfo(void);
char *GetrModuleID(void);

#endif // MODULE_TESTER_H

