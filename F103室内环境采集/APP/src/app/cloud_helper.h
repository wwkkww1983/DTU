#ifndef CLOUD_HELPER_H
#define CLOUD_HELPER_H

/*******************************************************************************
 * Release Note:
 * VERSION:1.0.0.0：
 * 具体内容请看使用说明。
 ******************************************************************************/

/*******************************************************************************
 * 使用说明
 * 1. 根据应用场景设置配置选项：
 *  MCU位数CH_CFG_MCU_BIT
 *  是否使用文本属性 CH_CFG_SUPPORT_TEXT_PROPERTY
 * 
 *  。。。
 *
 * 2. 调必要的接口函数
 *   ch_init 初始化，应在初始化时首先调用，并根据配置结构体，选择工作模式、实现回调函数等等
 *   ch_handle 逻辑处理，应在循环中调用
 *   ch_timer_ms 毫秒时间信号，每毫秒调用一次，用于计时
 *   ch_sync_property 同步属性值
 *   ch_set_wifi_config_type 设置WiFi配置状态
 *  UART模式需要调用的接口
 *   ch_uart_data_input 串口接收数据输入
 *
 *  3. 响应事件回调
 *
 ******************************************************************************/

/*******************************************************************************
 * 配置选项
 ******************************************************************************/
#define CH_CFG_ON 1             //配置开启
#define CH_CFG_OFF 0            //配置关闭

#define CH_CFG_ENABLE_LOG CH_CFG_ON
   
/**
 * MCU位数
 **/
#define CH_CFG_MCU_BIT 32

/**
 * 是否启用文本属性
 * 不使用文本属性可以减少ram的使用
 **/
#define CH_CFG_SUPPORT_TEXT_PROPERTY CH_CFG_ON

/**
 * 需回复帧重发次数
 **/
#define CH_CFG_FRAME_SEND_RETRIES 3

/* 日志输出 */
#if CH_CFG_ENABLE_LOG
#define ch_printf(...) printf(__VA_ARGS__)
#endif
#ifndef ch_printf
#define ch_printf(...)
#else
#endif

/* 类型定义 */
typedef unsigned char chuint8_t;
typedef signed char chint8_t;
typedef unsigned short chuint16_t;
typedef signed short chint16_t;
#if CH_CFG_MCU_BIT == 32
typedef unsigned int chuint32_t;
typedef signed int chint32_t;
#else
typedef unsigned long chuint32_t;
typedef signed long chint32_t;
#endif
typedef chuint8_t ch_bool;
#undef uint8_t
#define uint8_t chuint8_t
#undef int8_t
#define int8_t chint8_t
#undef uint16_t
#define uint16_t chuint16_t
#undef int16_t
#define int16_t chint16_t
#undef uint32_t
#define uint32_t chuint32_t
#undef int32_t
#define int32_t chint32_t
#undef bool
#define bool ch_bool
#undef true
#define true 1
#undef false
#define false 0
#ifndef NULL
#define NULL ((void *)0)
#endif

#define ch_log(...) ch_printf("CH::%s[%d]:", __func__, __LINE__);ch_printf(__VA_ARGS__);ch_printf("\n");

/* 设备类型长度 */
#define CH_DEVICE_TYPE_LEN 12

/*设备pin码长度*/
#define CH_DEVICE_PIN_LEN  32

/* 版本号长度 */
#define CH_VERSION_LEN 4

/* MD5长度 */
#define CH_MD5_LEN 16

/* 固件分块大小 */
#define CH_ROM_PART_SIZE 128


/* 属性类型 */
typedef enum
{
    CH_PROPERTY_TYPE_NUM,       //数值属性
    CH_PROPERTY_TYPE_TEXT       //文本属性
}ch_property_type_t;

/* 属性配置 */
typedef struct
{
    ch_property_type_t type;
}ch_property_config_t;

/* 事件 */
typedef enum
{
    CH_EVENT_MODULE_STARTUP,            //模块启动或收到查询，需同步所有属性
    CH_EVENT_MODULE_DIE,                //模块不工作, 需reset模块
    CH_EVENT_OPERATION,                 //收到操作命令，param 对应 ch_event_param_operation_t。
    CH_EVENT_NET_TIME_RESPONSE,         //网络时间回复, param 对应 ch_event_param_nettime_t*
    
	CH_EVENT_FIND_SSID_RESULT,          //搜索ssid返回的结果
	CH_EVENT_DEV_SELF_TEST,             //收到设备自检命令
	CH_EVENT_MODULE_QUERY,
	CH_EVENT_HISTORY_DATA_QUERY,
	
    CH_EVENT_ROM_UPGRADE_NOTIFY,        //固件升级通知, param 对应 ch_event_param_rom_notify_t*
    CH_EVENT_ROM_UPGRADE_INFO,          //固件升级信息, param 对应 ch_event_param_rom_info_t*
    CH_EVENT_ROM_UPGRADE_DATA,          //固件数据, param 对应 ch_event_param_rom_data_t*
    CH_EVENT_REQUEST_INFO_RESPONSE,
}ch_event_t;

/******网络时钟参数********/
typedef struct
{
    uint8_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
}ch_nettime_t;

typedef struct
{
    int8_t timezone;        //时区
    uint32_t seconds;       //零时区 1970/01/01 00:00:00 至今的秒数
    ch_nettime_t nettime;   //当前时区的时间（时：分：秒）
}ch_event_param_nettime_t;

typedef struct
{
    uint32_t startTime;
    uint32_t endTime;
}ch_event_param_history_t;


/******操作参数********/
typedef struct ch_event_param_operation_st
{
    uint8_t propertyid[2];
    union
    {
        int32_t num;
        const char *text;
    }value;
}ch_event_param_operation_t;

/* Wi-Fi配置状态 */
typedef enum
{
    CH_WIFI_CONFIG_TYPE_EXIT = 0,       //不在配置状态
    CH_WIFI_CONFIG_TYPE_SMART = 1,      //Smart配置状态
}ch_wifi_config_type_t;

/* WiFi信号强度 */
typedef enum
{
    CH_SIGNAL_NONE = 0,     //无信号
    CH_SIGNAL_LOW = 1,      //信号弱
    CH_SIGNAL_MID = 2,      //信号中等
    CH_SIGNAL_HIGH = 3,     //信号强
}ch_signal_t;

/* 模块工作状态 */
typedef enum
{
    CH_WIFI_STATUS_CONNECTED = 0,
    CH_WIFI_STATUS_IDLE = 1,
    CH_WIFI_STATUS_CONNECTING = 2,
    CH_WIFI_STATUS_GETTING_IP = 3,
    CH_WIFI_STATUS_PWD_WRONG = 4,
    CH_WIFI_STATUS_NOT_FOUND = 5,
    CH_WIFI_STATUS_CONNECT_FAIL = 6,
}ch_wifi_status_t;

/* IO电平状态 */
typedef enum
{
    CH_IO_LEVEL_LOW = 0,    //低电平
    CH_IO_LEVEL_HIGH = 1,   //高电平
}ch_io_level_t;

/**
 * 事件处理回调函数
 * 实现该回调函数对事件进行处理
 * @param event 发生的时间
 * @param param 对应事件的参数
 */
typedef void (*ch_event_handler_func_t)(ch_event_t event, void *param);

/**
 * 串口输出回调，使用串口模式时使用，
 * @param data 要输出的数据
 * @param len 数据长度
 */
typedef void (*ch_uart_write_func_t)(const uint8_t *data, uint16_t len);

/* 初始化配置结构体，每一项都需要设置 */
typedef struct
{
    /* 设备类型 */
    const char *device_type;
    
    /*智城云设备pin 码*/
    const char *pinCode;

    /* 事件处理回调 */
    ch_event_handler_func_t event_handler;

    /* 串口输出回调 */
    ch_uart_write_func_t uart_write;

    /* 版本号 */
    uint8_t version[CH_VERSION_LEN];

    /* 京东产品UUID */
    uint8_t use_jd_product_uuid;
    const char *jd_product_uuid;

    /* 微信设备类型 */
    uint8_t use_wechat_devtype;
    const char *wechat_devtype;
	
	/* 国美产品类型 */
    uint8_t use_gm_product_type;
    const char *gm_model;
    const char *gm_pin;
    
}ch_config_t;


typedef struct
{
    uint8_t *version;
} ch_event_param_rom_notify_t;

typedef struct
{
    const uint8_t *version;
    const uint8_t *md5;
    uint32_t rom_size;
} ch_event_param_rom_info_t;

typedef struct
{
    uint16_t part_index;
    uint8_t *data;
} ch_event_param_rom_data_t;


/**
 * 初始化
 * @param cfg 配置
 */
void ch_init(const ch_config_t *cfg);

/**
 * 逻辑处理
 * 加在代码循环中
 */
void ch_handle( void );

/**
 * 毫秒时间信号，应每毫秒调用一次该函数
 */
void ch_timer_ms(void);

/**
 * 串口数据输入, 将串口接收到的数据输入
 */
void ch_uart_data_input(const uint8_t *data, uint8_t len);

/**
 * 同步属性值
 * @param propertyid 属性ID
 * @param num 数值属性值
 * @param text 文本属性值
 */
bool ch_sync_property(uint16_t propertyid, int32_t num, const char *text);

/**
 * 故障上报
 * @param fids 故障ID
 * @param count 故障数量
 */
bool ch_set_fault(const uint8_t *fids, uint8_t count);

/**
 * 搜索ssid
 */
void ch_find_ssid(char *ssid);

/**
 * 模块是否在线
 * @return 0, 离线
 *         1, 在线
 */
uint8_t ch_is_online(void);

/**
 * 模块WiFi信号强度
 * @return 信号强度值
 */
ch_signal_t ch_get_wifi_signal(void);

/* 模块是否处于配置状态 */
#define ch_wifi_in_config() (ch_get_wifi_config_type() != CH_WIFI_CONFIG_TYPE_EXIT)

/* 模块是否连接路由器 */
#define ch_wifi_is_connected() (ch_get_wifi_status() == CH_WIFI_STATUS_CONNECTED)

/* 设置模块配置状态 */
bool ch_set_wifi_config_type(ch_wifi_config_type_t type);

/* 获取模块配置状态 */
ch_wifi_config_type_t ch_get_wifi_config_type(void);

/* 获取模块工作状态 */
ch_wifi_status_t ch_get_wifi_status(void);

/* 请求网络时间 */
void ch_request_nettime(void);

/* 清除接收数据 */
void ch_clear_recv(void);

/*
* 清除云端数据
*/
bool ch_reset_cloud(void);

/*
* 设备自检结果
*/
bool ch_dev_self_test_ret(const uint8_t *fids, uint8_t count);

/*
* 协议测试
* 0xfb, 0x00, 0x00, 0x28, 0x84, 0xa7 协议中的属性查询帧
* 协议移植后，用户通过串口对MCU发送此数据，若此函数返回为真，则说明协议移植成功
*/
bool ch_protocol_test(void);

/*
* 发送操作结果
* @param success 操作是否成功
* @param propertyid 操作对应的属性ID
*/
void ch_send_operation_result(uint8_t success, uint16_t propertyid);

//tools
void frame_recv_byte(uint8_t byte);
void parseUTCTime(ch_nettime_t *nettime, uint32_t seconds);

//多属性统计
uint16_t ch_sync_multi_property(uint16_t propertyid, int32_t num, const char *text);
//串口发送多属性同步
void ch_send_multi_property(uint8_t type);
int8_t  ch_send_history_data(const char *record, uint8_t recordEnd);
void  ch_send_history_data_end(void);
void ch_request_rom_info(void);
void ch_request_rom_data(uint16_t part_index);
void ch_request_rom_end(const uint8_t *ver);
void num_to_data(uint8_t *data, uint32_t num);
void ch_request_module_info(void);


#endif // CLOUD_HELPER_H

