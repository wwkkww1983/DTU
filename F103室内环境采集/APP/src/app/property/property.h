#ifndef PROPERTY_H
#define PROPERTY_H

#include "userSys.h"

void PropertyInit(void);
void PropertyPoll(void);
void post_all_property(uint8_t isAll, uint8_t type,uint32_t ts);
void record_all_property(uint32_t ts);

#endif // PROPERTY_H
