#include "userSys.h"
#include "ModuleTester.h"
#include <string.h>
#include <stdlib.h>
#include "cloud_helper.h"
#include "cloud_protocol.h"
#include "device.h"
#include "SDCardRecord.h"
#include "IndoorPid.h"

 typedef enum
 {
	 GW_PM_2_5_ID = 1,		   //当前PM25
	 GW_CO2_ID, 			  //当前CO2    
	 GW_CURRENT_T_ID,		  //当前室温
	 GW_CURRENT_H_ID,
 
	 //计量插座
	 GW_SK_1_POWER,
	 GW_SK_1_T_ELEC,
	 GW_SK_1_ONLINE,
	 GW_SK_2_POWER,
	 GW_SK_2_T_ELEC,
	 GW_SK_2_ONLINE,
 
	 //无线温湿度
	 GW_SHT20_1_T,
	 GW_SHT20_1_H, 
	 GW_SHT20_1_ONLINE,    
	 GW_SHT20_1_VOL,
	 GW_SHT20_2_T,
	 GW_SHT20_2_H, 
	 GW_SHT20_2_ONLINE,    
	 GW_SHT20_2_VOL,
 
	 //门磁
	 GW_WM_1_VOL,
	 GW_WM_1_STATE,
	 GW_WM_1_ONLINE,
	 GW_WM_2_VOL,
	 GW_WM_2_STATE,
	 GW_WM_2_ONLINE,
 
	 GW_SK_3_POWER = 101,
	 GW_SK_3_T_ELEC,
	 GW_SK_3_ONLINE,
	 GW_SK_4_POWER,
	 GW_SK_4_T_ELEC,
	 GW_SK_4_ONLINE,
	 GW_SK_5_POWER,
	 GW_SK_5_T_ELEC,
	 GW_SK_5_ONLINE,
 
	 GW_SHT20_3_T,
	 GW_SHT20_3_H, 
	 GW_SHT20_3_ONLINE,    
	 GW_SHT20_3_VOL,
	 GW_SHT20_4_T,
	 GW_SHT20_4_H, 
	 GW_SHT20_4_ONLINE,    
	 GW_SHT20_4_VOL,
	 GW_SHT20_5_T,
	 GW_SHT20_5_H, 
	 GW_SHT20_5_ONLINE,    
	 GW_SHT20_5_VOL,
 
	 GW_WM_3_VOL,
	 GW_WM_3_STATE,
	 GW_WM_3_ONLINE,
	 GW_WM_4_VOL,
	 GW_WM_4_STATE,
	 GW_WM_4_ONLINE,
	 GW_WM_5_VOL,
	 GW_WM_5_STATE,
	 GW_WM_5_ONLINE,
 
	 GW_STATUS_TS = 255,
 } AC_ControlPropertyID;
 
#define sht20(i, type) GW_SHT20_##i##_##type
#define sk(i,type)     GW_SK_##i##_##type
#define wm(i,type)     GW_WM_##i##_##type

static MenuconfigInfo_t *g_config = NULL;
static ModuleInfo_t *g_moduleInfo = NULL;
#if WM_ENABLE
static WM_device_status_t *g_wm_device_status;
#endif
static SHT_device_status_t *g_sht_device_status;
static SK_device_status_t *g_sk_device_status;


void InDoorPidInit( void )
{	
	g_config = GetConfig();
	g_moduleInfo = GetModuleTesterInfo();
	g_wm_device_status = g_moduleInfo->wm_online_status;
	g_sht_device_status = g_moduleInfo->sht_online_status;
	g_sk_device_status = g_moduleInfo->sk_online_status;
}

 static uint16_t Pid(const char *device, uint8_t num, const char *type)
{
	uint16_t id = 0;
	if(device == "sht20")
	{
		switch (num)
		{
			case 1:
			if(type == "online")
			{
				id = sht20(1,ONLINE);
			}
			if(type == "vol")
			{
				id = sht20(1,VOL);
			}
			if(type == "h")
			{
				id = sht20(1,H);
			}
			if(type == "t")
			{
				id = sht20(1,T);
			}
			break;
			
			case 2:
			if(type == "online")
			{
				id = sht20(2,ONLINE);
			}
			if(type == "vol")
			{
				id = sht20(2,VOL);
			}
			if(type == "h")
			{
				id = sht20(2,H);
			}
			if(type == "t")
			{
				id = sht20(2,T);
			}
			break;
			
			case 3:
			if(type == "online")
			{
				id = sht20(3,ONLINE);
			}
			if(type == "vol")
			{
				id = sht20(3,VOL);
			}
			if(type == "h")
			{
				id = sht20(3,H);
			}
			if(type == "t")
			{
				id = sht20(3,T);
			}
			break;
			
			case 4:
			if(type == "online")
			{
				id = sht20(4,ONLINE);
			}
			if(type == "vol")
			{
				id = sht20(4,VOL);
			}
			if(type == "h")
			{
				id = sht20(4,H);
			}
			if(type == "t")
			{
				id = sht20(4,T);
			}
			break;
			
			case 5:
			if(type == "online")
			{
				id = sht20(5,ONLINE);
			}
			if(type == "vol")
			{
				id = sht20(5,VOL);
			}
			if(type == "h")
			{
				id = sht20(5,H);
			}
			if(type == "t")
			{
				id = sht20(5,T);
			}
			break;
		}
	}
	else if(device == "wm")
	{
		switch (num)
		{
			case 1:
			if(type == "online")
			{
				id = wm(1,ONLINE);
			}
			if(type == "state")
			{
				id = wm(1,STATE);
			}
			if(type == "vol")
			{
				id = wm(1,VOL);
			}
			break;
			
			case 2:
			if(type == "online")
			{
				id = wm(2,ONLINE);
			}
			if(type == "state")
			{
				id = wm(2,STATE);
			}
			if(type == "vol")
			{
				id = wm(2,VOL);
			}
			break;

			case 3:
			if(type == "online")
			{
				id = wm(3,ONLINE);
			}
			if(type == "state")
			{
				id = wm(3,STATE);
			}
			if(type == "vol")
			{
				id = wm(3,VOL);
			}
			break;

			
			case 4:
			if(type == "online")
			{
				id = wm(4,ONLINE);
			}
			if(type == "state")
			{
				id = wm(4,STATE);
			}
			if(type == "vol")
			{
				id = wm(4,VOL);
			}
			break;
			
			case 5:
			if(type == "online")
			{
				id = wm(5,ONLINE);
			}
			if(type == "state")
			{
				id = wm(5,STATE);
			}
			if(type == "vol")
			{
				id = wm(5,VOL);
			}
			break;

		}	
	}
	else if(device == "sk")
	{
		switch (num)
		{
			case 1:
			if(type == "online")
			{
				id = sk(1,ONLINE);
			}
			if(type == "power")
			{
				id = sk(1,POWER);
			}
			if(type == "t_elec")
			{
				id = sk(1,T_ELEC);
			}
			break;
			
			case 2:
			if(type == "online")
			{
				id = sk(2,ONLINE);
			}
			if(type == "power")
			{
				id = sk(2,POWER);
			}
			if(type == "t_elec")
			{
				id = sk(2,T_ELEC);
			}
			break;
			
			case 3:
			if(type == "online")
			{
				id = sk(3,ONLINE);
			}
			if(type == "power")
			{
				id = sk(3,POWER);
			}
			if(type == "t_elec")
			{
				id = sk(3,T_ELEC);
			}
			break;

			case 4:
			if(type == "online")
			{
				id = sk(4,ONLINE);
			}
			if(type == "power")
			{
				id = sk(4,POWER);
			}
			if(type == "t_elec")
			{
				id = sk(4,T_ELEC);
			}
			break;

			case 5:
			if(type == "online")
			{
				id = sk(5,ONLINE);
			}
			if(type == "power")
			{
				id = sk(5,POWER);
			}
			if(type == "t_elec")
			{
				id = sk(5,T_ELEC);
			}
			break;
		}	
	}

	if(id == 0)
	{
		SysLog("no such pid:%s-%d-%s",device,num,type);
	}
	//SysLog("%s-%d-%s : %d",device,num,type,id);
	return id;
}



static char* trans_property_by_text(uint8_t pid, void *num)
{
	static char text[30] = {0};
	uint32_t tempu32 = 0;
	uint16_t tempu16 = 0;
	int32_t  convert = 0;
	uint8_t type = 0;  //1:温度    2:功率 电量
	memset(text, 0 , 30);

	if(pid == GW_CURRENT_T_ID)
	{
		type = 1;
		tempu32 = *(uint32_t *)num;
		convert = (int32_t)tempu32;
	}

	if(pid == GW_SHT20_1_T || GW_SHT20_2_T || GW_SHT20_3_T 
		|| GW_SHT20_4_T || GW_SHT20_5_T)
	{
		type = 1;
		tempu16 = *(uint16_t*)num;
		convert = (int32_t)tempu16;
	}

	if(pid == GW_SK_1_POWER || pid == GW_SK_1_T_ELEC
		|| pid == GW_SK_2_POWER || pid == GW_SK_2_T_ELEC
		|| pid == GW_SK_3_POWER || pid == GW_SK_3_T_ELEC
		|| pid == GW_SK_4_POWER || pid == GW_SK_4_T_ELEC
		|| pid == GW_SK_5_POWER || pid == GW_SK_5_T_ELEC)
	{
		type = 2;
		convert = *(int32_t*)num;
	}

	if(type == 1)
	{
		//温度 超过范围显示0.0
		if(convert && convert < 50000)
		{
			if(abs((convert - 27315) % 100 ) < 10)
			{
				sprintf(text, "%d.0%01d", (convert - 27315) / 100, abs((convert - 27315) % 100));
			}
			else
			{
				sprintf(text, "%d.%01d", (convert - 27315) / 100, abs((convert - 27315) % 100));
			}

		}
		else
		{
			sprintf(text, "%d", 0);
		}

	}
	else if(type == 2)
	{
		if(convert && convert != -1)
		{
			if((convert % 100) < 10)
			{
				sprintf(text, "%d.0%01d", convert / 100, abs(convert % 100));
			}
			else
			{
				sprintf(text, "%d.%01d", convert / 100, abs(convert % 100));
			}
		}
		else if(convert == -1)
		{
			sprintf(text, "%s", "--");
			return "";
		}
		else
		{
			sprintf(text, "%d", 0);
		}
	}
	
	return text;
}
 
 void Indoor_post_all_property(uint8_t isAll, uint8_t type, uint32_t ts)
{
  uint8_t i = 0;
  ch_sync_multi_property(GW_STATUS_TS, ts, NULL);
  
  if(g_moduleInfo->sensorTHWork || isAll)
  {
  	  if(g_config->tm)
  	  {
	  	ch_sync_multi_property(GW_CURRENT_T_ID, 0, trans_property_by_text(GW_CURRENT_T_ID, &g_moduleInfo->CurTemperature));
  	  }
	  
	  if(g_config->hum)
	  {
	  	ch_sync_multi_property(GW_CURRENT_H_ID, g_moduleInfo->CurHumidity, NULL);
	  }
  }
  
  if(g_moduleInfo->sensorPM25Work || isAll)
  {
	if(g_config->pm)
	{
		ch_sync_multi_property(GW_PM_2_5_ID, g_moduleInfo->PM25, NULL);
	}
  }
  
  if(g_moduleInfo->sensorCO2Work || isAll)
  {
  	if(g_config->co2)
  	{
  		ch_sync_multi_property(GW_CO2_ID, g_moduleInfo->CO2, NULL);
  	}
  }

  for(i=0; i < g_config->shtNum; i++)
  {
	ch_sync_multi_property(Pid("sht20", i+1, "online"), g_sht_device_status[i].online, NULL);
	if(g_sht_device_status[i].online || isAll)
	{
		  ch_sync_multi_property(Pid("sht20", i+1, "vol"), g_sht_device_status[i].voltage, NULL);
		  ch_sync_multi_property(Pid("sht20", i+1, "t"), 0, trans_property_by_text(Pid("sht20", i+1, "t"), &g_sht_device_status[i].temperature));
		  ch_sync_multi_property(Pid("sht20", i+1, "h"), g_sht_device_status[i].humidity, NULL);
	}
  }

#if WM_ENABLE
  for(i=0; i < g_config->wmNum; i++)
  {
	  ch_sync_multi_property(Pid("wm",i+1, "online"), g_wm_device_status[i].online, NULL);
	  if(g_wm_device_status[i].online || isAll)
	  {
		  ch_sync_multi_property(Pid("wm",i+1, "vol"), g_wm_device_status[i].voltage, NULL);
		  ch_sync_multi_property(Pid("wm",i+1, "state"), g_wm_device_status[i].alarm_status, NULL);
	  }
  }
#endif

  for(i=0; i < g_config->skNum; i++)
  {
	  ch_sync_multi_property(Pid("sk",i+1, "online"), g_sk_device_status[i].online, NULL);
	  if(g_sk_device_status[i].online || isAll)
	  {
		ch_sync_multi_property(Pid("sk",i+1, "power"), 0, trans_property_by_text(GW_SK_1_POWER, &g_sk_device_status[i].power)); 
		ch_sync_multi_property(Pid("sk",i+1, "t_elec"), 0, trans_property_by_text(GW_SK_1_T_ELEC, &g_sk_device_status[i].total_elec)); 
	  }
  }
  
  ch_send_multi_property(type);
}

 void Indoor_record_all_property(uint32_t ts)
{
  uint8_t i = 0;
  uint32_t startTime = RecordBuildStart(ts);
  if(!startTime)
  {
  	return;
  }
  
  if(g_moduleInfo->sensorTHWork )
  {
  	  if(g_config->tm)
  	  {
	  	RecordBuild(GW_CURRENT_T_ID, 0, trans_property_by_text(GW_CURRENT_T_ID, &g_moduleInfo->CurTemperature));	  
  	  }

	  if(g_config->hum)
	  {
	  	RecordBuild(GW_CURRENT_H_ID, g_moduleInfo->CurHumidity, NULL);
	  }
  }
  
  if(g_moduleInfo->sensorPM25Work )
  {
  	if(g_config->pm)
  	{
  		RecordBuild(GW_PM_2_5_ID, g_moduleInfo->PM25, NULL);
  	}
  }
  
  if(g_moduleInfo->sensorCO2Work)
  {
  	if(g_config->co2)
  	{
  		RecordBuild(GW_CO2_ID, g_moduleInfo->CO2, NULL);
  	}
  }

  for(i=0; i < g_config->shtNum; i++)
  {
	RecordBuild(Pid("sht20", i+1, "online"), g_sht_device_status[i].online, NULL);
	if(g_sht_device_status[i].online)
	{
		  RecordBuild(Pid("sht20", i+1, "vol"), g_sht_device_status[i].voltage, NULL);
		  RecordBuild(Pid("sht20", i+1, "t"), 0, trans_property_by_text(Pid("sht20", i+1, "t"), &g_sht_device_status[i].temperature));
		  RecordBuild(Pid("sht20", i+1, "h"), g_sht_device_status[i].humidity, NULL);
	}
  }

#if WM_ENABLE
  for(i=0; i < g_config->wmNum; i++)
  {
	  RecordBuild(Pid("wm",i+1, "online"), g_wm_device_status[i].online, NULL);
	  if(g_wm_device_status[i].online)
	  {
		  RecordBuild(Pid("wm",i+1, "vol"), g_wm_device_status[i].voltage, NULL);
		  RecordBuild(Pid("wm",i+1, "state"), g_wm_device_status[i].alarm_status, NULL);
	  }
  }
#endif

  for(i=0; i < g_config->skNum; i++)
  {
	  RecordBuild(Pid("sk",i+1, "online"), g_sk_device_status[i].online, NULL);
	  if(g_sk_device_status[i].online )
	  {
		RecordBuild(Pid("sk",i+1, "power"), 0, trans_property_by_text(GW_SK_1_POWER, &g_sk_device_status[i].power)); 
		RecordBuild(Pid("sk",i+1, "t_elec"), 0, trans_property_by_text(GW_SK_1_T_ELEC, &g_sk_device_status[i].total_elec)); 
	  }
  }
 
  RecordBuildEnd(startTime);
}

