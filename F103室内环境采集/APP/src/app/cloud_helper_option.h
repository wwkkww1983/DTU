#ifndef CLOUD_HELPER_OPTION_H
#define CLOUD_HELPER_OPTION_H

/*******************************************************************************
 * 配置选项
 ******************************************************************************/
#define CH_CFG_ON 1             //配置开启
#define CH_CFG_OFF 0            //配置关闭

/**
 * 日志开关
 **/
#define CH_CFG_ENABLE_LOG CH_CFG_ON

/**
 * MCU位数
 **/
#define CH_CFG_MCU_BIT 32

/**
 * 支持的属性数量，应大于0
 **/
#define CH_CFG_SUPPORT_PROPERTY_COUNT   61
#if CH_CFG_SUPPORT_PROPERTY_COUNT <= 0
    #error CH_CFG_SUPPORT_PROPERTY_COUNT 应大于0
#endif

/**
 * 是否启用文本属性
 * 不使用文本属性可以减少ram的使用
 **/
#define CH_CFG_SUPPORT_TEXT_PROPERTY CH_CFG_ON

/**
 * 是否启用IO模式
 * 关闭IO模式可以减少ram使用
 **/
#define CH_CFG_SUPPORT_IO_MODE CH_CFG_OFF

/**
 * 是否启用固件升级
 * 不启用固件升级可以减少ram使用
 **/
#define CH_CFG_SUPPORT_ROM_UPGRADE CH_CFG_OFF

/**
 * 需回复帧重发次数
 **/
//#define CH_CFG_FRAME_SEND_RETRIES   3

/**
 * 需回复帧发送队列大小(文本属性个数)
 **/
#define CH_CFG_TEXT_FRAME_SEND_QUEUE_SIZE   30

/**
 * 需回复帧发送队列大小(数值属性个数)
 **/
#define CH_CFG_NUM_FRAME_SEND_QUEUE_SIZE    CH_CFG_SUPPORT_PROPERTY_COUNT

/* 日志输出 */
#if CH_CFG_ENABLE_LOG
    #define ch_printf(...) printf(__VA_ARGS__)
#endif
#ifndef ch_printf
    #define ch_printf(...)
#else
#endif

#endif // CLOUD_HELPER_OPTION_H

