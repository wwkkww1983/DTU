#include "SysButton.h"

#define SYS_BUTTON_LIMIT 0x01

static void handleButton(SysButton_t *button);

static SysButton_t *g_buttons[SYS_BUTTON_LIMIT] = {NULL};

void SysButtonInit()
{
    memset((void *)g_buttons, 0, sizeof(g_buttons));
}

void SysButtonPoll()
{
    int i;
    for(i = 0; i < SYS_BUTTON_LIMIT; i++)
    {
        if(g_buttons[i])
        {
            handleButton(g_buttons[i]);
        }
    }
}

uint8_t SysButtonRegister(SysButton_t *button, SysButtonHandler_t handler, SysButtonGetState_t getState)
{
    int i;
    for(i = 0; i < SYS_BUTTON_LIMIT; i++)
    {
        if(g_buttons[i] == NULL)
        {
            g_buttons[i] = button;
            button->handled = 0;
            button->lastState = SYS_BUTTON_STATE_RELEASED;
            button->pressedTime = 0;
            button->getState = getState;
            button->handler = handler;
            return 1;
        }
    }
    return 0;
}

void SysButtonUnregister(SysButton_t *button)
{
    int i;
    for(i = 0; i < SYS_BUTTON_LIMIT; i++)
    {
        if(g_buttons[i] == button)
        {
            g_buttons[i] = NULL;
        }
    }
}


static void handleButton(SysButton_t *button)
{
    uint32_t curTime = SysTime();   //获取当前时间

    if(button->lastState == SYS_BUTTON_STATE_PRESSED)   //若上次扫描时按键为按下状态
    {
        button->lastState = button->getState(button);   //获取当前按键状态
        if(button->handler(button, curTime - button->pressedTime, button->lastState))
        {
            button->pressedTime = curTime;
            button->handled = 0;
        }
    }
    else    //上次扫描时,按键为release状态
    {
        button->lastState = button->getState(button);   //获取当前按键状态
        if(button->lastState == SYS_BUTTON_STATE_PRESSED)   //若当前为按下状态，则记录当前按下时间
        {
            button->handled = 0;
            button->pressedTime = curTime;
        }
    }
}
