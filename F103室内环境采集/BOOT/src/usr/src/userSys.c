#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "userSys.h"
#include "os_timer.h"
#include "net.h"
#include "SysTimer.h"
#include "SysPoll.h"
#include "Device.h"


//#define SYSTEMTICK_PERIOD_MS  10

/* this variable is used to create a time reference incremented by 10ms */
volatile uint32_t g_LwIPLocalTime = 0;
uint32_t timingdelay;
unsigned char devMAC[6];

uint8_t SysIsWdtResetStartup = 0;
//static uint32_t g_systemTime;
static bool g_sysInitComplete = false;


void SysOSInit(void)
{
    os_timer_init();
}
/*
static void SysUartCheckHandle(char *text)
{
    char *p1, *p2;
    uint8_t version=0;
    if((p1 = strstr(text, "REQUEST")) != 0)
    {
        p2 = strchr(p1, '=');
        if(p2)
        {
            if(strstr(p2+1, "READY?"))
            {
                printf("ACK=OK\n");
            }else if(strstr(p2+1, "MACCHIP?")){
                version = enc28j60getrev();

                if((version == 0x06) && LwIP_Is_Linkup())
                {
                    printf("ACK=OK\n");
                }else{
                    printf("ACK=NG\n");
                }
            }else if(strstr(p2+1, "RFCHIP?")){
                version = CC1101_GET_ID();
                //if((version!=0)&&(version!=0xff))
                if(version == 0x40)
                {
                    printf("ACK=OK\n");
                }else{
                    printf("ACK=NG\n");
                }
            }
        }
    }
}

static void SysUartCheckDevice(HalUart_t no, const uint8_t *data, uint16_t len)
{
    static char recvData[32]={0};
    uint16_t i;
    static uint8_t count=0;

    if(no == HAL_UART_0 && g_sysInitComplete)
    {
        for(i=0; i<len; i++)
        {
            if(data[i] == '\n')
            {
                count = 0;
                SysUartCheckHandle(recvData);
                memset(recvData, 0, 64);
            }else{
                recvData[count++] = data[i];
            }
        }
    }
}
*/
bool SysCheckMacChipVersion(void)
{
    //return (enc28j60getrev() == MAC_CHIP_VERSION);
    return 0;
}

bool SysInitCompleted(void)
{
    return g_sysInitComplete;
}

void SysMacChipReset(void)
{
#if 0
    SysLog("");
    if(enc28j60Init(devMAC))
    {
        SysRestart(SYS_RESTART_HW_ERR);
    }
#endif

}

/*外部设备初始化函数*/
void SysDevicesInit(void)
{
    sysPeriodInit();
    g_sysInitComplete = true;
}

/*
*系统延时函数
*@nCount: 单位10ms
*/
void SysDelay(uint32_t nCount)
{
    /* Capture the current local time */
    timingdelay = g_LwIPLocalTime + nCount;

    /* wait until the desired delay finish */
    while(timingdelay > g_LwIPLocalTime)
    {
    }
}


void SysLwIPPeriodicHandle(void)
{
    //Display_Periodic_Handle(g_LwIPLocalTime);
    /* LwIP periodic services are done here */
    //LwIP_Periodic_Handle(g_LwIPLocalTime);
}

/*系统周期任务处理函数*/
void SysPoll(void)
{

    //tbd wyx
    //PlatPoll();
    sysPeriodPoll();

    SysTimerPoll();

    SysButtonPoll();

    HalPoll();
//  SysTimerPoll();

}
//@retval : Xus
static uint32_t system_get_time()
{
    return os_timer_get_systime();  // * 1000;
}

static uint32_t fetchTime()
{
    return system_get_time();
}

#if 0
uint32_t SysTime(void)
{
    return os_timer_get_systime();
}
#endif

void SysFlashWrite(uint32_t addr, const void *data, uint16_t len)
{
    static unsigned char secBuf[SYS_FLASH_SECTOR_SIZE];
    uint32_t spiAddr = SPI_ADDR(addr);
    uint32_t sectorStart = SECTOR_START_ADDR(spiAddr);
    hal_flash_read(sectorStart, secBuf, 2048);
    memcpy(secBuf + ADDR_TO_SECTOR_OFFSET(spiAddr), data, len);
    //spi_flash_erase_sector(SPI_ADDR_TO_SECTOR(spiAddr));
    hal_flash_write(sectorStart, secBuf, SYS_FLASH_SECTOR_SIZE);
}

void SysFlashRead(uint32_t addr, void *data, uint16_t len)
{
    uint32_t spiAddr = SPI_ADDR(addr);
    hal_flash_read(spiAddr, (uint8_t *)data, len);
}

void SysUserDataWriteByte(uint32_t addr, uint8_t byte)
{
    if(VALID_USER_ADDR(addr))
    {
        SysFlashWrite(addr, &byte, 1);
    }
}

uint8_t SysUserDataReadByte(uint32_t addr)
{
    uint8_t byte;
    SysFlashRead(addr, &byte, 1);
    return byte;
}

void SysUserDataRead(uint32_t addr, uint8_t *data, uint16_t len)
{
    SysFlashRead(addr, data, len);
}

void SysUserDataWrite(uint32_t addr, const uint8_t *data, uint16_t len)
{
    if(VALID_USER_ADDR(addr))
    {
        //if((addr>=DEVICE_DID_DATA_ADDR&&addr<DEVICE_UPDATA_INFO_ADDR)||\
        //  ((addr+len)>=DEVICE_DID_DATA_ADDR&&(addr+len)<DEVICE_UPDATA_INFO_ADDR))
        //{
        //  printf("@@@@@@@@@@@@ ERROR: flash addr %x+%d is read only !!!!!!!\n", addr, len);
        //  return;
        //}
        SysFlashWrite(addr, data, len);
    }
}

void SysUserDataEraseAll(void)
{
    SysFlashWrite(USER_ADDR, NULL, 0);
}

void SysRestart(SYS_RESTART_INDEX index)
{
    //#ifndef COMPILE_WITH_LOG
    printf("\n@@@@ system restart [%d]!!!\n\n", index);
    //#endif

    HalRestart();
}

static SysTime_t g_utcTime;
SysTime_t SysGetUTCTimeSeconds()
{
    return g_utcTime;
}

void SysSetUTCTimeSeconds(SysTime_t sec)
{
    SysUTCTime_t utc;
    g_utcTime = sec;

    SysGetUTCTime(&utc);
    SysLog("hours %d min:%d sec:%d", utc.hour, utc.min, utc.sec);
}

void SysGetUTCTime(SysUTCTime_t *utcTime)
{
    SysTime_t tmp = g_utcTime % (24 * 60 * 60);
    utcTime->sec = (tmp % 60);
    tmp /= 60;
    utcTime->min = tmp % 60;
    utcTime->hour = tmp / 60;
}

void SysDataPrint(uint8_t *data, uint8_t len)
{
    int i;
    for(i = 0; i < len; i++)
    {
        SysPrintf("0x%02x ", data[i]);
    }
    SysPrintf("\n");
}

#if 0
void SysSetLedState(uint8_t mode, uint8_t state)
{
    static uint8_t ledMode = SYS_LED_MODE_NORMAL, ledLastState;
    if(ledMode == SYS_LED_MODE_BLINK)
    {
        if(mode == SYS_LED_MODE_BLINK)
        {

            if(state == SYS_LED_BLINK_OFF)
            {
                SysLEDCtrl(ledLastState);
                ledMode = SYS_LED_MODE_NORMAL;
            }
            else
            {
                SysLEDCtrl(state);
                ledMode = SYS_LED_MODE_BLINK;
            }
        }
        else
        {
            ledLastState = state;
        }
    }
    else
    {
        SysLEDCtrl(state);
        ledMode = mode;
        if(mode != SYS_LED_MODE_BLINK)
        {
            ledLastState = state;
        }
    }
}
#endif

bool SysIsLittleEndian(void)
{
    int i = 1;
    if(*(char *)&i)
    {
        return true;
    }
    else
    {
        return false;
    }
}

static bool g_tcpSendLock = false;

void SysTcpSendMutexLock(bool lock)
{
    g_tcpSendLock = lock;
}

bool SysTcpSendMutexState(void)
{
    return g_tcpSendLock;
}

int SysAtoi(const char* str)
{
    if (str == NULL)
        return 0;
 
    int slen = strlen(str);
    if(slen < 0)
    {
        return 0;
    }
    const char* c = str;
 
    int ret = 0;
    int sign=1;
    while(*c == ' ')
    {
        c++;
    }
    if(*c == '+')
    {
        c++;
    }
    else if(*c == '-')
    {
        sign = -1;
        c++;
    }
 
    int t = 0;
    while((*c >='0')&&(*c <= '9'))
    {
        t = *c-'0';
        ret = ret *10+t;
        c++;
    }
    return ret * sign;
}

