#include "Hal.h"
#include "userSys.h"
#define CMD_NUM 16
#define CMD_MAX_LEN 128
static MenuconfigInfo_t g_configInfo = {0};

void MenuConfigInit(void)
{
	SysUserDataRead(CONFIG_SAVED_DATA_ADDR, (uint8_t *)&g_configInfo, sizeof(MenuconfigInfo_t));
}

void MenuConfigPoll(void)
{

}

MenuconfigInfo_t *GetConfig(void)
{
	return &g_configInfo;
}

