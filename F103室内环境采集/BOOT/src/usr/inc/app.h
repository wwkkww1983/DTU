#ifndef APP_H
#define APP_H
#include "os.h"


#define VERSION_LEN               4

#define FACINFO_LEN               16
#define DEVUID_LEN                7
#define MAX_DEVICE_COUNT          OS_MAX_DEVICE_COUNT
#define NETKEY_LEN                3

#define APP_PAYLOAD_MAXLEN        (PHY_MAX_LEN - sizeof(nwk_header_t) - sizeof(app_header_t) - sizeof(phy_header_t))

typedef struct app_header
{
    uint8_t type;
    uint8_t payload[];
} app_header_t;

enum app_frame_t
{
    APP_FRAME_SEARCHDEVICE,
    APP_FRAME_ADDDEVICE,
    APP_FRAME_DELDEVICE,
    APP_FRAME_CONNECT,
    APP_FRAME_DATA,
};

void app_init(void);
void app_poll(void);
uint8_t *app_get_key(uint8_t addr);
void app_trans_result(uint8_t trans_id, uint8_t result);
void app_encrypt(uint8_t addr, void *data, uint8_t len);
void app_decrypt(uint8_t addr, void *data, uint8_t len);
void *app_get_trans_payload(void);
uint8_t app_transmit(uint8_t type, uint8_t addr, uint8_t len);
//uint8_t app_transmit_directly(uint8_t type, uint8_t addr, uint8_t len);
void app_rf_recv_deal(uint8_t *data, uint8_t len, uint8_t *segaddr, uint8_t srcaddr, \
                      uint8_t dstaddr, uint8_t rssi);
void app_ack_sync_content_cb(uint8_t *data, uint8_t len);
uint8_t app_ack_sync_content_set(uint8_t *data);

#endif // APP_MSG_H
