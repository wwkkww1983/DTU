#ifndef SYSTIMER_H
#define SYSTIMER_H
#include "userSys.h"

#define SYS_TIMER_REPEAT        1
#define SYS_TIMER_IMME          2
#define SYS_TIMER_NEED_UNSET    4 //need call unset to free the timer
#define SYS_TIMER_LIMIT  20


typedef void (*SysTimerRun_t)(void *arg);

typedef struct
{
    uint8_t alloced;
    uint8_t type;
    uint32_t span;
    uint32_t startTime;
    SysTimerRun_t run;
    void *arg;
} SysTimer_t;

SysTimer_t *SysTimerSet(SysTimerRun_t run, uint32_t span, uint8_t type, void *arg);
void SysTimerUnSet(SysTimer_t *timer);
void SysTimerInit(void);
void SysTimerPoll(void);
void SysTimerUpdate(SysTimer_t *timer);


#endif // SYSTIMER_H
