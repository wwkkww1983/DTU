#ifndef DEVICE_H
#define DEVICE_H
#include "userSys.h"

#define DEVICE_VERSION_LEN  4
#define DEVICE_KEY_LEN      3
#define DEVICE_TYPE_LEN     3

#define DEVICE_MODEL_SK  "500SCT007P02"  //开关
#define DEVICE_MODEL_BS  "500SCT011B01"  //红外
#define DEVICE_MODEL_WM  "500SCT012W01"  //门磁
#define DEVICE_MODEL_CL  "500SCT017C01"  //彩灯
#define DEVICE_MODEL_SS  "500SCT019Y01"  //烟雾传感器
#define DEVICE_MODEL_CS  "500SCT020R01"  //可燃气体传感器
#define DEVICE_MODEL_LC  "500SCT010D01"  //灯光控制器
#define DEVICE_MODEL_CC  "500SCT023C01"  //窗帘控制器
#define DEVICE_MODEL_WS  "500SCT021Q01"  //智能墙体插座
#define DEVICE_MODEL_TS  "500SCT022C01"  //墙体触摸开关
#define DEVICE_MODEL_TH  "500SCT022H01"  //无线温湿度传感器

#define DEVICE_ENABLE_DVID       1
#define DEVICE_SIGNAL_DVID       2
#define DEVICE_VOLTAGE_DVID      3
#define DEVICE_STATUS_DVID1      4
#define DEVICE_STATUS_DVID2      5
#define DEVICE_STATUS_DVID3      6
#define DEVICE_STATUS_DVID4      7

#define DEVICE_FREE_DVID         8


#define CHAR_DIGIT(ch) ((ch) - '0' < 10 ? (ch) - '0' : (ch) - 'a' + 10)

#define ALARM_LEVEL_3 3 //不缓存离线消息的通知
#define ALARM_LEVEL_4 4 //不缓存离线消息的告警
#define ALARM_LEVEL_5 5 //缓存离线消息的通知
#define ALARM_LEVEL_6 6 //缓存离线消息的告警

typedef struct
{
    uint8_t alarmEnableLevel;
    uint8_t alarmDisableLevel;
} DeviceAlarmLevel_t;

typedef struct
{
    bool     isAlarmDev;    /* 是否是报警设备 */
    DeviceAlarmLevel_t *level;  /* 报警设备等级 */
    char     *model;    /* 产品型号 */
    uint8_t  statusNum; /* 状态数量 */
} DeviceMiscData_t;

typedef struct Property_st
{
    uint8_t propertyid;
    uint8_t alarmLevel;
    uint8_t changed;
    uint32_t value;
    struct Property_st *next;
} Property_t;

#pragma pack(1)
struct DeviceId_st
{
    char type[2];
    char subType;
    uint32_t no;
};
typedef struct DeviceId_st DeviceId_t;
#pragma pack()

typedef struct
{
    uint8_t flag;   /* 是否是被启用标志 */
    uint8_t enabled : 1;    /* 该设备报警功能被使能标志 */
    uint8_t sleep : 1;  /* 是否是休眠设备 */
    uint8_t key[DEVICE_KEY_LEN];    /* 通信加密KEY */
    uint8_t version[DEVICE_VERSION_LEN];    /* 设备版本 */
    DeviceId_t id;  /* 设备ID */
} DeviceSavedData_t;

typedef struct
{
    uint8_t rfAddr;     /* 分配的RF网络地址 */
    uint8_t voltage;    /* 设备供电电压 */
    uint8_t online : 1; /* 是否在线 */
    uint8_t alloced : 1;/* 是否被配置 */
    uint8_t enabled : 1;/* 报警是否被使能 */
    uint8_t inUpgrading : 1;
    uint8_t statusChanged : 1; /* 状态改变 */
    uint8_t newAdded : 1;      /* 新添加设备 */
    uint8_t optResponsed : 1;  /*  */
    uint8_t signal : 2;        /* 信号强度 */
#if defined(TEST_DISPLAY_SIGNAL_VALUE)
    int16_t sigValue;
#endif
    uint8_t statusDataLen;  /* 状态数据长度 */
    uint8_t *statusData;    /* 状态数据 */
    DeviceSavedData_t *sd;  /* -cbl 0126 保存数据 指向对应的flash区域 */
    SysTime_t lastHbTime;   /* 记录上次心跳 */
    DeviceMiscData_t *miscData; /* 记录设备的功能 */
    uint8_t lastOptPropertyid;  /* 上一次操作属性ID */
    uint32_t lastOptMsgid;      /* 上一次操作消息ID */
    Property_t *firstProperty;  /* 第一个属性ID */
} Device_t;

#if WM_ENABLE
typedef struct
{
    uint8_t alloced;/* 已经被分配 */
    uint8_t online; /* 是否在线 */
    uint8_t voltage;/* 电量 */
    uint8_t alarm_status ;  /* 报警状态 */
} WM_device_status_t;
#endif

typedef struct
{
    uint8_t alloced;/* 已经被分配 */
    uint8_t online; /* 是否在线 */
    uint8_t voltage;/* 电量 */
    uint8_t humidity;   /* 湿度 */
    uint16_t temperature;   /* 温度 */
} SHT_device_status_t;


typedef struct
{
	uint8_t alloced;/* 已经被分配 */
    uint8_t online; /* 是否在线 */
	uint8_t last_online;
    uint8_t voltage;/* 电量 */
	int32_t power;  //功率
	int32_t total_elec;
}SK_device_status_t;
/**
 *  设备初始化
 */
void DeviceInit(Device_t *device, uint8_t rfAddr);

/**
 *  获取设备ID
 */
char *DeviceGetId(Device_t *device);

/**
 *  删除设备
 */
void DeviceDel(Device_t *device);

/**
 *  添加设备
 */
void DeviceAdd(Device_t *device, DeviceId_t *id, uint8_t *factoryinfo);
//void DeviceAdd(Device_t *device, uint8_t *id, uint8_t *factoryinfo);

/**
 *  设备接收rf数据
 */
void DeviceRFRecvData(Device_t *device, uint8_t *data, uint16_t len, int8_t rssi);

/**
 *  设备处理操作
 */
void DeviceHandleOperation(Device_t *device, uint8_t propertyid, uint32_t val);

/**
 *  设备设置使能
 */
void DeviceSetEnabled(Device_t *device, uint8_t enabled);

/**
 *  设备是否是休眠设备
 */
#define DeviceIsSleepDevice(device) ((device)->sd->sleep)

void DeviceGetMiscData(Device_t *device);

bool DeviceIsOldDevice(Device_t *device);

void GatewayRFFoundDevice(DeviceId_t *id, uint8_t *facInfo);

void GatewayInit(void);
void GatewayRFVersionSet(uint8_t addr, uint8_t *data, uint8_t len);
void GatewayRFRecvData(uint8_t addr, uint8_t *data, uint8_t len, int8_t rssi);

void startAddDevice(void);
#if WM_ENABLE
WM_device_status_t *gataway_get_wm_info(uint8_t num);
#endif
SHT_device_status_t *gataway_get_sht_info(uint8_t num);
SK_device_status_t *gataway_get_sk_info(uint8_t num);


#endif // DEVICE_H
