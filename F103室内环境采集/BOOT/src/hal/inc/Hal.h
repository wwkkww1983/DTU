#ifndef HAL_H
#define HAL_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include <stdio.h>
#include <string.h>
#include "HalGPIO.h"
#include "HalUart.h"
#include "HalSpi.h"
#include "halSensor.h"
#include "hal_timer.h"
#include "hal_flash.h"
#include "hal_wdt.h"
#include "hal_wait.h"
#include "hal_led.h"
#include "hal_aes.h"
#include "hal_rnd.h"
#include "hal_conf.h"
#include "hal_rtc.h"




/**********User Config******************/
//UART
#define WIFI_UART   HAL_UART_1
#define LOG_UART    HAL_UART_2
#define CO2_UART    HAL_UART_3
#define BLE_UART    HAL_UART_4
#define PM25_UART   HAL_UART_5
//

//LED
#define LED1_PORT  GPIOB
#define LED1_PIN   GPIO_Pin_3

#define LED2_PORT  GPIOB
#define LED2_PIN   GPIO_Pin_4

#define LED3_PORT  GPIOB
#define LED3_PIN   GPIO_Pin_5

//TODO moduleLed����
#define LED_PORT LED1_PORT
#define LED_PIN  LED1_PIN


//KEY
#define KEY_PORT   GPIOC
#define KEY_PIN    GPIO_Pin_5

/**********User Config End******************/

#define HAL_ENTER_CRITICAL(i_state) i_state=1;__disable_irq();
#define HAL_EXIT_CRITICAL(i_state) if(i_state)__enable_irq();
#define HAL_ALL_INT_OFF() ;
#define HAL_ALL_INT_ON() ;
#ifdef OS_WDT
    #define HAL_REBOOT()  HAL_ALL_INT_OFF();while(1)
#else
    #define HAL_REBOOT()  ;//asm("LJMP 0")
#endif


void HalInit(void);
void HalPoll(void);
void HalWdtInit(void);
void HalWdtFeed(void);
void HalRestart(void);
uint8_t HalInterruptsGetEnable(void);
void HalInterruptsSetEnable(uint8_t );
void HalJumpAppAddr(uint32_t addr);


#endif
