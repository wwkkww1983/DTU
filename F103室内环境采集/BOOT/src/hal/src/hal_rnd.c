#include "hal_rnd.h"

static uint16_t g_seed = 0;

void hal_rnd_init()
{
    //ENABLE_RANDOM_GENERATOR();
}
uint8_t hal_rnd_get()
{
    uint8_t res = 0;
    /*RNDL = g_seed & 0xff;
    RNDL = (g_seed >> 8) & 0xff;
    GET_RANDOM_BYTE(res);
    if(res == 0)
    {
      res = 1;
    }
    g_seed += res;*/
    return res;
}

void hal_rnd_seed()
{
    g_seed++;
}

uint8_t hal_rnd_range(uint8_t from, uint8_t to)
{
    uint8_t n = hal_rnd_get();
    n = (n % (to - from + 1)) + from;
    return n;
}


void hal_rnd_feed_seed(uint8_t seed)
{
    g_seed += seed;
}
