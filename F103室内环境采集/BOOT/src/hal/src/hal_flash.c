#include "Hal.h"

static uint32_t HalBufferToData(uint8_t *buf)
{
    return buf[3] << 24 | buf[2] << 16 | buf[1] << 8 | buf[0];
}

static uint32_t HalFlashErasePage(uint32_t Page_Address)
{
    if(FLASH_ErasePage((uint32_t)Page_Address) != FLASH_COMPLETE)
    {
        return 0xff;
    }

    return 0;
}

void hal_flash_write(uint32_t Address, const void *data, uint16_t len)
{

    uint32_t ucWordNum, ucSingleByte, i;
    uint8_t *str = (uint8_t *)data;
    uint32_t uiTempData;

    ucWordNum = len >> 2; //word编程数
    ucSingleByte = len & 0x03;  //不够word编程的字节数

	FLASH_Unlock();

	if(HalFlashErasePage(Address))
	{
		while(1);
	}

    if(0x00 != ucWordNum)
    {
        for(i = 0; i < ucWordNum; i++)
        {
            uiTempData = HalBufferToData(str);
            if (FLASH_ProgramWord(Address, uiTempData) == FLASH_COMPLETE)
            {
                Address += 4;
                str += 4;
            }
            else
            {
                while(1);
            }
        }
    }

    if(0x00 != ucSingleByte)
    {
        uiTempData = 0x00;

        for(i = 0; i < 4; i++)
        {
            if(i < ucSingleByte)
            {
                uiTempData |= str[i] << i * 8;
            }
            else
            {
                uiTempData |= 0xff << i * 8;
            }
        }

        if (FLASH_ProgramWord(Address, uiTempData) == FLASH_COMPLETE)
        {
            Address += 4;
            str += 4;
        }
        else
        {
            while(1);
        }
    }

    FLASH_Lock();
}

int hal_flash_read(uint32_t flash_addr, uint8_t *buf, uint16_t len)
{
    int i = 0;
    memcpy(buf, (const void *)flash_addr, len);
    return i;

}


