#include "cloud_helper.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "cloud_protocol.h"

#define PRECODE 0xfc
#define GET_SEND_FRAME_DATA() ((void *)(g_send_frame + 1))

#if CH_CFG_SUPPORT_TEXT_PROPERTY
#define NEED_RESP_FRAME_DATA_BUF (1024 + sizeof(operation_result_t))
#define RECV_BUF_SIZE (sizeof(frame_t) + NEED_RESP_FRAME_DATA_BUF)
#else
#define NEED_RESP_FRAME_DATA_BUF (((sizeof(property_t) + 4 + 1) * 4))
#if CH_CFG_SUPPORT_ROM_UPGRADE
#define RECV_BUF_SIZE (sizeof(frame_t) + sizeof(rom_t) + sizeof(rom_data_t) + CH_ROM_PART_SIZE)
#else
#define RECV_BUF_SIZE (sizeof(frame_t) + (sizeof(property_t) + 4) * 3 + 1)
#endif
#endif

#ifndef SEND_BUF_SIZE
#define SEND_BUF_SIZE (NEED_RESP_FRAME_DATA_BUF + sizeof(frame_t))
#endif

//心跳超时
#define HB_TIMEOUT 31000

#define ChTimeBeyond(cur, last, past) ((cur) - (last) >= (past))
#define IS_BIGEND 1
#define CHAR_MIN 0
#define CHAR_MAX 255
#define SHRT_MIN 256
#define SHRT_MAX 65535

/*******************************************************************************
* func declaration
******************************************************************************/
static void send_frame(frame_type_t type, uint16_t len, uint8_t ack, uint8_t retry);
static void num_to_data(uint8_t *data, uint32_t num);
static uint16_t build_property(uint8_t * buf, uint16_t propertyid, int32_t num, const char * text);
static void handle_recv(void);
static void handle_rom_upgrade(frame_t *frame);

/*******************************************************************************
* global variable
******************************************************************************/
static uint8_t g_justResponseConfig;
//在线
static uint8_t g_online;
//WiFi状态
static ch_wifi_status_t g_wifi_status = CH_WIFI_STATUS_IDLE;
//WiFi配置类型
static ch_wifi_config_type_t g_wifi_cfg_type;
//WiFi信号强度
static ch_signal_t g_wifi_signal;
//设备类型
static uint8_t g_device_type[CH_DEVICE_TYPE_LEN];
//设备pin码
static uint8_t g_device_pin[CH_DEVICE_PIN_LEN + 1];
//时间
static uint32_t g_sys_time;
//上一次接收时间
static uint32_t g_last_recv_time;
//版本
static uint8_t g_version[CH_VERSION_LEN] = {0, 0, 0, 0};

//callback
static ch_event_handler_func_t g_event_handler;
static ch_uart_write_func_t g_uart_write;


//帧接收
static uint8_t g_recving_frame_buf[RECV_BUF_SIZE];
static uint8_t g_recving_frame_buf_count;
static uint8_t g_frame_recved;

//帧发送
static uint8_t g_send_buf[SEND_BUF_SIZE];
static frame_t * const g_send_frame = (frame_t *)g_send_buf;

//京东产品UUID
static uint8_t g_use_jd_product_uuid;
static char g_jd_product_uuid[7];

//微信设备类型
static uint8_t g_use_wechat_devtype;
static char g_wechat_devtype[12] = {0};

//国美设备类型
static uint8_t g_use_gm_type;
static char g_gm_model[12];
static char g_gm_pin[33];

static uint8_t g_module_ready = 0;
static uint8_t g_wifiSSID[33] = {0};
static bool g_pro_test_start;
static bool g_pro_test_result;

static uint32_t data2Num(const uint8_t *data, uint8_t len, uint8_t bigEnd)
{
  uint8_t i;
  uint32_t num = 0;
  
  for(i = 0; i < len; i++)
  {
	num = num << 8;
	if(bigEnd)
	{
	  num += data[i];
	}
	else{
	  num += data[len - i - 1];
	}
  }
  return num;
}

void ch_init(const ch_config_t *cfg)
{
  g_sys_time = 0;
  g_last_recv_time = 0;
  
  memcpy(g_device_type, cfg->device_type, CH_DEVICE_TYPE_LEN);
  memcpy(g_device_pin, cfg->pinCode, CH_DEVICE_PIN_LEN);
  g_event_handler = cfg->event_handler;
  g_uart_write = cfg->uart_write;
  
  g_use_jd_product_uuid = cfg->use_jd_product_uuid;
  if(g_use_jd_product_uuid)
  {
	strcpy(g_jd_product_uuid, cfg->jd_product_uuid);
  }
  
  g_use_wechat_devtype = cfg->use_wechat_devtype;
  if(g_use_wechat_devtype)
  {
	strcpy(g_wechat_devtype, cfg->wechat_devtype);
  }
  
  g_use_gm_type = cfg->use_gm_product_type;
  if(g_use_gm_type)
  {
	strcpy(g_gm_model, cfg->gm_model);
	strcpy(g_gm_pin, cfg->gm_pin);
  }
  
  memcpy(g_version, cfg->version, CH_VERSION_LEN);
}

static uint16_t getFrameLen(uint8_t lsb, uint8_t msb)
{
  uint16_t len = msb;
  return ((len << 8) + lsb);
}

void ch_timer_ms()
{
  g_sys_time++;
}

void frame_recv_byte(uint8_t byte)
{
  frame_t *frame = (frame_t *)g_recving_frame_buf;
  
  if(g_frame_recved)
  {
	return;
  }
  
  g_recving_frame_buf[g_recving_frame_buf_count++] = byte;
  
  switch(g_recving_frame_buf_count)
  {
	//前导码
  case 1:
	if(frame->precode != PRECODE)
	{
	  g_recving_frame_buf_count = 0;
	}
	break;
	
	//长度
  case 3:
	if(getFrameLen(frame->lenLsb, frame->lenMsb) > RECV_BUF_SIZE - 6)
	{
	  g_recving_frame_buf_count = 0;
	}
	break;
	
	//接收完成
  default:
	if(g_recving_frame_buf_count == \
	  (getFrameLen(frame->lenLsb, frame->lenMsb) + sizeof(frame_t) + 1))
	{
	  g_frame_recved = 1;
	  g_recving_frame_buf_count = 0;
	}
  }
}

void ch_uart_data_input(const uint8_t *data, uint8_t len)
{
  uint8_t i;
  for(i = 0; i < len; i++)
  {
	frame_recv_byte(data[i]);
  }
}

static uint8_t calcChecksum(const void *buf, uint16_t len)
{
  const uint8_t *data = buf;
  uint8_t sum = 0;
  uint16_t i;
  for(i = 0; i < len; i++)
  {
	sum += data[i];
  }
  return sum;
}

static frame_t *recv_frame()
{
  if(g_frame_recved)
  {
	frame_t *frame = (frame_t *)g_recving_frame_buf;
	uint16_t frameLen = getFrameLen(frame->lenLsb, frame->lenMsb) + sizeof(frame_t);
	//计算校验
	if(calcChecksum((void *)frame, frameLen) == g_recving_frame_buf[frameLen])
	{
	  return frame;
	}
	else
	{
	  g_frame_recved = 0;
	}
  }
  return NULL;
}

static void handle_request_config(frame_t *frame)
{
  uint8_t i = 0;
  uint8_t *data = (uint8_t *)GET_SEND_FRAME_DATA();
  data[i++] = 1;                                       //协议版本
  
  data[i++] = CH_DEVICE_TYPE_LEN;                      //产品型号长度
  memcpy(&data[i], g_device_type, CH_DEVICE_TYPE_LEN); //产品型号
  i += CH_DEVICE_TYPE_LEN;
  
  data[i++] = CH_DEVICE_PIN_LEN;                       //pin码长度
  memcpy(&data[i], g_device_pin, CH_DEVICE_PIN_LEN);   //产品pin码
  i += CH_DEVICE_PIN_LEN;
  
  memcpy(&data[i], g_version, 4);                      //固件版本号
  i += 4;

  send_frame(FRAME_TYPE_REQUEST_CONFIG, i, 1, CH_CFG_FRAME_SEND_RETRIES);
}

static void comm_test(void)
{
  static uint32_t commTestTime = 0;
  if(!g_module_ready && ChTimeBeyond(g_sys_time, commTestTime, 500))
  {
	send_frame(FRAME_TYPE_COMM_TEST, 0, 0, 0);
	commTestTime = g_sys_time;
  }
}

static void send_data(const uint8_t *data, uint16_t len)
{
    uint16_t li;

    ch_log("%d", len);
    for(li = 0; li < len; li++)
    {
        ch_printf("%02x ", data[li]);
    }
    ch_printf("\n");
    g_uart_write(data, len);
}

static void send_frame(frame_type_t type, uint16_t len, uint8_t ack, uint8_t retry)
{
  g_send_frame->precode = PRECODE;
  g_send_frame->lenLsb = (uint8_t)(len & 0x00ff);
  g_send_frame->lenMsb = (uint8_t)((len & 0xff00) >> 8);
  g_send_frame->type = type;
  g_send_buf[len + sizeof(frame_t)] = calcChecksum(g_send_buf, len + sizeof(frame_t));
  send_data(g_send_buf, sizeof(frame_t) + len + 1);
}

static void handle_operation(frame_t *frame)
{
  uint16_t datalen;
  property_t *tmp_property;
  ch_event_param_operation_t event_operation;
  uint8_t *tmp_data;
  int16_t tmpNum16;
  
  uint16_t i = 0;
  uint16_t contentLen = getFrameLen(frame->lenLsb, frame->lenMsb);
  while(i < contentLen)
  {
	memset(&event_operation, 0, sizeof(ch_event_param_operation_t));
	tmp_property = (property_t *)(frame + 1);  //属性内容
	tmp_data = (uint8_t *)(tmp_property + 1); //属性值
	event_operation.propertyid[0] = tmp_property->id[0];
	event_operation.propertyid[1] = tmp_property->id[1];
	
	datalen = getFrameLen(tmp_property->lenLsb, tmp_property->lenMsb_type & 0x1F);
	
	if((tmp_property->lenMsb_type >> 5) == 1)
	{
	  //字符串末尾补0
	  tmp_data[datalen] = '\0';
	  event_operation.value.text = (char *)(tmp_data);
	}
	else
	{
	  if(datalen == 1)
	  {
		event_operation.value.num = (int8_t)tmp_data[0];
	  }
	  else if(datalen == 2)
	  {
		tmpNum16 = tmp_data[0];
		tmpNum16 = (tmpNum16 << 8) + tmp_data[1];
		event_operation.value.num = (int32_t)tmpNum16;
	  }
	  else
	  {
		event_operation.value.num = (int32_t)data2Num(tmp_data, 4, IS_BIGEND);
	  }
	}
	i += (sizeof(property_t) + datalen);
	g_event_handler(CH_EVENT_OPERATION, &event_operation);
  }
}

static void handle_heartbeat(frame_t *frame)
{
  heartbeat_t *hb;
  
  
  hb = (heartbeat_t *)(frame + 1);
  g_wifi_cfg_type = (ch_wifi_config_type_t)(hb->online_signal_cfg >> 4);
  g_wifi_signal = (ch_signal_t)((hb->online_signal_cfg >> 1) & 0x07);
  g_online = hb->online_signal_cfg & 0x01;
  g_wifi_status = (ch_wifi_status_t)(hb->wifi_status & 0x0F);
}

void parseUTCTime(ch_nettime_t *nettime, uint32_t seconds)
{
  uint32_t tmp = seconds % (24 * 60 * 60);
  nettime->sec = (tmp % 60);
  tmp /= 60;
  nettime->min = tmp % 60;
  nettime->hour = tmp / 60;
}

static void privateInfoReport(void)
{
  uint8_t i = 0, len;
  uint8_t *data = (uint8_t *)GET_SEND_FRAME_DATA();
  
  if(g_use_gm_type && g_gm_model[0] != 0 && g_gm_pin != 0)
  {
	i = 0;
	data[i++] = 0x03;
	len = strlen(g_gm_model);
	data[i++] = len;
	memcpy(&data[i], g_gm_model, len);
	i += len;
	
	len = strlen(g_gm_pin);
	data[i++] = len;
	memcpy(&data[i], g_gm_pin, len);
	i += len;
	
	send_frame(FRAME_TYPE_PRIVATE_INFO, i, 1, CH_CFG_FRAME_SEND_RETRIES);
  }
  
  if(g_use_jd_product_uuid && g_jd_product_uuid[0] != 0)
  {
	i = 0;
	data[i++] = 0x01;
	len = strlen(g_jd_product_uuid);
	data[i++] = len;
	memcpy(&data[i], g_jd_product_uuid, len);
	i += len;
	
	send_frame(FRAME_TYPE_PRIVATE_INFO, i, 1, CH_CFG_FRAME_SEND_RETRIES);
  }
  
  if(g_use_wechat_devtype && g_wechat_devtype[0] != 0)
  {
	i = 0;
	data[i++] = 0x02;
	len = strlen(g_wechat_devtype);
	data[i++] = len;
	memcpy(&data[i], g_wechat_devtype, len);
	i += len;
	
	send_frame(FRAME_TYPE_PRIVATE_INFO, i, 1, CH_CFG_FRAME_SEND_RETRIES);
  }
}

static void handle_ssid_ret(frame_t *frame)
{
  bool result = false;
  uint8_t *data = (uint8_t *)(frame + 1);
  if(g_wifiSSID[0] == '\0')
  {
	if(data)
	{
	  result = true;
	}
  }
  else
  {
	if(strstr((char *)data, (char *)g_wifiSSID) != NULL)
	{
	  result = true;
	}
  }
  g_event_handler(CH_EVENT_FIND_SSID_RESULT, &result);
}

static void handle_recv()
{
  uint16_t i;
  uint16_t len;
  frame_type_t type;
  frame_t *frame = recv_frame();
  uint8_t *data = (uint8_t *)frame;
  uint8_t test_part_data[5];
  uint8_t test_real_data[5] = {0x00,0x01,0x00,0x01,0x01};
  
  if(!frame)
  {
	return;
  }
  
  g_last_recv_time = g_sys_time;
  len = getFrameLen(frame->lenLsb, frame->lenMsb) + sizeof(frame_t) + 1;
  type = (frame_type_t)(frame->type);
  
  ch_log("");
  for(i = 0; i < len; i++)
  {
	ch_printf("%02x ", data[i]);
  }
  ch_printf("\n");
  
  if(FRAME_NEED_REPLY(frame))
  {
	send_frame(FRAME_TYPE_REPLY, 0, 0, 0);
  }
  
  if(type == FRAME_TYPE_HEARTBEAT)
  {
	if(g_justResponseConfig)
	{
	  g_justResponseConfig = 0;
	  g_event_handler(CH_EVENT_MODULE_STARTUP, NULL);
	}
	
	handle_heartbeat(frame);
  }
  else if(type == FRAME_TYPE_COMM_TEST)
  {
	g_justResponseConfig = 1;
	g_module_ready = 1;
	privateInfoReport();
	handle_request_config(frame);
  }
  else if(type == FRAME_TYPE_OPERATION)
  {
	handle_operation(frame);
  }
  else if(type == FRAME_TYPE_REPLY)
  {
	
  }
  else if(type == FRAME_TYPE_NET_TIME)
  {
	net_time_t *nettime = (net_time_t *)(frame + 1);
	ch_event_param_nettime_t nettime_param;
	nettime_param.timezone = nettime->timezone;
	nettime_param.seconds = data2Num(nettime->seconds, 4, IS_BIGEND);
	nettime_param.nettime.year = nettime->year;
	nettime_param.nettime.month = nettime->month;
	nettime_param.nettime.day= nettime->day;
	parseUTCTime(&nettime_param.nettime, nettime_param.seconds + nettime_param.timezone * 3600);
	g_event_handler(CH_EVENT_NET_TIME_RESPONSE, &nettime_param);
  }
  else if(type == FRAME_TYPE_QUERY)
  {
	if(g_pro_test_start)
	{
	  build_property(test_part_data, 1, 1, NULL);
	  if(memcmp(test_part_data, test_real_data, 5) == 0)
	  {
		g_pro_test_result = true;
	  }
	}
	g_event_handler(CH_EVENT_MODULE_QUERY, NULL);
  }
  else if(type == FRAME_TYPE_SSID_RETRIEVAL)
  {
	handle_ssid_ret(frame);
  }
  else if(type == FRAME_TYPE_ENTER_TEST)
  {
	g_event_handler(CH_EVENT_DEV_SELF_TEST, NULL);
  }
  else if(type == FRAME_TYPE_HISTORY_DATA_QUERY)
  {
  	history_data_t *history = (history_data_t*)(frame + 1);
  	ch_event_param_history_t historyParam;
	historyParam.startTime= data2Num(history->startSeconds, 4, IS_BIGEND);
	historyParam.endTime= data2Num(history->endSeconds, 4, IS_BIGEND);
	g_event_handler(CH_EVENT_HISTORY_DATA_QUERY, &historyParam);
  }
  else if(type == FRAME_TYPE_ROM_UPGRADE)
  {
  	handle_rom_upgrade(frame);
  }
  
  g_frame_recved = 0;
}

void ch_handle(void)
{
  handle_recv();
  comm_test();
  
  if(g_sys_time - g_last_recv_time > HB_TIMEOUT)
  {
	g_last_recv_time = g_sys_time;
	g_event_handler(CH_EVENT_MODULE_DIE, NULL);
  }
  
}

static uint16_t build_property(uint8_t * buf, uint16_t propertyid, int32_t num, const char * text)
{
  uint16_t len = sizeof(propert_reduced_t);
  propert_reduced_t *property = (propert_reduced_t *)buf;
  uint16_t val_2;
  
  property->id[0] = (uint8_t)(propertyid >> 8);
  property->id[1] = (uint8_t)(propertyid & 0x00ff);
  if(text == NULL)
  {
	if(num >= CHAR_MIN && num <= CHAR_MAX)
	{
	  property->lenMsb_type = 0;
	  property->lenLsb = 1;
	  buf[4] = (uint8_t)num;
	}
	else if(num >= SHRT_MIN && num <= SHRT_MAX)
	{
	  val_2 = (uint16_t)num;
	  property->lenMsb_type = 0;
	  property->lenLsb = 2;
	  buf[4] = (uint8_t)(val_2 >> 8);
	  buf[5] = (uint8_t)(val_2 & 0xff);
	}
	else
	{
	  property->lenMsb_type = 0;
	  property->lenLsb = 4;
	  num_to_data((uint8_t *)(property + 1), num);
	}
	len = len + property->lenLsb;
	
  }
  else
  {
	uint16_t textLen = strlen(text);
	
	property->lenMsb_type = (uint8_t)((textLen >> 8) & 0x001f) + 0x20;
	property->lenLsb = (uint8_t)(textLen & 0x00ff);
	strcpy((char *)(property + 1), text);
	len = len + textLen;
  }
  return len;
}

bool ch_set_fault(const uint8_t *fids, uint8_t count)
{
  uint8_t *data = GET_SEND_FRAME_DATA();
  memcpy(data, fids, count);
  send_frame(FRAME_TYPE_FAULT, count, 1, CH_CFG_FRAME_SEND_RETRIES);
  return 1;
}

void ch_request_nettime()
{
  send_frame(FRAME_TYPE_NET_TIME, 0, 0, 0);
}

bool ch_sync_property(uint16_t propertyid, int32_t num, const char *text)
{
  uint16_t len;
  uint8_t *data = (uint8_t *)GET_SEND_FRAME_DATA();
  
  len = build_property(data, propertyid, num, text);
  
  send_frame(FRAME_TYPE_SYNC_PROPERTY, len, 1, CH_CFG_FRAME_SEND_RETRIES);
  return 1;
}

void ch_find_ssid(char *ssid)
{
  char *data = (char *)GET_SEND_FRAME_DATA();
  if(!ssid)
  {
	g_wifiSSID[0] = '\0';
	send_frame(FRAME_TYPE_SSID_RETRIEVAL, 0, 0, 0);
  }
  else
  {
	memcpy(g_wifiSSID, ssid, strlen(ssid));
	g_wifiSSID[strlen(ssid)] = '\0';
	strcpy(data, ssid);
	send_frame(FRAME_TYPE_SSID_RETRIEVAL, strlen(ssid), 0, 0);
  }
}

bool ch_reset_cloud(void)
{
  char *reset = (char *)GET_SEND_FRAME_DATA();
  strcpy(reset, "YES");
  
  send_frame(FRAME_TYPE_CLOUD_DATA_CLEAR, 3, 1, CH_CFG_FRAME_SEND_RETRIES);
  return 1;
}

bool ch_dev_self_test_ret(const uint8_t *fids, uint8_t count)
{
  uint8_t *data = GET_SEND_FRAME_DATA();
  memcpy(data, fids, count);
  send_frame(FRAME_TYPE_TEST_RESULT, count, 1, CH_CFG_FRAME_SEND_RETRIES);
  return 1;
}

bool ch_protocol_test(void)
{
  static bool first = true;;
  g_pro_test_start = true;
  if(g_pro_test_result)
  {
	if(first)
	{
	  first = false;
	}
	g_pro_test_start = false;
	return true;
  }
  else
  {
	return false;
  }
}

//状态相关

bool ch_set_wifi_config_type(ch_wifi_config_type_t type)
{
  g_wifi_cfg_type = type;
  uint8_t *data = GET_SEND_FRAME_DATA();
  data[0] = type;
  send_frame(FRAME_TYPE_SET_WIFI_CONFIG_TYPE, 1, 1, CH_CFG_FRAME_SEND_RETRIES);
  return 1;
}

ch_wifi_config_type_t ch_get_wifi_config_type()
{
  return g_wifi_cfg_type;
}

ch_signal_t ch_get_wifi_signal()
{
  return g_wifi_signal;
}

uint8_t ch_is_online()
{
  return g_online;
}

ch_wifi_status_t ch_get_wifi_status()
{
  return g_wifi_status;
}

void ch_clear_recv()
{
  g_frame_recved = 0;
}

void ch_send_operation_result(uint8_t success, uint16_t propertyid)
{
  uint8_t len = sizeof(operation_result_t);
  
  operation_result_t * or = GET_SEND_FRAME_DATA();
  or ->success = success;
  or ->pid[0] = propertyid / 256;
  or ->pid[1] = propertyid % 256;
  
  send_frame(FRAME_TYPE_OPERATION, len, 0, 0);
}

//整形数转大端存储
void num_to_data(uint8_t *data, uint32_t num)
{
  data[3] = (num >> 0) & 0xff;
  data[2] = (num >> 8) & 0xff;
  data[1] = (num / 65536) & 0xff;
  data[0] = (num / 16777216) & 0xff;
}

//other
//大端储转整形数
uint32_t data_to_num(const uint8_t *data)
{
    uint32_t num;
    num = (((uint32_t)data[0]) << 24);
    num |= (((uint32_t)data[1]) << 16);
    num |= (((uint32_t)data[2]) << 8);
    num |= (((uint32_t)data[3]) << 0);
    return num;
}
#if 0
//整形数转小端存储
void num_to_data(uint8_t *data, uint32_t num)
{
    data[0] = (num >> 0) & 0xff;
    data[1] = (num >> 8) & 0xff;
    data[2] = (num >> 16) & 0xff;
    data[3] = (num >> 24) & 0xff;
}
#endif

static uint16_t g_multiPropertyLen = 0;
uint16_t ch_sync_multi_property(uint16_t propertyid, int32_t num, const char *text)
{
  ch_printf("  %d:%d:%s   ",propertyid,num,text?text:"");
  uint8_t *data = (uint8_t *)GET_SEND_FRAME_DATA();
  g_multiPropertyLen += build_property(data+g_multiPropertyLen, propertyid, num, text);
  
  return g_multiPropertyLen;
}

void ch_send_multi_property(uint8_t type)
{
	ch_printf("send muti-property\n");
	if(g_multiPropertyLen)
	{
		if(type == FRAME_TYPE_FOURCE_SYNC_PROPERTY || type == FRAME_TYPE_QUERY || type == FRAME_TYPE_SYNC_PROPERTY)
		{
			send_frame(type, g_multiPropertyLen, 1, CH_CFG_FRAME_SEND_RETRIES);
		}
		g_multiPropertyLen = 0;		
	}

}

static uint32_t g_historyLen = 0;
int8_t  ch_send_history_data(const char *record, uint8_t recordEnd)
{
	uint8_t dataFull = 0;
	uint8_t *historyData = (uint8_t *)GET_SEND_FRAME_DATA();
	if(strlen(record) > 1024)
	{
		return -1;
	}
	
	if((g_historyLen + strlen(record) + 1) > 1000)
	{
		dataFull = 1;
	}
	
	ch_printf("ch_send_history_data: %s   ##%d, %d ## recordLen=%d hisLen=%d\n",record, dataFull, recordEnd,strlen(record),g_historyLen);
	if(dataFull)
	{
	
		ch_printf("dataFull send\n");
		send_frame(FRAME_TYPE_HISTORY_DATA_POST, g_historyLen, 1, CH_CFG_FRAME_SEND_RETRIES);
		g_historyLen = 0;
		if(recordEnd)
		{
			hal_wait_ms(50);
			historyData[g_historyLen++] = (uint8_t)((strlen(record)& 0xff00) >> 8);
			historyData[g_historyLen++] = (uint8_t)(strlen(record) & 0x00ff);

			strcpy((char *)historyData+g_historyLen, record);
			g_historyLen += strlen(record);
			ch_send_history_data_end();
		}
	}
	else
	{
		historyData[g_historyLen++] = (uint8_t)((strlen(record)& 0xff00) >> 8);		
		historyData[g_historyLen++] = (uint8_t)(strlen(record) & 0x00ff);
		strcpy((char *)historyData+g_historyLen, record);
		g_historyLen += strlen(record);
		if(recordEnd)
		{
			ch_send_history_data_end();
		}
	}

	return dataFull|recordEnd;
}


void  ch_send_history_data_end(void)
{
	if(g_historyLen != 0)
	{
		ch_printf("ch_send_history_data_end\n");
		send_frame(FRAME_TYPE_HISTORY_DATA_POST, g_historyLen, 1, CH_CFG_FRAME_SEND_RETRIES);
		g_historyLen = 0;
	}
}

//ota
static void handle_rom_upgrade(frame_t *frame)
{
    rom_notify_t *tmp_rom_notify;
    rom_t *tmp_rom;
    rom_info_t *tmp_rom_info;
    rom_data_t *tmp_rom_data;
    ch_event_param_rom_notify_t event_notify;
    ch_event_param_rom_info_t event_info;
    ch_event_param_rom_data_t event_data;

    tmp_rom = (rom_t *)(frame + 1);
    if(tmp_rom->type == ROM_NOTIFY)
    {
        ch_log("recv ROM_NOTIFY");
        tmp_rom_notify = (rom_notify_t *)tmp_rom->data;
        event_notify.version = tmp_rom_notify->version;
        g_event_handler(CH_EVENT_ROM_UPGRADE_NOTIFY, &event_notify);
    }
    else if(tmp_rom->type == ROM_INFO)
    {
        ch_log("recv ROM_INFO");
        tmp_rom_info = (rom_info_t *)tmp_rom->data;
        event_info.md5 = tmp_rom_info->md5;
        event_info.version = tmp_rom_info->version;
        event_info.rom_size = data_to_num(tmp_rom_info->size);
        g_event_handler(CH_EVENT_ROM_UPGRADE_INFO, &event_info);
    }
    else if(tmp_rom->type == ROM_DATA)
    {
    	uint32_t offSet = 0;
        ch_log("recv ROM_DATA");
        tmp_rom_data = (rom_data_t *)tmp_rom->data;
		offSet = data_to_num(tmp_rom_data->addrOffset);
        event_data.part_index = (offSet/CH_ROM_PART_SIZE);
		
        ch_log("recv ROM_DATA offSet: %d, part_index:%d", offSet, event_data.part_index);
		
        event_data.data = tmp_rom_data->data;
        g_event_handler(CH_EVENT_ROM_UPGRADE_DATA, &event_data);
    }
}

void ch_request_rom_info()
{
    rom_t *rom = (rom_t *)GET_SEND_FRAME_DATA();
    rom->type = ROM_INFO;
    send_frame(FRAME_TYPE_ROM_UPGRADE, 1, 0, 0);
}

void ch_request_rom_data(uint16_t part_index, uint32_t FwSize)
{
    uint8_t partSize;
    rom_t *rom = (rom_t *)GET_SEND_FRAME_DATA();
    rom_data_t *rd = (rom_data_t *)rom->data;
    rom->type = ROM_DATA;
	if(FwSize - (part_index*CH_ROM_PART_SIZE) >= CH_ROM_PART_SIZE)
    {
        partSize = 128;
    }
    else
    {
        partSize = FwSize - (part_index*CH_ROM_PART_SIZE);
    }
	num_to_data(rd->addrOffset, part_index*CH_ROM_PART_SIZE);
	rd->dataLen[1] = partSize & 0xff;
    rd->dataLen[0] = (partSize >> 8) & 0xff;
	ch_log("ch_request_rom_data:%d partSize: %d",part_index,partSize);

    send_frame(FRAME_TYPE_ROM_UPGRADE, sizeof(rom_t) + sizeof(rom_data_t), 0, 0);
}

bool ch_request_rom_end(const uint8_t *ver)
{
    rom_t *rom = (rom_t *)GET_SEND_FRAME_DATA();
    rom->type = ROM_END;
    memcpy(rom->data, ver, CH_VERSION_LEN);

    send_frame(FRAME_TYPE_ROM_UPGRADE, 5, 0, 0);
}

