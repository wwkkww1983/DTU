#include "ModuleTester.h"
#include <string.h>
#include <stdlib.h>
#include "MD5.h"
#include "cloud_helper.h"
#include "cloud_protocol.h"
#include "hal_led.h"
#include "device.h"
#include "hal_wait.h"

#define SysTimeBeyond(cur, last, past) ((cur) - (last) > (past))
//电相关信息
typedef struct
{
	
    uint32_t CurTemperature;  //板载内部温度传感器
    uint32_t CurHumidity;     //采集到的当前湿度
    uint32_t PM25;            //PM2.5
    uint32_t CO2;             //CO2

    uint32_t WifiCfgTimeOut;  //wifi配置超时时间
    uint32_t ResetToDefaultsTimeOut;//恢复出厂设置超时
    uint32_t NormalTimeOut;

    uint8_t PreSmartFlag;   //预进入smart模式标志
    uint8_t SendReqFlag;    //发送配网指令标志

    uint8_t ResetToDefaultsFlag;  //恢复出厂设置标志
    uint8_t AppAdjCmd;      //校正Att7053命令值
    uint8_t AppOperate;     //收到操作指令
    uint8_t AppQry;         //APP查询状态
    uint8_t TestStatus;     //测试状态
    uint8_t NormalCount;    //计数

    uint8_t wm_online_status;
    uint8_t sht_online_status;
	uint8_t sk_online_status;

	uint8_t sensorTHWork;
	uint8_t sensorPM25Work;
	uint8_t sensorCO2Work;
} GW_ElectInfo_st;

typedef struct
{
    SysTime_t lastRequestNettimeTime;
    SysTime_t lastRtcUpdateTime;
    ch_nettime_t RtcTimer;
	int8_t timezone;        //时区
    uint32_t seconds;       //零时区 1970/01/01 00:00:00 至今的秒数
    uint8_t gotNetTime;
} Rtctime_t;

typedef struct UpdateInfo_st
{
    uint32_t lastRequestTime;   //记录请求数据或者固件信息帧的时间
    uint32_t flashUnlockTime;   //记录Flash解锁时间
    uint32_t FwSize;            //获取到的固件信息长度
    uint16_t Parts;             //固件所需要帧数
    uint16_t CurIndex;          //当前请求的固件数据编号
    uint8_t FwMd5[CH_MD5_LEN]; //固件md5值
    uint8_t FwVersion[CH_VERSION_LEN]; //固件版本号
    uint8_t FwFlag;             //标识固件升级还是码库下载 0x55 0xaa
    uint8_t UpdateFlag;       //处于升级状态标志位
    uint8_t ReqCount;           //升级请求次数
    uint8_t flashUnlockFlag;    //flash解锁标志
} UpdateInfo_t;



#define START_CODE  0XFC
#define IN_UPDATE_FLAG              (0xAA)
#define UPDATE_FLAG_DONE            (0xC5)

#define FLASH_WRITE_BLOCK_SIZE      128

#define BOOT_VERSION "1.0.0.1"
/*******************************************************************************
 * declaration
 ******************************************************************************/
static SysButtonState_t getButtonState(SysButton_t *button);
static uint8_t buttonHandler(SysButton_t *button, uint32_t pressTime, SysButtonState_t state);
static uint8_t uartRecv(const uint8_t *buf, uint32_t len);
static void chEventHandler(ch_event_t event, void *p);
static void chUartWrite(const uint8_t *data, uint16_t len);
//static uint8_t CheckPmUartInfo(const uint8_t *buf, uint32_t len);
static void syncProperties_all(void);
static void check_wm_sht_status(void *pt);
static void RtcTimerProcess(void);

static void parse_new_device_info_notify(const uint8_t *buf, uint8_t len);
static void answer_req_info_handler(const uint8_t *buf, uint8_t len);
static void parse_query_device_info_handler(const uint8_t *buf, uint8_t len);
static char *trans_property_by_text(uint8_t pid, void *num);
static void post_all_property(uint8_t isAll, uint8_t type);
static uint16_t Pid(const char *device, uint8_t num, const char *type);
static void RomInfoProcess(ch_event_param_rom_info_t *p);
static void RomDataProcess(ch_event_param_rom_data_t *p);
static void clrUpdateInfo(void);

static void num_to_data(uint8_t *data, uint32_t num);
static uint8_t checkMd5Val(uint32_t startAddr, const uint8_t *buffer, uint32_t fwSize);
static void updatePoll(void);
static void ClearUpdateFlag( void );


/*******************************************************************************
 * static variable
 ******************************************************************************/

//配置信息
static Rtctime_t g_rtc_time;
static SysButton_t g_ConfigButton;
static GW_ElectInfo_st g_gw_electInfo;

static SysTime_t g_lastSyncTime;

static bool g_needReset = false;
static MenuconfigInfo_t *g_config = NULL;
static UpdateInfo_t g_updateInfo = {0};


/*******************************************************************************
 * func
 ******************************************************************************/
//初始化
void ModuleTesterInit()
{
    ch_config_t cfg;
	printf("**********************************************************\n");
	printf("Boot Version:%s\n",BOOT_VERSION);
	printf("BuildTime:%s %s\n",__DATE__,__TIME__);
	printf("**********************************************************\n");
    HalUartConfig(WIFI_UART, 9600, uartRecv);
	g_config = GetConfig();

    //注册按键
    SysButtonRegister(&g_ConfigButton, buttonHandler, getButtonState);

    cfg.device_type = (const char *)g_config->id;
    cfg.pinCode = (const char *)g_config->pin;
    cfg.uart_write = chUartWrite;
    cfg.event_handler = chEventHandler;
    cfg.version[0] = g_config->version[0];
	cfg.version[1] = g_config->version[1];
	cfg.version[2] = g_config->version[2];
	cfg.version[3] = g_config->version[3];
    ch_init(&cfg);

    clrUpdateInfo();
}


//循环
void ModuleTesterPoll()
{
    ch_handle();    //接收与发送函数处理
	updatePoll();
}

/*******************************************************************************
 * cloud helper
 ******************************************************************************/
//事件处理函数
static void chEventHandler(ch_event_t event, void *p)
{
    ch_event_param_operation_t *operation = (ch_event_param_operation_t *)p;
    ch_event_param_nettime_t *nettime = (ch_event_param_nettime_t *)p;
	ch_event_param_history_t *history = (ch_event_param_history_t *)p;
	ch_event_param_rom_info_t *rom_info = (ch_event_param_rom_info_t *)p;
    ch_event_param_rom_data_t *rom_data = (ch_event_param_rom_data_t *)p;
    switch(event)
    {
        case CH_EVENT_MODULE_STARTUP:
		  if(g_needReset)//只能暂时加到这里，否则堆栈异常
		  {
		  	NVIC_SystemReset();
		  }
            //模块启动，需同步所有属性
            SysLog("CH_EVENT_MODULE_STARTUP");
            break;
        case CH_EVENT_OPERATION:
            //响应App操作，并回复操作结果
			uint16_t pid;
			
            SysLog("CH_EVENT_OPOERATION");
			pid = operation->propertyid[0] * 256 + operation->propertyid[1];
			ch_send_operation_result(true, pid);
            break;

        case CH_EVENT_MODULE_DIE:
            //应复位wifi模块
            SysLog("CH_EVENT_MODULE_DIE");
            //HalWifiDeviceReset();
            break;
        case CH_EVENT_NET_TIME_RESPONSE:
            SysLog("got nettime.");
            g_rtc_time.gotNetTime = true;
            g_rtc_time.seconds = nettime->seconds;
			g_rtc_time.timezone = nettime->timezone;
            g_rtc_time.RtcTimer.year = nettime->nettime.year;
            g_rtc_time.RtcTimer.month = nettime->nettime.month;
            g_rtc_time.RtcTimer.day = nettime->nettime.day;
            g_rtc_time.RtcTimer.hour = nettime->nettime.hour;
            g_rtc_time.RtcTimer.min = nettime->nettime.min;
            g_rtc_time.RtcTimer.sec = nettime->nettime.sec;
            g_rtc_time.lastRtcUpdateTime = SysTime(); //记录当前本地时钟

            SysLog("got nettime %04d.%02d.%02d,%02d:%02d:%02d.", g_rtc_time.RtcTimer.year+2000, \
                   nettime->nettime.month, g_rtc_time.RtcTimer.day, g_rtc_time.RtcTimer.hour, \
                   g_rtc_time.RtcTimer.min, g_rtc_time.RtcTimer.sec);

            break;

		case CH_EVENT_MODULE_QUERY:
			SysLog("CH_EVENT_MODULE_QUERY");
			break;

		case CH_EVENT_HISTORY_DATA_QUERY:
			SysLog("CH_EVENT_HISTORY_DATA_QUERY");
			//TODO  start report
			//记录下时间，在poll中上报，没条间隔300ms，不到1024
			break;
//ota
		case CH_EVENT_ROM_UPGRADE_NOTIFY:
            SysLog("CH_EVENT_ROM_UPGRADE_NOTIFY");
            ch_request_rom_info();
            break;

        case CH_EVENT_ROM_UPGRADE_INFO:
            RomInfoProcess(rom_info);
            break;

        case CH_EVENT_ROM_UPGRADE_DATA:
            SysLog("CH_EVENT_ROM_UPGRADE_DATA %d", rom_data->part_index);
            RomDataProcess(rom_data);
            break;
//
        default:
            break;
    }
}

static void chUartWrite(const uint8_t *data, uint16_t len)
{
    HalUartWrite(WIFI_UART, data, len);
}

static uint8_t uartRecv(const uint8_t *buf, uint32_t len)
{
	ch_uart_data_input(buf, len);
	return len;
}

//按键处理函数   2s-6s松开，添加   按住20s恢复出厂设置
static uint8_t buttonHandler(SysButton_t *button, uint32_t pressTime, SysButtonState_t state)
{
    uint8_t retVal = 0;
    if(button == &g_ConfigButton) //电源开关
    {
        if(state == SYS_BUTTON_STATE_RELEASED)
        {

            retVal = 1;
        }
        else  //按键未释放
        {
			if((pressTime > 20000) )
			{
				SysLog("恢复出厂设置");
				g_needReset = true;
				retVal = 1; 
			}

        }
    }

    return retVal;
}


//判断按键状态
static SysButtonState_t getButtonState(SysButton_t *button)
{
    if(button == &g_ConfigButton)
    {
        return (SysButtonState_t)(!GPIO_ReadInputDataBit(KEY_PORT, KEY_PIN));
    }

    return SYS_BUTTON_STATE_RELEASED;
}

//固件升级info处理
static void RomInfoProcess(ch_event_param_rom_info_t *p)
{
    uint32_t uiTempAddr;
    uint8_t md5NotZero, i;
    uint8_t tempFlagBuffer[4];

    if(IN_UPDATE_FLAG == g_updateInfo.UpdateFlag)
    {
        return;
    }

    if(p->version[0] != g_config->version[0]
            || p->version[1] != g_config->version[1]
            || p->version[2] != g_config->version[2]
            || p->version[3] != g_config->version[3])
    {
        g_updateInfo.FwFlag = ROM_DOWNLOAD_FLAG;  //固件升级标志
        g_updateInfo.FwSize = p->rom_size;
        g_updateInfo.Parts = (g_updateInfo.FwSize + CH_ROM_PART_SIZE - 1)  / CH_ROM_PART_SIZE;
        memcpy(g_updateInfo.FwMd5, p->md5, CH_MD5_LEN);
        g_updateInfo.CurIndex = 0;
        md5NotZero = 0x00;

        for(i = 0; i < CH_MD5_LEN; i++)
        {
            if(0x00 != g_updateInfo.FwMd5[i])
            {
                md5NotZero = 0xff;
                break;
            }
        }

        if((0x00 != g_updateInfo.FwSize) && (0x00 != md5NotZero))
        {
        	SysLog("FwSize:%d, md5:%02x%02x...",g_updateInfo.FwSize,g_updateInfo.FwMd5[0],g_updateInfo.FwMd5[1]);
            g_updateInfo.UpdateFlag = IN_UPDATE_FLAG;
            ch_request_rom_data(g_updateInfo.CurIndex, g_updateInfo.FwSize);
            g_updateInfo.lastRequestTime = SysTime();
			g_updateInfo.ReqCount = 0;

        }
    }
    return;
}

//固件升级data处理
static void RomDataProcess(ch_event_param_rom_data_t *p)
{
    uint8_t writeSize;
    uint8_t tempFlagBuffer[4];
    uint32_t uiTempAddr;

    if(IN_UPDATE_FLAG != g_updateInfo.UpdateFlag)
    {
        return;
    }

    if(g_updateInfo.FwSize - (g_updateInfo.CurIndex << 7) >= 128)
    {
        writeSize = 128;
    }
    else
    {
        writeSize = g_updateInfo.FwSize - (g_updateInfo.CurIndex << 7);
    }

    if(p->part_index == g_updateInfo.CurIndex)  //收到帧序号等于请求帧序号
    {
        uiTempAddr = APP_START_ADDR + g_updateInfo.CurIndex * FLASH_WRITE_BLOCK_SIZE;

        SysFlashWrite(uiTempAddr, p->data, writeSize);

        g_updateInfo.CurIndex++;
		g_updateInfo.ReqCount = 0;
    }

    if(g_updateInfo.CurIndex < g_updateInfo.Parts)
    {
        ch_request_rom_data(g_updateInfo.CurIndex, g_updateInfo.FwSize);
        g_updateInfo.lastRequestTime = SysTime();
    }
    else if(g_updateInfo.CurIndex == g_updateInfo.Parts)   //已经接受完成最后一帧
    {
        //game over
        if(checkMd5Val(APP_START_ADDR, g_updateInfo.FwMd5, g_updateInfo.FwSize))
        {
            SysLog("checkMd5Val err!!");
            HalRestart();    //重启,重新去升级
        }
        else
        {
            num_to_data(tempFlagBuffer, FW_UPDATE_DONE);
			SysUserDataWrite(OTA_UPDATE_INFO_ADDR, tempFlagBuffer, 4);
			
            g_updateInfo.UpdateFlag = UPDATE_FLAG_DONE;
            ch_request_rom_end(g_updateInfo.FwVersion);  //上报升级结束
            g_updateInfo.lastRequestTime = SysTime();
        }
    }
    return ;
}

static void clrUpdateInfo(void)
{
    g_updateInfo.CurIndex = 0x00;
    g_updateInfo.lastRequestTime = 0x00;
    g_updateInfo.FwSize = 0x00;
    g_updateInfo.Parts = 0x00;
    g_updateInfo.FwFlag = 0x00;
    g_updateInfo.UpdateFlag = 0x00;
    g_updateInfo.ReqCount = 0x00;
    g_updateInfo.flashUnlockFlag = 0x00;
}


//检查md5校验
static uint8_t checkMd5Val(uint32_t startAddr, const uint8_t *buffer, uint32_t fwSize)
{
    MD5_CTX md5;
    uint8_t md5Value[CH_MD5_LEN];
    uint8_t ReadRomData[FLASH_WRITE_BLOCK_SIZE];
    uint16_t NumOfPage = 0, NumOfSingle = 0;
    uint16_t i;

    NumOfPage = (fwSize >> 7);  //128BYTE/page
    NumOfSingle = (fwSize & (FLASH_WRITE_BLOCK_SIZE - 1));

    MD5Init(&md5);

    if(SPI_ADDR(startAddr) & 0x08000000)  //flash
    {
    	SysLog("count md5 NumOfPage:%d NumOfSingle: %d",NumOfPage, NumOfSingle);
        for(i = 0; i < NumOfPage; i++)
        {
            SysUserDataRead(startAddr, ReadRomData, FLASH_WRITE_BLOCK_SIZE);
            MD5Update(&md5, ReadRomData, FLASH_WRITE_BLOCK_SIZE);
            startAddr += FLASH_WRITE_BLOCK_SIZE;
        }

        if(0x00 != NumOfSingle)
        {
            SysUserDataRead(startAddr, ReadRomData, NumOfSingle);
            MD5Update(&md5, ReadRomData, NumOfSingle);
            startAddr += NumOfSingle;
        }
    }

	SysLog("startAddr:%x",startAddr);
    MD5Final(&md5, md5Value);

	//TODO
	{
	uint8_t i;
	SysLog("fw md5:");
	for(i=0;i<CH_MD5_LEN;i++)
	{
		SysPrintf("%02x-",buffer[i]);
	}

		SysLog("count md5:");
	for(i=0;i<CH_MD5_LEN;i++)
	{
		SysPrintf("%02x-",md5Value[i]);
	}

	}
	//
    if(memcmp(buffer, md5Value, CH_MD5_LEN) == 0)
    {
        return 0x00;    //相等
    }
    else
    {
        return 0xff;    //错误
    }
}

static void num_to_data(uint8_t *data, uint32_t num)
{
  data[3] = (num >> 0) & 0xff;
  data[2] = (num >> 8) & 0xff;
  data[1] = (num / 65536) & 0xff;
  data[0] = (num / 16777216) & 0xff;
}

void ModuleTesterCheckUpdate(void)
{
    uint32_t uiTempData;
    uint8_t uiTempBuffer[4];
    uint32_t uiTempAddr;

    SysUserDataRead(OTA_UPDATE_INFO_ADDR, uiTempBuffer, 4);

    uiTempData = data_to_num(uiTempBuffer);

    if(FW_NEED_UPDATE_FLAG != uiTempData)
    {
    	printf("boot%s Jump to App\n",BOOT_VERSION);
        HalJumpAppAddr(SPI_ADDR(APP_START_ADDR));
    }
	else
	{
		printf("Boot Update Start\n");
	}
}


static void updatePoll(void)
{
    //
    if(SysTimeBeyond(SysTime(), g_updateInfo.lastRequestTime, UPDATE_REQUEST_TIMEOUT))
    {
        if(IN_UPDATE_FLAG == g_updateInfo.UpdateFlag)
        {
            ch_request_rom_data(g_updateInfo.CurIndex, g_updateInfo.FwSize);
            g_updateInfo.ReqCount++;
        }
        else if(UPDATE_FLAG_DONE == g_updateInfo.UpdateFlag)
        {
            g_updateInfo.UpdateFlag = 0x00;
            HalRestart();    //升级完成重启到APP
        }
        else
        {
            ch_request_rom_info();
			g_updateInfo.ReqCount++;
        }

        g_updateInfo.lastRequestTime = SysTime();
        if(g_updateInfo.ReqCount > 15)
        {
        	ClearUpdateFlag();
            HalRestart();//重新请求总数超过15次重启
        }
    }

}

static void ClearUpdateFlag( void )
{
	uint8_t tempFlagBuffer[4] = {0};
	num_to_data(tempFlagBuffer, FW_UPDATE_DONE);
	SysUserDataWrite(OTA_UPDATE_INFO_ADDR, tempFlagBuffer, 4);
}

