#ifndef MODULE_TESTER_H
#define MODULE_TESTER_H

#include "userSys.h"
	
#define ROM_DOWNLOAD_FLAG       0XAA    //固件下载标志值
#define RESET_TO_DEFAULTS_FLAG  0X5A    //恢复出厂设置标志

//flash中存储标志地址
#define FW_NEED_UPDATE_FLAG     0XB007B007  //固件需要升级
#define FW_UPDATE_DONE          0X13145213  //存在完整固件或者升级完成

#define FLASH_LOCK_TIMEOUT      2000    //2S
#define UPDATE_REQUEST_TIMEOUT  500     //500ms


void ModuleTesterInit(void);
void ModuleTesterPoll(void);
void ModuleTesterCheckUpdate(void);

#endif // MODULE_TESTER_H

