#ifndef CLOUD_PROTOCOL_H
#define CLOUD_PROTOCOL_H
#include "cloud_helper.h"


//设备类型
#define DEV_TYPE_LEN 12
#define DEV_PIN_CODE_LEN 32

#define FRAME_NEED_REPLY(frame) ((frame->type == FRAME_TYPE_QUERY)\
|| (frame->type == FRAME_TYPE_ENTER_TEST))

//ota
#define ROM_NOTIFY  1
#define ROM_INFO    2
#define ROM_DATA    3
#define ROM_END     4

struct rom_st
{
    uint8_t type;
    uint8_t data[];
};
typedef struct rom_st rom_t;

struct rom_notify_st
{
    uint8_t version[4];
};
typedef struct rom_notify_st rom_notify_t;

struct rom_info_st
{
    uint8_t version[4];
    uint8_t size[4];
    uint8_t md5[16];
};
typedef struct rom_info_st rom_info_t;

struct rom_data_st
{
    uint8_t addrOffset[4];
	uint8_t dataLen[2];
    uint8_t data[];
};
typedef struct rom_data_st rom_data_t;


//帧类型
typedef enum
{
    FRAME_TYPE_REPLY                = 0x00,
    FRAME_TYPE_COMM_TEST            = 0x01,
    FRAME_TYPE_REQUEST_CONFIG       = 0x02, 
    FRAME_TYPE_HEARTBEAT            = 0x03,
    FRAME_TYPE_QUERY                = 0x04,
    FRAME_TYPE_SYNC_PROPERTY        = 0x05,
    FRAME_TYPE_SSID_RETRIEVAL       = 0x06,
    FRAME_TYPE_OPERATION            = 0x07,
    FRAME_TYPE_SET_WIFI_CONFIG_TYPE = 0x08,
    FRAME_TYPE_FAULT                = 0x09,
    FRAME_TYPE_ROM_UPGRADE          = 0x0a,
    FRAME_TYPE_NET_TIME             = 0x0b,
    FRAME_TYPE_CLOUD_DATA_CLEAR     = 0x0c,
    FRAME_TYPE_ENTER_TEST           = 0x0d,
    FRAME_TYPE_TEST_RESULT          = 0x0e,
    FRAME_TYPE_ACTIONS_SEND         = 0x20,
    FRAME_TYPE_ACTIONS_CONTRL       = 0x21,
    FRAME_TYPE_ACTIONS_STATUS       = 0x22,
    FRAME_TYPE_PRIVATE_INFO         = 0x40,
    FRAME_TYPE_FOURCE_SYNC_PROPERTY = 0x12,
    FRAME_TYPE_HISTORY_DATA_QUERY   = 0x13,
    FRAME_TYPE_HISTORY_DATA_POST    = 0x14,
}frame_type_t;

//帧头
struct frame_st
{
    uint8_t  precode;
    uint8_t  lenMsb;
    uint8_t  lenLsb;
    uint8_t  type;
};
typedef struct frame_st frame_t;

//请求配置帧
struct config_st
{
    uint8_t protoVersion;
    uint8_t typeLen;
    char device_type[DEV_TYPE_LEN];
    uint8_t pinCodeLen;
    char device_pinCode[DEV_PIN_CODE_LEN];
    uint8_t hwVersion[4];
};
typedef struct config_st config_t;

//心跳帧
struct heartbeat_st
{
    uint8_t online_signal_cfg;
    uint8_t wifi_status;
};
typedef struct heartbeat_st heartbeat_t;

//设置属性帧
struct property_st
{
    uint8_t lenMsb_type;
    uint8_t lenLsb; 
    uint8_t id[2];
};
typedef struct property_st property_t;

struct propert_reduced_st
{
    uint8_t lenMsb_type;
    uint8_t lenLsb;
    uint8_t id[2];
};
typedef struct propert_reduced_st propert_reduced_t;

//操作帧，主控端回复
struct operation_result_st
{
    uint8_t pid[2];
    uint8_t success;
};
typedef struct operation_result_st operation_result_t;

//网络时间同步帧
struct net_time_st
{
    int8_t timezone;
    uint8_t seconds[4];
    uint8_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
};
typedef struct net_time_st net_time_t;

struct history_data_st
{
    uint8_t startSeconds[4];
	uint8_t endSeconds[4];
};
typedef struct history_data_st history_data_t;


//ssid返回帧
typedef struct
{
    uint8_t len;
}ssid_result_t;

//设置其他平台型号帧
typedef struct
{
    uint8_t platFlag;
    uint8_t jd_uuid_len;
    char jd_uuid[6];
}set_jd_t;

typedef struct
{
    uint8_t platFlag;
    uint8_t typeLen;
    char device_type[DEV_TYPE_LEN];
    uint8_t pinCodeLen;
    char device_pinCode[DEV_PIN_CODE_LEN];
}set_gm_t;

typedef struct
{
    uint8_t platFlag;
    uint8_t wechat_len;
}set_wechat_t;

#endif // CLOUD_PROTOCOL_H

