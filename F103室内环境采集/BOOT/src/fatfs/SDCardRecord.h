#ifndef HAL_SDCARDRECORD_H
#define HAL_SDCARDRECORD_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"

void SDCardRecordInit(void);
void SDCardRecordPoll(void);
uint32_t RecordBuildStart(void);
uint16_t RecordBuild(uint16_t propertyid, int32_t num, const char *text);
void RecordBuildEnd(uint32_t rtcTime);
void RecordReportStart(uint32_t startTs, uint32_t endTs);


#endif
