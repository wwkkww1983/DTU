#ifndef __NWK_H_
#define __NWK_H_
#include "os.h"
#include "net.h"

#define NWK_END_CONTENT_LEN     1    //1byte信号强度
#define NWK_ACK_CONTENT_LEN     5    //4byte时间,1byte设备数量

#define NWK_DEF_RETRIES         3
#define NWK_BROADCAST_ADDR      0xff
#define NWK_NULL_SEGADDR        "0000"
#define NWK_NULL_ADDR           NWK_BROADCAST_ADDR - 1
#define NWK_MAX_HOPS            5
#define NWK_SEGADDR_LEN         4

#define NWK_FLAG_SIMPLE         1
#define NWK_FLAG_RELAY          2

enum nwk_frame_t
{
    NWK_FRAME_NORMAL = 1,
    NWK_FRAME_LINK,
    NWK_FRAME_ACK_ONEHOP,
    NWK_FRAME_ACK_LINK,
    NWK_FRAME_ACK_SUBCACHE,
};

enum nwk_role_t
{
    NWK_ROLE_ROUTER = 1,
    NWK_ROLE_ENDDEVICE,
    NWK_ROLE_COORDINATOR,
};

enum nwk_sleepmode_t
{
    NWK_SLEEPMODE_NORMAL,
    NWK_SLEEPMODE_SLEEP
};

typedef struct
{
    uint8_t type;
    uint8_t sleepmode;
    uint8_t segaddr[NWK_SEGADDR_LEN];
    uint8_t srcaddr;
    uint8_t dstaddr;
    uint8_t sender_addr;
    uint8_t recver_addr;
    uint8_t trans_id;
    uint8_t payload[];
} nwk_header_t;

void nwk_init(void);
void nwk_set_segaddr(const uint8_t *segaddr);
uint8_t *nwk_get_segaddr(void);
void nwk_set_myaddr(uint8_t addr);
uint8_t nwk_get_myaddr(void);
void nwk_set_role(uint8_t role);
uint8_t nwk_get_role(void);
void nwk_set_sleepmode(uint8_t sleepmode);

void nwk_trans_cb(nwk_header_t *pnwk);
void nwk_recv_cb(nwk_header_t *pnwk, uint8_t len, int8_t rssi);
uint8_t nwk_transmit(uint8_t type, uint8_t addr, uint8_t len, uint8_t flag);
uint8_t nwk_transmit_onehop(uint8_t dstaddr, uint8_t len, uint8_t directly);
//uint8_t nwk_transmit_directly(uint8_t type, uint8_t dstaddr, uint8_t len, uint8_t flag);
void nwk_poll(void);
void *nwk_get_trans_payload(void);
void nwk_mesh_enable(uint8_t enable);
void nwk_reply_acks(void);
void nwk_subcache_del(uint8_t addr);

#endif
