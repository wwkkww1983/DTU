#include "phy.h"
#include "net.h"
#include "Hal.h"
#include "userSys.h"

enum
{
    RADIOMODE_RX = 1,
    RADIOMODE_TX,
    RADIOMODE_IDLE,
};

#define TX_FIFO_SIZE      64
#define CCA_RETRIES       3     //50//chg cbl 0410
#define TOTX_TIME         2     //ms
#define TORX_TIME         2     //ms
#define MAX_TX_TIME       20    //ms
#define MAX_RF_WAIT       20    //ms  
#define UNIT_BACKOFFTIME  5     //ms
#define MAX_CTRL_DATA_LEN   20

#define CC1101_PTABLE_N5DB  0x3A  //-5db
#define CC1101_PTABLE_0DB   0x51  //0db
#define CC1101_PTABLE_5DB   0x85
#define CC1101_PTABLE_7DB   0xC8
#define CC1101_PTABLE_10DB  0xC0


#define CRC_CHECK(x) (x[x[0] + 1 + 1] & ( 1 << 7))

#define PHY_SET_RECV_MODE()     //{HalGPIOSet(HAL_GPIO_RX_ENS, 0); HalGPIOSet(HAL_GPIO_TX_ENS, 1);}
#define PHY_SET_SEND_MODE()     //{HalGPIOSet(HAL_GPIO_RX_ENS, 1); HalGPIOSet(HAL_GPIO_TX_ENS, 0);}

typedef struct
{
    uint8_t bytesLeft;
    uint8_t *pBuffer;
    bool    isLonger;
} TRANS_INFO;
static TRANS_INFO trans_longer;
//控制帧缓存
static uint8_t g_lock_ctrl_buf = 0;
static uint8_t g_ctrl_buf[sizeof(phy_header_t) + MAX_CTRL_DATA_LEN];

//发送接收缓存
static uint8_t g_tx_buf[PHY_MAX_LEN + 64]; //用于发送升级包
static uint8_t g_rx_buf[PHY_MAX_LEN + 2];  //CRC, RSSI
static uint8_t g_cc1101_id = 0;

INT8U halSpiReadReg(INT8U addr);


void _rx_dma_cb()
{
    phy_header_t *phy = (phy_header_t *)g_rx_buf;

    hal_rnd_feed_seed(g_rx_buf[g_rx_buf[0] + 1]);

    SysDataPrint((uint8_t *)g_rx_buf, 11);
    SysDataPrint((uint8_t *)g_rx_buf, g_rx_buf[0]);

    if(!CRC_CHECK(g_rx_buf))
    {
    	SysLog("CRC_CHECK err");
        goto recv_err;
    }

    //长度错误
    if(g_rx_buf[0] > PHY_MAX_LEN - 1)
    {
        goto recv_err;
    }

    //正常通信
    if(phy->type == PHY_TYPE_NORMAL)// && !g_is_upgrading)
    {
        nwk_recv_cb((nwk_header_t *)(g_rx_buf + sizeof(phy_header_t)), \
                    g_rx_buf[0] - PHY_HEAD_LEN, g_rx_buf[g_rx_buf[0] + 1]);
    }
    else
    {
        //特殊控制帧
        if(!g_lock_ctrl_buf)
        {
            g_lock_ctrl_buf = 1;
            memcpy(g_ctrl_buf, phy, sizeof(g_ctrl_buf));
        }

    }

recv_err:
    //RFIF &= ~IRQ_DONE;
    phy_set_rx_state(1);
}

void phy_rf_config_upgrade(void)
{
    halSpiWriteReg(CCxxx0_IOCFG0,       0x06);  //GDO0 Output Pin Configuration
    halSpiWriteReg(CCxxx0_PKTLEN,       0x3D);  //Packet Length: 61 data + 2 status +1 length=64, never overflow,make sure rx fifo empty before receive data
    halSpiWriteReg(CCxxx0_PKTCTRL0,     0x05);  //Packet Automation Control
    halSpiWriteReg(CCxxx0_ADDR,         0x00);    //Device Address
    halSpiWriteReg(CCxxx0_FSCTRL1,      0x0C); //Frequency Synthesizer Control ?
    halSpiWriteReg(CCxxx0_FREQ2,        0x10);   //Frequency Control Word, High Byte
    halSpiWriteReg(CCxxx0_FREQ1,        0xB1);   //Frequency Control Word, Middle Byte
    halSpiWriteReg(CCxxx0_FREQ0,        0x3B);   //Frequency Control Word, Low Byte
    halSpiWriteReg(CCxxx0_MDMCFG4,      0x2D); //Modem Configuration
    halSpiWriteReg(CCxxx0_MDMCFG3,      0x3B); //Modem Configuration
    halSpiWriteReg(CCxxx0_MDMCFG2,      0x13); //Modem Configuration
    halSpiWriteReg(CCxxx0_DEVIATN,      0x62);   //Modem Deviation Setting
    halSpiWriteReg(CCxxx0_MCSM0,        0x18);   //Main Radio Control State Machine Configuration
    halSpiWriteReg(CCxxx0_FOCCFG,       0x1D);  //Frequency Offset Compensation Configuration ???
    halSpiWriteReg(CCxxx0_BSCFG,        0x1C);  // bit synchronization configuration
    halSpiWriteReg(CCxxx0_WORCTRL,      0xFB); //Wake On Radio Control
    halSpiWriteReg(CCxxx0_AGCCTRL2,     0xC7); // agc control
    halSpiWriteReg(CCxxx0_AGCCTRL1,     0x00); // agc control
    halSpiWriteReg(CCxxx0_AGCCTRL0,     0xB0); // agc control
    halSpiWriteReg(CCxxx0_FREND1,       0xB6); // front end rx configuration
    halSpiWriteReg(CCxxx0_FSCAL3,       0xEA);  //Frequency Synthesizer Calibration
    halSpiWriteReg(CCxxx0_FSCAL2,       0x2A);  //Frequency Synthesizer Calibration
    halSpiWriteReg(CCxxx0_FSCAL1,       0x00);  //Frequency Synthesizer Calibration
    halSpiWriteReg(CCxxx0_FSCAL0,       0x1F);  //Frequency Synthesizer Calibration
    halSpiWriteReg(CCxxx0_TEST0,        0x09);   //Various Test Settings
    halSpiWriteReg(CCxxx0_PATABLE,  CC1101_PTABLE_10DB);//0xc0);   //Various Test Settings

    halSpiStrobe(CCxxx0_SIDLE);    //CCxxx0_SIDLE    0x36 //空闲状态
    halSpiStrobe(CCxxx0_SCAL);    //CCxxx0_SIDLE     0x36 //空闲状态
}

void phy_rf_config_normal()
{
    halRfWriteRfSettings();
}
#ifdef HAL_SPI_DMA
#ifdef HAL_SPI_DMA_RECV
static void dma_from_radio(uint32_t len)
{

    uint8_t i;
    DMA_Cmd(DMA1_Channel4, DISABLE);
    DMA_SetCurrDataCounter(DMA1_Channel4, len);
    DMA_Cmd(DMA1_Channel4, ENABLE);
    SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Rx, ENABLE);
    for(i = 0; i < len; i++)
    {
        hal_wait_ms_cond(MAX_RF_WAIT, !(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET) );
        /* Send byte through the SPI2 peripheral */
        SPI_I2S_SendData(SPI2, 0);
    }

    hal_wait_ms_cond(MAX_RF_WAIT, !(DMA_GetFlagStatus(DMA1_FLAG_TC4) == RESET));
    hal_wait_ms_cond(MAX_RF_WAIT, !(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET));
    DMA_ClearFlag(DMA1_IT_GL4);
    DMA_Cmd(DMA1_Channel4, DISABLE);

}
#endif
static void dma_to_radio(uint32_t len)
{

    DMA_Cmd(DMA1_Channel5, DISABLE);
    DMA_SetCurrDataCounter(DMA1_Channel5, len);
    DMA_Cmd(DMA1_Channel5, ENABLE); //Enable the DMA1 - Channel5
    // Enable the SPI2 RX & TX DMA requests
    SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Tx, ENABLE);
    //SysDataPrint(g_tx_buf, len);

    hal_wait_ms_cond(MAX_RF_WAIT, !(DMA_GetFlagStatus(DMA1_FLAG_TC5) == RESET));
    hal_wait_ms_cond(MAX_RF_WAIT, !(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET));

    DMA_ClearFlag(DMA1_IT_GL5);

    DMA_Cmd(DMA1_Channel5, DISABLE);

}

static void _radio_dma_init(void)
{
    DMA_InitTypeDef DMA_InitStructure;
    //--Enable DMA1 clock--
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

    //==Configure DMA1 - Channel5== (memory -> SPI)
    DMA_DeInit(DMA1_Channel5); //Set DMA registers to default values
    DMA_StructInit(&DMA_InitStructure);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&SPI2->DR; //Address of peripheral the DMA must map to
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)g_tx_buf; //Variable from which data will be transmitted
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = 1; //Buffer size
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel5, &DMA_InitStructure); //Initialise the DMA
    DMA_ClearFlag(DMA1_IT_GL5);
    DMA_ITConfig(DMA1_Channel5, DMA_IT_TC, DISABLE);
    DMA_ITConfig(DMA1_Channel5, DMA_IT_HT, DISABLE);
    DMA_ITConfig(DMA1_Channel5, DMA_IT_TE, DISABLE);
    //DMA_Cmd(DMA1_Channel5, ENABLE);
#ifdef HAL_SPI_DMA_RECV
    //==Configure DMA1 - Channel4== (SPI -> memory)
    DMA_DeInit(DMA1_Channel4); //Set DMA registers to default values
    DMA_StructInit(&DMA_InitStructure);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&SPI2->DR; //Address of peripheral the DMA must map to
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&g_rx_buf[1]; //Variable to which received data will be stored
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize = 1; //Buffer size
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel4, &DMA_InitStructure); //Initialise the DMA
    DMA_ClearFlag(DMA1_IT_GL4);
    DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, DISABLE);
    DMA_ITConfig(DMA1_Channel4, DMA_IT_HT, DISABLE);
    DMA_ITConfig(DMA1_Channel4, DMA_IT_TE, DISABLE);
    //DMA_Cmd(DMA1_Channel4, ENABLE); //Enable the DMA1 - Channel4
#endif
}
#endif
static void phy_delay(int ms)
{
    int i, j;
    for(i = ms; i > 0; i--)
        for(j = 0; j < 8000; j++)
        {
            __NOP();
        }
}

static void Delay(uint32_t nCount)
{
    int i, j;
    for(j = 0; j < nCount; j++)
    {
        for(i = 0; i < 10; i++)
        {
            __NOP();
        }
    }
}

static uint8_t _radio_set_mode(uint8_t mode)
{
    uint8_t res = 0, i;
    uint8_t MARCSTATE = 0;

    //OS_ENTER_CRITICAL(istate);
    MARCSTATE = halSpiReadStatus(CCxxx0_MARCSTATE);

    if(MARCSTATE != MARC_STATE_RX \
            || MARCSTATE != MARC_STATE_TX \
            || MARCSTATE != MARC_STATE_IDLE)
    {
        //SIDLE();
        halSpiStrobe(CCxxx0_SIDLE);
    }

    switch(mode)
    {
        case RADIOMODE_RX:
            while((halSpiReadStatus(CCxxx0_MARCSTATE)) != MARC_STATE_RX)
            {
                halSpiStrobe(CCxxx0_SIDLE);
                halSpiStrobe(CCxxx0_SRX);
                phy_delay(2);
                if(i >= 100)
                {
                    break;
                }
                i++;
            }
            res = 1;

            break;
        case RADIOMODE_TX:
            //STX();
            halSpiStrobe(CCxxx0_SIDLE);
            halSpiStrobe(CCxxx0_STX);
            if((halSpiReadStatus(CCxxx0_MARCSTATE)) == MARC_STATE_TX)
            {
                res = 1;
            }
            break;
        default:
            if((halSpiReadStatus(CCxxx0_MARCSTATE)) == MARC_STATE_IDLE)
            {
                break;
            }
            //SIDLE();
            //halSpiStrobe(CCxxx0_SIDLE);
    }
    //OS_EXIT_CRITICAL(istate);
    return res;
}

void phy_init()
{
    memset(&trans_longer, 0, sizeof(TRANS_INFO));
    SPI_CC1101_Init();
    CC1101_POWER_RESET();
    phy_rf_config_normal();
    GPIO_GDO0_ENABLE();
    halSpiStrobe(CCxxx0_SRX);
    hal_wait_ms(1);
    g_cc1101_id = SPI_CC1101_ReadID();	
	
    SysLog("CC1101 Version [%02x]\n", g_cc1101_id);
    //hal_rnd_feed_seed(halSpiReadStatus(CCxxx0_RSSI));
}

void phy_set_rx_state(uint8_t on_off)
{
    uint8_t ret = 0;
    if(on_off)
    {
        PHY_SET_RECV_MODE();
        ret = _radio_set_mode(RADIOMODE_RX);
//      if(!ret)
//          PRINT_DEBUG("@@@@@@@@@radio set receive mode failed !!!\r\t");
//      else
//          PRINT_DEBUG("@@@@@@@@@radio set receive mode OK\r\t");
    }
    else
    {
        ret = _radio_set_mode(RADIOMODE_IDLE);
    }
}

void phy_transmit_directly(uint8_t *buff, uint8_t len)
{
//  uint8_t  leng;
    PHY_SET_SEND_MODE();
    GPIO_GDO0_DISABLE(); //关闭中断
    memcpy(g_tx_buf, buff, len);
    halRfSendPacket(g_tx_buf, len);
    EXTI_ClearITPendingBit(GPIO_GDO0_EXTI_LINE);
    GPIO_GDO0_ENABLE(); //打开中断
    phy_set_rx_state(1);
}

uint8_t phy_transmit_upgrade(uint8_t len)
{
//  uint8_t  leng;
    INT8U sendByte;

    g_tx_buf[0] = len + 2; // + PHY_HEAD_LEN;
    g_tx_buf[1] = 55;
    g_tx_buf[2] = PHY_TYPE_UPGRADE;

    PHY_SET_SEND_MODE();
    trans_longer.isLonger  = false;
    sendByte = g_tx_buf[0] + 1;
    if(sendByte > TX_FIFO_SIZE)
    {
        trans_longer.isLonger  = true;
        trans_longer.pBuffer   = g_tx_buf + TX_FIFO_SIZE;
        trans_longer.bytesLeft = sendByte - TX_FIFO_SIZE;
        halSpiWriteReg(CCxxx0_FIFOTHR, 0x0C);
        halSpiWriteReg(CCxxx0_IOCFG0, 0x02);
        sendByte = TX_FIFO_SIZE;
    }
    halRfSendPacket(g_tx_buf, sendByte);
    phy_delay(10);
    phy_set_rx_state(1);

    return 1;
}

uint8_t phy_transmit(uint8_t type, uint8_t len)
{

    //uint8_t  leng;

    g_tx_buf[0] = len + 2; // + PHY_HEAD_LEN;
    g_tx_buf[1] = 55;
    g_tx_buf[2] = type;

    PHY_SET_SEND_MODE();
    GPIO_GDO0_DISABLE(); //关闭中断
    //halRfSendPacket(g_tx_buf, g_tx_buf[0]+2);
    halRfSendPacket(g_tx_buf, g_tx_buf[0] + 1);

    EXTI_ClearITPendingBit(GPIO_GDO0_EXTI_LINE);

    /*
    leng = 64+2;
    if (halRfReceivePacket(g_rx_buf,&leng)) // 读数据并判断正确与否 //如果接的字节数不为0
    {
        _rx_dma_cb();
    }
    */
    phy_set_rx_state(1);
    GPIO_GDO0_ENABLE(); //打开中断
    return 1;
}

void phy_set_channel(uint8_t channel)
{
    halSpiWriteReg(CCxxx0_CHANNR,   channel);
    _radio_set_mode(RADIOMODE_RX);
}

void phy_poll()
{

}

void *phy_get_trans_payload()
{
    return ((phy_header_t *)g_tx_buf)->payload;
}

uint8_t _tx_done()
{
    uint8_t ret = 0;
    ret = (halSpiReadStatus(CCxxx0_MARCSTATE)) == MARC_STATE_RX || (halSpiReadStatus(CCxxx0_MARCSTATE)) == MARC_STATE_IDLE;
    return ret;
}

void SPI_CC1101_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef   EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	HalSpiInit(HAL_SPI_2, 64);
	
	RCC_APB2PeriphClockCmd(RCC_GDO0, ENABLE);
    /* GD0 */
    GPIO_InitStructure.GPIO_Pin = GPIO_GDO0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIO_GDO0_PORT, &GPIO_InitStructure);	

    /* Enable SYSCFG clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    /* Connect EXTI8 Line to PB8 pin */
    GPIO_EXTILineConfig(GPIO_GDO0_PORT_SOURCE, GPIO_GDO0_SOURCE);

    /* Configure EXTI14 line */
    EXTI_InitStructure.EXTI_Line = GPIO_GDO0_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = DISABLE;
    EXTI_Init(&EXTI_InitStructure);

    /* Enable and set EXTI8 Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = GPIO_GDO0_EXTI_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0; //抢占优先级：1
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 01;        //子优先级：1
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/* 开启GD0中断 */
void GPIO_GDO0_ENABLE(void)
{
	EXTI_InitTypeDef   EXTI_InitStructure;
	GPIO_EXTILineConfig(GPIO_GDO0_PORT_SOURCE, GPIO_GDO0_SOURCE);

    /* Configure EXTI14 line */
    EXTI_InitStructure.EXTI_Line = GPIO_GDO0_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}

/* 关闭GD0中断 */
void GPIO_GDO0_DISABLE(void)
{
	EXTI_InitTypeDef   EXTI_InitStructure;
	GPIO_EXTILineConfig(GPIO_GDO0_PORT_SOURCE, GPIO_GDO0_SOURCE);

    /* Configure EXTI14 line */
    EXTI_InitStructure.EXTI_Line = GPIO_GDO0_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = DISABLE;
    EXTI_Init(&EXTI_InitStructure);
}

/*******************************************************************************
* Function Name  : SPI_RF_ReadByte
* Description    : Reads a byte from the SPI Flash.
*                  This function must be used only if the Start_Read_Sequence
*                  function has been previously called.
* Input          : None
* Output         : None
* Return         : Byte Read from the SPI Flash.
*******************************************************************************/
uint8_t SPI_RF_ReadByte(void)
{
    return (SPI_RF_SendByte(Dummy_Byte));
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SendByte
* Description    : Sends a byte through the SPI interface and return the byte
*                  received from the SPI bus.
* Input          : byte : byte to send.
* Output         : None
* Return         : The value of the received byte.
*******************************************************************************/
uint8_t SPI_RF_SendByte(uint8_t byte)
{
	return HalSpiReadWrite(HAL_SPI_2, byte);
}

/**********************************CC1101********************/


INT8U SPI_CC1101_ReadID(void)
{
    INT8U id;
    RF_SPI_CS_LOW();
    SPI_RF_SendByte(CCxxx0_SFSTXON);
    id = SPI_RF_SendByte(0xff);
    RF_SPI_CS_HIGH();

    return id;
}

INT8U CC1101_GET_ID(void)
{
    return g_cc1101_id;
}


void CC1101_POWER_RESET(void)
{
    RF_SPI_CS_HIGH();
    Delay(30);
    RF_SPI_CS_LOW();
    Delay(30);
    RF_SPI_CS_HIGH();
    Delay(45);
    RF_SPI_CS_LOW();
    hal_wait_ms_cond(MAX_RF_WAIT, !GPIO_ReadInputDataBit(RF_MISO_PORT, RF_MISO_PIN));
    SPI_RF_SendByte(CCxxx0_SRES);
    hal_wait_ms_cond(MAX_RF_WAIT, !GPIO_ReadInputDataBit(RF_MISO_PORT, RF_MISO_PIN));
    RF_SPI_CS_HIGH();
}


void halSpiWriteReg(INT8U addr, INT8U value)
{
    RF_SPI_CS_LOW();
    hal_wait_ms_cond(MAX_RF_WAIT, !GPIO_ReadInputDataBit(RF_MISO_PORT, RF_MISO_PIN));
    SPI_RF_SendByte(addr);       //写地址
    SPI_RF_SendByte(value);      //写入配置
    RF_SPI_CS_HIGH();
}

//*****************************************************************************************
//函数名：void halSpiWriteBurstReg(INT8U addr, INT8U *buffer, INT8U count)
//输入：地址，写入缓冲区，写入个数
//输出：无
//功能描述：SPI连续写配置寄存器
//*****************************************************************************************
void halSpiWriteBurstReg(INT8U addr, INT8U *buffer, INT8U count)
{
#ifndef HAL_SPI_DMA
    INT8U i;
#endif
    INT8U temp;
    temp = addr | WRITE_BURST;
    RF_SPI_CS_LOW();
    hal_wait_ms_cond(MAX_RF_WAIT, !GPIO_ReadInputDataBit(RF_MISO_PORT, RF_MISO_PIN));
    SPI_RF_SendByte(temp);
#ifndef HAL_SPI_DMA
    for (i = 0; i < count; i++)
    {
        SPI_RF_SendByte(buffer[i]);
    }
#else
    dma_to_radio(count);
#endif
    RF_SPI_CS_HIGH();
}

void phy_change_pa_power(PHY_TX_PA_POWER power)
{
    uint8_t pa_table[6] = {CC1101_PTABLE_N5DB, CC1101_PTABLE_0DB, CC1101_PTABLE_5DB,
                           CC1101_PTABLE_7DB,  CC1101_PTABLE_10DB, CC1101_PTABLE_7DB
                          };

    halSpiWriteReg(CCxxx0_PATABLE, pa_table[power]);
}

void phy_rx_attenuation(PHY_RX_ATTENUATION attenuation)
{
    uint8_t val = halSpiReadReg(CCxxx0_RXFIFO);
    val &= 0xcf;
    val = val | (attenuation << 4);
    halSpiWriteReg(CCxxx0_RXFIFO, val);
}

//*****************************************************************************************
//函数名：void halSpiStrobe(INT8U strobe)
//输入：命令
//输出：无
//功能描述：SPI写命令
//*****************************************************************************************
void halSpiStrobe(INT8U strobe)
{
    RF_SPI_CS_LOW();
    hal_wait_ms_cond(MAX_RF_WAIT, !GPIO_ReadInputDataBit(RF_MISO_PORT, RF_MISO_PIN));
    SPI_RF_SendByte(strobe);     //写入命令
    RF_SPI_CS_HIGH();
}

//*****************************************************************************************
//函数名：void halRfSendPacket(INT8U *txBuffer, INT8U size)
//输入：发送的缓冲区，发送数据个数
//输出：无
//功能描述：CC1100发送一组数据
//*****************************************************************************************

void halRfSendPacket(INT8U *txBuffer, INT8U size)
{
    halSpiStrobe(CCxxx0_SIDLE); //进入IDLE模式 +cbl 01230

    while(halSpiReadStatus(CCxxx0_MARCSTATE) != MARC_STATE_IDLE);
    halSpiStrobe(CCxxx0_SFRX);  // flush RX buffer  +cbl 01230
    halSpiStrobe(CCxxx0_SFTX);  // flush TX buffer

    halSpiWriteBurstReg(CCxxx0_TXFIFO, txBuffer, size); //写入要发送的数据
    halSpiStrobe(CCxxx0_SFRX);  // flush RX buffer  +cbl 01230
    halSpiStrobe(CCxxx0_STX);       //进入发送模式发送数据

    hal_wait_ms_cond(MAX_RF_WAIT, (GPIO_ReadInputDataBit(GPIO_GDO0_PORT, GPIO_GDO0)));
    hal_wait_ms_cond(MAX_RF_WAIT, (!GPIO_ReadInputDataBit(GPIO_GDO0_PORT, GPIO_GDO0) ));
}

//*****************************************************************************************
//函数名：void halRfSendData(INT8U txData)
//输入：发送的数据
//输出：无
//功能描述：CC1100发送一个数据
//*****************************************************************************************

void halRfSendData(INT8U txData)
{

    halSpiStrobe(CCxxx0_SFTX);
    halSpiWriteReg(CCxxx0_TXFIFO, txData);  //写入要发送的数据

    halSpiStrobe(CCxxx0_STX);       //进入发送模式发送数据

    // Wait for GDO0 to be set -> sync transmitted
    while (!GPIO_ReadInputDataBit(GPIO_GDO0_PORT, GPIO_GDO0) ); //while (!GDO0);
    
    // Wait for GDO0 to be cleared -> end of packet
    while (GPIO_ReadInputDataBit(GPIO_GDO0_PORT, GPIO_GDO0) ); // while (GDO0);
}

//*****************************************************************************************
//函数名：INT8U halSpiReadReg(INT8U addr)
//输入：地址
//输出：该寄存器的配置字
//功能描述：SPI读寄存器
//*****************************************************************************************
INT8U halSpiReadReg(INT8U addr)
{
    INT8U temp, value;
    temp = addr | READ_SINGLE; //读寄存器命令
    RF_SPI_CS_LOW();
//  while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );//MISO
    hal_wait_ms_cond(MAX_RF_WAIT, !(GPIO_ReadInputDataBit(RF_MISO_PORT, RF_MISO_PIN)));
    SPI_RF_SendByte(temp);
    value = SPI_RF_SendByte(0);
    RF_SPI_CS_HIGH();
    return value;
}

//*****************************************************************************************
//函数名：void halSpiReadBurstReg(INT8U addr, INT8U *buffer, INT8U count)
//输入：地址，读出数据后暂存的缓冲区，读出配置个数
//输出：无
//功能描述：SPI连续写配置寄存器
//*****************************************************************************************
void halSpiReadBurstReg(INT8U addr, INT8U *buffer, INT8U count)
{
    INT8U i, temp;
    temp = addr | READ_BURST;       //写入要读的配置寄存器地址和读命令
    RF_SPI_CS_LOW();
    hal_wait_ms_cond(MAX_RF_WAIT, (!GPIO_ReadInputDataBit(RF_MISO_PORT, RF_MISO_PIN)));
    SPI_RF_SendByte(temp);
//#if 1
#ifndef HAL_SPI_DMA_RECV
    for (i = 0; i < count; i++)
    {
        buffer[i] = SPI_RF_SendByte(0);
    }
#else
    dma_from_radio(count);
#endif

    RF_SPI_CS_HIGH();
}

//*****************************************************************************************
//函数名：INT8U halSpiReadReg(INT8U addr)
//输入：地址
//输出：该状态寄存器当前值
//功能描述：SPI读状态寄存器
//*****************************************************************************************
INT8U halSpiReadStatus(INT8U addr)
{
    INT8U value, temp;
    temp = addr | READ_BURST;       //写入要读的状态寄存器的地址同时写入读命令
    RF_SPI_CS_LOW();
    hal_wait_ms_cond(MAX_RF_WAIT, !(GPIO_ReadInputDataBit(RF_MISO_PORT, RF_MISO_PIN)));
    SPI_RF_SendByte(temp);
    value = SPI_RF_SendByte(0);
    RF_SPI_CS_HIGH();
    return value;
}

INT8U halRfReceivePacket(INT8U *rxBuffer, INT8U *length)
{
    uint8_t rxBytes1 = halSpiReadStatus(CCxxx0_RXBYTES);
    uint8_t rxBytes2 = halSpiReadStatus(CCxxx0_RXBYTES);

    if(rxBytes1 != rxBytes2)
    {
        rxBytes1 = rxBytes2;
        //printf("phy receive rxBytes1 = rxBytes2\n");
    }
    if ((rxBytes1 & 0x7F) && !(rxBytes1 & 0x80))
    {
        rxBuffer[0] = halSpiReadReg(CCxxx0_RXFIFO);
        if(rxBuffer[0] > CC1101_DATA_LEN)
        {
            //SysLog("out of data");
            rxBuffer[0] = 0;
        }
        else
        {
            halSpiReadBurstReg(CCxxx0_RXFIFO, rxBuffer + 1, rxBuffer[0] + 2); //read data+2 status(rssi+crc)
        }
    }
    else
    {
        rxBuffer[0] = 0;
    }
    halSpiStrobe(CCxxx0_SFRX); // flush Rx FIFO
    return rxBuffer[0];
}


void halRfWriteRfSettings(void)
{
    halSpiWriteReg(CCxxx0_IOCFG0,       0x06);   //GDO0 Output Pin Configuration
    halSpiWriteReg(CCxxx0_PKTLEN,       0x3D);   //Packet Length: 61 data + 2 status +1 length=64, never overflow,make sure rx fifo empty before receive data
    halSpiWriteReg(CCxxx0_PKTCTRL0,     0x05);   //Packet Automation Control
    halSpiWriteReg(CCxxx0_ADDR,         0x00);   //Device Address
    halSpiWriteReg(CCxxx0_FSCTRL1,      0x0C);   //Frequency Synthesizer Control ,IF=152.34 kHz
    halSpiWriteReg(CCxxx0_FSCTRL0,      0x00);
    halSpiWriteReg(CCxxx0_FREQ2,        0x10);   //Frequency Control Word, High Byte
    halSpiWriteReg(CCxxx0_FREQ1,        0xB1);   //Frequency Control Word, Middle Byte
    halSpiWriteReg(CCxxx0_FREQ0,        0x3B);   //Frequency Control Word, Low Byte
    halSpiWriteReg(CCxxx0_MDMCFG4,      0x2A);   //Modem Configuration
    halSpiWriteReg(CCxxx0_MDMCFG3,      0x83);   //Modem Configuration
    halSpiWriteReg(CCxxx0_MDMCFG2,      0x13);   //Modem Configuration
    halSpiWriteReg(CCxxx0_MDMCFG1,      0x22);//+
    halSpiWriteReg(CCxxx0_MDMCFG0,      0xf8);//+

    halSpiWriteReg(CCxxx0_DEVIATN,      0x62);   //Modem Deviation Setting
    halSpiWriteReg(CCxxx0_MCSM0,        0x18);   //Main Radio Control State Machine Configuration
    halSpiWriteReg(CCxxx0_FOCCFG,       0x1D);//0x16);   //Frequency Offset Compensation Configuration//
    halSpiWriteReg(CCxxx0_BSCFG,        0x1C);//+
    halSpiWriteReg(CCxxx0_WORCTRL,      0xFB);   //Wake On Radio Control

    halSpiWriteReg(CCxxx0_AGCCTRL2,     0xC7);//+AGC Control
    halSpiWriteReg(CCxxx0_AGCCTRL1,     0x00);//+AGC Control
    halSpiWriteReg(CCxxx0_AGCCTRL0,     0xB0);//+AGC Control

    halSpiWriteReg(CCxxx0_FREND1,       0xB6); //+ Front End RX Configuration
    halSpiWriteReg(CCxxx0_FSCAL3,       0xE9);   //Frequency Synthesizer Calibration//??
    halSpiWriteReg(CCxxx0_FSCAL2,       0x2A);   //Frequency Synthesizer Calibration
    halSpiWriteReg(CCxxx0_FSCAL1,       0x00);   //Frequency Synthesizer Calibration
    halSpiWriteReg(CCxxx0_FSCAL0,       0x1F);   //Frequency Synthesizer Calibration
    halSpiWriteReg(CCxxx0_TEST0,        0x09);   //Various Test Settings

    halSpiWriteReg(CCxxx0_FREND0, 0x10);
    phy_change_pa_power(PHY_POWER_MAX);

    phy_rx_attenuation(PHY_RX_ATTENUATION_0DB);
    halSpiStrobe(CCxxx0_SIDLE);    //CCxxx0_SIDLE    0x36 //空闲状态
    halSpiStrobe(CCxxx0_SCAL);    //CCxxx0_SIDLE     0x36 //空闲状态

	
	//printf("CCxxx0_PKTLEN = %02x\n",halSpiReadReg(CCxxx0_PKTLEN));
}

void EXTI9_5_IRQHandler(void)
{
    uint8_t  leng = 64 + 2;

    if(EXTI_GetITStatus(GPIO_GDO0_EXTI_LINE) != RESET)
    {
		EXTI_ClearITPendingBit(GPIO_GDO0_EXTI_LINE);
		//printf("IRQ\n");
        if(trans_longer.isLonger == true)
        {
            if(trans_longer.pBuffer)
            {
                halSpiWriteBurstReg(CCxxx0_TXFIFO, trans_longer.pBuffer, trans_longer.bytesLeft);
            }

            trans_longer.isLonger = false;
            trans_longer.pBuffer  = NULL;
            halSpiWriteReg(CCxxx0_IOCFG0, 0x06);
        }
        else
        {
            if (halRfReceivePacket(g_rx_buf, &leng)) // 读数据并判断正确与否
            {
                halSpiStrobe(CCxxx0_SIDLE);    //CCxxx0_SIDLE    0x36 //空闲状态
                _rx_dma_cb();
                halSpiStrobe(CCxxx0_SRX);
                halSpiStrobe(CCxxx0_SWORRST);
            }
            else
            {
                halSpiStrobe(CCxxx0_SIDLE);
                halSpiStrobe(CCxxx0_SRX);
                halSpiStrobe(CCxxx0_SWORRST);
            }

        }
       
    }

}

