#include "os.h"
#include "../net/net.h"
#include "stm32f0xx.h"
#include "hal_uart.h"

void os_own_init()
{
  os_timer_init();
}

void os_init()
{
	hal_uart_init();
	
  os_own_init();
	
  net_init();
	
  hal_init();
	
  app_init();
}

int main()
{
		
  os_init();

  while(1)
  {
    hal_poll();
		
    net_poll();
		
    os_timer_poll();
		
    app_poll();
  }
}





