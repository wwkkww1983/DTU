#ifndef OS_TIMER_H
#define OS_TIMER_H
#include "os.h"
#include "../libiar/mlist.h"

#define OS_TIMER_FLAG_IMME     1
#define OS_TIMER_FLAG_REPEAT   2

typedef mlist_list_size_t os_timer_t;
typedef void (*os_timer_func_t)(void *);
typedef uint32 os_time_t;

typedef mlist_list_size_t os_timer_handle;
void os_timer_init(void);
void os_timer_poll(void);
os_timer_t os_timer_set(os_timer_func_t func, os_time_t interval, void *param, uint8 flags);
uint8 os_timer_is_valid(os_timer_t timer);
void os_timer_reset(os_timer_t timer);
void os_timer_del(os_timer_t timer);
os_time_t os_timer_get_interval(os_timer_t timer);

void os_timer_pass_ms(void);
os_time_t os_timer_get_systime(void);
os_time_t os_timer_get_nettime(void);
void os_timer_set_nettime(os_time_t time);
void os_timer_add_systime(os_time_t time);

//void os_timer_set_time(void);

#endif // OS_TIMER_H
