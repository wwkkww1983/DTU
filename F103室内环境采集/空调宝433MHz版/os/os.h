#ifndef OS_H
#define OS_H

//#include "./spi_cc1101.h"

#include "stm32f0xx.h"
#ifdef USE_STM320518_EVAL
 #include "stm320518_eval.h"
 #include "stm320518_eval_lcd.h"
#endif /* USE_STM320518_EVAL */

#ifdef USE_STM32072B_EVAL
 #include "stm32072b_eval.h"
 #include "stm32072b_eval_lcd.h"
#endif /* USE_STM32072B_EVAL */


#include "../hal/hal.h"
#include <stdio.h>
#include <string.h>
#include "./os_timer.h"
#include "../app/app.h" //        12/19

#define SUB_MODULE
//#define DEBUG

#define LED_PORT GPIOA
#define LED_PIN  GPIO_Pin_10

#define RELAY_CTRL_PORT GPIOA
#define RELAY_CTRL_PIN GPIO_Pin_9
#define OS_ENTER_CRITICAL(i_state)  HAL_ENTER_CRITICAL(i_state)
#define OS_EXIT_CRITICAL(i_state)   HAL_EXIT_CRITICAL(i_state)

#define OS_RADIO_DATA_MAXLEN      64
#define OS_UART_DATA_MAXLEN       64
#define OS_MAX_DEVICE_COUNT       64

#define UINT32_IS_LESS(n1, n2) ((n1 - n2) & ((uint32)1 << 31))
#define OS_SYSTIME_BEYOND(time) UINT32_IS_LESS(time, os_timer_get_systime())
#define OS_SYSTIME_BEYOND_SPAN(time, span) (os_timer_get_systime() - time > span)

//#define os_printf(...)
#define os_printf(...) printf(__VA_ARGS__)
#define os_log(...) os_printf("%s[%d]:", __FUNCTION__, __LINE__);os_printf(__VA_ARGS__);os_printf("\r\n")

#endif /* OS_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

