#include "os.h"
#include "./spi_cc11.h"
#include "../net/phy.h"
//#include "../user/config.h"    			 // 头文件

//#ifndef  paTableLen
u8 PaTabel[] = {0x68,0x6c,0x1c,0x06,0x3a,0x51,0x85,0xc8,0xc0};
u8 paTableLen = 7; //对应功率值，默认4则为0dbm发送


u8 txBuffer1[8]={0x07,0xff,0x02,0x04,0x05,0xff,0xff,0Xff};
//u8 *rxBuffer1;
u8 rxBuffer1[64];


u8 receive_fig = 0;
//#endif
#define deta_size  7     ////数据包长度
#define		INT8U		unsigned char
/* Private define ------------------------------------------------------------*/
#define WRITE      0x82  /* Write to Memory instruction */
#define READ       0xD3  /* Read from Memory instruction */
#define RDSR       0xD7  /* Read Status Register instruction  */
#define RDID       0x9F  /* Read identification */
#define PE         0x81  /* Page Erase instruction */
#define BE1        0xC7  /* Bulk Erase instruction */
#define BE2        0x94  /* Bulk Erase instruction */
#define BE3        0x80  /* Bulk Erase instruction */
#define BE4        0x9A  /* Bulk Erase instruction */

#define BUSY_Flag  0x01 /* Ready/busy status flag */

#define Dummy_Byte 0xff

/*******************************************************************************/
#define 	WRITE_BURST     	0x40						//连续写入
#define 	READ_SINGLE     	0x80						//读
#define 	READ_BURST      	0xC0						//连续读
#define 	BYTES_IN_RXFIFO     0x7F  						//接收缓冲区的有效字节数
#define 	CRC_OK              0x80 						//CRC校验通过位标志
// CC1100 STROBE, CONTROL AND STATUS REGSITER


/*************************************************************************/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
* Function Name  : SPI_FLASH_Init
* Description    : Initializes the peripherals used by the SPI FLASH driver.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/

void SPI_CC1101_Init2(void)
{
	SPI_InitTypeDef  SPI_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	// Periph clock enable //
	RCC_AHBPeriphClockCmd(CC1101_SPI_CS_GPIO_CLK | CC1101_SPI_MOSI_GPIO_CLK |
	CC1101_SPI_EN_GPIO_CLK | CC1101_SPI_MISO_GPIO_CLK | CC1101_SPI_SCK_GPIO_CLK, ENABLE);

	// CC1101_SPI Periph clock enable //
	RCC_APB2PeriphClockCmd(CC1101_SPI_CLK, ENABLE);

	// Configure CC1101_SPI pins: SCK //
	GPIO_InitStructure.GPIO_Pin = CC1101_SPI_SCK_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(CC1101_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

	// Configure CC1101_SPI pins: MISO //
	GPIO_InitStructure.GPIO_Pin = CC1101_SPI_MISO_PIN;
	GPIO_Init(CC1101_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

	// Configure CC1101_SPI pins: MOSI //
	GPIO_InitStructure.GPIO_Pin = CC1101_SPI_MOSI_PIN;
	GPIO_Init(CC1101_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

	// Configure CC1101_SPI_CS pin: CC1101 CS pin //
	GPIO_InitStructure.GPIO_Pin = CC1101_SPI_CS_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(CC1101_SPI_CS_GPIO_PORT, &GPIO_InitStructure);
	CC1101_SPI_CS_HIGH2();

	// Configure CC1101_SPI_EN pin: CC1101 EN pin //
	GPIO_InitStructure.GPIO_Pin = CC1101_EN_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(CC1101_EN_GPIO_PORT, &GPIO_InitStructure);
	CC1101_ENABLE2();
	
	GPIO_InitStructure.GPIO_Pin =RF_IQR_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_2;
  GPIO_Init(RF_IQR_PORT , &GPIO_InitStructure);

	// Connect PXx  //
	GPIO_PinAFConfig(CC1101_SPI_SCK_GPIO_PORT, CC1101_SPI_SCK_SOURCE, CC1101_SPI_SCK_AF);
	GPIO_PinAFConfig(CC1101_SPI_MOSI_GPIO_PORT, CC1101_SPI_MOSI_SOURCE, CC1101_SPI_MOSI_AF);
	GPIO_PinAFConfig(CC1101_SPI_MISO_GPIO_PORT, CC1101_SPI_MISO_SOURCE, CC1101_SPI_MISO_AF);

	// SPI configuration -------------------------------------------------------//
	SPI_I2S_DeInit(CC1101_SPI);
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;	
	SPI_Init(CC1101_SPI, &SPI_InitStructure);	
	
	SPI_RxFIFOThresholdConfig(CC1101_SPI, SPI_RxFIFOThreshold_QF);
	// Enable SPI  //
	
//	dma_to_radio();
	
	SPI_Cmd(CC1101_SPI, ENABLE);	
}


void GPIO_GDO0_ENABLE2(void)
{
	  EXTI_InitTypeDef EXTI_InitStructure;
   		//中断初始化
    /* Connect GDO0 EXTI Line to Button GPIO Pin */
		SYSCFG_EXTILineConfig(RF_IQR_PORT_SOURCE, RF_IQR_SOURCE);
    /* Configure GDO0 EXTI line */	
		
		EXTI_InitStructure.EXTI_Line = RF_IQR_EXTI;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising; //上升沿中断
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
		EXTI_Init(&EXTI_InitStructure); 
}

void GPIO_GDO0_DISABLE2(void)
{
	  EXTI_InitTypeDef EXTI_InitStructure;
   		//中断初始化
    /* Connect GDO0 EXTI Line to Button GPIO Pin */
//		SYSCFG_EXTILineConfig(RF_IQR_PORT_SOURCE, RF_IQR_SOURCE);
    /* Configure GDO0 EXTI line */			
		EXTI_InitStructure.EXTI_Line = RF_IQR_EXTI;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising; //上升沿中断
		EXTI_InitStructure.EXTI_LineCmd = DISABLE;
		EXTI_Init(&EXTI_InitStructure); 
}

/*******************************************************************************
* Function Name  : SPI_FLASH_ReadByte
* Description    : Reads a byte from the SPI Flash.
*                  This function must be used only if the Start_Read_Sequence
*                  function has been previously called.
* Input          : None
* Output         : None
* Return         : Byte Read from the SPI Flash.
*******************************************************************************/
u8 SPI_FLASH_ReadByte2(void)
{
  return (SPI_FLASH_SendByte2(Dummy_Byte));
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SendByte
* Description    : Sends a byte through the SPI interface and return the byte
*                  received from the SPI bus.
* Input          : byte : byte to send.
* Output         : None
* Return         : The value of the received byte.
*******************************************************************************/
u8 SPI_FLASH_SendByte2(u8 byte)
{
  /* Loop while DR register in not emplty */
  while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);

  /* Send byte through the SPI2 peripheral */
  SPI_SendData8(SPI1, byte);

  /* Wait to receive a byte */
  while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);

  /* Return the byte read from the SPI bus */
  return SPI_ReceiveData8(SPI1);
}

/**********************************CC1101********************/

void Delay2(vu32 nCount)
{
  int i,j;
  for(j=0;j<nCount;j++)
  {
     for(i=0;i<10;i++);
  }
}


INT8U SPI_CC1101_ReadID2(void)
{
	 INT8U id;
	 SPI_FLASH_CS_LOW2();
	 	 
	 SPI_FLASH_SendByte2(CCxxx0_SFSTXON);
	 id = SPI_FLASH_SendByte2(0xff);
	 SPI_FLASH_CS_HIGH2();

	 return id;
}

void CC1101_POWER_RESET2(void)
{
  SPI_FLASH_CS_HIGH2();
  Delay2(30);
  SPI_FLASH_CS_LOW2();
  Delay2(30);
  SPI_FLASH_CS_HIGH2();
  Delay2(45);
  SPI_FLASH_CS_LOW2();
  while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );//waite SO =0
  SPI_FLASH_SendByte2(CCxxx0_SRES);
  while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );//waite SO =0 again 
  SPI_FLASH_CS_HIGH2(); 
}

//*****************************************************************************************
//函数名：void halSpiWriteReg(INT8U addr, INT8U value)
//输入：地址和配置字
//输出：无
//功能描述：SPI写寄存器
//*****************************************************************************************
void halSpiWriteReg2(INT8U addr, INT8U value) 
{
    SPI_FLASH_CS_LOW2();
    while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );
    SPI_FLASH_SendByte2(addr);		//写地址
    SPI_FLASH_SendByte2(value);		//写入配置
    SPI_FLASH_CS_HIGH2(); 
}

//*****************************************************************************************
//函数名：void halSpiWriteBurstReg(INT8U addr, INT8U *buffer, INT8U count)
//输入：地址，写入缓冲区，写入个数
//输出：无
//功能描述：SPI连续写配置寄存器
//*****************************************************************************************
void halSpiWriteBurstReg2(INT8U addr, INT8U *buffer, INT8U count) 
{
    INT8U i,temp;
		temp = addr | WRITE_BURST;
    SPI_FLASH_CS_LOW2();
    while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );
    SPI_FLASH_SendByte2(temp);
    for (i = 0; i < count; i++)
		{
        SPI_FLASH_SendByte2(buffer[i]);
    }
		//dma_to_radio();
		
    SPI_FLASH_CS_HIGH2(); 
}

//*****************************************************************************************
//函数名：void halSpiStrobe(INT8U strobe)
//输入：命令
//输出：无
//功能描述：SPI写命令
//*****************************************************************************************
void halSpiStrobe2(INT8U strobe) 
{
    SPI_FLASH_CS_LOW2();
    while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );
    SPI_FLASH_SendByte2(strobe);		//写入命令
    SPI_FLASH_CS_HIGH2();
}

//*****************************************************************************************
//函数名：void halRfSendPacket(INT8U *txBuffer, INT8U size)
//输入：发送的缓冲区，发送数据个数
//输出：无
//功能描述：CC1100发送一组数据
//*****************************************************************************************

void halRfSendPacket2(INT8U *txBuffer, INT8U size) 
{
	halSpiStrobe2(CCxxx0_SFTX);

  halSpiWriteBurstReg2(CCxxx0_TXFIFO, txBuffer, size);	//写入要发送的数据

  halSpiStrobe2(CCxxx0_STX);		//进入发送模式发送数据	
    // Wait for GDO0 to be set -> sync transmitted
  while (!GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) );//while (!GDO0);
    // Wait for GDO0 to be cleared -> end of packet
  while (GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) );// while (GDO0);
}

//*****************************************************************************************
//函数名：void halRfSendData(INT8U txData)
//输入：发送的数据
//输出：无
//功能描述：CC1100发送一个数据
//*****************************************************************************************

void halRfSendData2(INT8U txData)
{	
	
		halSpiStrobe2(CCxxx0_SFTX);
    halSpiWriteReg2(CCxxx0_TXFIFO, txData);	//写入要发送的数据

    halSpiStrobe2(CCxxx0_STX);		//进入发送模式发送数据	

    // Wait for GDO0 to be set -> sync transmitted
    while (!GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) );//while (!GDO0);
    // Wait for GDO0 to be cleared -> end of packet
    while (GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) );// while (GDO0);
//	halSpiStrobe(CCxxx0_SFTX);
}

//*****************************************************************************************
//函数名：INT8U halSpiReadReg(INT8U addr)
//输入：地址
//输出：该寄存器的配置字
//功能描述：SPI读寄存器
//*****************************************************************************************
INT8U halSpiReadReg2(INT8U addr) 
{
	INT8U temp, value;
    temp = addr|READ_SINGLE;//读寄存器命令
	SPI_FLASH_CS_LOW2();
	while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );//MISO
	SPI_FLASH_SendByte2(temp);
	value = SPI_FLASH_SendByte2(0);
	 SPI_FLASH_CS_HIGH2();
	return value;
}

//*****************************************************************************************
//函数名：void halSpiReadBurstReg(INT8U addr, INT8U *buffer, INT8U count)
//输入：地址，读出数据后暂存的缓冲区，读出配置个数
//输出：无
//功能描述：SPI连续写配置寄存器
//*****************************************************************************************
void halSpiReadBurstReg2(INT8U addr, INT8U *buffer, INT8U count) 
{
    INT8U i,temp;
	temp = addr | READ_BURST;		//写入要读的配置寄存器地址和读命令
    SPI_FLASH_CS_LOW2();
     while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN));
	SPI_FLASH_SendByte2(temp);   
    for (i = 0; i < count; i++) 
	{
        buffer[i] = SPI_FLASH_SendByte2(0);
    }
    SPI_FLASH_CS_HIGH2();
}

//*****************************************************************************************
//函数名：INT8U halSpiReadReg(INT8U addr)
//输入：地址
//输出：该状态寄存器当前值
//功能描述：SPI读状态寄存器
//*****************************************************************************************
INT8U halSpiReadStatus2(INT8U addr) 
{
    INT8U value,temp;
		temp = addr | READ_BURST;		//写入要读的状态寄存器的地址同时写入读命令
    SPI_FLASH_CS_LOW2();
    while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );
    SPI_FLASH_SendByte2(temp);
	value = SPI_FLASH_SendByte2(0);
	SPI_FLASH_CS_HIGH2();
	return value;
}

INT8U halRfReceivePacket2(INT8U *rxBuffer, INT8U *length) 
{
    INT8U status[2];
    INT8U packetLength;
	 
    if ((halSpiReadStatus2(CCxxx0_RXBYTES) & BYTES_IN_RXFIFO)) //如果接的字节数不为0
		{
	    packetLength = halSpiReadReg2(CCxxx0_RXFIFO);//读出第一个字节，此字节为该帧数据长度
			rxBuffer[0] = packetLength;//读出第一个字节，此字节为该帧数据长度
        if(packetLength <= *length) 		//如果所要的有效数据长度小于等于接收到的数据包的长度
				{
            halSpiReadBurstReg2(CCxxx0_RXFIFO, rxBuffer+1, packetLength); //读出所有接收到的数据
            *length = packetLength;				//把接收数据长度的修改为当前数据的长度
        
            // Read the 2 appended status bytes (status[0] = RSSI, status[1] = LQI)
            halSpiReadBurstReg2(CCxxx0_RXFIFO, status, 2); 	//读出CRC校验位
						halSpiStrobe2(CCxxx0_SFRX);		//清洗接收缓冲区
            return (status[1] & CRC_OK);			//如果校验成功返回接收成功
        }
		 else 
			{
            *length = packetLength;
            halSpiStrobe2(CCxxx0_SFRX);		//清洗接收缓冲区
            return 0;
			}
    } 
	else
 	return 0;
}

//*****************************************************************************************
//函数名：void halRfWriteRfSettings(RF_SETTINGS *pRfSettings)
//输入：无
//输出：无
//功能描述：配置CC1100的寄存器
//*****************************************************************************************
void halRfWriteRfSettings2(void) 
{
// Product = CC1100
// Crystal accuracy = 40 ppm
// X-tal frequency = 26 MHz
// RF output power = 0 dBm
// RX filterbandwidth = 540.000000 kHz
// Deviation = 0.000000
// Return state:  Return to RX state upon leaving either TX or RX
// Datarate = 250.000000 kbps
// Modulation = (7) MSK
// Manchester enable = (0) Manchester disabled
// RF Frequency = 433.000000 MHz
// Channel spacing = 199.951172 kHz
// Channel number = 0
// Optimization = Sensitivity
// Sync mode = (3) 30/32 sync word bits detected
// Format of RX/TX data = (0) Normal mode, use FIFOs for RX and TX
// CRC operation = (1) CRC calculation in TX and CRC check in RX enabled
// Forward Error Correction = (0) FEC disabled
// Length configuration = (1) Variable length packets, packet length configured by the first received byte after sync word.
// Packetlength = 255
// Preamble count = (2)  4 bytes
// Append status = 1
// Address check = (0) No address check
// FIFO autoflush = 0
// Device address = 0
// GDO0 signal selection = ( 6) Asserts when sync word has been sent / received, and de-asserts at the end of the packet
// GDO2 signal selection = (11) Serial Clock
//  //10000hz
////    halSpiWriteReg(CCxxx0_IOCFG2,   0x06); // GDO2 output pin config.
//    halSpiWriteReg(CCxxx0_IOCFG0,   0x06); // GDO0 output pin config.
//    halSpiWriteReg(CCxxx0_PKTLEN,   0x08); // Packet length.
//    halSpiWriteReg(CCxxx0_PKTCTRL1, 0x04); // Packet automation control.
//    halSpiWriteReg(CCxxx0_PKTCTRL0, 0x05); // Packet automation control.
//    halSpiWriteReg(CCxxx0_ADDR,     0x00); // Device address.
//    halSpiWriteReg(CCxxx0_CHANNR,   0x00); // Channel number.
//    halSpiWriteReg(CCxxx0_FSCTRL1,  0x06); // Freq synthesizer control.
//    halSpiWriteReg(CCxxx0_FSCTRL0,  0x00); // Freq synthesizer control.
//    halSpiWriteReg(CCxxx0_FREQ2,    0x10); // Freq control word, high byte
//    halSpiWriteReg(CCxxx0_FREQ1,    0xA7); // Freq control word, mid byte.
//    halSpiWriteReg(CCxxx0_FREQ0,    0x62); // Freq control word, low byte.
//    halSpiWriteReg(CCxxx0_MDMCFG4,  0x68); // Modem configuration.
//    halSpiWriteReg(CCxxx0_MDMCFG3,  0x93); // Modem configuration.
//    halSpiWriteReg(CCxxx0_MDMCFG2,  0x03); // Modem configuration.
//    halSpiWriteReg(CCxxx0_MDMCFG1,  0x22); // Modem configuration.
//    halSpiWriteReg(CCxxx0_MDMCFG0,  0xF8); // Modem configuration.
//    halSpiWriteReg(CCxxx0_DEVIATN,  0x34); // Modem dev (when FSK mod en)
//    halSpiWriteReg(CCxxx0_MCSM1 ,   0x30); // MainRadio Cntrl State Machine
//    halSpiWriteReg(CCxxx0_MCSM0 ,   0x18); // MainRadio Cntrl State Machine
//    halSpiWriteReg(CCxxx0_FOCCFG,   0x16); // Freq Offset Compens. Config
//    halSpiWriteReg(CCxxx0_BSCFG,    0x6C); //  Bit synchronization config.
//    halSpiWriteReg(CCxxx0_AGCCTRL2, 0x43); // AGC control.
//    halSpiWriteReg(CCxxx0_AGCCTRL1, 0x40); // AGC control.
//    halSpiWriteReg(CCxxx0_AGCCTRL0, 0x91); // AGC control.
//    halSpiWriteReg(CCxxx0_FREND1,   0x56); // Front end RX configuration.
//    halSpiWriteReg(CCxxx0_FREND0,   0x10); // Front end RX configuration.
//    halSpiWriteReg(CCxxx0_FSCAL3,   0xE9); // Frequency synthesizer cal.
//    halSpiWriteReg(CCxxx0_FSCAL2,   0x2A); // Frequency synthesizer cal.
//    halSpiWriteReg(CCxxx0_FSCAL1,   0x00); // Frequency synthesizer cal.
//    halSpiWriteReg(CCxxx0_FSCAL0,   0x1F); // Frequency synthesizer cal.
//    halSpiWriteReg(CCxxx0_FSTEST,   0x59); // Frequency synthesizer cal.
//    halSpiWriteReg(CCxxx0_TEST2,    0x81); // Various test settings.
//    halSpiWriteReg(CCxxx0_TEST1,    0x35); // Various test settings.
//    halSpiWriteReg(CCxxx0_TEST0,    0x09); // Various test settings.


			halSpiWriteReg2(CCxxx0_IOCFG0,		0x06);  //GDO0 Output Pin Configuration
			halSpiWriteReg2(CCxxx0_PKTLEN,		0xFF);  //Packet Length
			halSpiWriteReg2(CCxxx0_PKTCTRL0,	0x05);//Packet Automation Control
			halSpiWriteReg2(CCxxx0_ADDR,			0x00);    //Device Address
			halSpiWriteReg2(CCxxx0_CHANNR,		0x00);  //Channel Number
			halSpiWriteReg2(CCxxx0_FSCTRL1,	0x06); //Frequency Synthesizer Control
			halSpiWriteReg2(CCxxx0_FREQ2,		0x10);   //Frequency Control Word, High Byte
			halSpiWriteReg2(CCxxx0_FREQ1,		0xB1);   //Frequency Control Word, Middle Byte
			halSpiWriteReg2(CCxxx0_FREQ0,		0x3B);   //Frequency Control Word, Low Byte
			halSpiWriteReg2(CCxxx0_MDMCFG4,	0x2A); //Modem Configuration
			halSpiWriteReg2(CCxxx0_MDMCFG3,	0x83); //Modem Configuration
			halSpiWriteReg2(CCxxx0_MDMCFG2,	0x13); //Modem Configuration
			halSpiWriteReg2(CCxxx0_DEVIATN,	0x62); //Modem Deviation Setting
			halSpiWriteReg2(CCxxx0_MCSM0,		0x18);   //Main Radio Control State Machine Configuration
//			halSpiWriteReg(CCxxx0_MCSM1,		0x18);
			halSpiWriteReg2(CCxxx0_FOCCFG,		0x16);  //Frequency Offset Compensation Configuration
			halSpiWriteReg2(CCxxx0_WORCTRL,	0xFB); //Wake On Radio Control
			halSpiWriteReg2(CCxxx0_FSCAL3,		0xE9);  //Frequency Synthesizer Calibration
			halSpiWriteReg2(CCxxx0_FSCAL2,		0x2A);  //Frequency Synthesizer Calibration
			halSpiWriteReg2(CCxxx0_FSCAL1,		0x00);  //Frequency Synthesizer Calibration
			halSpiWriteReg2(CCxxx0_FSCAL0,		0x1F);  //Frequency Synthesizer Calibration
			halSpiWriteReg2(CCxxx0_TEST0,		0x09);   //Various Test Settings

}

unsigned char send_num=0;
u8 rx_aes[64];
u8 *rx_aes1;



u8 aes_key[]={
 0x10 ,0xB8 ,0X8B,0xFF,0xFF,0xFF,0xFF,0xFF,
 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF
};

void CC1101_Main2(void)
{		
	CC1101_POWER_RESET2();
	halRfWriteRfSettings2();
	halSpiWriteBurstReg2(CCxxx0_PATABLE, &PaTabel[paTableLen], 1);
	GPIO_GDO0_ENABLE2();
	halSpiStrobe2(CCxxx0_SRX);


//	SPI_CC1101_ReadID();
	
	while(1)
    {
			halSpiStrobe(CCxxx0_SIDLE);             //进入IDLE模式			
			halSpiStrobe(CCxxx0_SRX);
			Delay2(120000);
//			Delay(120000);
//			Delay(120000);		
			if(receive_fig != 0)
			{
				receive_fig = 0;
				//Delay2(120000);
				//AesCTRDecrypt(rxBuffer1+3,rx_aes,8,aes_key,16,iv);
			}

			
			
//		Delay(120000);
//		Delay2(120000);
//		halSpiStrobe2(CCxxx0_SIDLE);             //进入IDLE模式
////	  LedBlink(LED_R);
//		txBuffer1[0] =0x06;                             // Packet length
//		txBuffer1[1] = 0x00;                           // Packet address
//		txBuffer1[2] = 0x35;                       //写入当前发送数据个数
//		txBuffer1[3] = 0x34; 
//		txBuffer1[4] = 0x56; 
//		txBuffer1[5] = 0x78; 
//		txBuffer1[6] = 0x90;
//		txBuffer1[6] = 0x99;			
//		GPIO_GDO0_DISABLE2(); //关闭中断	
//		halRfSendPacket2(txBuffer1, deta_size);
//		EXTI_ClearITPendingBit(EXTI_Line3);
//		GPIO_GDO0_ENABLE2(); //关闭中断	
//		halSpiStrobe2(CCxxx0_SIDLE);             //进入IDLE模式

    } 	

}

/*************************************************************/
void EXTI2_3_IRQHandler(void)
{
	u8  leng = 64;
	if(EXTI_GetITStatus(EXTI_Line3) != RESET)
  {
		 if (halRfReceivePacket2(rxBuffer1,&leng))              // 读数据并判断正确与否
		 {
				halSpiStrobe2(CCxxx0_SIDLE);    //CCxxx0_SIDLE	 0x36 //空闲状态
				receive_fig = 1;
		 }
		 else 
		 {
			 receive_fig = 2;

		 }
		 EXTI_ClearITPendingBit(EXTI_Line3);
  }
}
