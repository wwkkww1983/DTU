 
#ifndef __SPI_CC1101_H
#define __SPI_CC1101_H
#include "./os.h"
/* Includes ------------------------------------------------------------------*/
//#include "../user/config.h"    			 // ͷ�ļ�
#include "stm32f0xx.h"
//#include "stdint_h"

typedef __IO uint32_t  vu32;


/* Exported macro ------------------------------------------------------------*/
/* Select SPI FLASH: Chip Select pin low  */
#define SPI_FLASH_CS_LOW2()       GPIO_ResetBits(RF_CS_PORT,RF_CS_PIN)
/* Deselect SPI FLASH: Chip Select pin high */
#define SPI_FLASH_CS_HIGH2()      GPIO_SetBits(RF_CS_PORT,RF_CS_PIN)


/* Exported macro ------------------------------------------------------------*/
// Chip Select pin low  //
#define CC1101_SPI_CS_LOW2()       GPIO_ResetBits(CC1101_SPI_CS_GPIO_PORT, CC1101_SPI_CS_GPIO_PIN)
// Chip Select pin high //
#define CC1101_SPI_CS_HIGH2()      GPIO_SetBits(CC1101_SPI_CS_GPIO_PORT, CC1101_SPI_CS_GPIO_PIN)
// Chip Enable pin low  //
#define CC1101_ENABLE2()       GPIO_ResetBits(CC1101_EN_GPIO_PORT, CC1101_EN_GPIO_PIN)
// Chip Enable pin high //
#define CC1101_DISABLE2()      GPIO_SetBits(CC1101_EN_GPIO_PORT, CC1101_EN_GPIO_PIN)

/**********************************************************************************/

/* Exported functions ------------------------------------------------------- */
/*----- High layer function -----*/
void SPI_FLASH_Init2(void);
void SPI_FLASH_PageErase2(u32 SectorAddr);
void SPI_FLASH_BulkErase2(void);
void SPI_FLASH_PageWrite2(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite);
void SPI_FLASH_BufferWrite2(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite);
void SPI_FLASH_BufferRead2(u8* pBuffer, u32 ReadAddr, u16 NumByteToRead);
u32 SPI_FLASH_ReadID2(void);

/*----- Low layer function -----*/
u8 SPI_FLASH_ReadByte2(void);
u8 SPI_FLASH_SendByte2(u8 byte);
u16 SPI_FLASH_SendHalfWord2(u16 HalfWord);
void SPI_FLASH_WaitForWriteEnd2(void);
void SPI_CC1101_Init2(void);
void CC1101_Main2(void);
void GPIO_GDO0_ENABLE2(void);
void GPIO_GDO0_DISABLE2(void);
void halRfSendPacket2(INT8U *txBuffer, INT8U size);
void halSpiStrobe2(INT8U strobe);
INT8U halRfReceivePacket2(INT8U *rxBuffer, INT8U *length);

//#endif /* __SPI_FLASH_H */

/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/

#endif
