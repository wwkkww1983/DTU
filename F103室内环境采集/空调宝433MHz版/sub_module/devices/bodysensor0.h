#ifndef BODYSENSOR0_H
#define BODYSENSOR0_H
#include "os.h"

void bodysensor0_init(void);
void bodysensor0_start_work(void);
void bodysensor0_poll(void);
void bodysensor0_control(uint8 type, uint8 *content);
uint8 bodysensor0_get_state(uint8 *data, uint8 *len);
uint8 bodysensor0_button_pressed(uint8 released, uint16 time);
void bodysensor0_before_sleep(void);

void bodysensor0_dect_fixup(void);

#endif //BODYSENSOR0_H
