#include "switch.h"
#include "i2c_eeprom.h"

#ifdef SWITCH


static void _save_state(uint8 state);
static uint8 _load_state();

static uint8 g_last_state;
static uint8 g_cur_state;

void _turn_on_off(uint8 onoff)
{

}

static void _switch_toggle()
{
	
}

static void _save_state(uint8 state)
{
  
}

static uint8 _load_state()
{
  return 0;
}

void switch_init()
{
  
}

void switch_start_work()
{
  
}

void switch_poll()
{
  
}

void switch_control(uint8 type, uint8 *content)
{
  
}

uint8 switch_get_state(uint8 *data, uint8 *len)
{
  uint8 state_changed = 0;
  
	
	return state_changed;
}

uint8 switch_button_pressed(uint8 released, uint16 time)
{
  if(released)
  {
    return 1;
  }
  return 0;
}

#endif // SWITCH
