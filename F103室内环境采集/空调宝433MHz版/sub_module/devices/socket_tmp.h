#ifndef SOCKET_TMP_H
#define SOCKET_TMP_H
#include "os.h"
#include "../device.h"

void socket_tmp_init(void);
void socket_tmp_start_work(void);
void socket_tmp_poll(void);
void socket_tmp_control(uint8 type, uint8 *content);
uint8 socket_tmp_get_state(uint8 *data, uint8 *len);
uint8 socket_tmp_button_pressed(uint8 released, uint16 time);

#endif // SOCKET_TMP_H

