#ifndef COLORLIGHT_H
#define COLORLIGHT_H
#include "os.h"
#include "../sub_module.h"

void colorlight_init(void);
void colorlight_start_work(void);
void colorlight_poll(void);
void colorlight_control(uint8 type, uint8 *content);
uint8 colorlight_get_state(uint8 *data, uint8 *len);
uint8 colorlight_button_pressed(uint8 released, uint16 time);

void colorlight_set_color(uint32 color);
#endif

