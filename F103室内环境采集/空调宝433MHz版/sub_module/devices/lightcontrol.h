#ifndef LIGHTCONTROL_H
#define LIGHTCONTROL_H
#include "os.h"
#include "device.h"

void lightcontrol_init(void);
void lightcontrol_start_work(void);
void lightcontrol_poll(void);
void lightcontrol_control(uint8 type, uint8 *content);
uint8 lightcontrol_get_state(uint8 *data, uint8 *len);
uint8 lightcontrol_button_pressed(uint8 released, uint16 time);

#endif
