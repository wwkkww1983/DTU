#include "socket.h"
#include "hal_sleep.h"
#include "button.h"
#include "h_t_collect.h"
#include "att7053_collect.h"
//#include "i2c_eeprom.h"

#ifdef SOCKET

#define SK_D_PORT     GPIOA
#define SK_D_PIN      GPIO_Pin_10

#define SK_D_PORT_LOW()       GPIO_ResetBits(SK_D_PORT,SK_D_PIN)
#define SK_D_PORT_HIGH()      GPIO_SetBits(SK_D_PORT,SK_D_PIN)

#define SK_C_PORT     GPIOA
#define SK_C_PIN      GPIO_Pin_9

#define SK_C_PORT_LOW()       GPIO_ResetBits(SK_C_PORT,SK_C_PIN)
#define SK_C_PORT_HIGH()      GPIO_SetBits(SK_C_PORT,SK_C_PIN)

extern uint8 rf_recv_fig;
os_time_t train_time_wait;

static uint8 g_last_state;
static uint8 g_cur_state;
static int32_t g_saved_power;
static int32_t g_saved_T_Energy;
static int32_t g_cur_power;
static int32_t g_cur_T_Energy;
static uint8 g_cur_state;


void _turn_on_off(uint8 onoff)
{
	if(onoff)
	{
		//hal_led_on(LED_PORT, LED_PIN);
		SK_D_PORT_HIGH();
		SK_C_PORT_HIGH();
		hal_wait_ms(1);
		SK_C_PORT_LOW();
		hal_wait_ms(1);
		SK_C_PORT_HIGH();
		hal_wait_ms(1);
		SK_C_PORT_LOW();
		hal_wait_ms(1);
		SK_C_PORT_HIGH();
	}
	else
	{
		//hal_led_off(LED_PORT, LED_PIN);
		SK_D_PORT_LOW();
		SK_C_PORT_HIGH();
		hal_wait_ms(1);
		SK_C_PORT_LOW();
		hal_wait_ms(1);
		SK_C_PORT_HIGH();
		hal_wait_ms(1);
		SK_C_PORT_LOW();
		hal_wait_ms(1);
		SK_C_PORT_HIGH();
	}
	g_cur_state = onoff;
	printf("g_cur_state change %d \n", onoff);
}

static void _socket_toggle()
{
	if(g_cur_state)
	{
		_turn_on_off(0);

	}
	else
	{
		_turn_on_off(1);
	}
}

void IO_GP_INIT(GPIO_TypeDef* port, uint16_t pin)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	if(port == GPIOA)
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	else
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	GPIO_InitStruct.GPIO_Pin = pin;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed =GPIO_Speed_Level_3;
	GPIO_Init(port, &GPIO_InitStruct);

	GPIO_SetBits(port, pin);
}

////////////////////////
//电相关信息
typedef struct
{
    int32_t WattVal;        //功率值
    int32_t VoltVal;        //电压值
    //int32_t PowerQ;         //无功功率值
    int32_t Energy;         //电量值
    int32_t M_Energy;       //增量电量值
    int32_t T_Energy;       //总计电量值

    int32_t AppAdjustPowP;
    uint32_t AppAdjustVolt;
	
    uint8_t NormalCount;    //计数
} AC_ElectInfo_st;

static AC_ElectInfo_st g_AC_ElectInfo;
static void eventAtt7053Sync(void)
{
    static int32_t Pre_Watt = 0, Pre_Volt = 0;
    static uint32_t TOutMeasUpdate = 0X00;  //测试数据测量上报间隔
    static uint32_t ToutEnergyUpdate = 0X00;
	static uint8_t  dubiData = 0;

    int32_t Dif_Watt, Dif_Volt, Dif_EP;
    uint32_t uiTempData;

    //测量得到功率值变化,2s更新一次测量状态
    //优化，大于5kw不上报
    if(OS_SYSTIME_BEYOND_SPAN(TOutMeasUpdate, 2000))
    {
        TOutMeasUpdate = os_timer_get_systime();

        Dif_Watt = g_AC_ElectInfo.WattVal - Pre_Watt;

        if((Dif_Watt <= -150) || (Dif_Watt >= 150))
        {
        	//5000K以上数据滤掉
        	if(g_AC_ElectInfo.WattVal <= 500000)
        	{
        		//变化超过2000kW,缓慢处理，稳定后再上报
        		if((Dif_Watt <= -200000) || (Dif_Watt >= 200000))
        		{
        			dubiData++;
        		}
				else
				{
					dubiData = 0;
				}

				if(dubiData == 0 || dubiData > 5)
				{
		      Pre_Watt = g_AC_ElectInfo.WattVal;
					os_log("WattVal : %d",g_AC_ElectInfo.WattVal);
					g_cur_power = g_AC_ElectInfo.WattVal;
					dubiData = 0;
				}
        	}
			else
			{
				dubiData = 0;
				os_log(" WattVal : %d -> abnormal",g_AC_ElectInfo.WattVal);
			}
        }

        Dif_Volt = g_AC_ElectInfo.VoltVal - Pre_Volt;
        //判断电压值变化并上报
        if((Dif_Volt <= -500) || (Dif_Volt >= 500))
        {
            Pre_Volt = g_AC_ElectInfo.VoltVal;
            //上报电压值
        }

        if(OS_SYSTIME_BEYOND_SPAN(ToutEnergyUpdate, 2*60000))//2min更新一次
        {
            ToutEnergyUpdate = os_timer_get_systime();

            uiTempData = HalATT7053GetEP();

            Dif_EP = uiTempData - g_AC_ElectInfo.Energy;

            if(Dif_EP < 0 || Dif_EP > 20)
            {
                //ATT7053经过复位
                g_AC_ElectInfo.Energy = uiTempData;
            }
            else if(Dif_EP != 0)
            {
                if(Dif_EP != g_AC_ElectInfo.M_Energy)
                {
                    g_AC_ElectInfo.M_Energy = Dif_EP;   //电量增量
                    g_AC_ElectInfo.Energy = uiTempData; //当前芯片存储的总电量
                    g_AC_ElectInfo.T_Energy += Dif_EP;  //电量总量

					//TODO  report 
					//ch_sync_property(AC_MENERGY_ID, Dif_EP, NULL);
					//ch_sync_property(AC_TENERGY_ID, g_AC_ElectInfo.T_Energy, NULL); //总电量

					g_cur_T_Energy += Dif_EP;
					os_log("Dif_EP:%d,T_Energy:%d,F_Energy",Dif_EP,g_AC_ElectInfo.T_Energy,g_cur_T_Energy);
                }
            }
        }
    }
}

static void measureThread(void)
{
    static uint32_t TimeOutMeasure = 0;
		static uint32_t TimeCheckATT = 0;
    static int32_t UtempVal[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    static int32_t WtempVal[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8_t SampleCount = 0;
    int32_t UsumVal = 0, WsumVal = 0, i = 0;

    if(OS_SYSTIME_BEYOND_SPAN(TimeOutMeasure, 1000))
    {
        TimeOutMeasure = os_timer_get_systime();

        UtempVal[SampleCount] = HalATT7053GetURms();
        WtempVal[SampleCount] = HalATT7053GetP1();  

        for(i = 0; i < 0x08; i++)
        {
            UsumVal += UtempVal[i];
            WsumVal += WtempVal[i];
        }

        g_AC_ElectInfo.VoltVal = UsumVal >> 3;
        g_AC_ElectInfo.WattVal = (WsumVal >> 3) - 100;

        if(g_AC_ElectInfo.WattVal < 0x00)
        {
            g_AC_ElectInfo.WattVal = 0x00;
        }

        SampleCount++;
        if(0x08 == SampleCount)
        {
            SampleCount = 0x00;
        }
    }
		
	if(OS_SYSTIME_BEYOND_SPAN(TimeCheckATT, 2000))
	{
		TimeCheckATT = os_timer_get_systime();
		HalCheckATT7053Parameter();
	}
}
///////////////////////////
void socket_init()
{
	//读取flash中的电量

	g_saved_power = -1;
	g_saved_T_Energy = -1;

	if((void *)ATT_SAVED_T_ENERGY != NULL)
	{
		memcpy(&g_cur_T_Energy, (void *)ATT_SAVED_T_ENERGY, sizeof(g_cur_T_Energy));
		os_log("read flash T_Energy: %d",g_cur_T_Energy);
	}
	
	if(g_cur_T_Energy < 0)
	{
			g_cur_T_Energy = 0;
	}
	//
	IO_GP_INIT(SK_D_PORT,SK_D_PIN);
	IO_GP_INIT(SK_C_PORT,SK_C_PIN);
	//  eeprom_init();

	g_cur_state = g_last_state = 0;
	
	if(g_cur_state == 0xff)
	{
		g_cur_state = g_last_state = 0;
		_turn_on_off(0);
	}
	_turn_on_off(g_cur_state);

	train_time_wait = os_timer_get_systime();
}

void socket_start_work()
{
  g_cur_state = 0;
}

extern uint8 rf_sleep_up;
void socket_poll()
{
	measureThread();
	eventAtt7053Sync();
	if((OS_SYSTIME_BEYOND_SPAN(train_time_wait, 60*60000))) //每1小时保存一次电量到flash中，下次开机读取
	{
/***************************************************************/
		int32_t saved_energy;
		memcpy(&saved_energy, (void *)ATT_SAVED_T_ENERGY, sizeof(saved_energy));
		if(saved_energy != g_cur_T_Energy)
		{
			os_log("wirte ATT_SAVED_T_ENERGY");
			hal_flash_write((void *)ATT_SAVED_T_ENERGY, &g_cur_T_Energy, sizeof(g_cur_T_Energy));
		}
/************************************************************************/
		train_time_wait = os_timer_get_systime();
	}

}

void socket_control(uint8 type, uint8 *content)
{
	if(type == DEVICE_CTRL)
  {
    _turn_on_off(content[0]);
  }
}

uint8 socket_get_state(uint8 *data, uint8 *len)
{
  
  uint8 state_changed = 0;

	memcpy(data, &g_cur_power, 4);
	memcpy(data+4, &g_cur_T_Energy, 4);

	*len = 8;
	if(g_saved_power != g_cur_power || \
        g_saved_T_Energy != g_cur_T_Energy )
	{
		g_saved_power = g_cur_power;
    g_saved_T_Energy = g_cur_T_Energy;
		state_changed = 1;
		
		os_log("get_state power: %d, T_energy:%d", g_cur_power, g_cur_T_Energy);
	}

	return state_changed;
}

uint8 socket_button_pressed(uint8 released, uint16 time)
{
  if(released)
  {
	rf_recv_fig = 0;

	return 1;
  }
  return 0;
}

//TODO  save and reboot func

void socket_reboot(void)
{
	int32_t saved_energy;
	memcpy(&saved_energy, (void *)ATT_SAVED_T_ENERGY, sizeof(saved_energy));
	if(saved_energy != g_cur_T_Energy)
	{
		os_log("wirte ATT_SAVED_T_ENERGY");
		hal_flash_write((void *)ATT_SAVED_T_ENERGY, &g_cur_T_Energy, sizeof(g_cur_T_Energy));
	}
	HAL_REBOOT();
}
#endif // SOCKET
