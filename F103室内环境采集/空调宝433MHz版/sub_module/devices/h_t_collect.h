#ifndef H_T_COLLECT_H
#define H_T_COLLECT_H
#include "os.h"
#include "sub_module.h"

#define SHT20_IIC_PORT         GPIOB
#define SHT20_IIC_SCL_PIN      GPIO_Pin_10
#define SHT20_IIC_SDA_PIN      GPIO_Pin_11


//io????
#define sht20_iic_sda_in()     (SHT20_IIC_PORT->MODER&=0XFF3FFFFF)
#define sht20_i2c_sda_out()    (SHT20_IIC_PORT->MODER|=0X00400000)

#define sht20_iic_scl_in()     (SHT20_IIC_PORT->MODER&=0XFFCFFFFF)
#define sht20_i2c_scl_out()    (SHT20_IIC_PORT->MODER|=0X00100000)

//io????
#define sht20_iic_scl_h()        (SHT20_IIC_PORT->BSRR=GPIO_Pin_10)  //??SCL
#define sht20_iic_scl_l()        (SHT20_IIC_PORT->BRR=GPIO_Pin_10)   //??SCL


#define sht20_iic_sda_h()        (SHT20_IIC_PORT->BSRR=GPIO_Pin_11)  //??SDA   
#define sht20_iic_sda_l()        (SHT20_IIC_PORT->BRR=GPIO_Pin_11)   //??SDA   
#define sht20_iic_read_sda()     ((SHT20_IIC_PORT->IDR>>11)&0x01)  //??SDA 
#define sht20_iic_read_scl()     ((SHT20_IIC_PORT->IDR>>10)&0x01)  //??SCL 


#define SHT20_IIC_DELAY_US         0x08   
#define SHT20_IIC_ACK_TIME_OUT     0xFF  

void HalSht20IIC_Init(void);
uint8_t Hal_sht20_get_humidity(uint32_t *h);
uint8_t Hal_sht20_get_temperature(int32_t *k);

#endif //H_T_COLLECT_H
