#ifndef CURTAINCONTROL_H
#define CURTAINCONTROL_H
#include "os.h"
#include "../device.h"

void  curtaincontrol_init(void);
void  curtaincontrol_start_work(void);
void  curtaincontrol_poll(void);
void  curtaincontrol_control(uint8 type, uint8 *content);
uint8 curtaincontrol_get_state(uint8 *data, uint8 *len);
uint8 curtaincontrol_button_pressed(uint8 released, uint16 time);

#endif

