#include "bs102.h"
#include "../device.h"
#include "../button.h"

#ifdef BS102



static uint8 _get_status(uint8 *data);
static void _set_status(uint8 *data);

void bs102_init()
{
  
}

void bs102_start_work()
{

}

void _fixup_int_edge()
{
  
}

void bs102_poll()
{

}

void bs102_control(uint8 type, uint8 *content)
{
  
}

static uint8 _get_status(uint8 *data)
{
  return STATUS_LEN;
}

static void _set_status(uint8 *data)
{
  
}

static uint8 _compare_status(uint8 *status1, uint8 *status2)
{
  
  return 0;
}

uint8 bs102_get_state(uint8 *data, uint8 *len)
{
  return 0;
}

uint8 bs102_button_pressed(uint8 released, uint16 time)
{ 
  return 0;
}

void bs102_before_sleep()
{
 
}

void bs102_dect_fixup()
{
  
}

#endif // BS102
