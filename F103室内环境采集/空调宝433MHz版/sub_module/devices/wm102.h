#ifndef WM102_H
#define WM102_H
#include "os.h"

void wm102_init(void);
void wm102_start_work(void);
void wm102_poll(void);
void wm102_control(uint8 type, uint8 *content);
uint8 wm102_get_state(uint8 *data, uint8 *len);
uint8 wm102_button_pressed(uint8 released, uint16 time);
//void BZ_Init(void);
//void halWait(BYTE wait);



#endif //WM102_H
