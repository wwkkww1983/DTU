#ifndef ATT7053_COLLECT_H
#define ATT7053_COLLECT_H

#include "os.h"
#include "sub_module.h"

#define HAL_CPU_HZ  (48000000UL)  //48MHZ

#define REG32(addr)     (*((volatile uint32_t *)(addr)))
#define REG16(addr)     (*((volatile uint16_t *)(addr)))
#define REG8(addr)      (*((volatile uint8_t *)(addr)))

#define GET_ST_OFFSET_ADDR(st,para)     ((uint32_t)(&((st*)0)->para))

//boot使用相关地址量
#define AC_IR_BOOT_FW_SIZE       (14*1024UL)       //预计BOOT大小14k
#define AC_IR_BOOT_FW_ADDR       (0x08000000UL)    //预计固件存放位置
#define BOOT_DEVICE_TYPE_ADDR    (AC_IR_BOOT_FW_ADDR+AC_IR_BOOT_FW_SIZE-0X10)  //BOOT设备类型

//FLASH参数
#define FLASH_WRITE_BLOCK_SIZE      128
#define FLASH_PAGE_SIZE             1024

//RAM配置参数
#define SRAM_VECTOR_TABLE_ADDR  (uint32_t)0x20000000
#define ADJUST_JUMP_FLAG_ADDR   (uint32_t)0x20001F00

//APP固件相关参数
#define AC_IR_APP_FW_SIZE       (37*1024UL)             //预留固件空间
#define AC_IR_APP_REAL_SIZE     (31*1024UL)             //app实际大小
#define AC_IR_APP_FW_ADDR       (AC_IR_BOOT_FW_ADDR+AC_IR_BOOT_FW_SIZE)     //预计固件存放位置
#define APP_FW_VERSION_ADDR     (AC_IR_APP_FW_ADDR+AC_IR_APP_REAL_SIZE-0X20)  //固件版本存放地址
#define APP_DEVICE_TYPE_ADDR    (AC_IR_APP_FW_ADDR+AC_IR_APP_REAL_SIZE-0X10)  //设备类型

//APP信息存放地址(包括升级相关信息)2个flash page
#define AC_IR_APP_INFO_SIZE     (2*1024UL)             //预计固件信息大小2k
#define AC_IR_APP_INFO_ADDR     (AC_IR_APP_FW_ADDR+AC_IR_APP_FW_SIZE)     //预计固件信息存放位置
#define UPDATE_INFO1_ADDR       (AC_IR_APP_INFO_ADDR)   //信息1
#define UPDATE_INFO2_ADDR       (AC_IR_APP_INFO_ADDR+FLASH_PAGE_SIZE)  //信息2

//电量检测相关参数配置地址
#define ATT7053_PARA_SIZE       (1024UL)    //att7053校验参数占用空间大小
#define ATT7053_PARA_START_ADDR (AC_IR_APP_INFO_ADDR+AC_IR_APP_INFO_SIZE)    //att7053校验默认值存放地址
#define ATT_DKURMS_4BYTE_ADDR   (ATT7053_PARA_START_ADDR)
#define ATT_DKPQS_4BYTE_ADDR    (ATT7053_PARA_START_ADDR+0X04)
#define ATT_GPHS_2BYTE_ADDR     (ATT7053_PARA_START_ADDR+0X08)
#define ATT_GP_2BYTE_ADDR       (ATT7053_PARA_START_ADDR+0X0A)
#define ATT_ENYCNT_2BYTE_ADDR   (ATT7053_PARA_START_ADDR+0X0C)  //累计点数为0.01度


//红外码库地址分配
#define AC_IR_LIB_MAX_SIZE      (10*1024UL)             //预计红外码库最大容量
#define AC_IR_LIB_ADDR          (AC_IR_BOOT_FW_ADDR+54*1024)   //预计红外码库存放位置
#define AC_FUN_TRANSFORM_ADDR   (REG32(AC_IR_LIB_ADDR))        //AC_ControlTransform
#define AC_FUN_DECODE_ADDR      (REG32(AC_IR_LIB_ADDR+4))      //ControlDecode
#define AC_FUN_GET_FUN_ADDR     (REG32(AC_IR_LIB_ADDR+8))      //GetFunctions

#define IR_LIB_DOWNLOAD_FLAG    0X55    //红外码库下载标志值
#define ROM_DOWNLOAD_FLAG       0XAA    //固件下载标志值
#define RESET_TO_DEFAULTS_FLAG  0X5A    //恢复出厂设置标志

//flash中存储标志地址
#define AC_CLR_FLASH_FLAG       0XFFFFFFFF  //清除FLASH中存储的标志
#define AC_NOT_INITED_FLAG      0XFFFFFFFF  //出厂默认标志
#define AC_INITED_FLAG          0XAA55AA55  //非出厂默认标志
#define AC_NOT_MATCHED          0XFFFFFFFF  //未匹配标志
#define AC_MATCHED_FLAG         0X55AA55AA  //匹配标志码
#define FW_NEED_UPDATE_FLAG     0XB007B007  //固件需要升级
#define FW_UPDATE_DONE          0X13145213  //存在完整固件或者升级完成

#define FLASH_LOCK_TIMEOUT      2000    //2S
#define UPDATE_REQUEST_TIMEOUT  500     //500ms

//引脚功能定义
#define BUTTON_DET_PIN          0x0b    //PA11
#define CLOUD_RST_PIN           0x01    //PA1

void HalSetInterruptEnable(uint8_t enable);
uint8_t HalGetInterruptEnable();

#define HAL_ENTER_CRITICAL(iState) iState = HalGetInterruptEnable();HalSetInterruptEnable(0)
#define HAL_EXIT_CRITICAL(iState) HalSetInterruptEnable(iState)


//att7053
//uint8_t getAtt7053InitStatus(void);
void HalATT7053Init(void);
int32_t HalATT7053GetURms(void); //ATT7053电压的有效值
int32_t HalATT7053GetP1(void); //ATT7053第一通道有功功率
int32_t HalATT7053GetQ1(void); //ATT7053第一通道无功功率
int32_t HalATT7053GetEP(void);    //ATT7053有功能量
void HalATT7053SetGphsGP(uint32_t Gphs, uint32_t GP); //设置ATT7053的Gphs、GP寄存器
void HalATT7053ResetAdjust(void);//重置ATT7953A的校验配置
void HalATT7053AdjustGphs(int32_t Preal, int32_t Qreal);  //校正ATT7053的角差校正值GPhs1
void HalATT7053AdjustVDK(int32_t Vreal);//校正ATT7053的电压系数
void HalATT7053AdjustGP(int32_t Preal, int32_t Qreal);//校正ATT7053的功率增益GP,GQ,GS
void HalATT7053AdjustPDK(int32_t Preal);   //校正ATT7053的功率值系数
void HalATT7053AdjustEcnt(int16_t Ecnt); //校正ATT7053的电量计数基准值
uint8_t HalATT7053SaveAdjDat(void);

uint32_t HalGetATT7053WorkStatus(void);

//crc32
uint32_t HalCalCRC32(const unsigned char *buf, uint32_t size);



#endif // ATT7053_COLLECT_H
