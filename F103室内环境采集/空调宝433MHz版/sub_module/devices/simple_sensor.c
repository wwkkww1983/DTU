#include "simple_sensor.h"
#include "../device.h"

#ifdef SIMPLE_SENSOR




void simple_sensor_init()
{
  
}

void simple_sensor_start_work()
{

}

void simple_sensor_poll()
{

}

void simple_sensor_control(uint8 type, uint8 *content)
{

}

uint8 simple_sensor_get_state(uint8 *data, uint8 *len)
{
  return 0;
}

uint8 simple_sensor_button_pressed(uint8 released, uint16 time)
{
  return 0;
}

#endif // SIMPLE_SENSOR
