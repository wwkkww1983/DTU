#include "att7053_collect.h"
#include <string.h>
#include <stdlib.h>

/* Extern define ---------------------------------------------------------*/
/* Private define ---------------------------------------------------------*/
static void HalATT7053ReInit(void);
//typedef  void (*pFunction)(void);
#if 0
#pragma location = SRAM_VECTOR_TABLE_ADDR
__no_init __IO uint32_t VectorTable[48];

#pragma location = ADJUST_JUMP_FLAG_ADDR
__no_init __IO uint32_t JumpFlagTable[64];  //?wifi??DMA?????RAM
#endif

void HalInit()
{
    //????,500ms???????ATT7053?????
    hal_wait_ms(500);
    HalATT7053Init();

}

void SysWait(uint16 msec)
{
		hal_wait_ms(msec);
}

void HalSetInterruptEnable(uint8_t enable)
{
    __set_PRIMASK(enable ? 0 : 1);
}

uint8_t HalGetInterruptEnable()
{
    return __get_PRIMASK() == 0;
}


/****************************************************
att7053
*//////////////////////////////////////////////////////

/* Private define ------------------------------------------------------------*/
//#pragma location = ATT7053_PARA_START_ADDR
//__root const uint32_t defaultAtt7053Parameter[] = {0x000021E1, 0x00000089, 0xF6AA0000, 0xFFFF0020};   //ATT7053校正参数

#define ATT7053_FEMU    921000      //ATT7053 EMU时钟

#define ATT7053AU_HFCONST       0x11f   //0x11f  
#define ATT7053AU_VAL_AMP       100     //测量值扩大倍数

/*在当前校准参数下，此HFCONST值对应的电能寄存
器最小单位代表的能量为0.01 kWh */
/************************************* SPI ***********************************/
#define ATT7053_SPI                             SPI1
#define ATT7053_SPI_CLK                         RCC_APB2Periph_SPI1
#define ATT7053_SPI_IRQn                        SPI1_IRQn
#define ATT7053_SPI_IRQHandler                  SPI1_IRQHandler

#define ATT7053_SPI_SCK_PIN                     GPIO_Pin_3
#define ATT7053_SPI_SCK_GPIO_PORT               GPIOB
#define ATT7053_SPI_SCK_GPIO_CLK                RCC_AHBPeriph_GPIOB
#define ATT7053_SPI_SCK_SOURCE                  GPIO_PinSource3
#define ATT7053_SPI_SCK_AF                      GPIO_AF_0

#define ATT7053_SPI_MISO_PIN                    GPIO_Pin_4
#define ATT7053_SPI_MISO_GPIO_PORT              GPIOB
#define ATT7053_SPI_MISO_GPIO_CLK               RCC_AHBPeriph_GPIOB
#define ATT7053_SPI_MISO_SOURCE                 GPIO_PinSource4
#define ATT7053_SPI_MISO_AF                     GPIO_AF_0

#define ATT7053_SPI_MOSI_PIN                    GPIO_Pin_5
#define ATT7053_SPI_MOSI_GPIO_PORT              GPIOB
#define ATT7053_SPI_MOSI_GPIO_CLK               RCC_AHBPeriph_GPIOB
#define ATT7053_SPI_MOSI_SOURCE                 GPIO_PinSource5
#define ATT7053_SPI_MOSI_AF                     GPIO_AF_0

/************************************* CS ***********************************/
#define ATT7053_SPI_CS_GPIO_PIN                 GPIO_Pin_15
#define ATT7053_SPI_CS_GPIO_PORT                GPIOA
#define ATT7053_SPI_CS_GPIO_CLK                 RCC_AHBPeriph_GPIOA

//硬件复位引脚
#define ATT7053_RESET_GPIO_PIN                  GPIO_Pin_12
#define ATT7053_RESET_GPIO_PORT                 GPIOA
#define ATT7053_RESET_GPIO_CLK                  RCC_AHBPeriph_GPIOA


/* Exported macro ------------------------------------------------------------*/
// Chip Select pin low  //
#define ATT7053_SPI_CS_LOW()       GPIO_ResetBits(ATT7053_SPI_CS_GPIO_PORT, ATT7053_SPI_CS_GPIO_PIN)
// Chip Select pin high //
#define ATT7053_SPI_CS_HIGH()      GPIO_SetBits(ATT7053_SPI_CS_GPIO_PORT, ATT7053_SPI_CS_GPIO_PIN)

// Chip reset pin low  //
#define ATT7053_SPI_RESET_LOW()       GPIO_ResetBits(ATT7053_RESET_GPIO_PORT, ATT7053_RESET_GPIO_PIN)
// Chip reset pin high //
#define ATT7053_SPI_RESET_HIGH()      GPIO_SetBits(ATT7053_RESET_GPIO_PORT, ATT7053_RESET_GPIO_PIN)


////////////////////////////////////////////////////////////////////////////
//ATT7053 寄存器地址
//指令表
#define REG_ATT7053_SPL_I1      0x00    //H3 电流通道 1 的 ADC 采样数据
#define REG_ATT7053_SPL_I2      0x01    //H3 电流通道 2 的 ADC 采样数据
#define REG_ATT7053_SPL_U       0x02    //H3 电压通道的 ADC 采样数据
#define REG_ATT7053_SPL_P       0x03    //H3 有功功率波形数据
#define REG_ATT7053_SPL_Q       0x04    //H3 无功功率波形数据
#define REG_ATT7053_SPL_S       0x05    //H3 视在功率波形数据
#define REG_ATT7053_RMS_I1      0x06    //H3 电流通道 1 的有效值
#define REG_ATT7053_RMS_I2      0x07    //H3 电流通道 2 的有效值
#define REG_ATT7053_RMS_U       0x08    //H3 电压通道的有效值
#define REG_ATT7053_FREQ_U      0x09    //H2 电压频率
#define REG_ATT7053_POWERP1     0x0A    //H3 第一通道有功功率
#define REG_ATT7053_POWERQ1     0x0B    //H3 第一通道无功功率
#define REG_ATT7053_POWER_S     0x0C    //H3 视在功率
#define REG_ATT7053_ENERGY_P    0x0D    //H3 有功能量
#define REG_ATT7053_ENERGY_Q    0x0E    //H3 无功能量
#define REG_ATT7053_ENERGY_S    0x0F    //H3 视在能量
#define REG_ATT7053_POWERP2     0x10    //H3 第二通道有功功率
#define REG_ATT7053_POWERQ2     0x11    //H3 第二通道无功功率
#define REG_ATT7053_I1ANGLE     0x12    //H3 电流通道 1 与电压通道夹角
#define REG_ATT7053_I2ANGLE     0x13    //H3 电流通道 2 与电压通道夹角
#define REG_ATT7053_TEMPDATA    0x14    //H1 温度测试数据

#define REG_ATT7053_BACKUPDATA  0x16    //H3 通讯数据备份寄存器
#define REG_ATT7053_COMCHECKSUM 0x17    //H2 通讯校验和寄存器
#define REG_ATT7053_SUMCHECKSUM 0x18    //H3 校表参数校验和寄存器
#define REG_ATT7053_EMUSR       0x19    //H1 EMU 状态寄存器

#define REG_ATT7053_EMUIE           0x30    //HEMU 中断使能寄存器
#define REG_ATT7053_EMUIF           0x31    //H中断标志寄存器
#define REG_ATT7053_WPREG           0x32    //H写保护寄存器
#define REG_ATT7053_SRSTREG         0x33    //H软件复位寄存器

#define REG_ATT7053_EMUCFG          0x40    //H配置寄存器
#define REG_ATT7053_FREQCFG         0x41    //H时钟/更新频率配置寄存器
#define REG_ATT7053_MODULEEN        0x42    //HEMU 模块使能寄存器
#define REG_ATT7053_ANAEN           0x43    //H模拟模块使能寄存器

#define REG_ATT7053_IOCFG           0x45    //HIO 输出配置寄存器
#define REG_ATT7053_GP1             0x50    //H通道 1 的有功功率校正
#define REG_ATT7053_GQ1             0x51    //H通道 1 的无功功率校正
#define REG_ATT7053_GS1             0x52    //H通道 1 的视在功率校正
#define REG_ATT7053_PHASE1          0x53    //H 通道 1 的相位校正（移采样点方式）
#define REG_ATT7053_GP2             0x54    //H 通道 2 的有功功率校正
#define REG_ATT7053_GQ2             0x55    //H 通道 2 的无功功率校正
#define REG_ATT7053_GS2             0x56    //H 通道 2 的视在功率校正
#define REG_ATT7053_PHASE2          0x57    //H 通道 2 的相位校正（移采样点方式）
#define REG_ATT7053_QPHSCAL         0x58    //H 无功相位补偿
#define REG_ATT7053_ADCCON          0x59    //H ADC 通道增益选择
#define REG_ATT7053_ALLGAIN         0x5A    //H 3 个 ADC 通道整体增益寄存器，主要针对 Vref变化引起的 ADC 满量程变化
#define REG_ATT7053_I2GAIN          0x5B    //H 电流通道 2 增益补偿
#define REG_ATT7053_I1OFF           0x5C    //H电流通道 1 的偏置校正
#define REG_ATT7053_I2OFF           0x5D    //H电流通道 2 的偏置校正
#define REG_ATT7053_UOFF            0x5E    //H电压通道的偏置校正
#define REG_ATT7053_PQSTART         0x5F    //H起动功率设置

#define REG_ATT7053_RMSSTART        0x60    //H 0040 2(16bit) 有效值启动值设置寄存器
#define REG_ATT7053_HFCONST         0x61    //H 0040 2(15bit) 输出脉冲频率设置
#define REG_ATT7053_CHK             0x62    //H 10 1(8bit) 窃电阈值设置
#define REG_ATT7053_IPTAMP          0x63    //H 0020 2(16bit) 窃电检测电流域值
#define REG_ATT7053_UCONST          0x64    //H 0000 2(16bit) 失压情况下参与计量的电压，断相仿窃电
#define REG_ATT7053_P1OFFSET        0x65    //H 00 1(8bit) 通道 1 有功功率偏执校正参数，为 8bit 补码
#define REG_ATT7053_P2OFFSET        0x66    //H 00 1(8bit) 通道 2 有功功率偏执校正参数，为 8bit 补码
#define REG_ATT7053_Q1OFFSET        0x67    //H 00 1(8bit) 通道 1 无功功率偏执校正参数，为 8bit 补码
#define REG_ATT7053_Q2OFFSET        0x68    //H 00 1(8bit) 通道 2 无功功率偏执校正参数，为 8bit 补码
#define REG_ATT7053_I1RMSOFFSET     0x69    //H 00 1(8bit) 通道 1 有效值补偿寄存器，为 8bit 无符号数
#define REG_ATT7053_I2RMSOFFSET     0x6A    //H 00 1(8bit) 通道 2 有效值补偿寄存器，为 8bit 无符号数
#define REG_ATT7053_URMSOFFSET      0x6B    //H 00 1(8bit) 电压通道（通道 3）有效值补偿寄存器，为 8bit无符号数
#define REG_ATT7053_ZCROSSCURRENT   0x6C    //H 0004 2(16bit) 电流过零阈值设置寄存器
#define REG_ATT7053_GPHS1           0x6D    //H 0000 2(16bit) 通道 1 的相位校正（ PQ 方式）
#define REG_ATT7053_GPHS2           0x6E    //H 0000 2(16bit) 通道 2 的相位校正（ PQ 方式）
#define REG_ATT7053_PFCNT           0x6F    //H 0000 2(16bit) 快速有功脉冲计数
#define REG_ATT7053_QFCNT           0x70    //H 0000 2(16bit) 快速无功脉冲计数
#define REG_ATT7053_SFCNT           0x71    //H 0000 2(16bit) 快速视在脉冲计数



enum ANAEN_BIT
{
    BOREN   = 0x20,
    TPS_EN  = 0x08,
    Adc_i2on = 0x04,
    Adc_i1on = 0x02,
    Adc_uon = 0x01,
};

enum ADCCON_BIT             //ADCCON是16位寄存器，低字节（0~7）为模拟通道增益选择
{
    I2Gain_1  = 0x00,
    I2Gain_2  = 0x10,
    I2Gain_8  = 0x20,
    I2Gain_16 = 0x30,
    I1Gain_1  = 0x00,
    I1Gain_2  = 0x04,
    I1Gain_8  = 0x08,
    I1Gain_16 = 0x0C,
    UGain_1   = 0x00,
    UGain_2   = 0x01,
    UGain_8   = 0x02,
    UGain_16  = 0x03,
};

#define ATT7053_SPI_ERR_FLAG    (0x5555aaaaul)

#define CMD_ATT7053_REGWRITE    0x80    //写寄存器命令
#define CMD_ATT7053_REGREAD     0x00    //读寄存器命令
/********************************************************
操作宏
********************************************************/


//全局变量声明
typedef struct
{
    int32_t  DKUrms;    //系数，电压有效值寄存器读数/DKUrms=电压有效值
    int32_t  DKpqs;     //系数，功率相关寄存器读数/DKpqs=功率值
    uint32_t PowerPReg;
    uint32_t PowerQReg;
    uint32_t URmsReg;
    uint32_t ErrFlag;   //att7053工作异常标志
    uint16_t ECnt;      //系数，累计超过这个值为0.01度
    uint16_t AdjGphs;   //Gphs：角差校正值
    uint16_t AdjGP;     //GP：功率增益校正值
    uint16_t ErrCount;  //通信出错计数
} Att7053Data_st;

static Att7053Data_st  g_Att7053Dat;     //tbd

//ATT7053 SPI 初始化
static void HalATT7053SpiInit(void)
{
    SPI_InitTypeDef  SPI_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    // ATT7053_SPI Periph clock enable //
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_APB2PeriphClockCmd(ATT7053_SPI_CLK, ENABLE);

    // Configure ATT7053_Reset pin: ATT7053 Reset pin //
    GPIO_InitStructure.GPIO_Pin = ATT7053_RESET_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(ATT7053_RESET_GPIO_PORT, &GPIO_InitStructure);
    ATT7053_SPI_RESET_HIGH();

    // Configure ATT7053_SPI pins: SCK //
    GPIO_InitStructure.GPIO_Pin = ATT7053_SPI_SCK_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(ATT7053_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

    // Configure ATT7053_SPI pins: MISO //
    GPIO_InitStructure.GPIO_Pin = ATT7053_SPI_MISO_PIN;
    GPIO_Init(ATT7053_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

    // Configure ATT7053_SPI pins: MOSI //
    GPIO_InitStructure.GPIO_Pin = ATT7053_SPI_MOSI_PIN;
    GPIO_Init(ATT7053_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

    // Configure ATT7053_SPI_CS pin: ATT7053 CS pin //
    GPIO_InitStructure.GPIO_Pin = ATT7053_SPI_CS_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(ATT7053_SPI_CS_GPIO_PORT, &GPIO_InitStructure);
    ATT7053_SPI_CS_HIGH();

    // Connect PXx  //
    GPIO_PinAFConfig(ATT7053_SPI_SCK_GPIO_PORT, ATT7053_SPI_SCK_SOURCE, ATT7053_SPI_SCK_AF);
    GPIO_PinAFConfig(ATT7053_SPI_MOSI_GPIO_PORT, ATT7053_SPI_MOSI_SOURCE, ATT7053_SPI_MOSI_AF);
    GPIO_PinAFConfig(ATT7053_SPI_MISO_GPIO_PORT, ATT7053_SPI_MISO_SOURCE, ATT7053_SPI_MISO_AF);

    // SPI configuration -------------------------------------------------------//
    SPI_I2S_DeInit(ATT7053_SPI);
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(ATT7053_SPI, &SPI_InitStructure);
    SPI_RxFIFOThresholdConfig(ATT7053_SPI, SPI_RxFIFOThreshold_QF);
    // Enable SPI  //
    SPI_Cmd(ATT7053_SPI, ENABLE);
}

//ATT7053从机输出是以 SCK 上升沿输出数据，需要主机下降沿采样数据
//ATT7053从机输入是从 SCK 下降沿采样数据，MSB在前，LSB在后
static uint8_t HalATT7053RwByte(uint8_t byte)//读或者写一个字节数据
{
    uint8_t tmp = 0x00;
    // Wait until the transmit buffer is empty //
    while(SPI_I2S_GetFlagStatus(ATT7053_SPI, SPI_I2S_FLAG_TXE) != SET);
    // Send the byte //
    SPI_SendData8(ATT7053_SPI, byte);
    // Wait to receive a byte //
    while(SPI_I2S_GetFlagStatus(ATT7053_SPI, SPI_I2S_FLAG_RXNE) != SET);
    // Return the byte read from the SPI bus //
    tmp = SPI_ReceiveData8(ATT7053_SPI);
    // Return read Data //
    return tmp;
}

//读ATT7053 BCKREG寄存器,用于spi数据通信校验
static void HalATT7053CheckBackupReg(uint8_t *src)
{
    uint8_t ucDataTemp[3];
    SysWait(1); //必须延时

    ATT7053_SPI_CS_LOW();                    // 片选
    HalATT7053RwByte(REG_ATT7053_BACKUPDATA | CMD_ATT7053_REGREAD); //Register地址(读)
    ucDataTemp[0] = HalATT7053RwByte(0xFF);   // 第一个字节,最高位
    ucDataTemp[1] = HalATT7053RwByte(0xFF);   // 第二个字节
    ucDataTemp[2] = HalATT7053RwByte(0xFF);   // 第三个字节,最低位

    ATT7053_SPI_CS_HIGH();                      // Disable

    if((src[0] != ucDataTemp[0])
            || (src[1] != ucDataTemp[1])
            || (src[2] != ucDataTemp[2]))
    {
        if(ATT7053_SPI_ERR_FLAG != g_Att7053Dat.ErrFlag)
        {
            g_Att7053Dat.ErrCount++;

            //连续三次出现通讯错误
            if(g_Att7053Dat.ErrCount > 3)
            {
                g_Att7053Dat.ErrFlag = ATT7053_SPI_ERR_FLAG;
            }
        }

        HalATT7053ReInit(); //重新初始化
    }
    else
    {
        g_Att7053Dat.ErrCount = 0x00;
    }
}


//写ATT7053寄存器,需要写入的数据，每一帧数据长度为3个byte
static void HalATT7053WrReg(uint8_t reg, uint8_t *dat)
{
    SysWait(1); //必须延时

    ATT7053_SPI_CS_LOW();                       // 片选
    HalATT7053RwByte(reg | CMD_ATT7053_REGWRITE);   //Register地址(写)
    HalATT7053RwByte(dat[0]);          // 第一个字节,最高位
    HalATT7053RwByte(dat[1]);          // 第二个字节
    HalATT7053RwByte(dat[2]);          // 第三个字节,最低位
    ATT7053_SPI_CS_HIGH();                      // Disable
}
//读ATT7053寄存器,需要读入的数据，每一帧数据长度为3个byte
static void HalATT7053RdReg(uint8_t reg, uint8_t *dat)
{
    SysWait(1);
    ATT7053_SPI_CS_LOW();                    // 片选
    HalATT7053RwByte(reg | CMD_ATT7053_REGREAD); //Register地址(读)
    dat[0] = HalATT7053RwByte(0xFF);   // 第一个字节,最高位
    dat[1] = HalATT7053RwByte(0xFF);   // 第二个字节
    dat[2] = HalATT7053RwByte(0xFF);   // 第三个字节,最低位
    ATT7053_SPI_CS_HIGH();                      // Disable

    HalATT7053CheckBackupReg(dat);
}

//ATT7053电压的有效值
int32_t HalATT7053GetURms(void)
{
    unsigned char reg_dat[3] = {0x00, 0x00, 0x00};
    int32_t URms = 0;
    HalATT7053RdReg(REG_ATT7053_RMS_U, reg_dat);  //读取电压有效值寄存器数据
    //计算电压有效值
    g_Att7053Dat.URmsReg = ((uint32_t)reg_dat[0]) * 0x10000 + ((uint32_t)reg_dat[1]) * 0x100 + reg_dat[2];
    URms = (int32_t)g_Att7053Dat.URmsReg * ATT7053AU_VAL_AMP / g_Att7053Dat.DKUrms;
		
		//os_log("REG_ATT7053_RMS_U: %02x %02x %02x  URms:%d",reg_dat[0],reg_dat[1] ,reg_dat[2],URms);
    return URms;
}

//ATT7053第一通道有功功率
int32_t HalATT7053GetP1(void)
{
    unsigned char reg_dat[3] = {0x00, 0x00, 0x00};
    int32_t PowerP1 = 0;

    HalATT7053RdReg(REG_ATT7053_POWERP1, reg_dat);  //读取第一通道有功功率寄存器值

    //计算第一通道有功功率
    g_Att7053Dat.PowerPReg = ((uint32_t)reg_dat[0]) * 0x10000 + ((uint32_t)reg_dat[1]) * 0x100 + reg_dat[2];
		
    if(g_Att7053Dat.PowerPReg < 0x800000)
    {
        //如果 PowerP1<2^23
        PowerP1 = g_Att7053Dat.PowerPReg;
    }
    else
    {
        //如果 PowerP1>=2^23
        PowerP1 = g_Att7053Dat.PowerPReg - 0x1000000;
    }
    PowerP1 = PowerP1 * ATT7053AU_VAL_AMP / g_Att7053Dat.DKpqs;
    if(PowerP1 < 0)
    {
        PowerP1 = 0;
    }
		//os_log("REG_ATT7053_POWERP1: %02x %02x %02x  PowerP1:%d",reg_dat[0],reg_dat[1] ,reg_dat[2],PowerP1);
    return PowerP1;
}

//ATT7053第一通道无功功率
int32_t HalATT7053GetQ1(void)
{
    unsigned char reg_dat[3] = {0x00, 0x00, 0x00};
    int32_t PowerQ1 = 0;

    HalATT7053RdReg(REG_ATT7053_POWERQ1, reg_dat);  //读取第一通道无功功率寄存器值

    //计算第一通道无功功率
    g_Att7053Dat.PowerQReg = ((uint32_t)reg_dat[0]) * 0x10000 + ((uint32_t)reg_dat[1]) * 0x100 + reg_dat[2];
    if(g_Att7053Dat.PowerQReg < 0x800000)
    {
        PowerQ1 = g_Att7053Dat.PowerQReg;    //如果 PowerQ1<2^23
    }
    else
    {
        PowerQ1 = g_Att7053Dat.PowerQReg - 0x1000000;    //如果 PowerQ1>=2^23
    }
    PowerQ1 = PowerQ1 * ATT7053AU_VAL_AMP / g_Att7053Dat.DKpqs;
    if(PowerQ1 < 0)
    {
        PowerQ1 = 0;
    }
    return PowerQ1;
}

//ATT7053有功能量
int32_t HalATT7053GetEP(void)
{
    unsigned char reg_dat[3] = {0x00, 0x00, 0x00};
    int32_t EnergyP = 0;

    HalATT7053RdReg(REG_ATT7053_ENERGY_P, reg_dat);  //读取有功能量寄存器值

    //计算有功能量
    EnergyP = ((uint32_t)reg_dat[0]) * 0x10000 + ((uint32_t)reg_dat[1]) * 0x100 + reg_dat[2];
    EnergyP = EnergyP / g_Att7053Dat.ECnt;
    return EnergyP;
}


//设置ATT7053的Gphs、GP寄存器
void HalATT7053SetGphsGP(uint32_t Gphs, uint32_t GP)
{
    unsigned char reg_dat[4] = {0x00, 0x00, 0x00};
    //校正GPhs1,角差校正值
    reg_dat[2] = 0xA6;
    HalATT7053WrReg(REG_ATT7053_WPREG, reg_dat); //打开写保护寄存器,0xA6:50~71,0xBC:40---45
    reg_dat[2] = Gphs;
    reg_dat[1] = Gphs >> 8; //校正GPhs1值
    HalATT7053WrReg(REG_ATT7053_GPHS1, reg_dat);
    reg_dat[2] = 0x00;
    HalATT7053WrReg(REG_ATT7053_WPREG, reg_dat); //关闭写保护寄存器

    //校正GP,GQ,GS,功率增益
    reg_dat[2] = 0xA6;
    HalATT7053WrReg(REG_ATT7053_WPREG, reg_dat); //打开写保护寄存器,0xA6:50~71,0xBC:40---45
    reg_dat[2] = GP;
    reg_dat[1] = GP >> 8;   //校正GP,GQ,GS值
    HalATT7053WrReg(REG_ATT7053_GP1, reg_dat);
    HalATT7053WrReg(REG_ATT7053_GQ1, reg_dat);
    HalATT7053WrReg(REG_ATT7053_GS1, reg_dat);
    reg_dat[2] = 0x00;
    HalATT7053WrReg(REG_ATT7053_WPREG, reg_dat); //关闭写保护寄存器
}


//ATT7053软件复位
static void HalATT7053SoftReset(void)
{
    uint8_t TempBuffer[4];

    TempBuffer[0] = 0x00;
    TempBuffer[1] = 0x00;
    TempBuffer[2] = 0x55;

    ATT7053_SPI_CS_LOW();                       // 片选
    HalATT7053RwByte(REG_ATT7053_SRSTREG | CMD_ATT7053_REGWRITE);   //Register地址(写)
    HalATT7053RwByte(TempBuffer[0]);          // 第一个字节,最高位
    HalATT7053RwByte(TempBuffer[1]);          // 第二个字节
    HalATT7053RwByte(TempBuffer[2]);          // 第三个字节,最低位
    ATT7053_SPI_CS_HIGH();                      // Disable

    SysWait(5);
}

//ATT7053引脚硬件复位
static void HalATT7053HwReset(void)
{
		os_log("HalATT7053HwReset");
    ATT7053_SPI_RESET_LOW();
    SysWait(5); //200us以上低电平
    ATT7053_SPI_RESET_HIGH();
    SysWait(5); //2000us以上
}

static uint8_t g_reInitCount = 0;
static void HalATT7053ParameterInit(void)
{
    uint8_t reg_dat[3];

    reg_dat[2] = 0xA6;
    reg_dat[1] = 0x00;
    reg_dat[0] = 0x00;
    HalATT7053WrReg(REG_ATT7053_WPREG, reg_dat); //打开写保护寄存器,0xA6:50~71,0xBC:40---45
    reg_dat[2] = I1Gain_16 | UGain_1; //电流通道1ADC增益*16，电压通道ADC增益*1
    reg_dat[1] = 0x00;
    reg_dat[0] = 0x00;
    HalATT7053WrReg(REG_ATT7053_ADCCON, reg_dat);
    reg_dat[2] = 0x00;
    reg_dat[1] = 0x00;
    reg_dat[0] = 0x00;
    HalATT7053WrReg(REG_ATT7053_WPREG, reg_dat); //关闭写保护寄存器

    //在校正数据存储区读取校准数据
    g_Att7053Dat.DKUrms = REG32(ATT_DKURMS_4BYTE_ADDR);
    g_Att7053Dat.DKpqs = REG32(ATT_DKPQS_4BYTE_ADDR);
    g_Att7053Dat.AdjGphs = REG16(ATT_GPHS_2BYTE_ADDR);
    g_Att7053Dat.AdjGP = REG16(ATT_GP_2BYTE_ADDR);
    g_Att7053Dat.ECnt = REG16(ATT_ENYCNT_2BYTE_ADDR);
		if(g_Att7053Dat.DKUrms == -1 && g_Att7053Dat.DKpqs == -1)
		{
			
			uint8_t paraInit[16] = {0xe1,0x21,0x00,0x00,  0x89,0x00,0x00,0x00,  0x00,0x00,0xaa,0xf6, 0x20,0x00,0xff,0xff};
			hal_flash_write((void *)ATT7053_PARA_START_ADDR, paraInit, sizeof(paraInit));
		}

    //校正HFconst
    reg_dat[2] = 0xA6;
    HalATT7053WrReg(REG_ATT7053_WPREG, reg_dat); //打开写保护寄存器,0xA6:50~71,0xBC:40---45
    reg_dat[2] = (uint8_t)ATT7053AU_HFCONST;
    reg_dat[1] = (uint8_t)(ATT7053AU_HFCONST >> 8);     //校正HFconst值
    reg_dat[0] = 0x00;
    HalATT7053WrReg(REG_ATT7053_HFCONST, reg_dat);
    reg_dat[2] = 0x00;
    reg_dat[1] = 0x00;
    reg_dat[0] = 0x00;
    HalATT7053WrReg(REG_ATT7053_WPREG, reg_dat); //关闭写保护寄存器

    //设置角差校正值GPhs1,功率增益GP,GQ,GS
    HalATT7053SetGphsGP(g_Att7053Dat.AdjGphs, g_Att7053Dat.AdjGP);
#if 1
		os_log("HalATT7053ParameterInit: addr:%02x  DKUrms:%d ,DKpqs: %d, AdjGphs:%d, AdjGP:%d, ECnt:%d", ATT7053_PARA_START_ADDR,g_Att7053Dat.DKUrms, g_Att7053Dat.DKpqs\
    ,g_Att7053Dat.AdjGphs \
    ,g_Att7053Dat.AdjGP \
    ,g_Att7053Dat.ECnt);
		
		//HalATT7053RdReg(REG_ATT7053_HFCONST, reg_dat);  //读取电压有效值寄存器数据
		//os_log("HalATT7053Init: %02x %02x %02x",reg_dat[0],reg_dat[1] ,reg_dat[2]);
		
		g_reInitCount++;
	  if(g_reInitCount >= 4)
		{
			os_log("HalATT7053ReInit count 4, reboot.");
			socket_reboot();
		}
#endif
}

//初始化ATT7053
void HalATT7053Init(void)
{
    g_Att7053Dat.ErrFlag = 0x00;
    g_Att7053Dat.ErrCount = 0x00;

    HalATT7053SpiInit();

    HalATT7053HwReset();

    SysWait(5); //2ms以上

    HalATT7053ParameterInit();
	
#if 0
	  unsigned char reg_dat[3] = {0x00, 0x00, 0x00};
    HalATT7053RdReg(REG_ATT7053_HFCONST, reg_dat);  //读取电压有效值寄存器数据
		os_log("HalATT7053Init: %02x %02x %02x",reg_dat[0],reg_dat[1] ,reg_dat[2]);
#endif
}

//重新初始化ATT7053
static void HalATT7053ReInit(void)
{
    HalATT7053HwReset();    //硬件复位

    SysWait(5);

    HalATT7053ParameterInit();  //att7053参数初始化

}

//重置ATT7953A的校验配置
void HalATT7053ResetAdjust(void)
{
    //设置角差校正值GPhs1,功率增益GP,GQ,GS
    g_Att7053Dat.AdjGphs = 0;
    g_Att7053Dat.AdjGP = 0;
    HalATT7053SetGphsGP(g_Att7053Dat.AdjGphs, g_Att7053Dat.AdjGP);

    g_Att7053Dat.DKUrms = 1;
    g_Att7053Dat.DKpqs = 1;
}

//校正ATT7053的角差校正值GPhs1
void HalATT7053AdjustGphs(int32_t Preal, int32_t Qreal)
{
    float a, b, c, d, Angle/*角度*/;

    HalATT7053GetP1();
    HalATT7053GetQ1();

    a = Preal * g_Att7053Dat.PowerQReg;
    b = Qreal * g_Att7053Dat.PowerPReg;
    c = Preal * g_Att7053Dat.PowerPReg;
    d = Qreal * g_Att7053Dat.PowerQReg;
    Angle = (a - b) / (c + d);
    if(Angle >= 0)
    {
        g_Att7053Dat.AdjGphs = (uint16_t)(Angle * 32768);
    }
    else
    {
        g_Att7053Dat.AdjGphs = (uint16_t)(Angle * 32768 + 65535);
    }
    g_Att7053Dat.AdjGP = 0;
    //设置角差校正值GPhs1
    HalATT7053SetGphsGP(g_Att7053Dat.AdjGphs, 0);
}

//校正ATT7053的电压系数
void HalATT7053AdjustVDK(int32_t Vreal)
{
    HalATT7053GetURms();
    g_Att7053Dat.DKUrms = (g_Att7053Dat.URmsReg * ATT7053AU_VAL_AMP) / Vreal;
}

//校正ATT7053的功率增益GP,GQ,GS
void HalATT7053AdjustGP(int32_t Preal, int32_t Qreal)
{
    float a, b, c, d, Angle/*角度*/, PowerP, P/*校正后的功率值*/, Pgain;
    HalATT7053GetP1();
    HalATT7053GetQ1();
    a = Preal * g_Att7053Dat.PowerQReg;
    b = Qreal * g_Att7053Dat.PowerPReg;
    c = Preal * g_Att7053Dat.PowerPReg;
    d = Qreal * g_Att7053Dat.PowerQReg;
    Angle = (a - b) / (c + d);
    PowerP = (float)g_Att7053Dat.PowerPReg + (float)g_Att7053Dat.PowerQReg * Angle;
    P = PowerP / 137.2;
    Pgain = Preal / P - 1;
    if(Pgain >= 0)
    {
        g_Att7053Dat.AdjGP = (uint16_t)(Pgain * 32768);
    }
    else
    {
        g_Att7053Dat.AdjGP = (uint16_t)(Pgain * 32768 + 65535);
    }

    //设置角差校正值GPhs1,功率增益GP,GQ,GS
    HalATT7053SetGphsGP(g_Att7053Dat.AdjGphs, g_Att7053Dat.AdjGP);
}

//校正ATT7053的功率值系数
void HalATT7053AdjustPDK(int32_t Preal)
{
    HalATT7053GetP1();
    g_Att7053Dat.DKpqs = (g_Att7053Dat.PowerPReg * ATT7053AU_VAL_AMP) / Preal;
}

//校正ATT7053的电量计数基准值
void HalATT7053AdjustEcnt(int16_t Ecnt)
{
    g_Att7053Dat.ECnt = Ecnt;
}

//将校正数据存储在FLASH中
uint8_t HalATT7053SaveAdjDat(void)
{
    uint32_t NbrOfPage = 0x00;
    uint32_t EraseCounter = 0x00;
    __IO FLASH_Status FLASHStatus = FLASH_COMPLETE;

    FLASH_Unlock();
    // Clear pending flags (if any)
    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR);
    // Define the number of page to be erased
    NbrOfPage = ATT7053_PARA_SIZE / FLASH_PAGE_SIZE;
    // Erase the FLASH pages //
    for(EraseCounter = 0; (EraseCounter < NbrOfPage) && (FLASHStatus == FLASH_COMPLETE); EraseCounter++)
    {
        if(FLASH_ErasePage(ATT7053_PARA_START_ADDR + (FLASH_PAGE_SIZE * EraseCounter)) != FLASH_COMPLETE)
        {
            return 0;
        }
    }
    // Program the user Flash area word by word
    //(area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/
    if(FLASH_ProgramWord(ATT_DKURMS_4BYTE_ADDR, g_Att7053Dat.DKUrms) != FLASH_COMPLETE)
    {
        return 0;
    }
    if(FLASH_ProgramWord(ATT_DKPQS_4BYTE_ADDR, g_Att7053Dat.DKpqs) != FLASH_COMPLETE)
    {
        return 0;
    }
    if(FLASH_ProgramHalfWord(ATT_GPHS_2BYTE_ADDR, g_Att7053Dat.AdjGphs) != FLASH_COMPLETE)
    {
        return 0;
    }
    if(FLASH_ProgramHalfWord(ATT_GP_2BYTE_ADDR, g_Att7053Dat.AdjGP) != FLASH_COMPLETE)
    {
        return 0;
    }
    if(FLASH_ProgramHalfWord(ATT_ENYCNT_2BYTE_ADDR, g_Att7053Dat.ECnt) != FLASH_COMPLETE)
    {
        return 0;
    }

    // Lock the Flash to disable the flash control register access (recommended
    // to protect the FLASH memory against possible unwanted operation) *********/
    FLASH_Lock();
    return 1;
}



uint32_t HalGetATT7053WorkStatus(void)
{
    if(ATT7053_SPI_ERR_FLAG == g_Att7053Dat.ErrFlag)
    {
        return 0xffffffff;
    }
    else
    {
        return 0x00;
    }
}

//att7053参数检查
void HalCheckATT7053Parameter(void *pt)
{
    uint8_t reg_dat[3] = {0x00, 0x00, 0x00};
    uint32_t uiTempData1, uiTempData2;

    pt = pt;

    HalATT7053RdReg(REG_ATT7053_HFCONST, reg_dat);  //读取HFCONST寄存器值
    //计算HFCONST值
    uiTempData1 = ((uint32_t)reg_dat[1]) * 0x100 + reg_dat[2];

    HalATT7053RdReg(REG_ATT7053_ADCCON, reg_dat);  //读取ADCCON寄存器值
    //计算ADCCON值
    uiTempData2 = ((uint32_t)reg_dat[1]) * 0x100 + reg_dat[2];

    if((ATT7053AU_HFCONST != uiTempData1) || (0x0c != uiTempData2))
    {
        HalATT7053ReInit();
    }
}


/* This polynomial ( 0xEDB88320L) DOES generate the same CRC values as ZMODEM
and PKZIP */
static const uint32_t crc32tab[] =
{
    0x00000000UL, 0x77073096UL, 0xee0e612cUL, 0x990951baUL,
    0x076dc419UL, 0x706af48fUL, 0xe963a535UL, 0x9e6495a3UL,
    0x0edb8832UL, 0x79dcb8a4UL, 0xe0d5e91eUL, 0x97d2d988UL,
    0x09b64c2bUL, 0x7eb17cbdUL, 0xe7b82d07UL, 0x90bf1d91UL,
    0x1db71064UL, 0x6ab020f2UL, 0xf3b97148UL, 0x84be41deUL,
    0x1adad47dUL, 0x6ddde4ebUL, 0xf4d4b551UL, 0x83d385c7UL,
    0x136c9856UL, 0x646ba8c0UL, 0xfd62f97aUL, 0x8a65c9ecUL,
    0x14015c4fUL, 0x63066cd9UL, 0xfa0f3d63UL, 0x8d080df5UL,
    0x3b6e20c8UL, 0x4c69105eUL, 0xd56041e4UL, 0xa2677172UL,
    0x3c03e4d1UL, 0x4b04d447UL, 0xd20d85fdUL, 0xa50ab56bUL,
    0x35b5a8faUL, 0x42b2986cUL, 0xdbbbc9d6UL, 0xacbcf940UL,
    0x32d86ce3UL, 0x45df5c75UL, 0xdcd60dcfUL, 0xabd13d59UL,
    0x26d930acUL, 0x51de003aUL, 0xc8d75180UL, 0xbfd06116UL,
    0x21b4f4b5UL, 0x56b3c423UL, 0xcfba9599UL, 0xb8bda50fUL,
    0x2802b89eUL, 0x5f058808UL, 0xc60cd9b2UL, 0xb10be924UL,
    0x2f6f7c87UL, 0x58684c11UL, 0xc1611dabUL, 0xb6662d3dUL,
    0x76dc4190UL, 0x01db7106UL, 0x98d220bcUL, 0xefd5102aUL,
    0x71b18589UL, 0x06b6b51fUL, 0x9fbfe4a5UL, 0xe8b8d433UL,
    0x7807c9a2UL, 0x0f00f934UL, 0x9609a88eUL, 0xe10e9818UL,
    0x7f6a0dbbUL, 0x086d3d2dUL, 0x91646c97UL, 0xe6635c01UL,
    0x6b6b51f4UL, 0x1c6c6162UL, 0x856530d8UL, 0xf262004eUL,
    0x6c0695edUL, 0x1b01a57bUL, 0x8208f4c1UL, 0xf50fc457UL,
    0x65b0d9c6UL, 0x12b7e950UL, 0x8bbeb8eaUL, 0xfcb9887cUL,
    0x62dd1ddfUL, 0x15da2d49UL, 0x8cd37cf3UL, 0xfbd44c65UL,
    0x4db26158UL, 0x3ab551ceUL, 0xa3bc0074UL, 0xd4bb30e2UL,
    0x4adfa541UL, 0x3dd895d7UL, 0xa4d1c46dUL, 0xd3d6f4fbUL,
    0x4369e96aUL, 0x346ed9fcUL, 0xad678846UL, 0xda60b8d0UL,
    0x44042d73UL, 0x33031de5UL, 0xaa0a4c5fUL, 0xdd0d7cc9UL,
    0x5005713cUL, 0x270241aaUL, 0xbe0b1010UL, 0xc90c2086UL,
    0x5768b525UL, 0x206f85b3UL, 0xb966d409UL, 0xce61e49fUL,
    0x5edef90eUL, 0x29d9c998UL, 0xb0d09822UL, 0xc7d7a8b4UL,
    0x59b33d17UL, 0x2eb40d81UL, 0xb7bd5c3bUL, 0xc0ba6cadUL,
    0xedb88320UL, 0x9abfb3b6UL, 0x03b6e20cUL, 0x74b1d29aUL,
    0xead54739UL, 0x9dd277afUL, 0x04db2615UL, 0x73dc1683UL,
    0xe3630b12UL, 0x94643b84UL, 0x0d6d6a3eUL, 0x7a6a5aa8UL,
    0xe40ecf0bUL, 0x9309ff9dUL, 0x0a00ae27UL, 0x7d079eb1UL,
    0xf00f9344UL, 0x8708a3d2UL, 0x1e01f268UL, 0x6906c2feUL,
    0xf762575dUL, 0x806567cbUL, 0x196c3671UL, 0x6e6b06e7UL,
    0xfed41b76UL, 0x89d32be0UL, 0x10da7a5aUL, 0x67dd4accUL,
    0xf9b9df6fUL, 0x8ebeeff9UL, 0x17b7be43UL, 0x60b08ed5UL,
    0xd6d6a3e8UL, 0xa1d1937eUL, 0x38d8c2c4UL, 0x4fdff252UL,
    0xd1bb67f1UL, 0xa6bc5767UL, 0x3fb506ddUL, 0x48b2364bUL,
    0xd80d2bdaUL, 0xaf0a1b4cUL, 0x36034af6UL, 0x41047a60UL,
    0xdf60efc3UL, 0xa867df55UL, 0x316e8eefUL, 0x4669be79UL,
    0xcb61b38cUL, 0xbc66831aUL, 0x256fd2a0UL, 0x5268e236UL,
    0xcc0c7795UL, 0xbb0b4703UL, 0x220216b9UL, 0x5505262fUL,
    0xc5ba3bbeUL, 0xb2bd0b28UL, 0x2bb45a92UL, 0x5cb36a04UL,
    0xc2d7ffa7UL, 0xb5d0cf31UL, 0x2cd99e8bUL, 0x5bdeae1dUL,
    0x9b64c2b0UL, 0xec63f226UL, 0x756aa39cUL, 0x026d930aUL,
    0x9c0906a9UL, 0xeb0e363fUL, 0x72076785UL, 0x05005713UL,
    0x95bf4a82UL, 0xe2b87a14UL, 0x7bb12baeUL, 0x0cb61b38UL,
    0x92d28e9bUL, 0xe5d5be0dUL, 0x7cdcefb7UL, 0x0bdbdf21UL,
    0x86d3d2d4UL, 0xf1d4e242UL, 0x68ddb3f8UL, 0x1fda836eUL,
    0x81be16cdUL, 0xf6b9265bUL, 0x6fb077e1UL, 0x18b74777UL,
    0x88085ae6UL, 0xff0f6a70UL, 0x66063bcaUL, 0x11010b5cUL,
    0x8f659effUL, 0xf862ae69UL, 0x616bffd3UL, 0x166ccf45UL,
    0xa00ae278UL, 0xd70dd2eeUL, 0x4e048354UL, 0x3903b3c2UL,
    0xa7672661UL, 0xd06016f7UL, 0x4969474dUL, 0x3e6e77dbUL,
    0xaed16a4aUL, 0xd9d65adcUL, 0x40df0b66UL, 0x37d83bf0UL,
    0xa9bcae53UL, 0xdebb9ec5UL, 0x47b2cf7fUL, 0x30b5ffe9UL,
    0xbdbdf21cUL, 0xcabac28aUL, 0x53b39330UL, 0x24b4a3a6UL,
    0xbad03605UL, 0xcdd70693UL, 0x54de5729UL, 0x23d967bfUL,
    0xb3667a2eUL, 0xc4614ab8UL, 0x5d681b02UL, 0x2a6f2b94UL,
    0xb40bbe37UL, 0xc30c8ea1UL, 0x5a05df1bUL, 0x2d02ef8dUL
};

//??CRC32
uint32_t HalCalCRC32(const unsigned char *buf, uint32_t size)
{
    uint32_t i, crc32;
    crc32 = 0xFFFFFFFF;
    for (i = 0; i < size; i++)
    {
        crc32 = crc32tab[(crc32 ^ buf[i]) & 0xff] ^ (crc32 >> 8);
    }
    return crc32 ^ 0xFFFFFFFF;
}





