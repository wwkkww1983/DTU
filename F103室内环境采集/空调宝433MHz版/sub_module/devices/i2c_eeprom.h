#ifndef I2C_EEPROM_H
#define I2C_EEPROM_H

void eeprom_init(void);
void eeprom_write_byte(unsigned int address, unsigned char data);
unsigned char eeprom_read_byte(unsigned int address);

#endif // I2C_EEPROM_H
