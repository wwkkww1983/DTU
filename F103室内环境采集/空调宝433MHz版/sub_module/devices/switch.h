#ifndef SWITCH_H
#define SWITCH_H
#include "os.h"
#include "device.h"

void switch_init(void);
void switch_start_work(void);
void switch_poll(void);
void switch_control(uint8 type, uint8 *content);
uint8 switch_get_state(uint8 *data, uint8 *len);
uint8 switch_button_pressed(uint8 released, uint16 time);

#endif

