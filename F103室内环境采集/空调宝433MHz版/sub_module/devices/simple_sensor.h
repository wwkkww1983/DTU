#ifndef SIMPLE_SENSOR_H
#define SIMPLE_SENSOR_H
#include "os.h"

void simple_sensor_init(void);
void simple_sensor_start_work(void);
void simple_sensor_poll(void);
void simple_sensor_control(uint8 type, uint8 *content);
uint8 simple_sensor_get_state(uint8 *data, uint8 *len);
uint8 simple_sensor_button_pressed(uint8 released, uint16 time);

#endif //SIMPLE_SENSOR_H
