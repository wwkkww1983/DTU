#include "windowmagnet0.h"
#include "../device.h"
#include "../button.h"

#ifdef WINDOWMAGNET0

void windowmagnet0_init()
{
  while(button_state() == BUTTON_PRESSED)
  {
    hal_wait_ms(5);
    press_time += 5;
  }
}

void windowmagnet0_start_work()
{

}

void windowmagnet0_poll()
{

}

void windowmagnet0_control(uint8 type, uint8 *content)
{

}

uint8 windowmagnet0_get_state(uint8 *data, uint8 *len)
{
  return 0;
}

uint8 windowmagnet0_button_pressed(uint8 released, uint16 time)
{
  //?????
  if(released == 1)
  {
  }
  return 0;
}



#endif // WINDOWMAGNET0
