#ifndef TEST_DEV_H
#define TEST_DEV_H
#include "os.h"
#include "sub_module.h"

void testdev_init(void);
void testdev_start_work(void);
void testdev_poll(void);
void testdev_control(uint8 type, uint8 *content);
uint8 testdev_get_state(uint8 *data, uint8 *len);
uint8 testdev_button_pressed(uint8 released, uint16 time);

#endif //TEST_DEV_H
