#include "testdev.h"
#include "../device.h"

#ifdef TESTDEV


void testdev_init()
{
  
}

void testdev_poll()
{
  
}

void testdev_start_work()
{
  
}

void testdev_control(uint8 type, uint8 *content)
{
  
}

uint8 testdev_get_state(uint8 *data, uint8 *len)
{
  
  return 0;
}

uint8 testdev_button_pressed(uint8 released, uint16 time)
{
  if(released)
  {
    
  }
  return 0;
}

#endif // TESTDEV
