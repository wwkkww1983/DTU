#ifndef SOCKET_H
#define SOCKET_H
#include "os.h"
#include "../device.h"

void _turn_on_off(uint8 onoff);
void socket_init(void);
void socket_start_work(void);
void socket_poll(void);
void socket_control(uint8 type, uint8 *content);
uint8 socket_get_state(uint8 *data, uint8 *len);
uint8 socket_button_pressed(uint8 released, uint16 time);
void socket_reboot(void);

#endif

