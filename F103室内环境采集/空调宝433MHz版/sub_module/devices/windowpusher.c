#include "windowpusher.h"
#include "../device.h"
#include "../button.h"


#ifdef WINDOWPUSHER



static void wp_stop(void *p)
{
  
}

static void set_stop()
{
  
}

static void wp_on()
{
  
}

static void wp_off()
{
  
}

void windowpusher_init()
{
  
}

void windowpusher_start_work()
{

}

void windowpusher_poll()
{

}

void windowpusher_control(uint8 type, uint8 *content)
{
 
}

uint8 windowpusher_get_state(uint8 *data, uint8 *len)
{
  return 0;
}

uint8 windowpusher_button_pressed(uint8 released, uint16 time)
{
  return 0;
}

#endif // WINDOWPUSHER
