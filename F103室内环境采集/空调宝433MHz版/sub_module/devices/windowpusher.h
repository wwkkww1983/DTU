#ifndef WINDOWPUSHER_H
#define WINDOWPUSHER_H
#include "os.h"

void windowpusher_init(void);
void windowpusher_start_work(void);
void windowpusher_poll(void);
void windowpusher_control(uint8 type, uint8 *content);
uint8 windowpusher_get_state(uint8 *data, uint8 *len);
uint8 windowpusher_button_pressed(uint8 released, uint16 time);

#endif //WINDOWPUSHER_H
