#include "bodysensor0.h"
#include "../device.h"

#ifdef BODYSENSOR0


void bodysensor0_init()
{

}

void bodysensor0_start_work()
{

}

void bodysensor0_poll()
{

}

void bodysensor0_control(uint8 type, uint8 *content)
{

}

uint8 bodysensor0_get_state(uint8 *data, uint8 *len)
{


  return 0;
}

uint8 bodysensor0_button_pressed(uint8 released, uint16 time)
{
  return 0;
}

void bodysensor0_before_sleep()
{

}

void bodysensor0_dect_fixup()
{

}

#endif // BODYSENSOR0
