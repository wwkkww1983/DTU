#ifndef BS102_H
#define BS102_H
#include "os.h"

void bs102_init(void);
void bs102_start_work(void);
void bs102_poll(void);
void bs102_control(uint8 type, uint8 *content);
uint8 bs102_get_state(uint8 *data, uint8 *len);
uint8 bs102_button_pressed(uint8 released, uint16 time);
void bs102_before_sleep(void);

void bs102_dect_fixup(void);

#endif //BS102_H
