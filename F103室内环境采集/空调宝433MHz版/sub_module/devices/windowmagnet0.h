#ifndef WINDOWMANGET0_H
#define WINDOWMANGET0_H
#include "os.h"

void windowmagnet0_init(void);
void windowmagnet0_start_work(void);
void windowmagnet0_poll(void);
void windowmagnet0_control(uint8 type, uint8 *content);
uint8 windowmagnet0_get_state(uint8 *data, uint8 *len);
uint8 windowmagnet0_button_pressed(uint8 released, uint16 time);

#endif //WINDOWMANGET0_H
