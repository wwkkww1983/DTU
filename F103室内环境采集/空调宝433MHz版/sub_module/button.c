#include "button.h"
#include "device.h"

#ifdef CONFIG_WITH_BUTTON

uint8 button_sleep_fig = 0;

void button_init()
{
#if 1
	GPIO_InitTypeDef GPIO_InitStruct; 
	EXTI_InitTypeDef EXTI_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	/* config the extiline(PB0) clock and AFIO clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	/* Configyre P[A|B|C|D|E]0  NIVC  */
	NVIC_InitStruct.NVIC_IRQChannel = EXTI4_15_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_11; 
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_Level_2;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP; // 上拉输入
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* EXTI line(PB0) mode config */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource11);
	EXTI_InitStruct.EXTI_Line = EXTI_Line11;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling; //下降沿中断
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStruct); 
#endif
}

uint8 button_state()
{
	if(((GPIOA->IDR >> 11)&0x01) == BUTTON_ACTIVE)
  {
    return BUTTON_PRESSED;
  }
  return BUTTON_RELEASED;
}

void Button_IRQHandler(void)
{
	uint8 dealed = 0;
  uint16 press_time = 0;
	
	if(EXTI_GetITStatus(EXTI_Line11) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line11);
#ifdef CONFIG_WITH_SLEEP
  		sub_module_sleep_set_enable(0);
#endif 
		if(button_sleep_fig == 1)
		{
			button_sleep_fig = 0;
			SystemInit();
		  hal_init();
		}

		while(button_state() == BUTTON_PRESSED && !dealed)
		{
			dealed = sub_module_button_pressed(0, press_time);
			press_time += 10;

			if(!dealed)
			{
				hal_wait_ms(10);
			}
		}
	
		if(!dealed)
		{
			sub_module_button_pressed(1, press_time);
		}

		EXTI_ClearITPendingBit(EXTI_Line11);
  }
	SystemInit();
}


#endif // CONFIG_WITH_BUTTON
