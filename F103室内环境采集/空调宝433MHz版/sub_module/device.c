#include "device.h"
#include "sub_module.h"
#include "../net/net.h"

#define SETUP_DEVICE_INSTANCE(dev_name, instance_name) \
  device_t instance_name = {\
    dev_name##_##init##,\
      dev_name##_##start_work##,\
        dev_name##_##poll##,\
          dev_name##_##control##,\
            dev_name##_##get_state##,\
              dev_name##_##button_pressed##,\
}

#ifdef TESTDEV
SETUP_DEVICE_INSTANCE(testdev, g_device);
#elif defined(BODYSENSOR0)
SETUP_DEVICE_INSTANCE(bodysensor0, g_device);
#elif defined(BS102)
SETUP_DEVICE_INSTANCE(bs102, g_device);
#elif defined(WINDOWMAGNET0)
SETUP_DEVICE_INSTANCE(windowmagnet0, g_device);
#elif defined(COLORLIGHT)
SETUP_DEVICE_INSTANCE(colorlight, g_device);
#elif defined(SOCKET)
SETUP_DEVICE_INSTANCE(socket, g_device);
#elif defined(SIMPLE_SENSOR)
SETUP_DEVICE_INSTANCE(simple_sensor, g_device);
#elif defined(SOCKET_TMP)
SETUP_DEVICE_INSTANCE(socket_tmp, g_device);
#elif defined(WINDOWPUSHER)
SETUP_DEVICE_INSTANCE(windowpusher, g_device);
#elif defined(WM102)
SETUP_DEVICE_INSTANCE(wm102, g_device);
#elif defined(LIGHTCONTROL)
SETUP_DEVICE_INSTANCE(lightcontrol, g_device);
#elif defined(SWITCH)
SETUP_DEVICE_INSTANCE(switch, g_device);
#elif defined(CURTAINCONTROL)
SETUP_DEVICE_INSTANCE(curtaincontrol, g_device);
#else
#error "未指定设备"
#endif // BODYSENSOR0

#define FOUCE_NOTIFY 6

static void _transmit_data(uint8 len);

//标记发送过notify 避免发送心跳
static uint8 g_send_notify = 0;
//标记发送失败，检测到发送失败则立即再次发送
static uint8 g_notify_fail = 0;
static uint8 g_enable = 1;

static void _transmit_data(uint8 len)
{
  g_notify_fail = 0;
  if(!app_transmit(APP_FRAME_DATA, 0, len))
  {
    g_notify_fail = 1;
  }
  else
  {
      os_log("trans success.........");
  }
#ifdef BODYSENSOR0
  bodysensor0_dect_fixup();
#endif // BODYSENSOR0
  
#ifdef BS102
  bs102_dect_fixup();
#endif // BS102
}

uint8 device_get_state(uint8 *data, uint8 *len)
{
  return g_device.get_state(data, len);
}

void device_init()
{
  g_device.init();
}

void device_start_work()
{
  g_device.start_work();
}


uint8 rf_recv_fig = 0;
extern uint8 rf_sleep_up;
os_time_t notify_time_wait;
static uint8 g_first = FOUCE_NOTIFY-2;

void device_poll()
{
  uint8 len = 0;
  uint8 *data = app_get_trans_payload();
  
  g_send_notify = 0;
  
  g_device.poll();
	
	if((OS_SYSTIME_BEYOND_SPAN(notify_time_wait, 10*1000))) //10s向网关上报一次数据  
	{
		g_first++;

		if((device_get_state(data + 1, &len)) 
			|| g_notify_fail 
		  || g_first == FOUCE_NOTIFY)
		{
			g_send_notify = 1;
				
			data[0] = DEVICE_NOTIFY;
			len++;     
			os_log("send  notify");
			_transmit_data(len);
			
			if(g_notify_fail)
			{
				device_control(DEVICE_HEARTBEAT, NULL);
			}
		}
		if(g_first == FOUCE_NOTIFY)
		{
				g_first = 0;
		}

		notify_time_wait = os_timer_get_systime();
	}

}

void device_control(uint8 type, uint8 *content)
{
  uint8 *data = app_get_trans_payload();
  uint8 len = 0;
  
  //通用控制
  switch(type)
  {
  case DEVICE_QUERY:
    device_get_state(data + 1, &len);
    data[0] = DEVICE_QUERY;
    len++;
    break;
    
  case DEVICE_HEARTBEAT:
    if(g_send_notify)
    {
      g_send_notify = 0;
      break;
    }
    
    //device_get_state(data + 1, &len);
    //发送数据是:状态值 使能 电压 上一跳节点地址
    data[0] = DEVICE_HEARTBEAT;
    len++;
    data[len++] = g_enable;
#if defined(BODYSENSOR0) || defined(WINDOWMAGNET0)
    data[len++] = (hal_adc_get_vol(7) + 50) / 100;
#elif defined(BS102)
    data[len++] = (hal_adc_get_vol(0) + 50) / 100;
#elif defined(WM102)
    hal_adc_get_vol(2);
    data[len++] = (hal_adc_get_vol(2) + 50) / 100 * 2;
    if(((hal_adc_get_vol(2) + 50) / 100 * 2) < 26)
    {
      PA_TABLE0 = 0x51; // pa power setting 0
    }
#else
    data[len++] = hal_adc_get_vol(1);
#endif // CONFIG_WITH_SLEEP
    data[len++] = link_get_father();
    
    break;
    
  case DEVICE_SETENABLE:
    
    if(content[0] == 1)
    {
      g_enable = 1;
      data[len++] = DEVICE_SETENABLE;
      data[len++] = g_enable;
    }
    else if(content[0] == 0)
    {
      g_enable = 0;
      data[len++] = DEVICE_SETENABLE;
      data[len++] = g_enable;
    }
    break;
    
  case DEVICE_START_UPGRADE:
    sub_module_start_upgrade(0);
    break;
    
  }
  
  if(len)
  {
    _transmit_data(len);
  }
  
  g_device.control(type, content);
}

uint8 device_button_pressed(uint8 released, uint16 time)
{
  return g_device.button_pressed(released, time);
}

uint8 device_send_fail()
{
  return g_notify_fail;
}

void device_send()
{
  uint8 len = 0;
  uint8 *data = app_get_trans_payload();
  
  g_send_notify = 0;
  
  device_control(DEVICE_HEARTBEAT, NULL);
  
  device_get_state(data + 1, &len);
				
  data[0] = DEVICE_NOTIFY;
  len++;
  os_log("first send");
  _transmit_data(len);
}

void device_first_report(void)
{
    device_send();
}
