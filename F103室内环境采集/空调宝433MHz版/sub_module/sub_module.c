#include "sub_module.h"
#include "device.h"
#include "button.h"


#define SLEEP_DEVICE_MAX_LINK_TIME       30000 //ms
#define MAX_CONNECT_RETRIES_IN_SLEEPMODE 5
#define ENTER_WORK_DELAY                 5000

typedef struct net_info
{
  uint8 segaddr[NWK_SEGADDR_LEN];
  uint8 myaddr;
  uint8 channel;
  uint8 key[NETKEY_LEN];
}net_info_t;

void _transmit_device_info(void *p);

typedef struct program_info
{
  uint8 version[VERSION_LEN];
  uint8 need_upgrade;
  uint8 work_channel;
}program_info_t;

enum button_pressed_action_t
{
  BUTTON_PRESSED_IDLE,
  BUTTON_PRESSED_BUILDNET,
  BUTTON_PRESSED_UPGRADE,
};

enum
{
  STATE_BUILDNET,
  STATE_CONNECTING,
  STATE_WORK,
};

static uint8 g_real_device_count = OS_MAX_DEVICE_COUNT;

#ifdef DEBUG
static uint8 g_facinfo[FACINFO_LEN+1] = "YHSK101DS4313100";
static uint8 g_devuid[DEVUID_LEN] = {'S', 'K', 101, 0, 0, 9, 9};//设备号每次加1    1200->456
//static uint8 g_facinfo[FACINFO_LEN] = "YHTH101DS4313101";
//static uint8 g_devuid[DEVUID_LEN] = {'T', 'H', 101, 0, 0, 1, 152}; 
#else
static const uint8 *g_facinfo= (uint8 *)DEVICE_INFO_FLASH_ADDR;
static const uint8 *g_devuid = (uint8 *)(DEVICE_INFO_FLASH_ADDR + FACINFO_LEN);
#endif // DEBUG

static net_info_t g_net_info;
static uint8 g_state = 0xff;
static uint8 g_later_enterwork = 0;
static os_time_t g_later_enterwork_time = 0;
static os_time_t g_next_connect_time = 0;
static os_time_t g_sub_upgrade = 0;
static os_time_t g_lastACKtime = 0;
static os_time_t g_lastRebootTime = 0;


#ifdef CONFIG_WITH_BUTTON
static uint8 g_button_pressed_action = BUTTON_PRESSED_IDLE;
#endif // CONFIG_WITH_BUTTON

#ifdef CONFIG_WITH_SLEEP
static uint8 g_sleep_on = 0;
#endif // CONIFG_WITH_SLEEP
static uint8 g_buttion_flag = 0;

static uint8 led_blink_fig = 1;
static uint8 led_blink_num = 0;
#ifdef SOCKET
		uint8 myaddr_num;
#endif

static void _connect_main_module()
{
  uint8 state_len;
  uint8 send_len = 0;
  uint8 *data = app_get_trans_payload();
	
#ifdef DEBUG
	uint8 tmp[4] = {1, 1, 1, 1};
	program_info_t program_info;
	memcpy(program_info.version, tmp, 4);
	memcpy(data, program_info.version, VERSION_LEN);
#else
  program_info_t *program_info = (program_info_t *)(PROGRAM_INFO_ADDR);
	memcpy(data, program_info->version, VERSION_LEN);
#endif
	
//#ifdef CONFIG_WITH_SLEEP
  static uint8 retries = 0;
//#endif // CONFIG_WITH_SLEEP

#ifdef CONFIG_WITH_LED
  hal_led_on(LED_PORT, LED_PIN);
#endif // CONFIG_WITH_LED
	
//  memcpy(data, program_info.version, VERSION_LEN);
  send_len += VERSION_LEN;
  device_get_state(data + send_len, &state_len);
  send_len += state_len;
	
  app_transmit(APP_FRAME_CONNECT, 0, VERSION_LEN);

  //休眠模式下只尝试连接一定次数之后休眠1小时
//#ifdef CONFIG_WITH_SLEEP
  if(retries < MAX_CONNECT_RETRIES_IN_SLEEPMODE)
  {
    retries++;
  }
  else
  {
	os_log("STATE_WORK!");
	retries = 0;
	g_state = STATE_WORK;
  }
//#endif // CONFIG_WITH_SLEEP
}

static void _start_connect_main_module()
{
  if(g_state == STATE_CONNECTING)
  {
    return;
  }
	
  g_state = STATE_CONNECTING;
  g_next_connect_time = os_timer_get_systime() + hal_rnd_range(1, 200) * 25;
//	os_log("w");
  _connect_main_module();

#ifdef CONFIG_WITH_LED
  //hal_led_off(LED_PORT, LED_PIN);
#endif // CONFIG_WITH_LED
}

static void _send_heartbeat(void *p)
{
  device_control(DEVICE_HEARTBEAT, NULL);
}

static void _connect_success()
{
  g_state = STATE_WORK;

#ifdef CONFIG_WITH_LED
  //hal_led_off(LED_PORT, LED_PIN);
#endif // CONFIG_WITH_LED

#ifdef CONFIG_WITH_SLEEP
  //连接成功开启休眠
  sub_module_sleep_set_enable(1);
  net_set_sleepmode(1);
#endif // CONFIG_WITH_SLEEP

  device_start_work();

#ifndef CONFIG_WITH_SLEEP
  os_timer_set(_send_heartbeat, 1000 + hal_rnd_range(1, 200) * 20, NULL, 0);
#else
  _send_heartbeat(NULL);
#endif // CONFIG_WITH_SLEEP
}

static void _enter_work()
{
  net_set_segaddr(g_net_info.segaddr);
  net_set_myaddr(g_net_info.myaddr);
  net_set_channel(g_net_info.channel);
//	os_log("channel is %d",g_net_info.channel);
  net_mesh_enable(1);
  _start_connect_main_module();
}

#ifdef CONFIG_WITH_LED
static void _enter_buildnet_led_blink(void *p)
{
  os_log("g_state = %d", g_state);
  if(g_state == STATE_BUILDNET)
  {
	led_blink_num++;
	if(led_blink_fig)
	{
		led_blink_num++;
		hal_led_toggle(LED_PORT, LED_PIN);
		os_timer_set(_enter_buildnet_led_blink, 300, NULL, 0);
		if(led_blink_num>=50)
		{
			led_blink_num = 0;
			led_blink_fig = 0;
		}
	}
	else
	{
		hal_led_on(LED_PORT, LED_PIN);
	}
  }
}
#endif // CONFIG_WITH_LED

static void _enter_buildnet()
{
  if(g_state == STATE_BUILDNET)
  {
		os_log("g_state = STATE_BUILDNET");
    return;
  }

#ifdef CONFIG_WITH_SLEEP
  //关闭休眠
  sub_module_sleep_set_enable(0);
  net_set_sleepmode(0);
#endif // CONFIG_WITH_SLEEP

  g_state = STATE_BUILDNET;
  net_set_segaddr(NWK_NULL_SEGADDR);
  net_set_myaddr(NWK_NULL_ADDR);
  net_set_channel(0);
  net_mesh_enable(0);

#ifdef CONFIG_WITH_LED
  _enter_buildnet_led_blink(NULL);
#endif // CONFIG_WITH_LED

#ifdef COLORLIGHT
  colorlight_set_color(0x0000ffff);
#endif // COLORLIGHT

}

void _later_enter_work(void *p)
{
  _enter_work();
}

void sub_module_init()
{
	uint8 i;

#ifdef CONFIG_WITH_SLEEP
  net_set_role(NWK_ROLE_ENDDEVICE);
#else
  net_set_role(NWK_ROLE_ROUTER);
#endif // CONFIG_WITH_SLEEP

  device_init();

#ifdef CONFIG_WITH_BUTTON
  button_init();
#endif // CONFIG_WITH_BUTTON

#ifdef CONFIG_WITH_LED
  hal_led_set_up(LED_PORT, LED_PIN);
#endif // CONFIG_WITH_LED

  memcpy(&g_net_info, (void *)NET_INFO_FLASH_ADDR, sizeof(g_net_info));
  _enter_buildnet();
  if(g_net_info.channel != 0xff)
  {
    g_later_enterwork = 1;
    g_later_enterwork_time = os_timer_get_systime() + ENTER_WORK_DELAY;
		myaddr_num = g_net_info.myaddr;
  }

  g_lastRebootTime = os_timer_get_systime();
  os_printf("******************************************\n");
  os_printf("Version: v1.0.0.2 \n");
  os_printf("BuildTime:%s %s\n",__DATE__,__TIME__);
  os_printf("Facinfo: %s\n",g_facinfo);
  os_printf("ManuUid:%c,%c,%d,%d,%d,%d,%d\n",g_devuid[0],g_devuid[1],g_devuid[2],g_devuid[3],g_devuid[4],g_devuid[5],g_devuid[6]);
	os_printf("DevUid:%c%c%d%d%d%02x%02x\n",g_devuid[0],g_devuid[1],g_devuid[2],g_devuid[3],g_devuid[4],g_devuid[5],g_devuid[6]);
  os_printf("Net Info: ");
  os_printf("addr: %d  channel:%d",g_net_info.myaddr, g_net_info.channel);
  os_printf("  segaddr:");
  for(i=0; i<NWK_SEGADDR_LEN; i++)
  {
	  os_printf("%02x", g_net_info.segaddr[i]);
  }
  
  os_printf("  key:");
  for(i=0; i<NETKEY_LEN; i++)
  {
	  os_printf("%02x", g_net_info.key[i]);
  }
  os_printf("\n");
  os_printf("******************************************\n");

}

void _transmit_device_info(void *p)
{
  uint8 send_len;
  uint8 *send_data;

  send_len = 0;
  send_data = app_get_trans_payload();
  memcpy(send_data, (void *)g_facinfo, FACINFO_LEN);
  send_len += FACINFO_LEN;
  memcpy(send_data + send_len, (void *)g_devuid, DEVUID_LEN);
  send_len += DEVUID_LEN;
  app_transmit(APP_FRAME_SEARCHDEVICE, NWK_BROADCAST_ADDR, send_len);
}

void sub_module_recv_deal(uint8 *data, uint8 len, uint8 *segaddr, uint8 srcaddr, uint8 dstaddr, uint8 rssi)
{
  app_header_t *app = (app_header_t *)data;

  os_log("");

  switch(app->type)
  {
  case APP_FRAME_SEARCHDEVICE:
		 os_log("APP_FRAME_SEARCHDEVICE");
    if(g_state != STATE_BUILDNET)
    {
      return;
    }

    os_timer_set(_transmit_device_info, hal_rnd_range(1, 20) * 45, NULL, 0);

    break;

  case APP_FRAME_ADDDEVICE:
		 os_log("APP_FRAME_ADDDEVICE");
    if(g_state != STATE_BUILDNET)
    {
      return;
    }

    if(memcmp(app->payload + 2, (void *)g_devuid, DEVUID_LEN) == 0)
    {
      memcpy(g_net_info.segaddr, segaddr, NWK_SEGADDR_LEN);
      g_net_info.myaddr = app->payload[0];
      g_net_info.channel = app->payload[1];
      memcpy(g_net_info.key, app->payload + 2 + DEVUID_LEN, NETKEY_LEN);

      net_set_myaddr(g_net_info.myaddr);
			#ifdef SOCKET
			myaddr_num = g_net_info.myaddr;
			#endif
      net_set_segaddr(g_net_info.segaddr);

      memcpy(app_get_trans_payload(), (void *)g_facinfo, FACINFO_LEN);
      if(app_transmit(APP_FRAME_ADDDEVICE, 0, FACINFO_LEN))
      {
        hal_flash_write((void *)NET_INFO_FLASH_ADDR, &g_net_info, sizeof(g_net_info));
        //校验
        if(memcmp((void *)NET_INFO_FLASH_ADDR, &g_net_info, sizeof(g_net_info)) == 0)
        {
          _enter_work();
          os_log("###add success.");
        }
      }
      else
      {
		os_log("####add failed");
        _enter_buildnet();
      }
    }
    break;

  case APP_FRAME_CONNECT:
		os_log("APP_FRAME_CONNECT");
    _connect_success();
    break;

  case APP_FRAME_DATA:
		os_log("APP_FRAME_DATA");
    device_control(app->payload[0], app->payload + 1);
    break;

  case APP_FRAME_DELDEVICE:
		os_log("APP_FRAME_DELDEVICE");
    //比较key值
    if(memcmp(g_net_info.key, app->payload, NETKEY_LEN) != 0)
    {
      break;
    }

#ifdef CONFIG_WITH_LED
    hal_led_blink(LED_PORT, LED_PIN, 5);
#endif // CONFIG_WITH_LED

    hal_flash_write((void *)NET_INFO_FLASH_ADDR, NULL, 0);
    HAL_REBOOT();
    break;
  }
}

//void sub_module_poll()
//{
//	device_poll();
//}

void sub_module_poll()
{
  if(g_buttion_flag && OS_SYSTIME_BEYOND_SPAN(g_sub_upgrade,1000))
  {
    g_buttion_flag = 0;
    g_sub_upgrade  = 0;
  }
//#ifdef CONFIG_WITH_SLEEP
  static os_time_t start_link_time = 0;
//#endif // CONFIG_WITH_SLEEP

  static os_time_t last_heartbeat_time = 0;
  os_time_t cur_time;

  //connect云盒
  if(g_state == STATE_CONNECTING && OS_SYSTIME_BEYOND(g_next_connect_time))
  {
    g_next_connect_time = os_timer_get_systime() + hal_rnd_range(1, 200) * 25;
    _connect_main_module();
		os_log("_connect_main_module");
  }

  //延迟进入工作模式
  if(g_state == STATE_BUILDNET && g_later_enterwork && OS_SYSTIME_BEYOND(g_later_enterwork_time))
  {
    g_later_enterwork = 0;
    _enter_work();
		os_log("_enter_work");
  }

  //升级和组网的按键处理
#ifdef CONFIG_WITH_BUTTON
  if(g_button_pressed_action == BUTTON_PRESSED_UPGRADE)
  {
    sub_module_start_upgrade(1);
    g_button_pressed_action = BUTTON_PRESSED_IDLE;
  }

  if(g_button_pressed_action == BUTTON_PRESSED_BUILDNET)
  {
    _enter_buildnet();
    g_button_pressed_action = BUTTON_PRESSED_IDLE;
  }
#endif // CONFIG_WITH_BUTTON

  if(g_state == STATE_WORK)
  {

    goto work;
  }
	
//	os_log("g_state != STATE_WORK");
  return;
work:

//#ifdef CONFIG_WITH_SLEEP
//  if(g_sleep_on)
//  {
    //30秒无法建立连接则休眠
    if(!link_is_build() || device_send_fail())
    {
        //强制休眠
        //hal_sleep_pm2(555);
    }
    else
    {
      //休眠之前再检查一遍设备状态
     // device_poll();
     // hal_sleep_pm2(555);
    }
//  }
//#endif // CONIFG_WITH_SLEEP

  device_poll();

  //TODO 心跳可能有问题
  cur_time = os_timer_get_systime();
  if(cur_time - last_heartbeat_time > MIN_HEARTBEAT_SPAN
     && (((os_timer_get_nettime() / 1000) % g_real_device_count
          == (net_get_myaddr() % g_real_device_count))
         ||(cur_time - last_heartbeat_time > (g_real_device_count + 1) *1000L)))
  {
		os_log("send heartbeat");
		last_heartbeat_time = cur_time;
		device_control(DEVICE_HEARTBEAT, NULL);

		if(OS_SYSTIME_BEYOND_SPAN(g_lastACKtime, 5*60*1000)
			|| OS_SYSTIME_BEYOND_SPAN(g_lastRebootTime, 12*60*60*1000))
		{
			//TODO  5分钟收不到心跳回复，重启设备，或者连续运行12h
			os_log("RF need reboot!!");
			socket_reboot();
		}
  }
}

void sub_module_enter_buildnet()
{
  _enter_buildnet();
}

uint8 *sub_module_get_key(uint8 addr)
{
  return g_net_info.key;
}


void sub_module_ack_sync_content_cb(uint8 *data, uint8 len)
{
	g_lastACKtime = os_timer_get_systime();
	if(len == 5)
	{
		os_time_t time;
		memcpy(&time, data, 4);
		os_timer_set_nettime(time);
		//os_timer_set_time();
		g_real_device_count = data[sizeof(os_time_t)];
		if(g_real_device_count == 0)
		{
			g_real_device_count = 1;
		}
	}
}

uint8 sub_module_ack_sync_content_set(uint8 *data)
{
	os_time_t time;
	time = os_timer_get_nettime();
	memcpy(data,&time,4);
	//(*(os_time_t *)data) = os_timer_get_nettime();
	data[sizeof(os_time_t)] = g_real_device_count;
	return 5;
}

#ifdef CONFIG_WITH_SLEEP
uint8 sub_module_is_in_sleep()
{
  return g_sleep_on;
}

void sub_module_sleep_set_enable(uint8 enable)
{
  g_sleep_on = enable;
}

#endif // CONFIG_WITH_SLEEP

void sub_module_start_upgrade(uint8 is_forced)
{
#ifndef DEBUG
  program_info_t program_info;
  memset(program_info.version, 0xff, VERSION_LEN);
  program_info.need_upgrade = 1;

  if(is_forced)
  {
    //置成0xff则进入手动升级模式
    program_info.work_channel = 0xff;
  }
  else
  {
    program_info.work_channel = g_net_info.channel;
  }

  //hal_flash_write((void *)PROGRAM_INFO_ADDR, &program_info, sizeof(program_info_t));
#ifdef CONIFG_WITH_LED
  hal_led_blink(LED_PORT, LED_PIN, 5);
#endif // CONFIG_WITH_LED

  HAL_REBOOT();
#endif //DEBUG
}

const uint8 *sub_module_devuid()
{
  return g_devuid;
}

void sub_module_reset_factory(const uint8 *devuid)
{
  if(memcmp(devuid, (void *)g_devuid, DEVUID_LEN) != 0)
  {
    return;
  }

  hal_flash_write((void *)NET_INFO_FLASH_ADDR, NULL, 0);
}

uint8 sub_module_button_pressed(uint8 released, os_time_t time)
{
#ifdef CONFIG_WITH_BUTTON
  if(!released && time > 20000 )//清空电量数据
  {
  	os_log("sub_module_reset_factory");
	int32 flash_clear = 0;
	hal_flash_write((void *)ATT_SAVED_T_ENERGY, &flash_clear, sizeof(flash_clear));
	HAL_REBOOT();
  }
  if(released && time > 5000 && time < 8000)//清空组网信息
  { 
  	hal_flash_write((void *)NET_INFO_FLASH_ADDR, NULL, 0);
	
	  HAL_REBOOT();
#if 0
    if(g_buttion_flag)
    {
       g_button_pressed_action = BUTTON_PRESSED_UPGRADE;
       g_buttion_flag = 0;
       return 0;
    }  
    g_buttion_flag = 1;
    g_sub_upgrade = os_timer_get_systime();
    return 0;
#endif
  }
#endif //CONFIG_WITH_BUTTON
  return device_button_pressed(released, time);
}
