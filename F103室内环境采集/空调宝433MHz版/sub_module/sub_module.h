#ifndef SUB_MODULE_H
#define SUB_MODULE_H
#include "os.h"
#include "../app/app.h"
#include "../net/net.h"

#ifndef DEBUG
#define DEBUG
#endif

#define SLEEP_SPAN              540  //seconds
#define MIN_HEARTBEAT_SPAN      10000 //ms
#define ATT_SAVED_T_ENERGY      (uint32_t)0x0800F800  //62  * 1024
#define DEVICE_INFO_FLASH_ADDR  (uint32_t)0x0800F400  //(61 * 1024)
#define NET_INFO_FLASH_ADDR     (uint32_t)0x0800F000  //(60 * 1024)
#define PROGRAM_INFO_ADDR       (uint32_t)0x0800EC00  //(19 * 1024)


void sub_module_init(void);
void sub_module_poll(void);
void sub_module_recv_deal(uint8 *data, uint8 len, uint8 *segaddr, uint8 srcaddr, uint8 dstaddr, uint8 rssi);
void sub_module_enter_buildnet(void);

uint8 sub_module_is_in_sleep(void);
void sub_module_sleep_set_enable(uint8 enable);

uint8 *sub_module_get_key(uint8 addr);
void sub_module_ack_sync_content_cb(uint8 *data, uint8 len);
uint8 sub_module_ack_sync_content_set(uint8 *data);

uint8 sub_module_button_pressed(uint8 released, os_time_t time);

const uint8 *sub_module_devuid(void);
void sub_module_start_upgrade(uint8 is_forced);
void sub_module_reset_factory(const uint8 *devuid);

//设备配置项

//#define TESTDEV
//#define BODYSENSOR0  //休眠红外设备
#define SOCKET
//#define WINDOWMAGNET0
//#define COLORLIGHT
//#define SIMPLE_SENSOR // 烟感 气感
//#define SOCKET_TMP
//#define WINDOWPUSHER
//#define BS102
//#define WM102
//#define SWITCH
//#define LIGHTCONTROL
//#define CURTAINCONTROL

#if !defined(COLORLIGHT) && !defined(WINDOWPUSHER)
#define CONFIG_WITH_LED
#endif // COLORLIGHT

//#ifdef CONFIG_WITH_LED
//#define LED_PORT GPIOA
//#define LED_PIN  GPIO_Pin_8
//#endif // CONFIG_WITH_LED

#if defined(BODYSENSOR0) || defined(WINDOWMAGNET0) || defined(SWITCH) || defined(BS102) || defined(WM102)
#define CONFIG_WITH_SLEEP
#endif // defined(BODYSENSOR_0) || defined(WINDOWMAGNET_0)

#ifndef WINDOWPUSHER
#define CONFIG_WITH_BUTTON
#endif

#endif // SUB_MODULE_H
