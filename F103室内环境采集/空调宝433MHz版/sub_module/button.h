#ifndef BUTTON_H
#define BUTTON_H
#include "os.h"
#include "sub_module.h"

#ifndef BUTTON_PORT
#define BUTTON_PORT 1
#endif // BUTTON_PORT

#ifndef BUTTON_PIN
#define BUTTON_PIN 11
#endif // BUTTON_PIN

#ifndef BUTTON_ACTIVE
#define BUTTON_ACTIVE 0
#endif // BUTTON_ACTIVE

#define BUTTON_PRESSED   1
#define BUTTON_RELEASED   2

void button_init(void);
uint8 button_state(void);
void Button_IRQHandler(void);

#endif //BUTTON_H
