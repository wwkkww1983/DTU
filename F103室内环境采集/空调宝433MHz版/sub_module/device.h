#ifndef DEVICE_H
#define DEVICE_H
#include "os.h"
#include "sub_module.h"
#include "devices/testdev.h"
#include "devices/bodysensor0.h"
#include "devices/windowmagnet0.h"
#include "devices/colorlight.h"
#include "devices/socket.h"
#include "devices/simple_sensor.h"
#include "devices/socket_tmp.h"
#include "devices/windowpusher.h"
#include "devices/bs102.h"
#include "devices/switch.h"
#include "devices/wm102.h"
#include "devices/lightcontrol.h"
#include "devices/curtaincontrol.h"

typedef void (*device_init_func_t)(void);
typedef void (*device_start_work_func_t)(void);
typedef void (*device_poll_func_t)(void);
typedef void (*device_control_func_t)(uint8 type, uint8 *content);
typedef uint8 (*device_get_state_func_t)(uint8 *data, uint8 *len);
typedef uint8 (*device_button_pressed_func_t)(uint8 released, uint16 time);
typedef void (*device_before_sleep_func_t)(void);

typedef struct device
{
  device_init_func_t init;
  device_start_work_func_t start_work;
  device_poll_func_t poll;
  device_control_func_t control;
  device_get_state_func_t get_state;
  device_button_pressed_func_t button_pressed;
  device_before_sleep_func_t before_sleep;
}device_t;

//设备控制码
#define DEVICE_QUERY                0x01            //设备查询命令
#define DEVICE_NOTIFY               0x02            //设备通知命令
#define DEVICE_CTRL                 0x03            //设置设备的状态
#define DEVICE_HEARTBEAT			      0x04			      //主动查询设备心跳
#define DEVICE_SETENABLE            0x05            //设备报警使能
#define DEVICE_STARTUP              0x06            //设备启动消息
#define DEVICE_START_UPGRADE        0x07            //设备开始升级

uint8 device_send_fail(void);

//初始化
void device_init(void);

//开始工作
void device_start_work(void);

//轮询
void device_poll(void);

//控制
void device_control(uint8 type, uint8 *content);

//获取状态
uint8 device_get_state(uint8 *data, uint8 *len);

//按键处理(可选)
uint8 device_button_pressed(uint8 released, uint16 time);

//开机并且绑定后首次上报数据
void device_first_report(void);

#endif // DEVICE_H
