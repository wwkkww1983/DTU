#ifndef LINK_H
#define LINK_H
#include "os.h"
#include "net.h"

typedef struct
{
  uint8 dstaddr;
  uint8 nextaddr;
  uint8 sleepmode;
}link_record_t;

typedef struct
{
  uint8 type;
  uint8 payload[];
}link_header_t;

void link_set_stable(uint8 is_stable);
void link_init(void);
void link_poll(void);
link_record_t *link_get(uint8 addr);
uint8 link_state(void);
void link_request(void);
void link_disattach(void);
uint8 link_is_build(void);
uint8 link_is_child(uint8 addr);
uint8 link_get_father(void);
void link_add_record(uint8 dstaddr, uint8 nextaddr, uint8 sleepmode);
link_record_t *link_get_record(uint8 addr);
void link_recv_cb(link_header_t *plink, uint8 srcaddr, uint8 lastaddr, uint8 sleepmode, int8 rssi);

#endif // _LINK_H
