#ifndef __NWK_H_
#define __NWK_H_
#include "os.h"
#include "net.h"

#define NWK_END_CONTENT_LEN     1    //1byte信号强度
#define NWK_ACK_CONTENT_LEN     5    //4byte时间,1byte设备数量

#define NWK_DEF_RETRIES         3
#define NWK_BROADCAST_ADDR      0xff
#define NWK_NULL_SEGADDR        "0000"
#define NWK_NULL_ADDR           NWK_BROADCAST_ADDR - 1
#define NWK_MAX_HOPS            5
#define NWK_SEGADDR_LEN         4

#define NWK_FLAG_SIMPLE         1
#define NWK_FLAG_RELAY          2

enum nwk_frame_t
{
  NWK_FRAME_NORMAL = 1,
  NWK_FRAME_LINK,
  NWK_FRAME_ACK_ONEHOP,
  NWK_FRAME_ACK_LINK,
  NWK_FRAME_ACK_SUBCACHE,
};

enum nwk_role_t
{
  NWK_ROLE_ROUTER = 1,
  NWK_ROLE_ENDDEVICE,
  NWK_ROLE_COORDINATOR,
};

enum nwk_sleepmode_t
{
  NWK_SLEEPMODE_NORMAL,
  NWK_SLEEPMODE_SLEEP
};

typedef struct
{
  uint8 type;
  uint8 sleepmode;
  uint8 segaddr[NWK_SEGADDR_LEN];
  uint8 srcaddr;
  uint8 dstaddr;
  uint8 sender_addr;
  uint8 recver_addr;
  uint8 trans_id;
  uint8 payload[];
}nwk_header_t;

void nwk_init(void);
void nwk_set_segaddr(const uint8 *segaddr);
uint8 *nwk_get_segaddr(void);
void nwk_set_myaddr(uint8 addr);
uint8 nwk_get_myaddr(void);
void nwk_set_role(uint8 role);
uint8 nwk_get_role(void);
void nwk_set_sleepmode(uint8 sleepmode);

void nwk_trans_cb(nwk_header_t *pnwk);
void nwk_recv_cb(nwk_header_t *pnwk, uint8 len, int8 rssi);
uint8 nwk_transmit(uint8 type, uint8 addr, uint8 len, uint8 flag);
uint8 nwk_transmit_onehop(uint8 addr, uint8 len);
void nwk_poll(void);
void *nwk_get_trans_payload(void);
void nwk_mesh_enable(uint8 enable);
void nwk_reply_acks(void);
void nwk_subcache_del(uint8 addr);

#endif
