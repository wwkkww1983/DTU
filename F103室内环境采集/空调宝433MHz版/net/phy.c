#include "phy.h"
#include "net.h"
#include "ctrl.h"
//#include "hal/dma/dma.h"
#include "../hal/hal_wait.h"
#include "sub_module.h"
#include "device.h"

INT8U SPI_CC1101_ReadID(void);
INT8U halSpiReadReg(INT8U addr) ;

enum {
  RADIOMODE_RX = 1,
  RADIOMODE_TX,
  RADIOMODE_IDLE,
};


#define CCA_RETRIES       50
#define TOTX_TIME         2  //ms
#define TORX_TIME         2  //ms
#define MAX_TX_TIME       20    //ms
#define MAX_RF_WAIT       20    //ms 
#define UNIT_BACKOFFTIME  5     //ms
#define MAX_CTRL_DATA_LEN   20

#define CRC_CHECK(x) (x[x[0] + 1 + 1] & ( 1 << 7))

//DMA
//static uint8 g_rx_dma_chn;
//static DMA_DESC *g_rx_dma_desc;
//static uint8 g_tx_dma_chn;
//static DMA_DESC *g_tx_dma_desc;

//控制帧缓存
static uint8 g_lock_ctrl_buf = 0;
static uint8 g_ctrl_buf[sizeof(phy_header_t) + MAX_CTRL_DATA_LEN];

//发送接收缓存
static uint8 g_tx_buf[PHY_MAX_LEN + 64]; //用于发送升级包
static uint8 g_rx_buf[PHY_MAX_LEN + 2];  //CRC, RSSI

void _rx_dma_cb()
{

  phy_header_t *phy = (phy_header_t *)g_rx_buf;

  hal_rnd_feed_seed(g_rx_buf[g_rx_buf[0] + 1]);

  if(!CRC_CHECK(g_rx_buf))
  {
    goto recv_err;
  }

  //长度错误
  if(g_rx_buf[0] > PHY_MAX_LEN - 1)
  {
    goto recv_err;
  }

  //正常通信
  if(phy->type == PHY_TYPE_NORMAL)
  {
    nwk_recv_cb((nwk_header_t *)(g_rx_buf + sizeof(phy_header_t)), \
      g_rx_buf[0] - PHY_HEAD_LEN, g_rx_buf[g_rx_buf[0] + 1]);
  }
  else
  {
    //特殊控制帧
    if(!g_lock_ctrl_buf)
    {
      g_lock_ctrl_buf = 1;
      memcpy(g_ctrl_buf, phy, sizeof(g_ctrl_buf));
    }

  }

recv_err:
	//RFIF &= ~IRQ_DONE;
  phy_set_rx_state(1);
}

void phy_rf_config_upgrade()
{
 
}

void phy_rf_config_normal()
{
	halRfWriteRfSettings();
}

void _radio_dma_init()
{

}

static uint8 _radio_set_mode(uint8 mode)
{
  //uint8 istate;
  uint8 res = 0;
	uint8 MARCSTATE = 0;
	uint8 i;

  //OS_ENTER_CRITICAL(istate);
	MARCSTATE = halSpiReadStatus(CCxxx0_MARCSTATE);

  if(MARCSTATE != MARC_STATE_RX \
    || MARCSTATE != MARC_STATE_TX \
      || MARCSTATE != MARC_STATE_IDLE)
  {
    //SIDLE();
		halSpiStrobe(CCxxx0_SIDLE);
//    while((halSpiReadStatus(CCxxx0_MARCSTATE)) != MARC_STATE_IDLE);
  }

  switch(mode)
  {
  case RADIOMODE_RX:
    while(!((halSpiReadStatus(CCxxx0_MARCSTATE)) == MARC_STATE_RX))
    {
			halSpiStrobe(CCxxx0_SIDLE);
			halSpiStrobe(CCxxx0_SRX);
			hal_wait_ms(2);
			if(i>100)
			{
				break;
			}
			i++;
    }
		
		res = 1;
		
    break;
  case RADIOMODE_TX:
    //STX();
		halSpiStrobe(CCxxx0_SIDLE);
		halSpiStrobe(CCxxx0_STX);
//    hal_wait_us_cond(TOTX_TIME, (halSpiReadStatus(CCxxx0_MARCSTATE)) == MARC_STATE_TX);
    if((halSpiReadStatus(CCxxx0_MARCSTATE)) == MARC_STATE_TX)
    {
      res = 1;
    }
    break;
  default:
    if((halSpiReadStatus(CCxxx0_MARCSTATE)) == MARC_STATE_IDLE)
      break;
    //SIDLE();
		halSpiStrobe(CCxxx0_SIDLE);
//    while((halSpiReadStatus(CCxxx0_MARCSTATE)) != MARC_STATE_IDLE);
  }
  //OS_EXIT_CRITICAL(istate);
  return res;
}

void phy_init1(void)
{
	phy_rf_config_normal();
	
	//WOR唤醒功能初始化
//	CC1101_WOR_Init(3000/*us*/, 2000/*ms*/);	//CC1101进入WOR模式
	
	GPIO_GDO0_ENABLE();
	halSpiStrobe(CCxxx0_SFRX);
	halSpiStrobe(CCxxx0_SRX);

  hal_wait_us(10);
  //hal_rnd_feed_seed(halSpiReadStatus(CCxxx0_RSSI));
}

void phy_init()
{
	NVIC_Configuration();
	SPI_CC1101_Init();
	CC1101_POWER_RESET();
	phy_rf_config_normal();

	GPIO_GDO0_ENABLE();
	halSpiStrobe(CCxxx0_SRX);

  hal_wait_us(10);
  //hal_rnd_feed_seed(halSpiReadStatus(CCxxx0_RSSI));
		
   os_log("CC1101 Version [%02x]\n", SPI_CC1101_ReadID());
}

void phy_set_rx_state(uint8 on_off)
{
  if(on_off)
  {
		_radio_set_mode(RADIOMODE_RX);
  }
  else
  {
		_radio_set_mode(RADIOMODE_IDLE);
  }
}

uint8 _is_not_busy()
{
	return 1;
//  _radio_set_mode(RADIOMODE_RX);
//  //等待一会才能检测到CCA
//  hal_wait_us(10);
//	
//  return ((halSpiReadStatus(CCxxx0_PKTSTATUS))&CCA_FLAG);
}

uint8 phy_transmit(uint8 type, uint8 len)
{
	uint8 res = 0;
  uint8 retries = CCA_RETRIES;

  g_tx_buf[0] = len + PHY_HEAD_LEN;
  g_tx_buf[1] = 55;
  g_tx_buf[2] = type;

  while(retries--)
  {
    //CSMA-CA
    if(!_is_not_busy())
    {
      hal_wait_ms(hal_rnd_range(1, 8) * UNIT_BACKOFFTIME);
      continue;
    }

			halSpiStrobe(CCxxx0_SIDLE);             //进入IDLE模式//			halSpiStrobe(CCxxx0_SRX);
			GPIO_GDO0_DISABLE(); //关闭中断
			halRfSendPacket(g_tx_buf, g_tx_buf[0]+2);
			EXTI_ClearITPendingBit(CC1101_GDO0_EXTI_LINE);
			GPIO_GDO0_ENABLE(); //打开中断
//			u8  leng = 64+2;
//			if (halRfReceivePacket(g_rx_buf,&leng)) // 读数据并判断正确与否 //如果接的字节数不为0
//			{
//				_rx_dma_cb();
//			}		
//      hal_wait_ms_cond(MAX_TX_TIME, _tx_done());
      res = 1;
      break;

  }
	phy_set_rx_state(1);
  return res;
}

void phy_set_channel(uint8 channel)
{
	os_log("phy_set_channel %d", channel);
	halSpiWriteReg(CCxxx0_CHANNR,	channel);
	_radio_set_mode(RADIOMODE_RX);
}

void phy_poll()
{
  if(g_lock_ctrl_buf)
  {
    ctrl_deal((phy_header_t *)g_ctrl_buf);
    g_lock_ctrl_buf = 0;
  }
}

void *phy_get_trans_payload()
{
  return ((phy_header_t *)g_tx_buf)->payload;
}

uint8 _tx_done()
{
	uint8 ret = 0;
  ret = (halSpiReadStatus(CCxxx0_MARCSTATE)) == MARC_STATE_RX || (halSpiReadStatus(CCxxx0_MARCSTATE)) == MARC_STATE_IDLE;
  return ret;
}
void NVIC_Configuration()
{
		NVIC_InitTypeDef   NVIC_InitStructure;
		
			  /* Enable and set EXTI3 Interrupt */
		NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
}
void SPI_CC1101_Init(void)
{
	SPI_InitTypeDef  SPI_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	// Periph clock enable //
	RCC_AHBPeriphClockCmd(CC1101_SPI_CS_GPIO_CLK | CC1101_SPI_MOSI_GPIO_CLK |
	CC1101_SPI_EN_GPIO_CLK | CC1101_SPI_MISO_GPIO_CLK | CC1101_SPI_SCK_GPIO_CLK, ENABLE);

	// CC1101_SPI Periph clock enable //
	RCC_APB2PeriphClockCmd(CC1101_SPI_CLK, ENABLE);

	// Configure CC1101_SPI pins: SCK //
	GPIO_InitStructure.GPIO_Pin = CC1101_SPI_SCK_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(CC1101_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

	// Configure CC1101_SPI pins: MISO //
	GPIO_InitStructure.GPIO_Pin = CC1101_SPI_MISO_PIN;
	GPIO_Init(CC1101_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

	// Configure CC1101_SPI pins: MOSI //
	GPIO_InitStructure.GPIO_Pin = CC1101_SPI_MOSI_PIN;
	GPIO_Init(CC1101_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

	// Configure CC1101_SPI_CS pin: CC1101 CS pin //
	GPIO_InitStructure.GPIO_Pin = CC1101_SPI_CS_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(CC1101_SPI_CS_GPIO_PORT, &GPIO_InitStructure);
	CC1101_SPI_CS_HIGH();

	// Configure CC1101_SPI_EN pin: CC1101 EN pin //
	//GPIO_InitStructure.GPIO_Pin = CC1101_EN_GPIO_PIN;
	//GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	//GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	//GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	//GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	//GPIO_Init(CC1101_EN_GPIO_PORT, &GPIO_InitStructure);
	//CC1101_ENABLE();
	
	GPIO_InitStructure.GPIO_Pin =RF_IQR_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_2;
  GPIO_Init(RF_IQR_PORT , &GPIO_InitStructure);

	// Connect PXx  //
	GPIO_PinAFConfig(CC1101_SPI_SCK_GPIO_PORT, CC1101_SPI_SCK_SOURCE, CC1101_SPI_SCK_AF);
	GPIO_PinAFConfig(CC1101_SPI_MOSI_GPIO_PORT, CC1101_SPI_MOSI_SOURCE, CC1101_SPI_MOSI_AF);
	GPIO_PinAFConfig(CC1101_SPI_MISO_GPIO_PORT, CC1101_SPI_MISO_SOURCE, CC1101_SPI_MISO_AF);

	// SPI configuration -------------------------------------------------------//
	SPI_I2S_DeInit(CC1101_SPI);
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;	
	SPI_Init(CC1101_SPI, &SPI_InitStructure);	
	
	SPI_RxFIFOThresholdConfig(CC1101_SPI, SPI_RxFIFOThreshold_QF);
	// Enable SPI  //
	
//	dma_to_radio();
	
	SPI_Cmd(CC1101_SPI, ENABLE);	
}


void GPIO_GDO0_ENABLE(void)
{
		//os_log("GPIO_GDO0_ENABLE");
	  EXTI_InitTypeDef EXTI_InitStructure;
	//TODO   chenlh
		//RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
   		//中断初始化
    /* Connect GDO0 EXTI Line to Button GPIO Pin */
		SYSCFG_EXTILineConfig(RF_IQR_PORT_SOURCE, RF_IQR_SOURCE);
    /* Configure GDO0 EXTI line */	
		
		EXTI_InitStructure.EXTI_Line = RF_IQR_EXTI;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling; //上升沿中断
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
		EXTI_Init(&EXTI_InitStructure); 
}

void GPIO_GDO0_DISABLE(void)
{
		//os_log("GPIO_GDO0_DISABLE");
	  EXTI_InitTypeDef EXTI_InitStructure;
   		//中断初始化
    /* Connect GDO0 EXTI Line to Button GPIO Pin */
//		SYSCFG_EXTILineConfig(RF_IQR_PORT_SOURCE, RF_IQR_SOURCE);
    /* Configure GDO0 EXTI line */			
		EXTI_InitStructure.EXTI_Line = RF_IQR_EXTI;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling; //上升沿中断
		EXTI_InitStructure.EXTI_LineCmd = DISABLE;
		EXTI_Init(&EXTI_InitStructure); 
}

/*******************************************************************************
* Function Name  : SPI_FLASH_ReadByte
* Description    : Reads a byte from the SPI Flash.
*                  This function must be used only if the Start_Read_Sequence
*                  function has been previously called.
* Input          : None
* Output         : None
* Return         : Byte Read from the SPI Flash.
*******************************************************************************/
u8 SPI_FLASH_ReadByte(void)
{
  return (SPI_FLASH_SendByte(Dummy_Byte));
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SendByte
* Description    : Sends a byte through the SPI interface and return the byte
*                  received from the SPI bus.
* Input          : byte : byte to send.
* Output         : None
* Return         : The value of the received byte.
*******************************************************************************/
u8 SPI_FLASH_SendByte(u8 byte)
{
  /* Loop while DR register in not emplty */
//  while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	hal_wait_ms_cond(MAX_RF_WAIT, !SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
  /* Send byte through the SPI2 peripheral */
  SPI_SendData8(SPI1, byte);

  /* Wait to receive a byte */
//  while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
	hal_wait_ms_cond(MAX_RF_WAIT, !SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
  /* Return the byte read from the SPI bus */
  return SPI_ReceiveData8(SPI1);
}

/**********************************CC1101********************/

void Delay(uint32 nCount)
{
  int i,j;
  for(j=0;j<nCount;j++)
  {
     for(i=0;i<10;i++);
  }
}


INT8U SPI_CC1101_ReadID(void)
{
	 INT8U id;
	 SPI_FLASH_CS_LOW();
	 	 
	 SPI_FLASH_SendByte(CCxxx0_SFSTXON);
	 id = SPI_FLASH_SendByte(0xff);
	 SPI_FLASH_CS_HIGH();

	 return id;
}

void CC1101_POWER_RESET(void)
{
  SPI_FLASH_CS_HIGH();
  Delay(30);
  SPI_FLASH_CS_LOW();
  Delay(30);
  SPI_FLASH_CS_HIGH();
  Delay(45);
  SPI_FLASH_CS_LOW();
//  while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );//waite SO =0
  hal_wait_ms_cond(MAX_RF_WAIT, !GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN));
	SPI_FLASH_SendByte(CCxxx0_SRES);
//  while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );//waite SO =0 again 
	hal_wait_ms_cond(MAX_RF_WAIT, !GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN));
  SPI_FLASH_CS_HIGH(); 
}


void halSpiWriteReg(INT8U addr, INT8U value) 
{
    SPI_FLASH_CS_LOW();
//    while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN));
		hal_wait_ms_cond(MAX_RF_WAIT, !GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN));
    SPI_FLASH_SendByte(addr);		//写地址
    SPI_FLASH_SendByte(value);		//写入配置
    SPI_FLASH_CS_HIGH(); 
}

//*****************************************************************************************
//函数名：void halSpiWriteBurstReg(INT8U addr, INT8U *buffer, INT8U count)
//输入：地址，写入缓冲区，写入个数
//输出：无
//功能描述：SPI连续写配置寄存器
//*****************************************************************************************
void halSpiWriteBurstReg(INT8U addr, INT8U *buffer, INT8U count) 
{
    INT8U i, temp;
		temp = addr | WRITE_BURST;
    SPI_FLASH_CS_LOW();
//    while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );
		hal_wait_ms_cond(MAX_RF_WAIT, !GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN));
    SPI_FLASH_SendByte(temp);
    for (i = 0; i < count; i++)
		{
        SPI_FLASH_SendByte(buffer[i]);
    }
    SPI_FLASH_CS_HIGH(); 
}

//*****************************************************************************************
//函数名：void halSpiStrobe(INT8U strobe)
//输入：命令
//输出：无
//功能描述：SPI写命令
//*****************************************************************************************
void halSpiStrobe(INT8U strobe) 
{
    SPI_FLASH_CS_LOW();
//    while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );
		hal_wait_ms_cond(MAX_RF_WAIT, !GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN));
    SPI_FLASH_SendByte(strobe);		//写入命令
    SPI_FLASH_CS_HIGH();
}

//*****************************************************************************************
//函数名：void halRfSendPacket(INT8U *txBuffer, INT8U size)
//输入：发送的缓冲区，发送数据个数
//输出：无
//功能描述：CC1100发送一组数据
//*****************************************************************************************

void halRfSendPacket(INT8U *txBuffer, INT8U size) 
{
	halSpiStrobe(CCxxx0_SFTX);

  halSpiWriteBurstReg(CCxxx0_TXFIFO, txBuffer, size);	//写入要发送的数据

  halSpiStrobe(CCxxx0_STX);		//进入发送模式发送数据	
    // Wait for GDO0 to be set -> sync transmitted
//  while (!GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) );//while (!GDO0);
	hal_wait_ms_cond(MAX_RF_WAIT, (GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN)));
  // Wait for GDO0 to be cleared -> end of packet
//  while (GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) );// while (GDO0);
	hal_wait_ms_cond(MAX_RF_WAIT, (!GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) ));
}

//*****************************************************************************************
//函数名：void halRfSendData(INT8U txData)
//输入：发送的数据
//输出：无
//功能描述：CC1100发送一个数据
//*****************************************************************************************

void halRfSendData(INT8U txData)
{	
	
		halSpiStrobe(CCxxx0_SFTX);
    halSpiWriteReg(CCxxx0_TXFIFO, txData);	//写入要发送的数据

    halSpiStrobe(CCxxx0_STX);		//进入发送模式发送数据	

    // Wait for GDO0 to be set -> sync transmitted
    hal_wait_ms_cond(MAX_RF_WAIT, (GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) ));
	// Wait for GDO0 to be cleared -> end of packet
		hal_wait_ms_cond(MAX_RF_WAIT, (!GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) ));

}

//*****************************************************************************************
//函数名：INT8U halSpiReadReg(INT8U addr)
//输入：地址
//输出：该寄存器的配置字
//功能描述：SPI读寄存器
//*****************************************************************************************
INT8U halSpiReadReg(INT8U addr) 
{
	INT8U temp, value;
    temp = addr|READ_SINGLE;//读寄存器命令
	SPI_FLASH_CS_LOW();
//	while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );//MISO
	hal_wait_ms_cond(MAX_RF_WAIT, !(GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN)));
	SPI_FLASH_SendByte(temp);
	value = SPI_FLASH_SendByte(0);
	 SPI_FLASH_CS_HIGH();
	return value;
}

//*****************************************************************************************
//函数名：void halSpiReadBurstReg(INT8U addr, INT8U *buffer, INT8U count)
//输入：地址，读出数据后暂存的缓冲区，读出配置个数
//输出：无
//功能描述：SPI连续写配置寄存器
//*****************************************************************************************
void halSpiReadBurstReg(INT8U addr, INT8U *buffer, INT8U count) 
{
    INT8U i,temp;
	temp = addr | READ_BURST;		//写入要读的配置寄存器地址和读命令
    SPI_FLASH_CS_LOW();
 //    while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN));
	hal_wait_ms_cond(MAX_RF_WAIT, (!GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN)));
	SPI_FLASH_SendByte(temp);   
    for (i = 0; i < count; i++) 
	{
        buffer[i] = SPI_FLASH_SendByte(0);
    }
    SPI_FLASH_CS_HIGH();
}

//*****************************************************************************************
//函数名：INT8U halSpiReadReg(INT8U addr)
//输入：地址
//输出：该状态寄存器当前值
//功能描述：SPI读状态寄存器
//*****************************************************************************************
INT8U halSpiReadStatus(INT8U addr) 
{
    INT8U value,temp;
		temp = addr | READ_BURST;		//写入要读的状态寄存器的地址同时写入读命令
    SPI_FLASH_CS_LOW();
//    while (GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN) );
		hal_wait_ms_cond(MAX_RF_WAIT, !(GPIO_ReadInputDataBit(RF_MISO_PORT,RF_MISO_PIN)));
    SPI_FLASH_SendByte(temp);
	value = SPI_FLASH_SendByte(0);
	SPI_FLASH_CS_HIGH();
	return value;
}

INT8U halRfReceivePacket(INT8U *rxBuffer, INT8U *length) 
{
    //INT8U status[2];
    INT8U packetLength;
	 
    if ((halSpiReadStatus(CCxxx0_RXBYTES) & BYTES_IN_RXFIFO)) //如果接的字节数不为0
		{
	    packetLength = halSpiReadReg(CCxxx0_RXFIFO);//读出第一个字节，此字节为该帧数据长度
			rxBuffer[0] = packetLength;//读出第一个字节，此字节为该帧数据长度
        if(packetLength <= *length) 		//如果所要的有效数据长度小于等于接收到的数据包的长度
				{
            halSpiReadBurstReg(CCxxx0_RXFIFO, rxBuffer+1, packetLength+2); //读出所有接收到的数据
            *length = packetLength;				//把接收数据长度的修改为当前数据的长度
        
            // Read the 2 appended status bytes (status[0] = RSSI, status[1] = LQI)
//            halSpiReadBurstReg(CCxxx0_RXFIFO, status, 2); 	//读出CRC校验位
						halSpiStrobe(CCxxx0_SFRX);		//清洗接收缓冲区
            return 1;//(status[1] & CRC_OK);			//如果校验成功返回接收成功
        }
		 else 
			{
            *length = packetLength;
            halSpiStrobe(CCxxx0_SFRX);		//清洗接收缓冲区
            return 1;
			}
    } 
	else
 	return 0;
}

void halRfWriteRfSettings(void) 
{
#if 0
		halSpiWriteReg(CCxxx0_IOCFG0,		0x06);  //GDO0 Output Pin Configuration
		halSpiWriteReg(CCxxx0_PKTLEN,		0xFF);  //Packet Length
		halSpiWriteReg(CCxxx0_PKTCTRL0,	0x05);  //Packet Automation Control
		halSpiWriteReg(CCxxx0_ADDR,			0x00);    //Device Address
//			halSpiWriteReg(CCxxx0_CHANNR,		0x00);  //Channel Number
		halSpiWriteReg(CCxxx0_FSCTRL1,	0x06); //Frequency Synthesizer Control
		halSpiWriteReg(CCxxx0_FREQ2,		0x10);   //Frequency Control Word, High Byte
		halSpiWriteReg(CCxxx0_FREQ1,		0xB1);   //Frequency Control Word, Middle Byte
		halSpiWriteReg(CCxxx0_FREQ0,		0x3B);   //Frequency Control Word, Low Byte
		halSpiWriteReg(CCxxx0_MDMCFG4,	0x2A); //Modem Configuration
		halSpiWriteReg(CCxxx0_MDMCFG3,	0x83); //Modem Configuration
		halSpiWriteReg(CCxxx0_MDMCFG2,	0x13); //Modem Configuration
		halSpiWriteReg(CCxxx0_DEVIATN,	0x62); //Modem Deviation Setting
		halSpiWriteReg(CCxxx0_MCSM0,		0x18);   //Main Radio Control State Machine Configuration
	  halSpiWriteReg(CCxxx0_MCSM1,		0x3f);
		halSpiWriteReg(CCxxx0_FOCCFG,		0x16);  //Frequency Offset Compensation Configuration
		halSpiWriteReg(CCxxx0_WORCTRL,	0xFB); //Wake On Radio Control
		halSpiWriteReg(CCxxx0_FSCAL3,		0xE9);  //Frequency Synthesizer Calibration
		halSpiWriteReg(CCxxx0_FSCAL2,		0x2A);  //Frequency Synthesizer Calibration
		halSpiWriteReg(CCxxx0_FSCAL1,		0x00);  //Frequency Synthesizer Calibration
		halSpiWriteReg(CCxxx0_FSCAL0,		0x1F);  //Frequency Synthesizer Calibration
		halSpiWriteReg(CCxxx0_TEST0,		0x09);   //Various Test Settings
		halSpiWriteReg(CCxxx0_PATABLE,	0xc0);   //Various Test Settings
		
		halSpiStrobe(CCxxx0_SIDLE);    //CCxxx0_SIDLE	 0x36 //空闲状态
		halSpiStrobe(CCxxx0_SCAL);    //CCxxx0_SIDLE	 0x36 //空闲状态
#else
	halSpiWriteReg(CCxxx0_IOCFG0,       0x06);   //GDO0 Output Pin Configuration
    halSpiWriteReg(CCxxx0_PKTLEN,       0x3D);   //Packet Length: 61 data + 2 status +1 length=64, never overflow,make sure rx fifo empty before receive data
    halSpiWriteReg(CCxxx0_PKTCTRL0,     0x05);   //Packet Automation Control
    halSpiWriteReg(CCxxx0_ADDR,         0x00);   //Device Address
    halSpiWriteReg(CCxxx0_FSCTRL1,      0x0C);   //Frequency Synthesizer Control ,IF=152.34 kHz
    halSpiWriteReg(CCxxx0_FSCTRL0,      0x00);
    halSpiWriteReg(CCxxx0_FREQ2,        0x10);   //Frequency Control Word, High Byte
    halSpiWriteReg(CCxxx0_FREQ1,        0xB1);   //Frequency Control Word, Middle Byte
    halSpiWriteReg(CCxxx0_FREQ0,        0x3B);   //Frequency Control Word, Low Byte
    halSpiWriteReg(CCxxx0_MDMCFG4,      0x2A);   //Modem Configuration
    halSpiWriteReg(CCxxx0_MDMCFG3,      0x83);   //Modem Configuration
    halSpiWriteReg(CCxxx0_MDMCFG2,      0x13);   //Modem Configuration
    halSpiWriteReg(CCxxx0_MDMCFG1,      0x22);//+
    halSpiWriteReg(CCxxx0_MDMCFG0,      0xf8);//+

    halSpiWriteReg(CCxxx0_DEVIATN,      0x62);   //Modem Deviation Setting
    halSpiWriteReg(CCxxx0_MCSM0,        0x18);   //Main Radio Control State Machine Configuration
    halSpiWriteReg(CCxxx0_FOCCFG,       0x1D);//0x16);   //Frequency Offset Compensation Configuration//
    halSpiWriteReg(CCxxx0_BSCFG,        0x1C);//+
    halSpiWriteReg(CCxxx0_WORCTRL,      0xFB);   //Wake On Radio Control

    halSpiWriteReg(CCxxx0_AGCCTRL2,     0xC7);//+AGC Control
    halSpiWriteReg(CCxxx0_AGCCTRL1,     0x00);//+AGC Control
    halSpiWriteReg(CCxxx0_AGCCTRL0,     0xB0);//+AGC Control

    halSpiWriteReg(CCxxx0_FREND1,       0xB6); //+ Front End RX Configuration
    halSpiWriteReg(CCxxx0_FSCAL3,       0xE9);   //Frequency Synthesizer Calibration//??
    halSpiWriteReg(CCxxx0_FSCAL2,       0x2A);   //Frequency Synthesizer Calibration
    halSpiWriteReg(CCxxx0_FSCAL1,       0x00);   //Frequency Synthesizer Calibration
    halSpiWriteReg(CCxxx0_FSCAL0,       0x1F);   //Frequency Synthesizer Calibration
    halSpiWriteReg(CCxxx0_TEST0,        0x09);   //Various Test Settings

    halSpiWriteReg(CCxxx0_FREND0, 0x10);
	//TODO
	//halSpiWriteReg(CCxxx0_PATABLE,	0xc0);

    halSpiStrobe(CCxxx0_SIDLE);    //CCxxx0_SIDLE    0x36 //空闲状态
    halSpiStrobe(CCxxx0_SCAL);    //CCxxx0_SIDLE     0x36 //空闲状态
#endif

//	os_log("cc1101 read: %02x", halSpiReadReg(CCxxx0_WORCTRL));
}

uint8 rf_sleep_up = 0;
extern uint8 rf_recv_fig;
extern uint8 myaddr_num;

void EXTI4_15_IRQHandler(void)
{
	u8  leng = 64+2;
	uint8 i_state;
//	u8 i;
	OS_ENTER_CRITICAL(i_state);
	if(EXTI_GetITStatus(CC1101_GDO0_EXTI_LINE) != RESET)
  {
		 if (halRfReceivePacket(g_rx_buf,&leng)) // 读数据并判断正确与否
		 {
			 
			 halSpiStrobe(CCxxx0_SIDLE);    //CCxxx0_SIDLE	 0x36 //空闲状态
			 halSpiStrobe(CCxxx0_SRX);
			 if(leng <= 64)
			 {
			 
			 if(g_rx_buf[0]==4 && (g_rx_buf[3] == myaddr_num))
			 {
				 if(g_rx_buf[4])
				 {
						_turn_on_off(g_rx_buf[4]);
						rf_sleep_up = 1;
				 }
				 else
				 {
						_turn_on_off(g_rx_buf[4]);
						rf_sleep_up = 1;
				 }
			 }
			 else
			 {
					_rx_dma_cb();
				 rf_recv_fig = 1;
				//os_log("control");
			 }
		 		/* os_log("g_rx_buf %d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",g_rx_buf[0],\
									g_rx_buf[1],g_rx_buf[2],g_rx_buf[3],g_rx_buf[4],g_rx_buf[5],g_rx_buf[6] \
												,g_rx_buf[7],g_rx_buf[8],g_rx_buf[9],g_rx_buf[10],g_rx_buf[11],g_rx_buf[12]);
			 */
		 }
	 }
//		 os_log("cc recv！");
		 EXTI_ClearITPendingBit(CC1101_GDO0_EXTI_LINE);
  }
	else 
	{
		Button_IRQHandler();
	}
	OS_EXIT_CRITICAL(i_state);
}

//#ifdef CONFIG_WITH_SLEEP
//______________________________________________________________________________

uint16_t CC1101_WOR_Init(uint32_t Time)
{
    uint32 EVENT0=0;
    uint16 WOR_RES=1;
    uint16 WOR_rest=1; //2^(5*WOR_RES)
        
    if(Time<15 | Time>61946643) return 0;
    if(Time<1890) WOR_RES=0;
    else if(Time<60494) WOR_RES=1;
    else if(Time<1935832) WOR_RES=2;
    else if(Time<61946643) WOR_RES=3;

    WOR_rest <<= 5*WOR_RES;
    EVENT0 = 26000000/1000;
    if(EVENT0>Time)
    {
    EVENT0 = EVENT0*Time;
    EVENT0 = EVENT0/(750*WOR_rest);
    }
    else
    {
    EVENT0 = (Time/(750*WOR_rest))*EVENT0;
    }
    halSpiStrobe(CCxxx0_SIDLE);
    //halSpiWriteReg(CCxxx0_MCSM2, 0x10);
    // Enable automatic FS calibration when going from IDLE to RX/TX/FSTXON (in between EVENT0 and EVENT1)
    //halSpiWriteReg(CCxxx0_MCSM0, 0x18); 
    //halSpiWriteReg(CCxxx0_WOREVT1, (uint8)(EVENT0>>8)); // High byte Event0 timeout
    //halSpiWriteReg(CCxxx0_WOREVT0, (uint8)EVENT0); // Low byte Event0 timeout.
    //halSpiWriteReg(CCxxx0_WORCTRL, 0x78| WOR_RES); //tEvent1 =0111

    halSpiWriteReg(CCxxx0_IOCFG2, 0x06);
    halSpiStrobe(CCxxx0_SFRX);
    halSpiStrobe(CCxxx0_SWORRST);
    halSpiStrobe(CCxxx0_SWOR);
    return 1;
}
/*uint16_t CC1101_WOR_Init(uint16_t RxTimeout_us, uint16_t Tevent0_ms)
{
	uint32_t duty, duty_array[2]={125001,19531};	//单位1，×1000000
	uint16_t Tevent0_Max[2]={1890, 60493};	//单位ms
	int8_t i, j;
	uint16_t EVENT0;    //WOREVT1:WOREVT0
	float T_event0;
	
	for(i=6; i>=0; i--)
	{
		for(j=0; j<2; j++)
		{
			duty = duty_array[j]>>(i-j*3);
			if(duty > 150)	//是可以使用的占空比
			{
				T_event0 = (float)RxTimeout_us/duty*1000;
				if((T_event0 < Tevent0_Max[j]) && (T_event0 < Tevent0_ms))
				{
					if(j)
						EVENT0 = (uint16_t)(T_event0*1.083333333);
					else
						EVENT0 = (uint16_t)(T_event0*34.666666667);
					
					//配置WOR需要用到的寄存器
//					halSpiStrobe(CCxxx0_SIDLE); //空闲模式
					halSpiWriteReg(CCxxx0_IOCFG2, 0x24);//GDO0 设置成中断触发IO 当有数据过来时 置低
					halSpiWriteReg(CCxxx0_IOCFG0, 0x06);
					halSpiWriteReg(CCxxx0_WORCTRL, 0x78);//0x48 | j); //tEvent1时间设置为0.444ms，tEvent1=0111
					halSpiWriteReg(CCxxx0_WOREVT1, 0x68);//(uint8_t)(EVENT0>>8));	//事件0时间，高位
					halSpiWriteReg(CCxxx0_WOREVT0, 0x68);//(uint8_t)EVENT0); 		//事件0时间，低位
					halSpiWriteReg(CCxxx0_MCSM2, 0x10);//i-j*3);	//RX_TIME[2:0]相关
					halSpiWriteReg(CCxxx0_MCSM0, 0x18); //在TX,RX后 自动校准XSOC时限 (10) 149-155uS
					
//					halSpiStrobe(CCxxx0_SFRX);		//清空接收缓冲区
					halSpiStrobe(CCxxx0_SWORRST); 	//复位到 事件1
					halSpiStrobe(CCxxx0_SIDLE); //空闲模式
					halSpiStrobe(CCxxx0_SWOR); 		//启动WOR
				
					return (uint16_t)T_event0;
				}
			}
		}	
	}
	return 0;
}*/
//______________________________________________________________________________
/**********************************************************
** 函数名称: CC1101_WOR_Send
** 功能描述: CC1101连续发送同一个数据包，唤醒WOR设备
** 输　  入: *txBuffer:缓存区，size:数据长度，cnt:发送次数
** 输    出: 
** 全局变量: 
** 调用模块: 
** 说    明： 
** 注    意：
***********************************************************/ 
void CC1101_WOR_Send(uint8_t *txBuffer, uint8_t size, uint16_t cnt)
{
	uint16_t i;
	
	halSpiStrobe(CCxxx0_SIDLE);
	halSpiStrobe(CCxxx0_SFSTXON);	
	for(i=0; i<cnt; i++)
	{		
		halSpiWriteReg(CCxxx0_TXFIFO, size);
		halSpiWriteBurstReg(CCxxx0_TXFIFO, txBuffer, size);	//写入要发送的数据
		halSpiStrobe(CCxxx0_STX);		//进入发送模式发送数据	

    hal_wait_ms_cond(MAX_RF_WAIT, (GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) ));

		hal_wait_ms_cond(MAX_RF_WAIT, (!GPIO_ReadInputDataBit(RF_IQR_PORT,RF_IQR_PIN) ));
	}
}

void RF_CC1101_SLEEP(void)
{	
	halSpiStrobe(CCxxx0_SPWD); 
	halSpiStrobe(CCxxx0_SWOR); 		//启动WOR
}

void g_rx_buf_clear(void)
{
	memset(g_rx_buf,0 ,sizeof(g_rx_buf));
}


//#endif

