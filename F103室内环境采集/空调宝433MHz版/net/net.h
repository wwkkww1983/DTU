#ifndef NET_H
#define NET_H
#include "phy.h"
#include "nwk.h"
#include "link.h"
#include "../hal/hal_wdt.h"
#include "ctrl.h"

#define net_set_sleepmode(sleepmode) nwk_set_sleepmode(sleepmode)
#define net_set_channel(chn) phy_set_channel(chn)
#define net_set_segaddr(addr) nwk_set_segaddr(addr)
#define net_set_myaddr(addr) nwk_set_myaddr(addr)
#define net_set_role(role) nwk_set_role(role)
#define net_get_myaddr() nwk_get_myaddr()
#define net_mesh_enable(enable) nwk_mesh_enable(enable)
#define net_transmit(addr, len) nwk_transmit(NWK_FRAME_NORMAL, addr, len, 0)
#define net_on() phy_set_rx_state(1)
#define net_off() phy_set_rx_state(0)
#define net_get_trans_payload() nwk_get_trans_payload()
#define net_rf_config_upgrade() phy_rf_config_upgrade()
#define net_rf_config_normal() phy_rf_config_normal()
#define net_relay_acks() nwk_reply_acks()

#define NET_BROADCAST_ADDR NWK_BROADCAST_ADDR

void net_init(void);
void net_poll(void);

#endif // NET_H
