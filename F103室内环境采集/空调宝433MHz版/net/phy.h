#ifndef PHY_H
#define PHY_H
#include "os.h"

#define MARC_STATE_IDLE                   0x01
#define MARC_STATE_RX                     0x0D
#define MARC_STATE_TX                     0x13

#define deta_size  7     ////数据包长度
#define		INT8U		unsigned char

#define WRITE      0x82  /* Write to Memory instruction */
#define READ       0xD3  /* Read from Memory instruction */
#define RDSR       0xD7  /* Read Status Register instruction  */
#define RDID       0x9F  /* Read identification */
#define PE         0x81  /* Page Erase instruction */
#define BE1        0xC7  /* Bulk Erase instruction */
#define BE2        0x94  /* Bulk Erase instruction */
#define BE3        0x80  /* Bulk Erase instruction */
#define BE4        0x9A  /* Bulk Erase instruction */

#define BUSY_Flag  0x01 /* Ready/busy status flag */

#define Dummy_Byte 0xff


/*******************************************************************************/
#define 	WRITE_BURST     	0x40						//连续写入
#define 	READ_SINGLE     	0x80						//读
#define 	READ_BURST      	0xC0						//连续读
#define 	BYTES_IN_RXFIFO     0x7F  						//接收缓冲区的有效字节数
#define 	CRC_OK              0x80 						//CRC校验通过位标志

#define PHY_MAX_LEN           OS_RADIO_DATA_MAXLEN
#define PHY_HEAD_LEN          (sizeof(phy_header_t) - 1)
#define PHY_BROADCAST_CHANNEL 0
#define PHY_UPGRADE_CHANNEL   1

#define CCxxx0_IOCFG2       0x00        // GDO2 output pin configuration
#define CCxxx0_IOCFG1       0x01        // GDO1 output pin configuration
#define CCxxx0_IOCFG0       0x02        // GDO0 output pin configuration
#define CCxxx0_FIFOTHR      0x03        // RX FIFO and TX FIFO thresholds
#define CCxxx0_SYNC1        0x04        // Sync word, high INT8U
#define CCxxx0_SYNC0        0x05        // Sync word, low INT8U
#define CCxxx0_PKTLEN       0x06        // Packet length
#define CCxxx0_PKTCTRL1     0x07        // Packet automation control
#define CCxxx0_PKTCTRL0     0x08        // Packet automation control
#define CCxxx0_ADDR         0x09        // Device address
#define CCxxx0_CHANNR       0x0A        // Channel number
#define CCxxx0_FSCTRL1      0x0B        // Frequency synthesizer control
#define CCxxx0_FSCTRL0      0x0C        // Frequency synthesizer control
#define CCxxx0_FREQ2        0x0D        // Frequency control word, high INT8U
#define CCxxx0_FREQ1        0x0E        // Frequency control word, middle INT8U
#define CCxxx0_FREQ0        0x0F        // Frequency control word, low INT8U
#define CCxxx0_MDMCFG4      0x10        // Modem configuration
#define CCxxx0_MDMCFG3      0x11        // Modem configuration
#define CCxxx0_MDMCFG2      0x12        // Modem configuration
#define CCxxx0_MDMCFG1      0x13        // Modem configuration
#define CCxxx0_MDMCFG0      0x14        // Modem configuration
#define CCxxx0_DEVIATN      0x15        // Modem deviation setting
#define CCxxx0_MCSM2        0x16        // Main Radio Control State Machine configuration
#define CCxxx0_MCSM1        0x17        // Main Radio Control State Machine configuration
#define CCxxx0_MCSM0        0x18        // Main Radio Control State Machine configuration
#define CCxxx0_FOCCFG       0x19        // Frequency Offset Compensation configuration
#define CCxxx0_BSCFG        0x1A        // Bit Synchronization configuration
#define CCxxx0_AGCCTRL2     0x1B        // AGC control
#define CCxxx0_AGCCTRL1     0x1C        // AGC control
#define CCxxx0_AGCCTRL0     0x1D        // AGC control
#define CCxxx0_WOREVT1      0x1E        // High INT8U Event 0 timeout
#define CCxxx0_WOREVT0      0x1F        // Low INT8U Event 0 timeout
#define CCxxx0_WORCTRL      0x20        // Wake On Radio control
#define CCxxx0_FREND1       0x21        // Front end RX configuration
#define CCxxx0_FREND0       0x22        // Front end TX configuration
#define CCxxx0_FSCAL3       0x23        // Frequency synthesizer calibration
#define CCxxx0_FSCAL2       0x24        // Frequency synthesizer calibration
#define CCxxx0_FSCAL1       0x25        // Frequency synthesizer calibration
#define CCxxx0_FSCAL0       0x26        // Frequency synthesizer calibration
#define CCxxx0_RCCTRL1      0x27        // RC oscillator configuration
#define CCxxx0_RCCTRL0      0x28        // RC oscillator configuration
#define CCxxx0_FSTEST       0x29        // Frequency synthesizer calibration control
#define CCxxx0_PTEST        0x2A        // Production test
#define CCxxx0_AGCTEST      0x2B        // AGC test
#define CCxxx0_TEST2        0x2C        // Various test settings
#define CCxxx0_TEST1        0x2D        // Various test settings
#define CCxxx0_TEST0        0x2E        // Various test settings

// Strobe commands
#define CCxxx0_SRES         0x30        // Reset chip.
#define CCxxx0_SFSTXON      0x31        // Enable and calibrate frequency synthesizer (if MCSM0.FS_AUTOCAL=1).
                                        // If in RX/TX: Go to a wait state where only the synthesizer is
                                        // running (for quick RX / TX turnaround).
#define CCxxx0_SXOFF        0x32        // Turn off crystal oscillator.
#define CCxxx0_SCAL         0x33        // Calibrate frequency synthesizer and turn it off
                                        // (enables quick start).
#define CCxxx0_SRX          0x34        // Enable RX. Perform calibration first if coming from IDLE and
                                        // MCSM0.FS_AUTOCAL=1.
#define CCxxx0_STX          0x35        // In IDLE state: Enable TX. Perform calibration first if
                                        // MCSM0.FS_AUTOCAL=1. If in RX state and CCA is enabled:
                                        // Only go to TX if channel is clear.
#define CCxxx0_SIDLE        0x36        // Exit RX / TX, turn off frequency synthesizer and exit
                                        // Wake-On-Radio mode if applicable.
#define CCxxx0_SAFC         0x37        // Perform AFC adjustment of the frequency synthesizer
#define CCxxx0_SWOR         0x38        // Start automatic RX polling sequence (Wake-on-Radio)
#define CCxxx0_SPWD         0x39        // Enter power down mode when CSn goes high.
#define CCxxx0_SFRX         0x3A        // Flush the RX FIFO buffer.
#define CCxxx0_SFTX         0x3B        // Flush the TX FIFO buffer.
#define CCxxx0_SWORRST      0x3C        // Reset real time clock.
#define CCxxx0_SNOP         0x3D        // No operation. May be used to pad strobe commands to two
                                        // INT8Us for simpler software.

#define CCxxx0_PARTNUM      0x30
#define CCxxx0_VERSION      0x31
#define CCxxx0_FREQEST      0x32
#define CCxxx0_LQI          0x33
#define CCxxx0_RSSI         0x34
#define CCxxx0_MARCSTATE    0x35
#define CCxxx0_WORTIME1     0x36
#define CCxxx0_WORTIME0     0x37
#define CCxxx0_PKTSTATUS    0x38
#define CCxxx0_VCO_VC_DAC   0x39
#define CCxxx0_TXBYTES      0x3A
#define CCxxx0_RXBYTES      0x3B

#define CCxxx0_PATABLE      0x3E
#define CCxxx0_TXFIFO       0x3F
#define CCxxx0_RXFIFO       0x3F


/*******************************************************************************/

// #define GPIO_CS_PIN                  GPIOA
// #define RCC_APB2Periph_GPIO_CS       RCC_AHBPeriph_GPIOA
// #define GPIO_Pin_CS                  GPIO_Pin_4 
// 
// #define GPIO_Pin_SCLK			  GPIO_Pin_13
// #define GPIO_Pin_SO			  GPIO_Pin_14
// #define GPIO_Pin_SI			  GPIO_Pin_15
// #define GPIO_Pin_GD2			  GPIO_Pin_0
// #define GPIO_Pin_GD0			  GPIO_Pin_3


#define RF_CS_PIN        GPIO_Pin_12
#define RF_CS_PORT       GPIOB
#define RF_CS_PIN_SCK    RCC_AHBPeriph_GPIOB

#define RF_IQR_PIN       			  GPIO_Pin_4
#define RF_IQR_PORT      			  GPIOA
#define RF_IQR_EXTI      			  EXTI_Line4
#define RF_IQR_SOURCE						EXTI_PinSource4
#define RF_IQR_PORT_SOURCE			EXTI_PortSourceGPIOA
#define RF_IQR_PIN_SCK    			RCC_AHBPeriph_GPIOA


#define RF_SCK_PIN        GPIO_Pin_3
#define RF_SCK_PORT       GPIOB
#define RF_SCK_PIN_SCK    RCC_AHBPeriph_GPIOB
#define RF_SCK_SOURCE     GPIO_PinSource3
#define RF_SCK_AF         GPIO_AF_0

#define RF_MISO_PIN       GPIO_Pin_4
#define RF_MISO_PORT      GPIOB
#define RF_MISO_PIN_SCK   RCC_AHBPeriph_GPIOB
#define RF_MISO_SOURCE    GPIO_PinSource4
#define RF_MISO_AF        GPIO_AF_0

#define RF_MOSI_PIN       GPIO_Pin_5
#define RF_MOSI_PORT      GPIOB
#define RF_MOSI_PIN_SCK   RCC_AHBPeriph_GPIOB
#define RF_MOSI_SOURCE    GPIO_PinSource5
#define RF_MOSI_AF        GPIO_AF_0


#define RF_SPI2               RCC_APB1Periph_SPI1
/* Exported macro ------------------------------------------------------------*/
/* Select SPI FLASH: Chip Select pin low  */
#define SPI_FLASH_CS_LOW()       GPIO_ResetBits(RF_CS_PORT,RF_CS_PIN)
/* Deselect SPI FLASH: Chip Select pin high */
#define SPI_FLASH_CS_HIGH()      GPIO_SetBits(RF_CS_PORT,RF_CS_PIN)

/************************************* EXTI ***********************************/
////#define _EXTI
//#define RCC_GDO0          		   				 RCC_APB2Periph_GPIOB
//#define GPIO_GDO0_PORT     							 GPIOB
//#define GPIO_GDO0          							 GPIO_Pin_9
//#define GPIO_GDO0_EXTI_LINE							 EXTI_Line9
//#define GPIO_GDO0_EXTI_PORT_SOURCE  		 GPIO_PortSourceGPIOB
//#define GPIO_GDO0_EXTI_PIN_SOURCE        GPIO_PinSource9
//#define GPIO_GDO0_EXTI_IRQn              EXTI9_5_IRQn



/**********************************************************************************/
/************************************* SPI ***********************************/
#define CC1101_SPI                             SPI1
#define CC1101_SPI_CLK                         RCC_APB2Periph_SPI1
#define CC1101_SPI_IRQn                        SPI1_IRQn
#define CC1101_SPI_IRQHandler                  SPI1_IRQHandler

#define CC1101_SPI_SCK_PIN                     GPIO_Pin_3
#define CC1101_SPI_SCK_GPIO_PORT               GPIOB
#define CC1101_SPI_SCK_GPIO_CLK                RCC_AHBPeriph_GPIOB
#define CC1101_SPI_SCK_SOURCE                  GPIO_PinSource3
#define CC1101_SPI_SCK_AF                      GPIO_AF_0

#define CC1101_SPI_MISO_PIN                    GPIO_Pin_4
#define CC1101_SPI_MISO_GPIO_PORT              GPIOB
#define CC1101_SPI_MISO_GPIO_CLK               RCC_AHBPeriph_GPIOB
#define CC1101_SPI_MISO_SOURCE                 GPIO_PinSource4
#define CC1101_SPI_MISO_AF                     GPIO_AF_0

#define CC1101_SPI_MOSI_PIN                    GPIO_Pin_5
#define CC1101_SPI_MOSI_GPIO_PORT              GPIOB
#define CC1101_SPI_MOSI_GPIO_CLK               RCC_AHBPeriph_GPIOB
#define CC1101_SPI_MOSI_SOURCE                 GPIO_PinSource5
#define CC1101_SPI_MOSI_AF                     GPIO_AF_0

/************************************* CS ***********************************/
#define CC1101_SPI_CS_GPIO_PIN					GPIO_Pin_12
#define CC1101_SPI_CS_GPIO_PORT					GPIOB
#define CC1101_SPI_CS_GPIO_CLK   				RCC_AHBPeriph_GPIOB

/************************************* CHIP EN ***********************************/
#define CC1101_EN_GPIO_PIN						GPIO_Pin_1 
#define CC1101_EN_GPIO_PORT						GPIOB
#define CC1101_SPI_EN_GPIO_CLK   		  RCC_AHBPeriph_GPIOB

/************************************* GDO0 ***********************************/
#define CC1101_GDO0_GPIO_CLK					RCC_AHBPeriph_GPIOA
#define CC1101_GDO0_GPIO_PORT					GPIOA
#define CC1101_GDO0_GPIO_PIN          GPIO_Pin_4
#define CC1101_GDO0_EXTI_LINE				 	EXTI_Line4
#define CC1101_GDO0_EXTI_PORT_SOURCE  EXTI_PortSourceGPIOA
#define CC1101_GDO0_EXTI_PIN_SOURCE   EXTI_PinSource4
#define CC1101_GDO0_EXTI_IRQn         EXTI4_15_IRQn
#define CC1101_GDO0_EXTI_PRIORITY           1

/* Exported macro ------------------------------------------------------------*/
// Chip Select pin low  //
#define CC1101_SPI_CS_LOW()       GPIO_ResetBits(CC1101_SPI_CS_GPIO_PORT, CC1101_SPI_CS_GPIO_PIN)
// Chip Select pin high //
#define CC1101_SPI_CS_HIGH()      GPIO_SetBits(CC1101_SPI_CS_GPIO_PORT, CC1101_SPI_CS_GPIO_PIN)
// Chip Enable pin low  //
#define CC1101_ENABLE()       GPIO_ResetBits(CC1101_EN_GPIO_PORT, CC1101_EN_GPIO_PIN)
// Chip Enable pin high //
#define CC1101_DISABLE()      GPIO_SetBits(CC1101_EN_GPIO_PORT, CC1101_EN_GPIO_PIN)

uint8 _tx_done(void);
uint8 _is_not_busy(void);
void _rx_dma_cb(void);
void _radio_dma_init(void);
static uint8 _radio_set_mode(uint8 mode);
void halRfWriteRfSettings(void);
void otherRfWriteRfSettings(void);
void halSpiWriteReg(INT8U addr, INT8U value);
void halSpiWriteBurstReg(INT8U addr, INT8U *buffer, INT8U count);
void CC1101_POWER_RESET(void);

void SPI_FLASH_Init(void);
void SPI_FLASH_PageErase(u32 SectorAddr);
void SPI_FLASH_BulkErase(void);
void SPI_FLASH_PageWrite(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite);
void SPI_FLASH_BufferWrite(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite);
void SPI_FLASH_BufferRead(u8* pBuffer, u32 ReadAddr, u16 NumByteToRead);
u32 SPI_FLASH_ReadID(void);

u8 SPI_FLASH_ReadByte(void);
u8 SPI_FLASH_SendByte(u8 byte);
u16 SPI_FLASH_SendHalfWord(u16 HalfWord);
void SPI_FLASH_WaitForWriteEnd(void);
void NVIC_Configuration(void);
void SPI_CC1101_Init(void);
void CC1101_Main(void);
void GPIO_GDO0_ENABLE(void);
void GPIO_GDO0_DISABLE(void);
void halRfSendPacket(INT8U *txBuffer, INT8U size);
void halSpiStrobe(INT8U strobe);
INT8U halSpiReadStatus(INT8U addr);
INT8U halRfReceivePacket(INT8U *rxBuffer, INT8U *length);

enum
{
  PHY_TYPE_NORMAL = 0,
  //控制帧
  PHY_TYPE_UPGRADE = 0xff,
};

typedef struct {
  uint8 len;
  uint8 resvered;
  uint8 type;
  uint8 payload[];
}phy_header_t;

void phy_init1(void);
void phy_init(void);
void phy_set_channel(uint8 chn);
void phy_set_rx_state(uint8 on_off);
uint8 phy_transmit(uint8 type, uint8 len);
void *phy_get_trans_payload(void);
void phy_poll(void);
void phy_rf_config_normal(void);
void phy_rf_config_upgrade(void);

//#ifdef CONFIG_WITH_SLEEP
uint16_t CC1101_WOR_Init(uint32_t Time);
void RF_CC1101_SLEEP(void);
void g_rx_buf_clear(void);
//#endif

#endif // PHY_H
