#include "net.h"


void net_init()
{
  phy_init();
  phy_set_rx_state(1);
  nwk_init();
}

void net_poll()
{
  nwk_poll();
}
