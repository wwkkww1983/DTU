#ifndef CONTROL_H
#define CONTROL_H
#include "os.h"
#include "phy.h"

//无线控制帧的处理，包括强制设备控制和设备升级

void ctrl_deal(phy_header_t *phy);
void ctrl_upgrade_transmit(uint8 len);
void *ctrl_upgrade_get_trans_payload(void);

#endif // CONTROL_H
