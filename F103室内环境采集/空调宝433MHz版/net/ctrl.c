#include "ctrl.h"
#include "net.h"
//#include "hal/dma/dma.h"
#include "../hal/hal_wait.h"

#ifdef MAIN_MODULE
#include "app/main_module/yunhe_mid.h"
#else
#include "../sub_module/sub_module.h"
#endif // MAIN_MODULE

#define UPGRADE_PROGRAM_INFO   0x03
#define UPGRADE_PROGRAM_DATA   0x04
#define UPGRADE_PART_SIZE      64

#define UP_FROM_MAIN          1
#define UP_FROM_SUB           0

typedef struct up_frame_header
{
  uint8 from;
  uint8 segaddr[NWK_SEGADDR_LEN];
  uint8 payload[];
}up_frame_header_t;

void ctrl_upgrade_transmit(uint8 len)
{
  up_frame_header_t *up_frm = phy_get_trans_payload();
  up_frm->from = UP_FROM_MAIN;
  memcpy(up_frm->segaddr, nwk_get_segaddr(), NWK_SEGADDR_LEN);
  phy_transmit(PHY_TYPE_UPGRADE, len + sizeof(up_frame_header_t));
}

void *ctrl_upgrade_get_trans_payload()
{
  return ((up_frame_header_t *)phy_get_trans_payload())->payload;
}

static void _ctrl_upgrade_deal(up_frame_header_t *up_frm, uint8 len)
{
#ifdef MAIN_MODULE

  if(up_frm->from != UP_FROM_SUB
     || (memcmp(up_frm->segaddr, NWK_NULL_SEGADDR, NWK_SEGADDR_LEN) != 0
         && memcmp(up_frm->segaddr, nwk_get_segaddr(), NWK_SEGADDR_LEN) != 0)) //��ַ����
  {
    return;
  }

  uint8 *send_data;
  send_data = yunhe_mid_get_send_buf();

  memcpy(send_data, up_frm->payload, len - sizeof(up_frame_header_t));
  yunhe_mid_send(DEVICE_UPGRADE, len - sizeof(up_frame_header_t));
#endif // MAIN_MODULE
}

void ctrl_deal(phy_header_t *phy)
{
  uint8 data_len;
  data_len = phy->len - PHY_HEAD_LEN;

  switch(phy->type)
  {
  case PHY_TYPE_UPGRADE:
    _ctrl_upgrade_deal((up_frame_header_t *)phy->payload, data_len);
    break;
  }
}
