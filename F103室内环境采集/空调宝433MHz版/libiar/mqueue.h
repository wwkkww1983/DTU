#ifndef MQUEUE_H
#define MQUEUE_H

#ifndef NULL
#define NULL 0
#endif // NULL

//必须为符号整形，入队错误时返回负数否则返回索引
typedef unsigned char mqueue_queue_size_t;
//可容纳最大数据
typedef unsigned char mqueue_data_size_t;

typedef struct mqueue
{
  mqueue_queue_size_t head;
  mqueue_queue_size_t tail;
  mqueue_queue_size_t count;
  mqueue_queue_size_t size;
  mqueue_data_size_t data_size;
  unsigned char *data;
}mqueue_t;

void mqueue_init(mqueue_t *queue, void *items, mqueue_queue_size_t size, mqueue_data_size_t data_size);
unsigned char mqueue_in(mqueue_t *queue, void *item);
void *mqueue_first(mqueue_t *queue);
void mqueue_out(mqueue_t *queue);
unsigned char mqueue_is_full(mqueue_t *queue);
unsigned char mqueue_is_empty(mqueue_t *queue);

#endif // MQUEUE_H

