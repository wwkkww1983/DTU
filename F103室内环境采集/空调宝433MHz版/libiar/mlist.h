#ifndef MLIST_H
#define MLIST_H

#ifndef NULL
#define NULL 0
#endif // NULL

typedef signed char mlist_data_size_t;
typedef signed char mlist_list_size_t;

typedef unsigned char (*mlist_foreach_func_t)(void *item, mlist_list_size_t index);
typedef unsigned char (*mlist_find_func_t)(void *item, void *userdata);

typedef struct mlist
{
  mlist_list_size_t size;
  mlist_data_size_t data_size;
  unsigned char *used;
  unsigned char *data;
}mlist_t;

void mlist_init(mlist_t *list, void *used, void *items, mlist_list_size_t size, mlist_data_size_t data_size);
void *mlist_find(mlist_t *list, mlist_find_func_t findFunc, mlist_list_size_t *index, void *userdata);
void mlist_foreach(mlist_t *list, mlist_foreach_func_t foreachFunc);
mlist_list_size_t mlist_add(mlist_t *list, void *item, mlist_data_size_t len);
void mlist_del(mlist_t *list, mlist_list_size_t index);
void *mlist_get(mlist_t *list, mlist_list_size_t index); 

#endif // MLIST_H
