#include "hal_wait.h"
#include "hal.h"
#include "hal_wdt.h"

static uint8 g_waitType;

void TIM14_Init(void);
void TIM14_Config_Us(void);
void TIM14_Config_Ms(void);

void hal_wait_init(uint8 type)
{
  g_waitType = type;
	TIM14_Init();
  if(type == HAL_WAIT_US)
  {
    TIM14_Config_Us();
  }
  if(type == HAL_WAIT_MS)
  {
    TIM14_Config_Ms();
  }
}

void hal_wait_check(uint8 type)
{
  if(type != g_waitType)
  {
    hal_wait_init(type);
  }
}

void hal_wait(uint16 utime, uint8 type)
{
  uint16 count = 0;
  while(count < utime)
  {
    hal_wait_check(type);
    hal_wdt_feed();
    if((TIM14->SR)&TIM_IT_Update)
    {
      TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
      count++;
    }
  }
}

void hal_wait_us(uint16 usec)
{
  hal_wait_init(HAL_WAIT_US);
  hal_wait(usec, HAL_WAIT_US);
}

void hal_wait_ms(uint16 msec)
{
  hal_wait_init(HAL_WAIT_MS);
  hal_wait(msec, HAL_WAIT_MS);
}


void TIM14_Init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);
		NVIC_InitStructure.NVIC_IRQChannel = TIM14_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPriority = 3;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
}
void TIM14_Config_Us(void)
{
	  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    TIM_TimeBaseStructure.TIM_Period = (48 - 1);
    TIM_TimeBaseStructure.TIM_Prescaler = (1 - 1);
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM14, &TIM_TimeBaseStructure);

    // Clear TIM14 update pending flag[清除TIM14溢出中断标志] 
    TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
    // TIM IT enable 
    //TIM_ITConfig(TIM14, TIM_IT_Update, ENABLE);
    // TIM14 enable counter 
    TIM_Cmd(TIM14, ENABLE);
}
void TIM14_Config_Ms(void)
{
	  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    TIM_TimeBaseStructure.TIM_Period = (48 - 1);
    TIM_TimeBaseStructure.TIM_Prescaler = (1000 - 1);
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM14, &TIM_TimeBaseStructure);

    // Clear TIM14 update pending flag[清除TIM14溢出中断标志] 
    TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
    // TIM IT enable 
    //TIM_ITConfig(TIM14, TIM_IT_Update, ENABLE);
    // TIM14 enable counter 
    TIM_Cmd(TIM14, ENABLE);
}
