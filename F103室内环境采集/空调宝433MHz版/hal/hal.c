#include "hal.h"
#include "hal_uart.h"
#include "hal_timer.h"
#include "hal_adc.h"

void hal_init()
{
  hal_wdt_init();
  hal_timer_init();
  hal_rnd_init();
  //hal_aes_init();
  //dma_init();
  
  hal_adc_int();
	
  hal_led_set_up(LED_PORT, LED_PIN);
  hal_led_on(LED_PORT, LED_PIN);
  
  hal_wait_ms(200);
  HalATT7053Init();
  
  os_log(" Hal init success...");
}

void hal_poll()
{
  hal_rnd_seed();
  hal_wdt_feed();
}
