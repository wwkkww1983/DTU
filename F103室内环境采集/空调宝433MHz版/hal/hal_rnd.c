#include "hal_rnd.h"
#include "../net/phy.h"
#include<stdio.h>
#include<stdlib.h>

static uint16 g_seed = 0;

void hal_rnd_init()
{

}
uint8 hal_rnd_get()
{
  uint8 res = 0;
	srand(g_seed);
	res = rand();
	g_seed += res;
  /*RNDL = g_seed & 0xff;
  RNDL = (g_seed >> 8) & 0xff;
  GET_RANDOM_BYTE(res);
  if(res == 0)
  {
	  res = 1;
  }
  g_seed += res;*/
  return res;
}

void hal_rnd_seed()
{
  g_seed++;
}

uint8 hal_rnd_range(uint8 from, uint8 to)
{
  uint8 n = hal_rnd_get();
  n = (n % (to - from + 1)) + from;
  return n;
}


void hal_rnd_feed_seed(uint8 seed)
{
  g_seed += seed;
}
