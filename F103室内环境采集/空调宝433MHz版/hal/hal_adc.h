#ifndef HAL_ADC_H
#define HAL_ADC_H
#include "../os/os.h"

void hal_adc_int(void);
uint16 hal_adc_get_vol(uint8 pin);

#endif //HAL_ADC_H
