#ifndef HAL_BOARD_DMA_H
#define HAL_BOARD_DMA_H
#include "hal.h"
#include "os.h"

#define DMA_COUNT      5

#define DMA_FLAG_PERMANENT  1

typedef DMA_DESC dma_desc_t;

//#pragma bitfields=reversed
//typedef struct
//{
//  uint8 SRCADDRH;
//  uint8 SRCADDRL;
//  uint8 DESTADDRH;
//  uint8 DESTADDRL;
//  uint8 VLEN      : 3;
//  uint8 LENH      : 5;
//  uint8 LENL      : 8;
//  uint8 WORDSIZE  : 1;
//  uint8 TMODE     : 2;
//  uint8 TRIG      : 5;
//  uint8 SRCINC    : 2;
//  uint8 DESTINC   : 2;
//  uint8 IRQMASK   : 1;
//  uint8 M8        : 1;
//  uint8 PRIORITY  : 2;
//} dma_desc_t;
//#pragma bitfields=default

typedef void (*dma_callback_t)();

typedef struct dma_table_item
{
  uint8 flag;
  uint8 allocated;
  dma_callback_t callback;    
}dma_table_item_t;

void dma_init();
dma_desc_t *dma_allocate(uint8 *dma_chn, dma_callback_t callback, uint8 flag);
void dma_free(uint8 dma_chn);

void dma_from_radio(dma_desc_t *dma_desc, uint8 len, void *dst_addr, uint8 int_enable);
void dma_to_radio(dma_desc_t *dma_desc, uint8 len, void *src_addr, uint8 int_enable);
void dma_to_uart0(dma_desc_t* dma_desc, uint8 len, uint8 *src_addr, uint8 int_enable);
void dma_from_uart0(dma_desc_t* dma_desc, uint8 len, void *dst_addr, uint8 int_enable);

#endif // HAL_BOARD_DMA
