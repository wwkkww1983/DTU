#ifndef HAL_FLASH_H
#define HAL_FLASH_H
#include "../os/os.h"

void hal_flash_write(void *flash_addr, void *src_buf, uint16 len);

#endif // HAL_FLASH_H

