#ifndef HAL_LED_H
#define HAL_LED_H
#include "../os/os.h"

void hal_led_set_up(GPIO_TypeDef* port, uint16_t pin);;

void hal_led_on(GPIO_TypeDef* port, uint16_t pin);

void hal_led_off(GPIO_TypeDef* port, uint16_t pin);

void hal_led_toggle(GPIO_TypeDef* port, uint16_t pin);

void hal_led_blink(GPIO_TypeDef* port, uint16_t pin, uint8_t times);

#endif

