#include "hal_sleep.h"
#include "../net/net.h"
#include "stm32f0xx_rtc.h"
#include "../sub_module/devices/h_t_collect.h"
#include "button.h"
#include "stm32f0xx_adc.h"

//static void _set_rc_clock(void);
//static void _set_sleep_timer(uint16 seconds);

static uint8 g_enable = 1;
static uint8 g_wakeup_state = 0;

extern uint8 button_sleep_fig;

static void _set_sleep_timer(uint16 seconds)
{
  
}

static void _set_rc_clock()
{
  
}

uint8 hal_sleep_pm2_int_off(uint16 seconds)
{
	hal_sleep_pm2(seconds);
  return 0;
}

extern os_time_t train_time_wait;

uint8 hal_sleep_pm2(uint16 seconds)
{
	  if(!g_enable)
		{
			return 0;
		}
		halSpiStrobe(CCxxx0_SRX);         //开接收
		//CC1101_WOR_Init(3000/*us*/, 2000/*ms*/);	//CC1101进入WOR模式
    CC1101_WOR_Init(3000);
		RF_CC1101_SLEEP();	
		g_wakeup_state = HAL_SLEEP_WAKEUP_INT;
		
		os_log("sleep");
		button_sleep_fig = 1;
    hal_led_set_up(LED_PORT, LED_PIN);
    hal_led_off(LED_PORT, LED_PIN);
//		
///*************************************************************/
////休眠前端口配置
    RTC_Config(seconds);
    TIM_Cmd(TIM3, DISABLE);
    TIM_Cmd(TIM14, DISABLE);
		USART_Cmd(USART1, DISABLE);
    ADC_Cmd(ADC1, DISABLE);
    SPI_Cmd(SPI1, DISABLE);
		Init_sotpmode_port();
///*************************************************************/
		
		PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
    
		SystemInit();
    button_init();
		hal_init();
		phy_init1();
    SPI_CC1101_Init();
		phy_set_rx_state(1);
		//TODO chenlh
    //HalSht20IIC_Init();
		//

		os_log("wake up");
		os_timer_add_systime(seconds * 1000L);
		train_time_wait = os_timer_get_systime();
	  
  return g_wakeup_state;
}

void hal_sleep_set_enable(uint8 enable)
{
  g_enable = enable;
}

uint8 hal_sleep_get_enable()
{
  return g_enable;
}

void RTC_Config(uint16 seconds)
{
	uint8 sect = 0 ,secu = 0;
	uint8 mnt = 0,mnu = 0;
	uint8 hout = 0,houu = 0;
	
  RTC_InitTypeDef   RTC_InitStructure;
  RTC_TimeTypeDef   RTC_TimeStructure;
  RTC_AlarmTypeDef  RTC_AlarmStructure;
  EXTI_InitTypeDef EXTI_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
  
  /* RTC Configuration **********************************************************/
  
  /* Enable the PWR clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
  
  /* Allow access to RTC */
  PWR_BackupAccessCmd(ENABLE);
  
  /* Enable the LSI OSC */
  RCC_LSICmd(ENABLE);
  
  /* Wait till LSI is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {}
  
  /* Select the RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
  
  /* Enable the RTC Clock */
  RCC_RTCCLKCmd(ENABLE);
  
  /* Wait for RTC APB registers synchronisation */
  RTC_WaitForSynchro();  
  
  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
  RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
  RTC_InitStructure.RTC_SynchPrediv = 0x0138;
  
  if (RTC_Init(&RTC_InitStructure) == ERROR)
  {
 //   while(1);
  }
	
	  /* Set the time to 01h 00mn 00s AM */
  RTC_TimeStructure.RTC_H12     = RTC_H12_AM;
  RTC_TimeStructure.RTC_Hours   = 0x01;
  RTC_TimeStructure.RTC_Minutes = 0x00;
  RTC_TimeStructure.RTC_Seconds = 0x00;  
  
  RTC_SetTime(RTC_Format_BCD, &RTC_TimeStructure);
    
  /* EXTI configuration */
  EXTI_ClearITPendingBit(EXTI_Line17);
  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
  
  /* NVIC configuration */
  NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  /* Set the alarm X+5s */
		hout = (seconds / 3600)/10;
		houu = (seconds / 3600)%10;
		
		mnt = seconds / 600;
		mnu = (seconds % 600)/60;
		
		sect = (seconds % 60)/10;
		secu = (seconds % 60)%10;

  /* Disable the Alarm A */
  RTC_AlarmCmd(RTC_Alarm_A, DISABLE);   //配置之前先失能闹铃
	/* Get the current time */
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
  RTC_AlarmStructure.RTC_AlarmTime.RTC_H12     = RTC_H12_AM;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours   = ((hout<<4)+ houu +0x01);
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = ((mnt<<4) + mnu);
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = ((sect<<4) + secu);
  RTC_AlarmStructure.RTC_AlarmDateWeekDay = 31;
  RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
  RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;
  RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);
  
  /* Enable the alarm */
  RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
  
  /* Enable the RTC Alarm A interrupt */
  RTC_ITConfig(RTC_IT_ALRA, ENABLE);
	
	/* Clear RTC Alarm Flag */ 
  RTC_ClearFlag(RTC_FLAG_ALRAF);
  
  /* Clear the Alarm A Pending Bit */
  RTC_ClearITPendingBit(RTC_IT_ALRA);  
}


void SYSCLKConfig_STOP(void)
{  
  /* After wake-up from STOP reconfigure the system clock */
  /* Enable HSE */
  RCC_HSEConfig(RCC_HSE_ON);
  
  /* Wait till HSE is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET)
  {}
  
  /* Enable PLL */
  RCC_PLLCmd(ENABLE);
  
  /* Wait till PLL is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
  {}
  
  /* Select PLL as system clock source */
  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
  
  /* Wait till PLL is used as system clock source */
  while (RCC_GetSYSCLKSource() != 0x08)
  {}
}

void Enter_StopMode(void)
{
//  PWR_BackupAccessCmd(ENABLE);	// Allow access to RTC    
//	RCC_LSICmd(ENABLE);//使能内部低速RC时钟，RTC Clock may varies due to LSI frequency dispersion.  
//	while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);// Wait till LSI is ready   
//	
	// Enter Stop Mode 
	PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
	
}

void RTC_IRQHandler(void)
{
  if (RTC_GetITStatus(RTC_IT_ALRA) != RESET)
  {
//  /* Clear the Alarm A Pending Bit */
    RTC_ClearITPendingBit(RTC_IT_ALRA);
//    
//  /* Clear EXTI line17 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line17);    
  }
  
}

void Init_sotpmode_port(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	
	//RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOF, ENABLE);
	
    /*GPIO_InitStruct.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_15 | GPIO_Pin_13;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
//	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
//	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOF, &GPIO_InitStruct);
	
//	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_4 | GPIO_Pin_3 | \
//														 GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_8 | GPIO_Pin_11 | GPIO_Pin_13 | \
//														 GPIO_Pin_14;
	GPIO_InitStruct.GPIO_Pin = (GPIO_Pin_All & (~GPIO_Pin_3) & \
																  (~GPIO_Pin_4) & (~GPIO_Pin_5) & \
																				(~GPIO_Pin_6) & (~GPIO_Pin_7) & (~GPIO_Pin_0));
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
//	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin = (GPIO_Pin_All & (~GPIO_Pin_6) & (~GPIO_Pin_7));//& (~GPIO_Pin_6) & (~GPIO_Pin_7)
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
//	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOB, &GPIO_InitStruct);*/
    
    GPIO_InitStruct.GPIO_Pin = (GPIO_Pin_All & (~GPIO_Pin_0) & (~GPIO_Pin_4) & (~GPIO_Pin_3) & (~GPIO_Pin_1)  & (~GPIO_Pin_5) & (~GPIO_Pin_6) & (~GPIO_Pin_7));
    //GPIO_InitStruct.GPIO_Pin = (GPIO_Pin_All & (~GPIO_Pin_0) & (~GPIO_Pin_4));
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
    
    GPIO_InitStruct.GPIO_Pin = (GPIO_Pin_All & (~GPIO_Pin_6) & (~GPIO_Pin_7) & (~GPIO_Pin_10) & (~GPIO_Pin_11) & (~GPIO_Pin_15));
    //GPIO_InitStruct.GPIO_Pin = (GPIO_Pin_All);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOB, &GPIO_InitStruct);
    
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOC, &GPIO_InitStruct);
  
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOF, &GPIO_InitStruct);
    
    
    //RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, DISABLE);
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, DISABLE);
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, DISABLE);
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOF, DISABLE);
}


