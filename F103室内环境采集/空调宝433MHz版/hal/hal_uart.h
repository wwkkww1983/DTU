#ifndef HAL_UART_H
#define HAL_UART_H
#include "hal.h"
#include "../os/os.h"

void hal_uart_init(void);
uint8 hal_uart_write(uint8 *data, uint8 len);
void uart_putc(unsigned char c);
void uart_puts(char *s );
void UART_send_byte(uint8_t byte);

#endif 
