#include "hal_timer.h"
#include "hal.h"
#include "os.h"

void hal_timer_init()
{
	TIM3_Init();
	TIM3_Config();
}

void TIM3_IRQHandler(void)
{
  TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	os_timer_pass_ms();
}

void TIM3_Init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
		NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
}
void TIM3_Config(void)
{
	  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	
    TIM_TimeBaseStructure.TIM_Period = (48 - 1);
    TIM_TimeBaseStructure.TIM_Prescaler = (1000 - 1);
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

    TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
    // TIM IT enable 
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
    // TIM3 enable counter 
    TIM_Cmd(TIM3, ENABLE);
}
