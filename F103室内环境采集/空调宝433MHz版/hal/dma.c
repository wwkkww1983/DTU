#include "dma.h"
#include "hal.h"
#include "os.h"

#include <string.h>

dma_desc_t g_dma_desc[DMA_COUNT];
dma_table_item_t g_dma_table[DMA_COUNT];

void dma_init()
{
  
}



dma_desc_t *dma_allocate(uint8 *dma_chn, dma_callback_t callback, uint8 flag)
{

  return g_dma_desc + 0;
}

void dma_free(uint8 dma_chn)
{
  
}

void dma_from_radio(dma_desc_t *dma_desc, uint8 len, void *dst_addr, uint8 int_enable)
{
  
}

void dma_to_radio(dma_desc_t *dma_desc, uint8 len, void *src_addr, uint8 int_enable)
{
  
}

void dma_to_uart0(dma_desc_t* dma_desc, uint8 len, uint8 *dest_addr, uint8 int_enable)
{
 
}

void dma_from_uart0(dma_desc_t* dma_desc, uint8 len, void *dst_addr, uint8 int_enable)
{
  
}
