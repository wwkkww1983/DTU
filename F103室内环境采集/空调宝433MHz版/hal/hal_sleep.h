#ifndef HAL_SLEEP_H
#define HAL_SLEEP_H
#include "../os/os.h"
#include "stm32f0xx_rtc.h"

#define HAL_SLEEP_WAKEUP_INT      1
#define HAL_SLEEP_WAKEUP_TIMEUP   2

uint8 hal_sleep_get_enable(void);
void hal_sleep_set_enable(uint8 enable);
uint8 hal_sleep_pm2(uint16 seconds);
uint8 hal_sleep_pm2_int_off(uint16 seconds);

void RTC_Config(uint16 seconds);
void SYSCLKConfig_STOP(void);
void Enter_StopMode(void);
void Init_sotpmode_port(void);

#endif //HAL_SLEEP_H
