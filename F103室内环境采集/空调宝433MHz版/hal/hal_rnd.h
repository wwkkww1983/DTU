#ifndef HAL_RND_H
#define HAL_RND_H
#include "../os/os.h"

void hal_rnd_init(void);
void hal_rnd_seed(void);
void hal_rnd_feed_seed(uint8 seed);
uint8 hal_rnd_get(void);
uint8 hal_rnd_range(uint8 from, uint8 to);

#endif // HAL_RND_H

