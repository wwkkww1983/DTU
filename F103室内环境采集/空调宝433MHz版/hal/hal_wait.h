#ifndef HAL_WAIT_H
#define HAL_WAIT_H
#include "../os/os.h"

#define HAL_WAIT_US 0
#define HAL_WAIT_MS 1

void hal_wait(uint16 utime, uint8 type);
void hal_wait_init(uint8 type);
void hal_wait_stop(void);
void hal_wait_us(uint16 usec);
void hal_wait_ms(uint16 msec);
void hal_wait_check(uint8 type);

#define hal_wait_us_cond(us, expression) \
  do{\
    uint16 tmp = 0;\
      hal_wait_init(HAL_WAIT_US);\
        while(tmp < us && !(expression))\
          {\
            hal_wait_check(HAL_WAIT_US);\
              hal_wdt_feed();\
                if((TIM14->SR)&TIM_IT_Update)\
                  {\
                      TIM_ClearITPendingBit(TIM14, TIM_IT_Update);\
                        tmp++;\
      }\
    }\
  }while(0)

#define hal_wait_ms_cond(ms, expression) \
  do{\
    uint16 tmp = 0;\
      hal_wait_init(HAL_WAIT_MS);\
        while(tmp < ms && !(expression))\
          {\
            hal_wait_check(HAL_WAIT_MS);\
              hal_wdt_feed();\
                if((TIM14->SR)&TIM_IT_Update)\
                  {\
                    TIM_ClearITPendingBit(TIM14, TIM_IT_Update);\
                        tmp++;\
      }\
    }\
  }while(0)

#define hal_wait_cond(type, time, expression, flag) \
  do{\
    flag = 0;\
      uint16 count = 0;\
        hal_wait_init(type);\
          while(count < time && !(expression))\
            {\
              hal_wait_check(type);\
                hal_wdt_feed();\
                  if((TIM14->SR)&TIM_IT_Update)\
                    {\
                      TIM_ClearITPendingBit(TIM14, TIM_IT_Update);\
                          count++;\
      }\
    }\
      if(count == time)flag = 1;\
  }while(0)

#endif // HAL_WAIT_H
