#include "hal_led.h"
#include "hal_wait.h"

void hal_led_set_up(GPIO_TypeDef* port, uint16_t pin)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	if(port == GPIOA)
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	else
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	GPIO_InitStruct.GPIO_Pin = pin;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed =GPIO_Speed_Level_3;
	GPIO_Init(port, &GPIO_InitStruct);

	GPIO_SetBits(port, pin);
}

void hal_led_on(GPIO_TypeDef* port, uint16_t pin)
{
	GPIO_ResetBits(port, pin);
}

void hal_led_off(GPIO_TypeDef* port, uint16_t pin)
{
	GPIO_SetBits(port, pin);
}

void hal_led_toggle(GPIO_TypeDef* port, uint16_t pin)
{
	#if 1
  //	GPIOA->ODR ^=GPIO_Pin_11;
  GPIO_WriteBit(port, pin, 
		               (BitAction)((1-GPIO_ReadOutputDataBit(port, pin))));
	#endif
}

void hal_led_blink(GPIO_TypeDef* port, uint16_t pin, uint8_t times)
{
  while(times--)
  {
    hal_led_toggle(port, pin);
    hal_wait_ms(150);
    hal_led_toggle(port, pin);
    hal_wait_ms(150);
  }
  hal_led_off(port, pin);
}
