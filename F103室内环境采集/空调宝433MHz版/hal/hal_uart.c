#include "hal_uart.h"
//#include "lib/mqueue.h"
//#include "dma/dma.h"
//#include <string.h>
//#include <stdarg.h>
//#include <stdio.h>

#ifndef MicroLIB
#pragma import(__use_no_semihosting)          
              
struct __FILE 
{ 
	int handle; 
}; 

FILE __stdout;       
int _sys_exit(int x) 
{ 
	x = x; 
	return 1;
} 
 
int fputc(int ch, FILE *f)
{
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
    {}

    /* e.g. write a character to the USART */
    USART_SendData(USART1, (uint8_t) ch);

    return ch;
}

int ferror(FILE *f) {  
    /* Your implementation of ferror */  
    return EOF;  
} 
#endif 

void USART1_IRQHandler(void)
{
  if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
  {
#ifdef MAIN_MODULE
		yunhe_mid_recv(USART_ReceiveData(USART1));
#endif 
  }
}

void hal_uart_init()
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
//	NVIC_InitTypeDef NVIC_InitStructure;
                
  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE );
                
  GPIO_PinAFConfig(GPIOB,GPIO_PinSource6,GPIO_AF_0);
  GPIO_PinAFConfig(GPIOB,GPIO_PinSource7,GPIO_AF_0);        
                               
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7;                 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; 
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
  GPIO_Init(GPIOB, &GPIO_InitStructure);        
        
  USART_InitStructure.USART_BaudRate = 115200;//设置串口波特率
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;//设置数据位
  USART_InitStructure.USART_StopBits = USART_StopBits_1;//设置停止位
  USART_InitStructure.USART_Parity = USART_Parity_No;//设置效验位
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//设置流控制
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;//设置工作模式(双工)
  USART_Init(USART1, &USART_InitStructure); //配置入结构体
				
//	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
//	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x01;
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_Init(&NVIC_InitStructure);
				
//	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

  USART_Cmd(USART1, ENABLE);//使能串口1
}

uint8 hal_uart_write(uint8 *data, uint8 len)
{
	while(len != 0)
	{
		while(!((USART1->ISR)&(1<<7)));//等待发送完
		USART1->TDR= *data;
		data++;
		len--;
	}
	
	return len;
}

void UART_send_byte(uint8_t byte) //发送1字节数据
{
 while(!((USART1->ISR)&(1<<7)));
 USART1->TDR=byte;	
}	

void uart_putc(unsigned char c)
{
	while(!((USART1->ISR)&(1<<7)));
	USART1->TDR=c;	
}
void uart_puts(char *s )
{
	while (*s)
	uart_putc(*s++);
}

/* 

#ifdef __GNUC__

  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif 

uint8_t UART_Recive(void)
{	
	while(!(USART1->ISR & (1<<5)));//等待接收到数据
	return(USART1->RDR);			 //读出数据
}


PUTCHAR_PROTOTYPE 
{
// 将Printf内容发往串口 
  USART_SendData(USART1,(uint8_t)  ch);
  while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
	{}
 
  return (ch);
}*/






		
