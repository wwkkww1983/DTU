#ifndef HAL_TIMER_H
#define HAL_TIMER_H
#include "stm32f0xx.h"

void hal_timer_init(void);
void TIM3_Init(void);
void TIM3_Config(void);

#endif // HAL_TIMER_H
