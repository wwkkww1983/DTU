#ifndef APP_H
#define APP_H
#include "os.h"


#define VERSION_LEN               4

#define FACINFO_LEN               16
#define DEVUID_LEN                7
#define MAX_DEVICE_COUNT          OS_MAX_DEVICE_COUNT
#define NETKEY_LEN                3

#define APP_PAYLOAD_MAXLEN        (PHY_MAX_LEN - sizeof(nwk_header_t) - sizeof(app_header_t) - sizeof(phy_header_t))

typedef struct app_header
{
  uint8 type;
  uint8 payload[];
}app_header_t;

enum app_frame_t{
  APP_FRAME_SEARCHDEVICE,
  APP_FRAME_ADDDEVICE,
  APP_FRAME_DELDEVICE,
  APP_FRAME_CONNECT,
  APP_FRAME_DATA,
};

void app_init(void);
void app_poll(void);
uint8 *app_get_key(uint8 addr);
void app_trans_result(uint8 trans_id, uint8 result);
void app_encrypt(uint8 addr, void *data, uint8 len);
void app_decrypt(uint8 addr, void *data, uint8 len);
void *app_get_trans_payload(void);
uint8 app_transmit(uint8 type, uint8 addr, uint8 len);
void app_rf_recv_deal(uint8 *data, uint8 len, uint8 *segaddr, uint8 srcaddr, uint8 dstaddr, uint8 rssi);
void app_ack_sync_content_cb(uint8 *data, uint8 len);
uint8 app_ack_sync_content_set(uint8 *data);

#endif // APP_MSG_H
