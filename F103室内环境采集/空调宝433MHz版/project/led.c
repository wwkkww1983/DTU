#include "stm32f0xx.h"
#include "led.h"

void LED_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed =GPIO_Speed_Level_3;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_SetBits(GPIOA, GPIO_Pin_8);
}

void LED_Open(void)
{
	 GPIO_ResetBits(GPIOA, GPIO_Pin_8);
}

void LED_Close(void)
{
		GPIO_SetBits(GPIOA, GPIO_Pin_8);
}
void LED_Toggle(void)
{
//	GPIOA->ODR ^=GPIO_Pin_11;
  GPIO_WriteBit(GPIOA, GPIO_Pin_8, 
		               (BitAction)((1-GPIO_ReadOutputDataBit(GPIOA, GPIO_Pin_8))));
}
