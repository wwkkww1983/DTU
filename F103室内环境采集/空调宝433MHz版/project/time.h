#ifndef _TIME_H_
#define _TIME_H_

#include "stm32f0xx.h"



void TIM_INT_Config(void);
void TIM_OUT_Config(void);
void TIM3_Init(void);
void TIM3_Config(void);


#endif  /* _TIME_H_ */
