#ifndef CONTROL_H
#define CONTROL_H
#include "os.h"
#include "phy.h"
//#include "device.h"

#define DEVICE_UPGRADE_READY   0x03
#define DEVICE_UPGRADE_START   0x04
#define DEVICE_UPGRADE_SUCCESS 0x05

#define UPGRADE_PROGRAM_PART_SIZE 64

//无线控制帧的处理，包括强制设备控制和设备升级
typedef enum
{
    UPGRADE_IDLE,
    UPGRADE_START_ACK,
    UPGRADE_READY_FOR_RECV,
    UPGRADE_SUCCESS,
} UPGRADE_FLAG;

typedef struct
{
    uint8_t type;
    uint8_t device_type[DEVICE_TYPE_LEN];
} UPGRADE_DEVICE_TYPE;

typedef struct
{
    UPGRADE_DEVICE_TYPE devInfo;
    uint8_t version[DEVICE_VERSION_LEN];
    uint8_t part_count[2];
} UPGRADE_CONTENT_INFO;

typedef struct
{
    UPGRADE_DEVICE_TYPE devInfo;
    uint8_t part_index[2];
    uint8_t data[UPGRADE_PROGRAM_PART_SIZE];
} UPGRADE_DATA_T;

void ctrl_deal(phy_header_t *phy);
void ctrl_upgrade_transmit(uint8_t len);
void *ctrl_upgrade_get_trans_payload(void);
//void ctrl_upgrade_start(bool state);

#endif // CONTROL_H
