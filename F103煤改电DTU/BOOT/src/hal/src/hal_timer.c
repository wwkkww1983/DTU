#include "Hal.h"
#include "os.h"

static void TIM5_Init(void);
static void TIM5InterruptConfig(void);

void HalTimerInit(void)
{
	RCC_ClocksTypeDef RCC_ClocksStructure;
	RCC_GetClocksFreq(&RCC_ClocksStructure);
	//TODO  那个准?
	//SysTick_Config(RCC_ClocksStructure.SYSCLK_Frequency / 1000);
    //TIM5_Init();
    //TIM5InterruptConfig();
}

#if 0
static void TIM5InterruptConfig(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	/* Enable the TIM5 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	NVIC_Init(&NVIC_InitStructure);
}

static void TIM5_Init(void)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
  
  //这个就是自动装载的计数值，由于计数是从0开始的
  TIM_TimeBaseStructure.TIM_Period = (100 - 1);
  // 这个就是预分频系数，当由于为0时表示不分频所以要减1
  TIM_TimeBaseStructure.TIM_Prescaler = (80 - 1); //720-1
 
  // 使用的采样频率之间的分频比例
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  //向上计数
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  //初始化定时器5
  TIM_TimeBaseInit(TIM5, &TIM_TimeBaseStructure);


  /* TIM IT enable */ //定时器中断使能
  TIM_ITConfig(TIM5, TIM_IT_Update, ENABLE);

  /* TIM5 enable counter */
  TIM_Cmd(TIM5, ENABLE);  //计数器使能，开始工作
}

void TIM5_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM5, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM5, TIM_IT_Update);
		os_timer_pass_ms();
	}
}
#endif
