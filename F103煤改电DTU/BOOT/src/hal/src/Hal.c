#include "Hal.h"
#include "userSys.h"

#define HAL_CPU_HZ  (8000000UL)  //8MHZ

void HalInit(void)
{	
	SysTick_Config(HAL_CPU_HZ / 1000);
	HalUartConfig(LOG_UART, 115200, NULL);
	//RTCInit();
	ModuleTesterCheckUpdate();

	HalGPIOInitialize();
	HalTimerInit();
	printf("Hal Init Success...\n");
	
}

void HalPoll(void)
{
    hal_wdt_feed();
}

void HalRestart(void)
{
	SysLog("HalRestart!!!!!!!!!!");
    NVIC_SystemReset();
}


void HalWdtFeed(void)
{
	hal_wdt_feed();
}

#define REG32(addr)     (*((volatile uint32_t *)(addr)))
#define REG16(addr)     (*((volatile uint16_t *)(addr)))
#define REG8(addr)      (*((volatile uint8_t *)(addr)))


typedef  void (*pFunction)(void);
void HalJumpAppAddr(uint32_t addr)
{
    uint32_t JumpAddress;
    pFunction Jump_To_Application;

    // Test if user code is programmed starting from address "APPLICATION_ADDRESS" //
    if((REG32(addr) & 0x2FFE0000) == 0x20000000)
    {
        // Jump to user application //
        JumpAddress = REG32(addr + 4);
        Jump_To_Application = (pFunction)JumpAddress;

        // Initialize user application's Stack Pointer //
        __set_MSP(REG32(addr));

        // Jump to application //
        Jump_To_Application();
    }
}



