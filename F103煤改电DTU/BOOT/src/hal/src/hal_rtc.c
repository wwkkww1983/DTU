#include "Hal.h"
#include "userSys.h"

const u8 mon_table[12]={31,28,31,30,31,30,31,31,30,31,30,31};
static rtc_time_t g_rtcTime = {0};

void RTC_Configuration(void)
{
	/* Enable PWR and BKP clocks */
	 RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
	
	 /* Allow access to BKP Domain */
	 PWR_BackupAccessCmd(ENABLE);
	
	 /* Reset Backup Domain */
	 BKP_DeInit(); //??0XA5A5 ?????????
	
	 /* Enable LSE */
	 RCC_LSEConfig(RCC_LSE_ON);
	 /* Wait till LSE is ready */
	 hal_wait_ms_cond(20, !(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET) );
	 /* Select LSE as RTC Clock Source */
	 RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
	
	 /* Enable RTC Clock */
	 RCC_RTCCLKCmd(ENABLE);
	
	 /* Wait for RTC registers synchronization */
	 RTC_WaitForSynchro();
	
	 /* Wait until last write operation on RTC registers has finished */
	 RTC_WaitForLastTask();
	
	 /* Enable the RTC Second */
	 RTC_ITConfig(RTC_IT_SEC, ENABLE);
	
	 /* Wait until last write operation on RTC registers has finished */
	 RTC_WaitForLastTask();
	
	 /* Set RTC prescaler: set RTC period to 1sec */
	 RTC_SetPrescaler(32767); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */
	
	 /* Wait until last write operation on RTC registers has finished */
	 RTC_WaitForLastTask();


}

void RTCInit(void)
{
    if(BKP_ReadBackupRegister(BKP_DR1)!=0x1016)
    {
    	printf("RTC_Init1\n");	
        RTC_Configuration();
    }
    else
    {
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
		PWR_BackupAccessCmd(ENABLE);

    	printf("RTC_Init2\n");
        RTC_WaitForSynchro();
        RTC_ITConfig(RTC_IT_SEC, ENABLE);
        RTC_WaitForLastTask();
    }

	
    NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); 
	NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

u8 Is_Leap_Year(u16 pyear)
{
    if(pyear%4==0)
    {
        if(pyear%100==0)
        {
            if(pyear%400==0)    return 1;
            else    return 0;
        }
        else
            return 1;
    }
    else
        return 0;
}

u32 RTCTransSecCount(u16 year,u8 mon,u8 day,u8 hour,u8 min,u8 sec)
{
	u16 t;
	u32 secCount=0;
	if(year<1970||year>2099)
		return 1;//����
	for(t=1970;t<year;t++)
	{
		if(Is_Leap_Year(t))
			secCount+=31622400;
		else
			secCount+=31536000;    
	}
	mon-=1;
	for(t=0;t<mon;t++)
	{
		secCount+=(u32)mon_table[t]*86400;
		if(Is_Leap_Year(year)&&t==1)
			secCount+=86400;
	}

	secCount+=(u32)(day-1)*86400;
	secCount+=(u32)hour*3600;
	secCount+=(u32)min*60;
	secCount+=sec;
	secCount-=8*3600;
	
	return secCount;
}

rtc_time_t *RTCTransDate(u32 secCount)
{
    static u16 dayCount=0;
    u32 tmp=0;
    u16 tmp1=0;
	//������
	secCount+=8*3600;
    tmp=(secCount)/86400;
    if(dayCount!=tmp)
    {
        dayCount=tmp;
        tmp1=1970;
        while(tmp>=365)
        {
            if(Is_Leap_Year(tmp1))
            {
                if(tmp>=366)    
                    tmp-=366;
                else
                {
                    break;
                }
            }
            else
                tmp-=365;
            tmp1++;
        }
        g_rtcTime.year=tmp1;
        tmp1=0;
        while(tmp>=28)
        {
            if(Is_Leap_Year(g_rtcTime.year)&&tmp1==1)
            {
                if(tmp>=29)    
                    tmp-=29;
                else
                    break;
            }
            else
            {
                if(tmp>=mon_table[tmp1])
                    tmp-=mon_table[tmp1];
                else
                    break;
            }
            tmp1++;
        }
        g_rtcTime.month=tmp1+1;
        g_rtcTime.date=tmp+1;
    }
    tmp=secCount%86400;
    g_rtcTime.hour=tmp/3600;
    g_rtcTime.min=(tmp%3600)/60;
    g_rtcTime.sec=(tmp%3600)%60;
	
	return &g_rtcTime;
}

uint8_t SetRTCTime(uint32_t secCount)
{
	if(BKP_ReadBackupRegister(BKP_DR1)!=0x1016)
	{
		BKP_WriteBackupRegister(BKP_DR1, 0x1016);
	}
	
    RTC_WaitForLastTask();
    RTC_SetCounter(secCount);
    return 0;
}

uint32_t GetRTCTime(void)
{
	if(BKP_ReadBackupRegister(BKP_DR1)!=0x1016)
	{
		return 0;
	}
    return RTC_GetCounter();
}

void RTC_IRQHandler(void)
{         
    if (RTC_GetITStatus(RTC_IT_SEC) != RESET)
    {
    	
    }
    if(RTC_GetITStatus(RTC_IT_ALR)!= RESET)
    {
        RTC_ClearITPendingBit(RTC_IT_ALR);        
    }                                                    
    RTC_ClearITPendingBit(RTC_IT_SEC|RTC_IT_OW);
    RTC_WaitForLastTask();                                                   
}

