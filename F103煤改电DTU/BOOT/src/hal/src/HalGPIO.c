#include "Hal.h"

static void HalButtonInit(void);
static void HalLedInit(void);

void HalGPIOInitialize(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,  ENABLE);
	HalButtonInit();
	HalLedInit();
}

void HalGPIOPoll(void)
{

}

// 弃用初始化
#if 0
#define IO_MAX 16

static EXTITrigger_TypeDef trgToIntType(HalGPIOIntTrigger_t trg);

void numToGPIO(HalIO_t num, HALGPIO *gpio)
{
    HalIO_t port;
	
    port           = (num >> 4) & 0x0f;
    gpio->pin      = (uint16_t)(1 << (num & 0x0f)); //eg. GPIO_Pin_11=(uint16_t)0x0800
    gpio->extiLine = (uint32_t)(1 << (num & 0x0f)); //eg. EXTI_Line3 = (uint32_t)0x00008
    gpio->intPin   = (uint8_t)(num & 0x0f);         //eg. GPIO_PinSource1=(uint8_t)0x01
    
    switch(port)
    {
        case 0:
            gpio->port = GPIOA;
            gpio->rcc  = RCC_AHBPeriph_GPIOA;
            break;
        case 1:
            gpio->port = GPIOB;
            gpio->rcc  = RCC_AHBPeriph_GPIOB;
            break;
        case 2:
            gpio->port = GPIOC;
            gpio->rcc  = RCC_AHBPeriph_GPIOC;
            break;
        case 3:
            gpio->port = GPIOD;
            gpio->rcc  = RCC_AHBPeriph_GPIOD;
            break;
        case 4:
            gpio->port = GPIOE;
            gpio->rcc  = RCC_AHBPeriph_GPIOE;
            break;
        case 5:
            gpio->port = GPIOF;
            gpio->rcc  = RCC_AHBPeriph_GPIOF;
            break;
        default:
            break;
    }

}

/*
*@num=0xAB,( A=portA-F(0-5).  B=pinX(X=0-F))
*/
void HalGPIOInit(HalIO_t num, HalGPIODir_t dir)
{

    HALGPIO gpio;
    GPIO_InitTypeDef gpioStruct;

    numToGPIO(num, &gpio);

    RCC_APB2PeriphClockCmd(gpio.rcc, ENABLE); //使能时钟

    gpioStruct.GPIO_Pin   = gpio.pin;
    gpioStruct.GPIO_Speed = GPIO_Speed_2MHz;
    switch(dir)
    {
        case HAL_GPIO_DIR_IN_WITH_INT:
            gpioStruct.GPIO_Mode  = GPIO_Mode_IN;
            break;
        case HAL_GPIO_DIR_IN:
            gpioStruct.GPIO_Mode  = GPIO_Mode_IN;
            break;
        case HAL_GPIO_DIR_OUT:
            gpioStruct.GPIO_Mode  = GPIO_Mode_OUT;
            break;
    }

    GPIO_Init(gpio.port, &gpioStruct);
}

static EXTITrigger_TypeDef trgToIntType(HalGPIOIntTrigger_t trg)
{
    EXTITrigger_TypeDef intType;
    switch(trg)
    {
        case HAL_GPIO_INT_TRG_RISING_EDGE:
            intType = EXTI_Trigger_Rising;
            break;

        case HAL_GPIO_INT_TRG_FAILLING_EDGE:
            intType = EXTI_Trigger_Falling;
            break;

        case HAL_GPIO_INT_TRG_ANY_EDGE:
            intType = EXTI_Trigger_Rising_Falling;
            break;
    }
    return intType;
}

void HalGPIOSetInt(HalIO_t io, HalGPIOIntTrigger_t trg, HalGPIOIntCallback_t cb)
{
    HALGPIO gpio;
    EXTI_InitTypeDef EXTI_InitStructure;

    numToGPIO(io, &gpio);

    EXTI_InitStructure.EXTI_Line    = gpio.extiLine;
    EXTI_InitStructure.EXTI_Mode    = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = trgToIntType(trg);
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}



void HalGPIOToggle(HalIO_t io)
{
    HalGPIOSet(io, !HalGPIOGet(io, HAL_GPIO_DIR_OUT));
}

/*
*@value : write pin value 1 or 0.
*/
void HalGPIOSet(HalIO_t io, uint8_t value)
{
    HALGPIO gpio;

    numToGPIO(io, &gpio);
	
    GPIO_WriteBit(gpio.port, gpio.pin, (BitAction)value);
}

/*
*@dir: GPIO方向，输入输出
*/
uint8_t HalGPIOGet(HalIO_t io, HalGPIODir_t dir)
{
    HALGPIO gpio;
    uint8_t val = 0;

    numToGPIO(io, &gpio);
	
    if(HAL_GPIO_DIR_IN == dir)
    {
        val = GPIO_ReadInputDataBit(gpio.port, gpio.pin);
    }
    else if(HAL_GPIO_DIR_OUT == dir)
    {
        val = GPIO_ReadOutputDataBit(gpio.port, gpio.pin);
    }
	
    return val;
}
#endif

//暂不需要模拟串口
#if 0
/* GPIO 模拟串口输出 */
#define LOG_OUT_PIN     GPIO_Pin_9
#define LOG_OUT_PORT    GPIOA
#define LOG_OUT_HIGH    (LOG_OUT_PORT->BSRR=LOG_OUT_PIN)    //GPIO_SetBits(LOG_OUT_PORT, LOG_OUT_PIN)
#define LOG_OUT_LOW     (LOG_OUT_PORT->BRR=LOG_OUT_PIN)     //GPIO_ResetBits(LOG_OUT_PORT, LOG_OUT_PIN);

static uint32_t g_LogOutDelay;
static void HalLogOutInit(void)
{
	#if 0
    GPIO_InitTypeDef GPIO_InitStructure;

	//RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

    GPIO_InitStructure.GPIO_Pin = LOG_OUT_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(LOG_OUT_PORT, &GPIO_InitStructure);
    GPIO_SetBits(LOG_OUT_PORT, LOG_OUT_PIN);

    //g_LogOutDelay = 45; /* 蓝灯板子 */
    g_LogOutDelay = 46;
	#endif
}

static void HalLogOutDelay(uint32_t num)
{
    while(num--)
    {
        __NOP();
    }
}

static void HalLogOutChar(uint8_t ch)
{
    uint32_t i = 8;

    LOG_OUT_LOW;
    HalLogOutDelay(g_LogOutDelay);

    while(i--)
    {
        if(ch & 0x01)
        {
            LOG_OUT_HIGH;
        }
        else
        {
            LOG_OUT_LOW;
        }

        ch >>= 0x01;
        HalLogOutDelay(g_LogOutDelay);
    }

    LOG_OUT_LOW;
    HalLogOutDelay(g_LogOutDelay);
    LOG_OUT_HIGH;
    HalLogOutDelay(g_LogOutDelay);
}

int putchar(int ch)
{
    char c = ch;
#if 0
    HalUartWrite(HAL_UART_1, (uint8_t *)&c, 1);
#else
    HalLogOutChar(c);
#endif
    return ch;
}

static void HalLogOutBaudrateTest(void)
{
    for(g_LogOutDelay = 1; g_LogOutDelay < 100; g_LogOutDelay++)
    {
        printf("HalLogOutBaudrateTest %d \r\n", g_LogOutDelay);
    }

    while(1);
}
#endif
static void HalButtonInit(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = KEY_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(KEY_PORT, &GPIO_InitStructure);
}

static void HalLedInit(void)
{

    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;

    GPIO_InitStructure.GPIO_Pin = LED1_PIN;
    GPIO_Init(LED1_PORT, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = LED2_PIN;
	GPIO_Init(LED2_PORT, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = LED3_PIN;
	GPIO_Init(LED3_PORT, &GPIO_InitStructure);

	GPIO_SetBits(LED3_PORT, LED3_PIN);
	GPIO_SetBits(LED2_PORT, LED2_PIN);
	GPIO_SetBits(LED1_PORT, LED1_PIN);

	GPIO_ResetBits(LED3_PORT, LED3_PIN);
	GPIO_ResetBits(LED2_PORT, LED2_PIN);
	GPIO_ResetBits(LED1_PORT, LED1_PIN);
}

