#include "Hal.h"

void hal_led_set_up(GPIO_TypeDef *port, uint16_t pin)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(port, &GPIO_InitStructure);

	hal_led_off(port,pin);
}

void hal_led_on(GPIO_TypeDef *port, uint16_t pin)
{
  GPIO_ResetBits(port, pin);
}

void hal_led_off(GPIO_TypeDef *port, uint16_t pin)
{
  GPIO_SetBits(port, pin);
}

void hal_led_toggle(GPIO_TypeDef *port, uint16_t pin)
{

}

void hal_led_blink(GPIO_TypeDef *port, uint16_t pin, uint8_t times)
{

}

void hal_gateway_led_on(GATEWAY_LED_NO no)
{
	if(GATEWAY_LED1 == no)
	{
		GPIO_ResetBits(LED1_PORT, LED1_PIN);
	}
	else
	{
		GPIO_ResetBits(LED2_PORT, LED2_PIN);
	}	
}

void hal_gateway_led_off(GATEWAY_LED_NO no)
{
	if(GATEWAY_LED1 == no)
	{
		GPIO_SetBits(LED1_PORT, LED1_PIN);
	}
	else
	{
		GPIO_SetBits(LED2_PORT, LED2_PIN);
	}	
}

static void hal_gateway_led_toggle(GATEWAY_LED_NO no)
{
	if(GATEWAY_LED1 == no)
	{
		if(GPIO_ReadOutputDataBit(LED1_PORT, LED1_PIN))
		{
			GPIO_ResetBits(LED1_PORT, LED1_PIN);
		}
		else
		{
			GPIO_SetBits(LED1_PORT, LED1_PIN);
		}
	}
	else
	{
		if(GPIO_ReadOutputDataBit(LED2_PORT, LED2_PIN))
		{
			GPIO_ResetBits(LED2_PORT, LED2_PIN);
		}
		else
		{
			GPIO_SetBits(LED2_PORT, LED2_PIN);
		}
	}		
}

static uint32_t led1_blink_time = 0;
static uint32_t led2_blink_time = 0;

#define LedTimeBeyond(cur, last, past) ((cur) - (last) > (past))

void hal_gateway_led_blink(GATEWAY_LED_NO no,GATEWAY_LED_BLINK_TYPE type)
{	
	if(GATEWAY_LED1 == no)
	{
		if(FAST_LED == type)
		{
			if(LedTimeBeyond(os_timer_get_systime(),led1_blink_time,500))
			{
				led1_blink_time = os_timer_get_systime();
				hal_gateway_led_toggle(GATEWAY_LED1);
			}
		}
		else
		{
			if(LedTimeBeyond(os_timer_get_systime(),led1_blink_time,1000))
			{
				led1_blink_time = os_timer_get_systime();
				hal_gateway_led_toggle(GATEWAY_LED1);
			}
		}		
	}
	else
	{
		if(FAST_LED == type)
		{
			if(LedTimeBeyond(os_timer_get_systime(),led2_blink_time,500))
			{
				led2_blink_time = os_timer_get_systime();
				hal_gateway_led_toggle(GATEWAY_LED2);
			}
		}
		else
		{
			if(LedTimeBeyond(os_timer_get_systime(),led2_blink_time,1000))
			{
				led2_blink_time = os_timer_get_systime();
				hal_gateway_led_toggle(GATEWAY_LED2);
			}
		}			
	}
}




