#include "Hal.h"

static uint8_t g_waitType;

static void hal_wait_TIM_init(void);
static void hal_wait_TIM_config_us(void);
static void hal_wait_TIM_config_ms(void);

void hal_wait_init(uint8_t type)
{
    g_waitType = type;
    hal_wait_TIM_init();
    if(type == HAL_WAIT_US)
    {
        hal_wait_TIM_config_us();
    }
    if(type == HAL_WAIT_MS)
    {
        hal_wait_TIM_config_ms();
    }
}

void hal_wait_check(uint8_t type)
{
    if(type != g_waitType)
    {
        hal_wait_init(type);
    }
}

void hal_wait(uint16_t utime, uint8_t type)
{
    uint16_t count = 0;
    while(count < utime)
    {
        hal_wait_check(type);
        hal_wdt_feed();
		
        if((HAL_WAIT_TIM->SR)&TIM_IT_Update)
        {
            TIM_ClearITPendingBit(HAL_WAIT_TIM, TIM_IT_Update);
            count++;
        }
    }
}

void hal_wait_us(uint16_t usec)
{
    hal_wait_init(HAL_WAIT_US);
    hal_wait(usec, HAL_WAIT_US);
}

void hal_wait_ms(uint16_t msec)
{
    hal_wait_init(HAL_WAIT_MS);
    hal_wait(msec, HAL_WAIT_MS);
}


static void hal_wait_TIM_init(void)
{
    RCC_APB1PeriphClockCmd(HAL_WAIT_TIM_RCC, ENABLE);
}
static void hal_wait_TIM_config_us(void)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    RCC_APB1PeriphClockCmd(HAL_WAIT_TIM_RCC, ENABLE); //TIM16 时钟使能

    TIM_TimeBaseStructure.TIM_Period = 10;      //1us
    TIM_TimeBaseStructure.TIM_Prescaler = 8-1;    //预分频器,1M的计数频率,1us加1.
    TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0x00;

    TIM_TimeBaseInit(HAL_WAIT_TIM, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx

	TIM_ClearFlag(HAL_WAIT_TIM, TIM_IT_Update);

    TIM_Cmd(HAL_WAIT_TIM, ENABLE);   //使能定时器16	
}

static void hal_wait_TIM_config_ms(void)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    RCC_APB1PeriphClockCmd(HAL_WAIT_TIM_RCC, ENABLE); //TIM16 时钟使能

    TIM_TimeBaseStructure.TIM_Period = 100-1;      //1ms
    TIM_TimeBaseStructure.TIM_Prescaler = 80-1;//720-1     //预分频器,1M的计数频率,1us加1.
    TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0x00;
    TIM_TimeBaseInit(HAL_WAIT_TIM, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx

	TIM_ClearFlag(HAL_WAIT_TIM, TIM_IT_Update);

    TIM_Cmd(HAL_WAIT_TIM, ENABLE);   //使能定时器16
}

