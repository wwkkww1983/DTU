#ifndef HAL_LED_H
#define HAL_LED_H
#include "os.h"

typedef enum
{	
	GATEWAY_LED1,	/* 显示433设备在线 */
	GATEWAY_LED2,	/* wifi 配网 & 显示 手环在线 */
	GATEWAY_LED3,
}GATEWAY_LED_NO;

typedef enum
{
	FAST_LED,
	SLOW_LED,
}GATEWAY_LED_BLINK_TYPE;

void hal_led_set_up(GPIO_TypeDef *port, uint16_t pin);;

void hal_led_on(GPIO_TypeDef *port, uint16_t pin);

void hal_led_off(GPIO_TypeDef *port, uint16_t pin);

void hal_led_toggle(GPIO_TypeDef *port, uint16_t pin);

void hal_led_blink(GPIO_TypeDef *port, uint16_t pin, uint8_t times);


void hal_gateway_led_blink(GATEWAY_LED_NO no,GATEWAY_LED_BLINK_TYPE type);
void hal_gateway_led_off(GATEWAY_LED_NO no);
void hal_gateway_led_on(GATEWAY_LED_NO no);


#endif

