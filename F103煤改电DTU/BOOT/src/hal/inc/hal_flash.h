#ifndef HAL_FLASH_H
#define HAL_FLASH_H
#include "os.h"

void hal_flash_write(unsigned int flash_addr, const void *src_buf, unsigned short len);
int  hal_flash_read(unsigned int flash_addr, unsigned char *buf, unsigned short len);

#endif // HAL_FLASH_H

