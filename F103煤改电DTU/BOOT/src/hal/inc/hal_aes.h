#ifndef HAL_AES_H
#define HAL_AES_H
//#include "os.h"

#define FUNC_IN_FLASH

int AesCTREncrypt(const unsigned char *in, unsigned char *out, int len,
                  const unsigned char *key, int keylen/*, unsigned char *iv*/);

int AesEncrypt(const unsigned char *in, unsigned char *out, int len,
               const unsigned char *key, int keylen);
int AesDecrypt(const unsigned char *in, unsigned char *out, int len,
               const unsigned char *key, int keylen);

FUNC_IN_FLASH int AesCTRDecrypt(const unsigned char *in, unsigned char *out, int len,
                                const unsigned char *key, int keylen/*, unsigned char *iv*/);

#endif
