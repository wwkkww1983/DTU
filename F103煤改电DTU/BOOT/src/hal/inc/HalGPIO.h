#ifndef HALGPIO_H
#define HALGPIO_H

#include "stm32f10x.h"
typedef uint8_t HalIO_t;

#define HAL_GPIO_PORTA_PIN0     (HalIO_t)0x00
#define HAL_GPIO_PORTA_PIN1     (HalIO_t)0x01
#define HAL_GPIO_PORTA_PIN2     (HalIO_t)0x02
#define HAL_GPIO_PORTA_PIN3     (HalIO_t)0x03
#define HAL_GPIO_PORTA_PIN4     (HalIO_t)0x04
#define HAL_GPIO_PORTA_PIN5     (HalIO_t)0x05
#define HAL_GPIO_PORTA_PIN6     (HalIO_t)0x06
#define HAL_GPIO_PORTA_PIN7     (HalIO_t)0x07
#define HAL_GPIO_PORTA_PIN8     (HalIO_t)0x08
#define HAL_GPIO_PORTA_PIN9     (HalIO_t)0x09
#define HAL_GPIO_PORTA_PIN10    (HalIO_t)0x0a
#define HAL_GPIO_PORTA_PIN11    (HalIO_t)0x0b
#define HAL_GPIO_PORTA_PIN12    (HalIO_t)0x0c
#define HAL_GPIO_PORTA_PIN13    (HalIO_t)0x0d
#define HAL_GPIO_PORTA_PIN14    (HalIO_t)0x0e
#define HAL_GPIO_PORTA_PIN15    (HalIO_t)0x0f

#define HAL_GPIO_PORTB_PIN0     (HalIO_t)0x10
#define HAL_GPIO_PORTB_PIN1     (HalIO_t)0x11
#define HAL_GPIO_PORTB_PIN2     (HalIO_t)0x12
#define HAL_GPIO_PORTB_PIN3     (HalIO_t)0x13
#define HAL_GPIO_PORTB_PIN4     (HalIO_t)0x14
#define HAL_GPIO_PORTB_PIN5     (HalIO_t)0x15
#define HAL_GPIO_PORTB_PIN6     (HalIO_t)0x16
#define HAL_GPIO_PORTB_PIN7     (HalIO_t)0x17
#define HAL_GPIO_PORTB_PIN8     (HalIO_t)0x18
#define HAL_GPIO_PORTB_PIN9     (HalIO_t)0x19
#define HAL_GPIO_PORTB_PIN10    (HalIO_t)0x1a
#define HAL_GPIO_PORTB_PIN11    (HalIO_t)0x1b
#define HAL_GPIO_PORTB_PIN12    (HalIO_t)0x1c
#define HAL_GPIO_PORTB_PIN13    (HalIO_t)0x1d
#define HAL_GPIO_PORTB_PIN14    (HalIO_t)0x1e
#define HAL_GPIO_PORTB_PIN15    (HalIO_t)0x1f

#define HAL_GPIO_PORTC_PIN0     (HalIO_t)0x20
#define HAL_GPIO_PORTC_PIN1     (HalIO_t)0x21
#define HAL_GPIO_PORTC_PIN2     (HalIO_t)0x22
#define HAL_GPIO_PORTC_PIN3     (HalIO_t)0x23
#define HAL_GPIO_PORTC_PIN4     (HalIO_t)0x24
#define HAL_GPIO_PORTC_PIN5     (HalIO_t)0x25
#define HAL_GPIO_PORTC_PIN6     (HalIO_t)0x26
#define HAL_GPIO_PORTC_PIN7     (HalIO_t)0x27
#define HAL_GPIO_PORTC_PIN8     (HalIO_t)0x28
#define HAL_GPIO_PORTC_PIN9     (HalIO_t)0x29
#define HAL_GPIO_PORTC_PIN10    (HalIO_t)0x2a
#define HAL_GPIO_PORTC_PIN11    (HalIO_t)0x2b
#define HAL_GPIO_PORTC_PIN12    (HalIO_t)0x2c
#define HAL_GPIO_PORTC_PIN13    (HalIO_t)0x2d
#define HAL_GPIO_PORTC_PIN14    (HalIO_t)0x2e
#define HAL_GPIO_PORTC_PIN15    (HalIO_t)0x2f

#define HAL_GPIO_PORTD_PIN0     (HalIO_t)0x30
#define HAL_GPIO_PORTD_PIN1     (HalIO_t)0x31
#define HAL_GPIO_PORTD_PIN2     (HalIO_t)0x32
#define HAL_GPIO_PORTD_PIN3     (HalIO_t)0x33
#define HAL_GPIO_PORTD_PIN4     (HalIO_t)0x34
#define HAL_GPIO_PORTD_PIN5     (HalIO_t)0x35
#define HAL_GPIO_PORTD_PIN6     (HalIO_t)0x36
#define HAL_GPIO_PORTD_PIN7     (HalIO_t)0x37
#define HAL_GPIO_PORTD_PIN8     (HalIO_t)0x38
#define HAL_GPIO_PORTD_PIN9     (HalIO_t)0x39
#define HAL_GPIO_PORTD_PIN10    (HalIO_t)0x3a
#define HAL_GPIO_PORTD_PIN11    (HalIO_t)0x3b
#define HAL_GPIO_PORTD_PIN12    (HalIO_t)0x3c
#define HAL_GPIO_PORTD_PIN13    (HalIO_t)0x3d
#define HAL_GPIO_PORTD_PIN14    (HalIO_t)0x3e
#define HAL_GPIO_PORTD_PIN15    (HalIO_t)0x3f


#define HAL_GPIO_CC1101_GD0   HAL_GPIO_PORTA_PIN11
#define HAL_GPIO_CC1101_GD2   HAL_GPIO_PORTA_PIN12
#define HAL_GPIO_RX_ENS       HAL_GPIO_PORTA_PIN9
#define HAL_GPIO_TX_ENS       HAL_GPIO_PORTA_PIN10
#define HAL_GPIO_ENC_INT      HAL_GPIO_PORTB_PIN0
#define HAL_GPIO_ENC_RESET    HAL_GPIO_PORTB_PIN1
#define HAL_GPIO_BUTTON       HAL_GPIO_PORTA_PIN2


/** GPIO方向 */
typedef enum
{
    HAL_GPIO_DIR_IN, // 输入
    HAL_GPIO_DIR_IN_WITH_INT, //输入同时启用中断
    HAL_GPIO_DIR_OUT, //输出
} HalGPIODir_t;

/** 中断触发??*/
typedef enum
{
    HAL_GPIO_INT_TRG_FAILLING_EDGE, 
    HAL_GPIO_INT_TRG_RISING_EDGE, 
    HAL_GPIO_INT_TRG_ANY_EDGE, 
} HalGPIOIntTrigger_t;

struct HalGPIOCapture_st;
typedef struct HalGPIOCapture_st HalGPIOCapture_t;

typedef void (*HalGPIOCaptureHandler_t)(HalGPIOCapture_t *cfg, uint8_t ioLevel, uint32_t us);

struct HalGPIOCapture_st
{
    uint8_t value;
    HalIO_t io;
    uint32_t startTime;
    HalGPIOCaptureHandler_t handler;
    struct HalGPIOCapture_st *next;
};

/*gpio ????*/
typedef struct halGpio
{
    GPIO_TypeDef *port;
    uint16_t pin;
    uint32_t rcc; //???
    uint8_t  intPort;
    uint8_t  intPin;
    uint32_t extiLine;

} HALGPIO;

/**
 *  取反IO状??
 *  @param io 指定IO
 */
void HalGPIOToggle(HalIO_t io);

/**
 *  GPIO 初始??
 *  @param io 指定IO
 *  @param dir IO方向
 */
void HalGPIOInit(HalIO_t num, HalGPIODir_t dir);

typedef void (*HalGPIOIntCallback_t)(HalIO_t io, uint8_t ioLevel);

/**
 *  设置GPIO触发方式
 *  @param io  指定IO
 *  @param trg 触发方式
 *  @param cb 中断回调函数
 */
void HalGPIOSetInt(HalIO_t io, HalGPIOIntTrigger_t trg, HalGPIOIntCallback_t cb);

/**
 *  设置IO的电平状??
 *  @param io 指定IO
 *  @param value 0低电平，1高电??
 */
void HalGPIOSet(HalIO_t io, uint8_t value);

/**
 *  获取IO电平状??
 *  @param HalIO_t 指定IO
 *  @return 0低电平，1高电??
 */
uint8_t HalGPIOGet(HalIO_t io, HalGPIODir_t dir);

/**
 *  注册输入捕捉
 *
 *  @param capture 捕捉配置
 *  @param io      指定IO
 *  @param handler IO捕捉处理函数
 */
void HalGPIOCaptureRegist(HalGPIOCapture_t *capture, HalIO_t io, HalGPIOCaptureHandler_t handler);

/**
 *  取消输入捕捉
 *
 *  @param capture 捕捉配置
 */
void HalGPIOCaptureUnRegist(HalGPIOCapture_t *capture);

void HalGPIOInitialize(void);



#endif // HALGPIO_H

