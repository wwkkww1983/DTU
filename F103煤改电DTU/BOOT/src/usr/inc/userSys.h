#ifndef SYS_H
#define SYS_H

#include "hal.h"
#include "stm32f10x.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "SysButton.h"
#include "SysTimer.h"
#include "VTList.h"
#include "VTStaticQueue.h"
#include "menuConfig.h"

typedef uint32_t SysTime_t;

typedef struct
{
    SysTime_t hour;
    SysTime_t min;
    SysTime_t sec;
} SysUTCTime_t;

typedef enum
{
    SYS_RESTART_HARDFAULT,
    SYS_RESTART_NET_MALLOC,
    SYS_RESTART_NORMAL,
    SYS_RESTART_HW_ERR,
    SYS_RESTART_TIMEOUT,
} SYS_RESTART_INDEX;

#define BLE_ENABLE  0
#define WM_ENABLE  1

#define USE_2G_MODULE 1
#if USE_2G_MODULE
#define RT_PROPERTY_ENABLE  0
#else
#define RT_PROPERTY_ENABLE  1
#endif

#define NULL 0
#define bool unsigned char
#define false 0
#define true  1
/*
#define SYS_LOG_EMERG     5
#define SYS_LOG_CRIT      4
#define SYS_LOG_ERR       3
#define SYS_LOG_WARNING   2
#define SYS_LOG_INFO      1
#define SYS_LOG_DEBUG     0

#define SYS_LOG_LEVEL    SYS_LOG_DEBUG//SYS_LOG_WARNING
*/

#define TEST_DISPLAY_SIGNAL_VALUE

#define COMPILE_WITH_LOG

#ifdef COMPILE_WITH_LOG
    #define SysPrintf(...) HalWdtFeed();printf(__VA_ARGS__)
    //#define SysLog(...) SysPrintf("%s:%s[%d]:", __FILE__, __func__, __LINE__);SysPrintf(__VA_ARGS__);SysPrintf("\n");
    #define SysLog(...) SysPrintf("%s[%d]:",__func__, __LINE__);SysPrintf(__VA_ARGS__);SysPrintf("\n");
    #define SysLogError(...) SysPrintf("Error!!!:");SysLog(SYS_LOG_ERR, __VA_ARGS__)
    #define SYS_ASSERT(desc, exp) if(!(exp)){SysLog("assert fail! %s(%s)", desc, #exp);perror("error"); SysRestart(SYS_RESTART_HARDFAULT);}
#else
    #define SysPrintf(...)
    #define SysLogError(...)
    #define SysLog(...)
    #define SYS_ASSERT(desc, exp)
#endif // SYS_LOG

#define SYS_GATEWAY_VERSION "1.0.1.2"
#define SYS_GATEWAY_VERSION_NUM  4

#define SYS_ENTER_CRITICAL(istate) istate = HalInterruptsGetEnable();HalInterruptsSetEnable(0);
#define SYS_EXIT_CRITICAL(istate) HalInterruptsSetEnable(istate);

#define SYS_TCP_SEND_MUTEX_LOCKED()    SysTcpSendMutexLock(true)
#define SYS_TCP_SEND_MUTEX_UNLOCKED()  SysTcpSendMutexLock(false)
#define SYS_IS_TCP_SEND_LOCKED()       SysTcpSendMutexState()
//#define SYS_MAC_CHIP_RESET()           SysMacChipReset()
//OTA
#define SYS_OTA_DOWNLOAD_START      1
#define SYS_OTA_DOWNLOAD_ING        2
#define SYS_OTA_DOWNLOAD_SUCCESS    3
#define SYS_OTA_DOWNLOAD_FAILE      4
#define SYS_OTA_UPGRADE_START       5
#define SYS_OTA_UPGRADE_ING         6
#define SYS_OTA_UPGRADE_SUCCESS     7
#define SYS_OTA_UPGRADE_FAILE       8

//TOOLs 

#define SYS_SWAP(a,b) {a=a+b; b=a-b; a=a-b; }

//TEMP_VAR(var,__LINE__) not works
#define VAR(var) var##__LINE__
#define LOOP(var,conut) uint32_t VAR(var);for(VAR(var) = 0;VAR(var) < conut; VAR(var)++)


#if 0
uint32_t SysTime(void);

#define SysTimeDiff(new, old) ((new) - (old))
#define SysTimeDiffCurrent(oldTime) SysTimeDiff(SysTime(), (oldTime))
#define SysTimeHasPast(oldTime, pastTime) (SysTimeDiffCurrent((oldTime)) > (pastTime))
#endif

#define SYS_LED_BLINK_OFF    ENC28J60_LED_SHUT
#define SYS_LED_LIGHT        ENC28J60_LED_LIGHT
#define SYS_LED_BLINK_FAST   ENC28J60_LED_BLINK_FAST
#define SYS_LED_BLINK_NORMAL ENC28J60_LED_BLINK_SLOW //ENC28J60_LED_BLINK_NORMAL

#define SYS_LED_MODE_NORMAL  0
#define SYS_LED_MODE_BLINK   1


#define SysLEDCtrl(state) {enc28j60LEDControl(ENC28J60_LEDA, state); enc28j60LEDControl(ENC28J60_LEDB, state);}

#define SPI_FLASH_START 0x08000000  //0x801C000 //last 16k bytes

#define DEVICE_CONNECT_UPGRADE_INFO  0x3EC00 //0x1F000
#define DEVICE_DID_DATA_ADDR     0x3F400 //设备DID 地址
#define DEVICE_PWD_DATA_ADDR     0x3F420 //登录密码地址
#define DEVICE_RF_SEGMENT_ADDR   0x3F460 //无线433 段地址
#define DEVICE_MAC_ADDR          0x3F480 //MAC地址
#define DEVICE_UPDATA_INFO_ADDR  0x3F490 //升级信息地址
#define DEVICE_SAVED_DATA_ADDR   0x3F400 //设备信息地址

#define DEVICE_UPDATA_CACHE_ADDR 0x00400 //升级缓存地址
#define DEVICE_DEFAULT_BIN_ADDR  0X00    //可执行程序地址
#define DEVICE_BOOTLOADER_ADDR   0x0     //bootloader 地址

#define CONFIG_SAVED_DATA_ADDR   0x3FC00 

//TODO
#define APP_START_ADDR           0x10000 //60K
#define OTA_UPDATE_INFO_ADDR     0x3F000 //252k


#define SPI_FLASH_END  (SPI_FLASH_START + 16 * SYS_FLASH_SECTOR_SIZE)

#define USER_ADDR_VALID(spiAddr) ((spiAddr) >= SPI_FLASH_START && (spiAddr) < SPI_FLASH_END)

#define SYS_FLASH_SECTOR_SIZE 2048
#define SPI_ADDR(addr) ((SPI_FLASH_START + addr))
#define SECTOR_START_ADDR(addr) ((addr) &(~(SYS_FLASH_SECTOR_SIZE - 1)))
#define SPI_ADDR_TO_SECTOR(addr) ((addr) / SYS_FLASH_SECTOR_SIZE)
#define ADDR_TO_SECTOR_OFFSET(addr) ((addr) - SECTOR_START_ADDR(addr))

#define USER_ADDR 0
#define VALID_USER_ADDR(addr) (addr < 0x40000)

void SysUserDataWrite(uint32_t addr, const uint8_t *data, uint16_t len);
void SysUserDataEraseAll(void);
void SysUserDataRead(uint32_t addr, uint8_t *data, uint16_t len);
uint8_t SysUserDataReadByte(uint32_t addr);
void SysUserDataWriteByte(uint32_t addr, uint8_t byte);

SysTime_t SysGetUTCTimeSeconds(void);
void SysSetUTCTimeSeconds(SysTime_t sec);
void SysGetUTCTime(SysUTCTime_t *utcTime);

void SysOSInit(void);
void SysRestart(SYS_RESTART_INDEX index);
void SysDevicesInit(void);
void SysDelay(uint32_t nCount);
//void SysTimeUpdate(void);
void SysPoll(void);
void SysDataPrint(uint8_t *data, uint8_t len);
void SysLwIPPeriodicHandle(void);
void SysSetLedState(uint8_t mode, uint8_t state);
bool SysIsLittleEndian(void);
bool SysTcpSendMutexState(void);
void SysTcpSendMutexLock(bool lock);
bool SysCheckMacChipVersion(void);
void SysMacChipReset(void);
bool SysInitCompleted(void);
int SysAtoi(const char* str);


#endif

