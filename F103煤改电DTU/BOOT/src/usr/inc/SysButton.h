#ifndef SYSBUTTON_H
#define SYSBUTTON_H
#include "userSys.h"

typedef enum
{
    SYS_BUTTON_STATE_RELEASED,
    SYS_BUTTON_STATE_PRESSED,
} SysButtonState_t;

struct SysButton_st;
typedef struct SysButton_st SysButton_t;

//返回1 表示按住重复
typedef uint8_t (*SysButtonHandler_t)(SysButton_t *button, uint32_t pressTime, SysButtonState_t state);
typedef SysButtonState_t (*SysButtonGetState_t)(SysButton_t *button);

struct SysButton_st
{
    SysButtonState_t lastState;     //上次的按键状态，按下、释放
    uint8_t handled;                //是否处理
    uint32_t pressedTime;           //按下时间
    SysButtonHandler_t handler;     //函数指针，指向按键处理函数
    SysButtonGetState_t getState;   //函数指针，指向获取按键状态函数
};

void SysButtonInit(void);
void SysButtonPoll(void);
uint8_t SysButtonRegister(SysButton_t *button, SysButtonHandler_t handler, SysButtonGetState_t getState);


#endif // SYSBUTTON_H


