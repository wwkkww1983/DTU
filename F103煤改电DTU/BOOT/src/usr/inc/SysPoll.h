#ifndef SYS_POLL_H
#define SYS_POLL_H

#include "userSys.h"

#define SYS_POLL_NUM  5

typedef enum
{
    POLL_ONCE = 0,
    POLL_ALLWAYS = 0xff,
} POLL_TIMES_T;

typedef uint8_t (*POLL_HANDLE_T)(uint16_t pos, void *args);

typedef struct
{
    bool          alloced;
    uint16_t      roundNum;
    uint16_t      posNow;
    POLL_TIMES_T  times;
    POLL_HANDLE_T pollHandle;
    void          *args;
    uint8_t       delay;
    uint32_t      now;
} SYS_POLL_T;

void sysPeriodInit(void);
void sysPeriodPoll(void);
void sysPeriodPollClear(SYS_POLL_T *period);
SYS_POLL_T *sysPeriodPollSet(uint16_t round, POLL_TIMES_T time, POLL_HANDLE_T handle, void *args, uint8_t delay);


#endif

