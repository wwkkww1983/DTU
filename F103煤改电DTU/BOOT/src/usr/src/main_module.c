#include "main_module.h"
#include "hal_led.h"
#include "device.h"

#define MAX_CHANNEL     20
#define LED_BLINK_SPAN  50 //ms

typedef struct        //设备信息
{
    uint8_t enable;
    uint8_t key[NETKEY_LEN];
    //DeviceId_t *id;//+cbl 0522
} device_info_t;

enum work_state   //工作模式类型  type
{
    WORK_STATE_REQADDR, /*  */
    WORK_STATE_HASADDR, /* 已经分配段地址 */
    WORK_STATE_BUILDNET,/* 组网 */
    WORK_STATE_WORKING, /* 工作 */
    WORK_STATE_UPGRADE,
};

static uint16_t g_led_blink_count = 0;

static uint8_t g_real_device_count = MAX_DEVICE_COUNT;
static uint8_t g_work_channel = 6;
static uint8_t g_work_state = WORK_STATE_REQADDR;
static device_info_t g_device_info[MAX_DEVICE_COUNT];
static uint8_t g_broadcast_times = 0;
static uint8_t g_wakeup_dev_addr = 0, g_wakeup_dev_value = 0;
static bool g_isCheckMode = false;
//static os_timer_t g_timer_hb;

void main_module_add_led_blink_count(uint8_t n)
{
    g_led_blink_count += n;
}


static void _request_addr_led_blink(void *p)
{
    hal_led_toggle(LED_PORT, LED_PIN);
    if(g_work_state == WORK_STATE_REQADDR)
    {
        os_timer_set(_request_addr_led_blink, 100, NULL, 0);
    }
    else
    {
        hal_led_off(LED_PORT, LED_PIN);
    }
}

static void _request_addr(void *p)
{
    if(g_work_state == WORK_STATE_REQADDR)
    {
//    yunhe_mid_send(NETADDRESS, 0);
        os_timer_set(_request_addr, 500, NULL, 0);
    }
}

//static void _start_request_addr(void)
//{
//  g_work_state = WORK_STATE_REQADDR;
//  _request_addr(NULL);
//  _request_addr_led_blink(NULL);
//}

void main_module_setaddr(uint8_t *addr)
{
    net_set_segaddr(addr);
    g_work_state = WORK_STATE_HASADDR;

    //根据地址设置信道,0,1用于组网和升级
    g_work_channel = (addr[0] + addr[1] + addr[2] + addr[3]) % (MAX_CHANNEL - 2) + 2;

    SysLog("RF channel is %d\n", g_work_channel);

    main_module_enter_work();
}

void main_module_init()
{
    uint8_t rfAddr[NWK_SEGADDR_LEN];
    uint8_t *pt = (uint8_t *)0x1FFFF7B8;
    //uint8_t *rfAddr = (uint8_t *)SPI_ADDR(DEVICE_RF_SEGMENT_ADDR);

    //tbd wyx
    //memcpy(rfAddr, (char *)SPI_ADDR(DEVICE_RF_SEGMENT_ADDR), NWK_SEGADDR_LEN);

    rfAddr[0] = *pt++;
    rfAddr[1] = *pt++;
    rfAddr[2] = *pt++;
    rfAddr[3] = *pt++;

    printf("RF ADDR:%02x-%02x-%02x-%02x\n", rfAddr[0], rfAddr[1], rfAddr[2], rfAddr[3]);
    net_set_myaddr(0);
    net_set_role(NWK_ROLE_COORDINATOR);
    net_off();

    //_start_request_addr();
    main_module_setaddr(rfAddr);   //固定一个段地址

    //os_timer_set(_working_led_blink, LED_BLINK_SPAN, NULL, OS_TIMER_FLAG_REPEAT);//-cbl 0205
    //g_timer_hb = os_timer_set(_send_heartbeat, UART_HEARTBEAT_SPAN, NULL, OS_TIMER_FLAG_REPEAT);//-cbl 0205
}

void main_module_enter_work()
{
    if(g_work_state == WORK_STATE_REQADDR)
    {
        return;
    }

//  os_log("");

    g_work_state = WORK_STATE_WORKING;
    net_rf_config_normal();
    net_set_channel(g_work_channel);
    net_mesh_enable(1);
    net_on();
}

void _send_searchdevice_broadcast(void *p)
{
    if(g_work_state == WORK_STATE_BUILDNET && g_broadcast_times)
    {
        g_broadcast_times--;
        if(g_broadcast_times)
        {
            phy_rx_attenuation(PHY_RX_ATTENUATION_6DB);
            net_tx_pa_power(PHY_POWER_0DB);
        }
        else
        {
            phy_rx_attenuation(PHY_RX_ATTENUATION_0DB);
            net_tx_pa_power(PHY_POWER_MAX);
        }
        app_transmit(APP_FRAME_SEARCHDEVICE, NET_BROADCAST_ADDR, 0);
        os_timer_set(_send_searchdevice_broadcast, 1000, NULL, 0);
    }
}

static void _buildnet_led_blink(void *p)
{
    hal_led_toggle(LED_PORT, LED_PIN);
    if(g_work_state == WORK_STATE_BUILDNET)
    {
        os_timer_set(_buildnet_led_blink, 300, NULL, 0);
    }
    else
    {
        hal_led_off(LED_PORT, LED_PIN);
    }
}

void main_module_enter_upgrade()
{
    if(g_work_state == WORK_STATE_REQADDR)
    {
        return;
    }

    g_work_state = WORK_STATE_UPGRADE;
    net_rf_config_upgrade();
    net_set_channel(PHY_UPGRADE_CHANNEL);
}


void main_module_enter_buildnet(uint8_t broadcast_times)
{
    if(g_work_state == WORK_STATE_REQADDR)
    {
        return;
    }

//  os_log("");

    if(g_work_state != WORK_STATE_BUILDNET)
    {
        net_rf_config_normal();//-cbl 0407
        g_work_state = WORK_STATE_BUILDNET;
        //_buildnet_led_blink(NULL);
        net_set_channel(0);
        net_mesh_enable(0);
        g_broadcast_times = 0;
    }

    if(g_broadcast_times == 0)
    {
        g_broadcast_times = broadcast_times;
        _send_searchdevice_broadcast(NULL);
    }
    else
    {
        g_broadcast_times = broadcast_times;
    }
}

/* 设置设备列表,获取设备的key,用于设备通讯 */
void main_module_set_devicelist(uint8_t *data)
{
    uint8_t i;
    uint8_t count = data[0];
    uint8_t *dev;
    for(i = 0; i < count; i++)
    {
        dev = data + 1 + i * 4;
        g_device_info[dev[0]].enable = 1;
        memcpy(g_device_info[dev[0]].key, dev + 1, NETKEY_LEN);
    }
}
/*

void main_module_set_devicelist(uint8_t index, uint8_t *key, DeviceId_t *id)
{
    g_device_info[index].enable = 1;
    g_device_info[index].key    = key;
    g_device_info[index].id     = id;
}
*/

void main_module_add_device(uint8_t addr, uint8_t *uid, uint8_t *key)
{
    uint8_t len = 0;
    uint8_t *data = app_get_trans_payload();

    if(addr < OS_MAX_DEVICE_COUNT)  //最多添加64个设备
    {
        data[len++] = addr;   //编号
        data[len++] = g_work_channel;
        memcpy(data + len, uid, DEVUID_LEN);
        len += DEVUID_LEN;
        memcpy(data + len, key, NETKEY_LEN);
        len += NETKEY_LEN;
        g_device_info[addr].enable = 1;
        memcpy(g_device_info[addr].key, key, NETKEY_LEN);
        app_transmit(APP_FRAME_ADDDEVICE, NET_BROADCAST_ADDR, len);
		os_log("++++++APP_FRAME_ADDDEVICE+++");
    }
}

void main_module_del_device(uint8_t addr)
{

    if(addr < OS_MAX_DEVICE_COUNT)
    {
        memcpy(app_get_trans_payload(), g_device_info[addr].key, NETKEY_LEN);
//  for(count=0; count<3; count++)//+cbl 0204 some device cannot release
        app_transmit(APP_FRAME_DELDEVICE, addr, NETKEY_LEN);
        //g_device_info[addr].enable = 0;
        memset(&g_device_info[addr], 0, sizeof(device_info_t));
    }
}

void main_module_relay_msg_to_device(uint8_t addr, uint8_t *data, uint8_t len, uint8_t signal)
{
    uint8_t *data_buf = app_get_trans_payload();

    if(len > APP_PAYLOAD_MAXLEN)
    {
        return;
    }

    memcpy(data_buf, data, len);
    app_transmit(APP_FRAME_DATA, addr, len);
}

void main_module_set_args(uint8_t addr, uint8_t value)
{
    g_wakeup_dev_addr  = addr;
    g_wakeup_dev_value = value;
}

void main_module_wakeup_sleep_device(void)
{
    uint8_t data[5] = {0x04, 0x04, 0xff, 0x00, 0x00};
    data[3] = g_wakeup_dev_addr;
    data[4] = g_wakeup_dev_value;
    phy_transmit_directly(data, 5);
}


void main_module_set_device_real_count(uint8_t count)
{
    g_real_device_count = count;
}

uint8_t main_module_get_device_real_count(void)
{
    return g_real_device_count;
}

void _response_connect(void *p)
{
    uint8_t addr = (uint32_t)p;

    app_transmit(APP_FRAME_CONNECT, addr, 0);
}

void main_module_set_check_mode(bool flag)
{
    g_isCheckMode = flag;
}

void main_module_recv_deal(uint8_t *data, uint8_t len, uint8_t *segaddr, uint8_t srcaddr, uint8_t dstaddr, uint8_t rssi)
{
    //uint8_t buf[128];

    app_header_t *app = (app_header_t *)data;

    uint8_t *pt = app->payload + 16;

    if(srcaddr != NWK_NULL_ADDR
            && dstaddr != NWK_BROADCAST_ADDR
            && !g_device_info[srcaddr].enable)
    {
        return;
    }

    switch(app->type)
    {
        case APP_FRAME_SEARCHDEVICE:

            SysLog("APP_FRAME_SEARCHDEVICE %c%c%03d \n", pt[0], pt[1], pt[2]);

            if(g_work_state != WORK_STATE_BUILDNET)
            {
                break;
            }

            if(g_isCheckMode)
            {
                //checkFoundDevice(true);
            }
            else
            {
                //tbd wyx
                GatewayRFFoundDevice((DeviceId_t *)(app->payload + 16), app->payload);
            }

            break;


        case APP_FRAME_CONNECT:
            SysLog("APP_FRAME_CONNECT %");

            if(g_work_state != WORK_STATE_WORKING)
            {
                break;
            }

            //tbd wyx
            GatewayRFVersionSet(srcaddr, app->payload, len - sizeof(app_header_t));
            os_timer_set(_response_connect, 20, (void *)srcaddr, 0);
            break;

        case APP_FRAME_DATA:
            SysLog("APP_FRAME_DATA");

            if(g_work_state != WORK_STATE_WORKING)
            {
                break;
            }

            //tbd wyx
            GatewayRFRecvData(srcaddr,  app->payload, len - sizeof(app_header_t), rssi);
            break;

        default:
            break;
    }
}

uint8_t *main_module_get_key(uint8_t addr)
{
    if(g_device_info[addr].enable)
    {
        return g_device_info[addr].key;
    }

    return NULL;
}

void main_module_poll()
{
    //yunhe_mid_poll();
}

void main_module_ack_sync_content_cb(uint8_t *data, uint8_t len)
{

}

uint8_t main_module_ack_sync_content_set(uint8_t *data)
{
    os_time_t time;

    time = os_timer_get_nettime();

    data[0] = (uint8_t)(time & 0xff);
    data[1] = (uint8_t)(time >> 8);
    data[2] = (uint8_t)(time >> 16);
    data[3] = (uint8_t)(time >> 24);

    //bug wyx 字节对齐

    //*(os_time_t *)data = os_timer_get_nettime();

    data[sizeof(os_time_t)] = g_real_device_count;
    return 5;
}

void main_module_reset_heartbeat_time()
{
    //os_timer_reset(g_timer_hb);
}

