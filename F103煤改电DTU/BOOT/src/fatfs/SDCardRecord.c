#include "Hal.h"
#include "userSys.h"
#include "SDCardRecord.h"
#include "ff.h"
#include "userSys.h"
#include <stdlib.h>

typedef struct recordReport_st
{
	uint32_t startTs;
	uint32_t endTs;
	uint32_t lastReportTs;	
	char filePath[64];	
	FIL file;
	uint8_t loadFile;
}recordReport_t;

//TODO 懒得注释
static char *getRecordFilePath(uint32_t secCount);
static void makeRecordFilePath(char *filePath);
uint32_t RecordPraseTs(const char *record);
static void RecordReportProess(void);
static char *getRecordFileNextPath(char *filePath);
void SDCardGetCap(void);

static FATFS fs;		// Work area (file system object) for logical drive
static FIL fsrc;		// file objects
static uint8_t g_lasSDDet = 0;
static recordReport_t g_rReport = {0};
SysTime_t g_lastReportTime;

void SDCardRecordInit(void)
{
	SysLog("SD DET:%d",SD_DET());
	if(SD_DET())
	{
		f_mount(0, &fs);
	}
	g_lasSDDet = SD_DET();
	g_lastReportTime = SysTime();
}

void SDCardRecordPoll(void)
{
	//热插拔
	if(g_lasSDDet != SD_DET())
	{
		SDCardRecordInit();
	}

	if(SD_DET())
	{
		RecordReportProess();		
		//TODO SDCARD 容量管理， 容量到达80%,删除文件名显示时间最晚的文件夹
	}
}

#define RECORD_BUILD_MAX_LEN 512
static char g_recordBuild[RECORD_BUILD_MAX_LEN] = {0};

uint32_t RecordBuildStart(void)
{
	uint32_t rtcTime = GetRTCTime();
	if(rtcTime == 0 )
	{
		SysLog("Record:RTC getTime err");
		return 0;
	}
	if(!SD_DET())
	{
		SysLog("Record:sdcard det:0");
		return 0;
	}
	memset(g_recordBuild, 0, RECORD_BUILD_MAX_LEN);
	sprintf(g_recordBuild,"{\"ts\":%d,\"as\":{",rtcTime);
	return rtcTime;
}

uint16_t RecordBuild(uint16_t propertyid, int32_t num, const char *text)
{
	if(text==NULL)
	{
		sprintf(g_recordBuild+strlen(g_recordBuild),"\"%d\":%d,",propertyid,num);
	}
	else
	{
		sprintf(g_recordBuild+strlen(g_recordBuild),"\"%d\":\"%s\",",propertyid,text);
	}
	return 0;
}

void RecordBuildEnd(uint32_t secCount)
{
	sprintf(g_recordBuild + strlen(g_recordBuild)-1,"}}\r\n");
	printf("RecordBuild:%s", g_recordBuild);

	//TODO  获取文件名,打开文件，写入文件，写入前检查，关闭文件
	if(SD_DET())
	{
		char *filePath = NULL;
		FRESULT res;
		
		filePath = getRecordFilePath(secCount);
		makeRecordFilePath(filePath);
		SysLog("fopen %s",filePath);

		res = f_open(&fsrc, filePath, FA_OPEN_ALWAYS | FA_WRITE | FA_READ);
		if(res != FR_OK)
		{
			printf("f_open err! res=%d\n",res);
		}
		else
		{
			f_lseek(&fsrc, fsrc.fsize);
			ffputs(g_recordBuild,&fsrc);
			//f_write(&fsrc, g_recordBuild, strlen(g_recordBuild), &bw); 
			f_sync(&fsrc);
			f_close(&fsrc);
		}
	}
}

void RecordReportStart(uint32_t startTs, uint32_t endTs)
{
	if(!startTs || !endTs)
	{
		return;
	}

	if(startTs > endTs)
	{
		SYS_SWAP(startTs,endTs);
	}
	
	if(g_rReport.startTs == 0)
	{
		g_rReport.startTs = startTs;
		g_rReport.endTs   = endTs;
		g_rReport.lastReportTs = startTs - 1;//用于查找可用的ts，-1包含本条，之后在遇到相同ts不上报，用于容错
		g_rReport.loadFile = 0;		
		strcpy(g_rReport.filePath,getRecordFilePath(g_rReport.startTs));
	}
}

void RecordReportStop(void)
{
	g_rReport.startTs = 0;
	g_rReport.endTs   = 0;
	g_rReport.lastReportTs = 0;
	g_rReport.loadFile = 0; 	
	memset(g_rReport.filePath, 0, sizeof(g_rReport.filePath));
	ch_send_history_data_end();
	
}

static void RecordReportProess(void)
{
	if(g_rReport.startTs != 0)
	{
		if(SysTimeHasPast(g_lastReportTime,300))
		{
			FRESULT res;
			char readBuf[1024] = {0};
			uint8_t  loopLimit = 5;
			uint32_t readTs = 0;
			g_lastReportTime = SysTime();

			//loadFile
			if(!g_rReport.loadFile)
			{
				while(loopLimit--)
				{
					HalWdtFeed();
					res = f_open(&g_rReport.file, g_rReport.filePath, FA_OPEN_EXISTING | FA_READ);
					if(res != FR_OK)
					{
						// 获取下个文件名填入filePath
						strcpy(g_rReport.filePath,getRecordFileNextPath(g_rReport.filePath));
						if(loopLimit <= 1)
						{	
							//向下读取loopLimit个文件都不能打开，停止上报
							SysLog("find record file err.");
							RecordReportStop();
							return;
						}
						continue;
					}
					else
					{
						SysLog("find record file.");
						g_rReport.loadFile = 1;
						break;
					}
					
				}
			}

			// 读文件，读到大于startts上传，每次上传一条，读到文件尾loadfile=0;fileName=next,读到endts，reportStop
			loopLimit = 500;
			while(loopLimit-- && g_rReport.loadFile)
			{
				memset(readBuf, 0, 1024);
				HalWdtFeed();
				if(ffgets(readBuf, 1024, &g_rReport.file))
				{
					readTs = RecordPraseTs(readBuf);
					if(readTs >  g_rReport.lastReportTs && readTs <= g_rReport.endTs)
					{
						//  可用数据  上报
						g_rReport.lastReportTs = readTs;
						if(ch_send_history_data(readBuf, 0) == 1)
						{
							SysLog("report break");
							break;
						}
						
					}
					else if(readTs > g_rReport.endTs)
					{
						SysLog("report end");
						RecordReportStop();
						f_close(&g_rReport.file);
						return;
					}
					else if(readTs <=  g_rReport.lastReportTs)
					{
						continue;
					}
				}
				else
				{
					//  读文件错误或者读到文件末，获取下个文件名后，重新loadFile
					SysLog("read file EOF or ERR");
					strcpy(g_rReport.filePath,getRecordFileNextPath(g_rReport.filePath));
					g_rReport.loadFile = 0;
					f_close(&g_rReport.file);
					break;
				}
			}
			
		}
	}

}

uint32_t RecordPraseTs(const char *record)
{
	//TODO check record format
	char tsString[32] = {0};
	memcpy(tsString, strchr(record, ':')+1, strchr(record, ',') - strchr(record, ':')  );
	SysLog("tsString:%s atoi:%d",tsString, (uint32_t)atoi(tsString));
	return (uint32_t)atoi(tsString);
}

static char *getRecordFilePath(uint32_t secCount)
{
	static char filePath[64] = {0};

	memset(filePath, 0, 64);
	if(secCount)
	{
		rtc_time_t * rtcTime = RTCTransDate(secCount);//%d-%02d-%02d.txt
		sprintf(filePath,"0:record/%d-%02d/%02d.txt",
			rtcTime->year, rtcTime->month,rtcTime->date);
		printf("NowTime: %d-%02d-%02d %d-%d-%d\n",rtcTime->year, rtcTime->month, rtcTime->date, rtcTime->hour, rtcTime->min, rtcTime->sec);

		return filePath;
	}
	return NULL;
}

static char *getRecordFileNextPath(char *filePath)
{
	uint16_t year;
	uint8_t  month,date,len;
	uint32_t secCount;
	rtc_time_t * rtcTime;
	char *nextFilePath = NULL;
		
	char transString[16] = {0};
	memcpy(transString, strchr(filePath, '/')+1, strchr(filePath, '-') - strchr(filePath, '/'));
	len = strchr(filePath, '/') - filePath + 1;
	year = atoi(transString);
	
	memset(transString, 0, 16);
	memcpy(transString, strchr(filePath+len, '-')+1, strchr(filePath+len, '/') - strchr(filePath+len, '-'));
	len = strchr(filePath+len,'/') - filePath;
	month = atoi(transString);
	
	memset(transString, 0, 16);
	memcpy(transString, strchr(filePath+len, '/')+1, strchr(filePath+len, '.') - strchr(filePath+len, '/'));
	date = atoi(transString);

	SysLog("year:%d, month:%d, date:%d",year, month, date);

	secCount = RTCTransSecCount(year, month, date, 0, 0, 0);
	//next day
	secCount += 25*3600;

	nextFilePath = getRecordFilePath(secCount);
	
	SysLog("next:%s",nextFilePath);
	return nextFilePath;
}

static void makeRecordFilePath(char *filePath)
{
	if(filePath != NULL)
	{
		char dir[32] = {0};
		memcpy(dir, filePath, strchr(filePath, '/') - filePath);
		f_mkdir(dir);	
		
		memset(dir, 0, 32);
		memcpy(dir, filePath, strchr(strchr(filePath, '/')+1, '/') - filePath);
		f_mkdir(dir);
	}
}

void SDCardGetCap(void)
{
	//FATFS f_getfree函数存在bug，偶尔会引起sd卡不能再读
	DWORD fre_clust;
	FATFS *fss;
	fss = &fs;
	f_getfree("", &fre_clust, &fss);
	SysLog("total size: %d freesize:%d",(fs.max_clust - 2) * fs.csize/2, fre_clust * fs.csize/2);
}

