#include "nwk.h"
#include "net.h"
#include "mlist.h"
#include "mqueue.h"
#include "os_timer.h"
#include "hal_wait.h"
#include "hal_rnd.h"
#include "userSys.h"

#define ACK_RETRIES               3
#define ONEHOP_ACK_WAITTIME       200
#define LINK_ACK_WAITTIME         (NWK_MAX_HOPS * ACK_RETRIES * ONEHOP_ACK_WAITTIME)
#define ACK_QUEUE_SIZE            5
#define SUBNODE_CACHE_DATA_SIZE   10
#define SUBNODE_CACHE_LIST_SIZE   5
#define RECV_LIST_SIZE            5// 4 chg cbl 0316 always full

#define HAS_ADDR() (g_myaddr != NWK_NULL_ADDR)
#define IS_VALID_ADDR(addr) (addr != NWK_NULL_ADDR && addr != NWK_BROADCAST_ADDR)

typedef struct ack_wait_info
{
    uint8_t addr;
    uint8_t state;
} ack_wait_info_t;

enum
{
    ACK_STATE_WAIT_ONEHOP,
    ACK_STATE_WAIT_LINK,
    ACK_STATE_RECV,
};

typedef struct ack_queue_item
{
    uint8_t addr;
    uint8_t type;
} ack_queue_item_t;

typedef struct ack_queue
{
    mqueue_t queue;
    ack_queue_item_t items[ACK_QUEUE_SIZE];
} ack_queue_t;

typedef struct ack_queue_list
{
    mlist_t queue;
    uint8_t used[ACK_QUEUE_SIZE];
    ack_queue_item_t items[ACK_QUEUE_SIZE];
} ack_queue_list_t;

typedef struct subnode_cache_list_item
{
    uint8_t addr;
    uint8_t len;
    os_time_t set_time;
    uint8_t buf[SUBNODE_CACHE_DATA_SIZE];
} subnode_cache_list_item_t;

typedef struct subnode_cache_list
{
    mlist_t list;
    uint8_t used[SUBNODE_CACHE_LIST_SIZE];
    subnode_cache_list_item_t items[SUBNODE_CACHE_LIST_SIZE];
} subnode_cache_list_t;

typedef struct recv_list_item
{
    uint8_t dealed;
    uint8_t buf_len;
    uint8_t rssi;
    uint8_t buf[OS_RADIO_DATA_MAXLEN];
} recv_list_item_t;

typedef struct recv_list
{
    mlist_t list;
    uint8_t used[RECV_LIST_SIZE];
    recv_list_item_t items[RECV_LIST_SIZE];
} recv_list_t;

static void _relay_the_frame(void *p);
static uint8_t _recv_list_foreach(void *item , mlist_list_size_t index);
static uint8_t _subcache_list_find(void *item, void *userdata);
static uint8_t _addto_subcache(uint8_t addr, uint8_t *data, uint8_t len);
static void _recv_list_del(mlist_list_size_t index);
static uint8_t _subcache_list_foreach(void *item, mlist_list_size_t index);


static uint8_t g_segaddr[NWK_SEGADDR_LEN] = NWK_NULL_SEGADDR;
static uint8_t g_myaddr = NWK_NULL_ADDR;
static uint8_t g_role = NWK_ROLE_ENDDEVICE;
static uint8_t g_sleepmode = NWK_SLEEPMODE_NORMAL;
static uint8_t g_mesh_enable = 0;
static uint8_t g_transmit_fail_count = 0;

static uint8_t g_trans_id;
static ack_wait_info_t g_ack_wait_info;

static ack_queue_t g_ack_queue;
//static ack_queue_list_t g_ack_queue_list;
static subnode_cache_list_t g_subcache_list;
static subnode_cache_list_item_t g_my_subcache;
static recv_list_t g_recv_list;

static uint8_t g_addr_transid[OS_MAX_DEVICE_COUNT];

void nwk_init()
{
    mqueue_init(&g_ack_queue.queue, g_ack_queue.items, ACK_QUEUE_SIZE, sizeof(ack_queue_item_t));
//  mlist_init(&g_ack_queue_list.queue, g_ack_queue_list.used, g_ack_queue_list.items, ACK_QUEUE_SIZE, sizeof(ack_queue_item_t));
    mlist_init(&g_subcache_list.list, g_subcache_list.used, g_subcache_list.items, SUBNODE_CACHE_LIST_SIZE, sizeof(subnode_cache_list_item_t));
    mlist_init(&g_recv_list.list, g_recv_list.used, g_recv_list.items, RECV_LIST_SIZE, sizeof(recv_list_item_t));
    g_my_subcache.addr = 0xff;
    link_init();
}

void nwk_set_segaddr(const uint8_t *segaddr)
{
    memcpy(g_segaddr, segaddr, NWK_SEGADDR_LEN);
}

uint8_t *nwk_get_segaddr()
{
    return g_segaddr;
}

void nwk_set_myaddr(uint8_t addr)
{
    g_myaddr = addr;
}

uint8_t nwk_get_myaddr()
{
    return g_myaddr;
}

void nwk_set_role(uint8_t role)
{
    g_role = role;
    link_init();
}

void nwk_set_sleepmode(uint8_t sleepmode)
{
    g_sleepmode = sleepmode;
}

static void _relay_the_frame(void *p)
{
    nwk_header_t *nwk;
    link_record_t *record;
    mlist_list_size_t index = (uint32_t)p;
    recv_list_item_t *recv_item;


    recv_item = mlist_get(&g_recv_list.list, index);
    if(!recv_item)
    {
        //os_log_err("recv_item is null.");
        return;
    }

    nwk = (nwk_header_t *)recv_item->buf;

    record = link_get_record(nwk->dstaddr);

    if(record != NULL)
    {
        //判断开启mesh网并直接发送到休眠设备的数据缓存不发送
        if(g_mesh_enable && nwk->type == NWK_FRAME_NORMAL && record->sleepmode && nwk->dstaddr == record->nextaddr)
        {
            _addto_subcache(nwk->dstaddr, nwk->payload, recv_item->buf_len - sizeof(nwk_header_t));
        }
        else
        {
            memcpy(phy_get_trans_payload() , recv_item->buf, recv_item->buf_len);
            nwk_transmit_onehop(record->nextaddr, recv_item->buf_len, 0);
        }
    }
    _recv_list_del(index);
}

void main_module_add_led_blink_count(uint8_t n);

void nwk_recv_cb(nwk_header_t *pnwk, uint8_t len, int8_t rssi)
{
    uint8_t need_recv = 0;
    ack_queue_item_t ack_item;
    //ack_queue_item_t *ack_item;
    mlist_list_size_t index;
//  mlist_list_size_t index_queue;
    recv_list_item_t *recv_item = NULL;

    if(len < sizeof(nwk_header_t))
    {
        return;
    }

#ifdef MAIN_MODULE
    main_module_add_led_blink_count(1);
#endif // MAIN_MODULE

    //地址过滤 -------------------------------------------------------------------
    if(pnwk->srcaddr == g_myaddr)
    {
        return;
    }

    if(memcmp(g_segaddr, pnwk->segaddr, NWK_SEGADDR_LEN) == 0)
    {

    }
    else
    {
        if(memcmp(pnwk->segaddr, NWK_NULL_SEGADDR, NWK_SEGADDR_LEN) != 0
                && memcmp(g_segaddr, NWK_NULL_SEGADDR, NWK_SEGADDR_LEN) != 0)
        {
            return;
        }
    }

    if(g_myaddr != pnwk->recver_addr && pnwk->recver_addr != NWK_BROADCAST_ADDR)
    {
        return;
    }
    //end 地址过滤 ---------------------------------------------------------------
    //更新节点的休眠模式
    link_add_record(pnwk->srcaddr, pnwk->sender_addr, pnwk->sleepmode);
    //SysDataPrint((uint8_t *)pnwk, 11);
    //link 处理
    if(pnwk->type == NWK_FRAME_LINK)
    {
//      os_log("pnwk->type = NWK_FRAME_LINK");
        link_recv_cb((link_header_t *)pnwk->payload, pnwk->srcaddr, pnwk->sender_addr,
                     pnwk->sleepmode, rssi);
    }

    // 单跳ACK 处理
    if(pnwk->type == NWK_FRAME_ACK_ONEHOP)
    {
        //os_log("len %d sender_addr %d", len, pnwk->sender_addr);
        if(pnwk->srcaddr == g_ack_wait_info.addr)
        {
            g_ack_wait_info.state = ACK_STATE_RECV;
            //SysLog("g_ack_wait_info.state = ACK_STATE_RECV");
        }

        if(pnwk->sender_addr == link_get_father())
        {
            app_ack_sync_content_cb(pnwk->payload + 1 + pnwk->payload[0],
                                    len - sizeof(nwk_header_t) - NWK_END_CONTENT_LEN - pnwk->payload[0] - 1);
        }

        //判断ack是否携带数据
        if(pnwk->payload[0] && g_my_subcache.addr == 0xff)
        {
            g_my_subcache.len = pnwk->payload[0];
            if(g_my_subcache.len <= SUBNODE_CACHE_DATA_SIZE)
            {
                memcpy(g_my_subcache.buf, pnwk->payload + 1, g_my_subcache.len);
                g_my_subcache.addr = pnwk->srcaddr;
            }
        }

        return;
    }

    //休眠确认收到缓存消息后删除
    if(pnwk->type == NWK_FRAME_ACK_SUBCACHE)
    {
        nwk_subcache_del(pnwk->srcaddr);
    }
    //printf("sleepmode = %d\n", pnwk->sleepmode);

    //只有转发,normal帧需要进入接收列表
    if((pnwk->dstaddr != g_myaddr && IS_VALID_ADDR(pnwk->dstaddr))
            || pnwk->type == NWK_FRAME_NORMAL)
    {
        //填充接收信号强度
        if(pnwk->payload[len - sizeof(nwk_header_t) - NWK_END_CONTENT_LEN] == 0)
        {
            pnwk->payload[len - sizeof(nwk_header_t) - NWK_END_CONTENT_LEN] = rssi;
        }

        need_recv = 1;

        //有效地址发来的消息判断trans_id过滤
        if(pnwk->srcaddr < OS_MAX_DEVICE_COUNT && pnwk->dstaddr == g_myaddr)
        {
            if(pnwk->trans_id == g_addr_transid[pnwk->srcaddr] && pnwk->trans_id != 0)
            {
                need_recv = 0;
            }
        }

        if(need_recv)
        {
            index = mlist_add(&g_recv_list.list, NULL, 0);
            if(index < 0)
            {
                //SysLog("recv list is full.");
                return;
            }
            recv_item = mlist_get(&g_recv_list.list, index);
            recv_item->buf_len = len;
            recv_item->dealed = 0;
            recv_item->rssi = rssi;
            memcpy(recv_item->buf, pnwk, len);

            //成功保存后再修改trans_id
            if(pnwk->srcaddr < OS_MAX_DEVICE_COUNT && pnwk->dstaddr == g_myaddr)
            {
                if(pnwk->trans_id == g_addr_transid[pnwk->srcaddr] && pnwk->trans_id != 0)
                {
                    g_addr_transid[pnwk->srcaddr] = pnwk->trans_id;
                }
            }

        }
    }

    //回复ACK onehop
    if(IS_VALID_ADDR(pnwk->recver_addr) && IS_VALID_ADDR(pnwk->sender_addr))
    {
//      index_queue = mlist_add(&g_ack_queue_list.queue,NULL,0);
//      if(index_queue < 0)
//      {
//          return ;
//      }
//
//      ack_item = mlist_get(&g_ack_queue_list.queue, index_queue);
        ack_item.addr = pnwk->sender_addr;
        ack_item.type = NWK_FRAME_ACK_ONEHOP;
        mqueue_in(&g_ack_queue.queue, &ack_item);
    }

    //处理转发
    if(pnwk->dstaddr != g_myaddr && pnwk->dstaddr != NWK_BROADCAST_ADDR)
    {
        recv_item->dealed = 1;
        //转发timer设置失败时把缓冲清除
        if(os_timer_set(_relay_the_frame, 20, (void *)index, 0) < 0)
        {
            mlist_del(&g_recv_list.list, index);
        }
        return;
    }

    //ACK link
    if(pnwk->type == NWK_FRAME_ACK_LINK)
    {
        if(pnwk->srcaddr == g_ack_wait_info.addr)
        {
            g_ack_wait_info.state = ACK_STATE_RECV;
        }
        return;
    }

    //回复ACK link
    if(pnwk->srcaddr != pnwk->sender_addr)
    {
//      index_queue = mlist_add(&g_ack_queue_list.queue,NULL,0);
//      if(index_queue < 0)
//      {
//          return ;
//      }
//
//      ack_item = mlist_get(&g_ack_queue_list.queue, index_queue);
        ack_item.addr = pnwk->srcaddr;
        ack_item.type = NWK_FRAME_ACK_LINK;
        mqueue_in(&g_ack_queue.queue, &ack_item);
    }
}

void nwk_poll()
{
//  static uint32_t time=0;
//  uint32_t interval = SysTime()-time;
//  if(interval>20)
//  {
//    SysLog("cost time %d", interval);
//  }
//
//  time=SysTime();
    nwk_reply_acks();

    mlist_foreach(&g_recv_list.list, _recv_list_foreach);
    mlist_foreach(&g_subcache_list.list, _subcache_list_foreach);

    if(HAS_ADDR() && !link_is_build() && g_mesh_enable)
    {
        link_request();
    }

    //有通过ack携带的数据还未处理
    if(g_my_subcache.addr != 0xff)
    {
        nwk_transmit(NWK_FRAME_ACK_SUBCACHE, g_my_subcache.addr, 0, NWK_FLAG_SIMPLE);
        app_rf_recv_deal(g_my_subcache.buf, g_my_subcache.len, g_segaddr, 0, g_myaddr, 0);
        g_my_subcache.addr = 0xff;
    }

    link_poll();
    phy_poll();
}

void *nwk_get_trans_payload()
{
    return ((nwk_header_t *)phy_get_trans_payload())->payload;
}

uint8_t nwk_get_role()
{
    return g_role;
}

static uint8_t _recv_list_foreach(void *item , mlist_list_size_t index)
{
    nwk_header_t *pnwk;
    recv_list_item_t *recv_item = item;

    if(recv_item->dealed)
    {
        //SysLog("dealed");
        return 0;
    }
    else
    {
        recv_item->dealed = 1;
        pnwk = (nwk_header_t *)recv_item->buf;
        app_rf_recv_deal(pnwk->payload, recv_item->buf_len - sizeof(nwk_header_t),
                         pnwk->segaddr, pnwk->srcaddr, pnwk->dstaddr, recv_item->rssi);
        _recv_list_del(index);
        return 1;
    }
}


static void _recv_list_del(mlist_list_size_t index)
{
    uint8_t i_state;
    HAL_ENTER_CRITICAL(i_state);
    mlist_del(&g_recv_list.list, index);
    HAL_EXIT_CRITICAL(i_state);
}

void nwk_mesh_enable(uint8_t enable)
{
    g_mesh_enable = enable;
}
#if 0
uint8_t nwk_transmit_directly(uint8_t type, uint8_t dstaddr, uint8_t len, uint8_t flag)
{
//  uint8_t res = 0;
    uint8_t recver_addr;
//  uint8_t retries;
    link_record_t *record = NULL;
    nwk_header_t *nwk = phy_get_trans_payload();

    if(g_mesh_enable && !link_is_build() && type == NWK_FRAME_NORMAL)
    {
        return 0;
    }
    nwk->type = type;
    nwk->sleepmode = g_sleepmode;
    nwk->srcaddr = g_myaddr;
    nwk->dstaddr = dstaddr;
    memcpy(nwk->segaddr, g_segaddr, NWK_SEGADDR_LEN);
    nwk->sender_addr = g_myaddr;

    nwk->payload[len] = 0;
    len += NWK_END_CONTENT_LEN;

    if(dstaddr == NWK_BROADCAST_ADDR || !g_mesh_enable || flag & NWK_FLAG_SIMPLE)
    {
        recver_addr = dstaddr;
    }
    else
    {
        record = link_get_record(dstaddr);
        if(record)
        {
            recver_addr = record->nextaddr;
        }
        else
        {
            recver_addr = dstaddr;
            return 0;
        }
    }
    nwk->trans_id = g_trans_id++;

    nwk_transmit_onehop(recver_addr, len + sizeof(nwk_header_t), 1);


    return 0;
}

#endif
uint8_t nwk_transmit(uint8_t type, uint8_t dstaddr, uint8_t len, uint8_t flag)
{
    uint8_t res = 0;
    uint8_t recver_addr;
    uint8_t retries;
    link_record_t *record = NULL;
    nwk_header_t *nwk = phy_get_trans_payload();

    //启用mesh网后未连接时不允许发送数据帧
    if(g_mesh_enable && !link_is_build() && type == NWK_FRAME_NORMAL)
    {
        goto fail;
    }

    nwk->type = type;
    nwk->sleepmode = g_sleepmode;
    nwk->srcaddr = g_myaddr;
    nwk->dstaddr = dstaddr;
    memcpy(nwk->segaddr, g_segaddr, NWK_SEGADDR_LEN);

    nwk->sender_addr = g_myaddr;

    //用于填充末尾数据  信号强度
    nwk->payload[len] = 0;
    len += NWK_END_CONTENT_LEN;

    if(dstaddr == NWK_BROADCAST_ADDR || !g_mesh_enable || flag & NWK_FLAG_SIMPLE)
    {
        recver_addr = dstaddr;
    }
    else
    {
        record = link_get_record(dstaddr);
        if(record)
        {
            recver_addr = record->nextaddr;
        }
        else
        {
            recver_addr = dstaddr;
            goto fail;
        }
    }

    nwk->trans_id = g_trans_id++;

    if(dstaddr != recver_addr)
    {
        retries = ACK_RETRIES;
    }
    else
    {
        retries = 1; //3;chg cbl 0205, device cannot receive sometimes
    }

    //开启mesh时发送给休眠节点的数据缓存,不发送
    if(g_mesh_enable
            && type == NWK_FRAME_NORMAL
            && dstaddr == recver_addr
            && record
            && record->sleepmode
            && !(flag & NWK_FLAG_SIMPLE))
    {
        return _addto_subcache(nwk->dstaddr, nwk->payload, len);
    }

    while(retries--)
    {
        if(nwk_transmit_onehop(recver_addr, len + sizeof(nwk_header_t), 0))
        {
            if(nwk->dstaddr == nwk->recver_addr || nwk->type == NWK_FRAME_ACK_LINK) // || g_myaddr == 0
            {
                res = 1;
                break;
            }

            g_ack_wait_info.addr = dstaddr;
            //g_ack_wait_info.state = ACK_STATE_WAIT_LINK;
            hal_wait_ms_cond(LINK_ACK_WAITTIME, g_ack_wait_info.state == ACK_STATE_RECV);

            if(g_ack_wait_info.state == ACK_STATE_RECV)
            {
                res = 1;
                SysLog("");
                break;
            }
            //SysLog("transmit retry!");
        }
        else
        {
            goto fail;
        }
    }

    if(res == 0)
    {
        goto fail;
    }

    //往中心节点成功发送则设置连接为stable
    if(dstaddr == 0 && g_mesh_enable && link_is_build())
    {
        g_transmit_fail_count = 0;
        link_set_stable(1);
    }

    return 1;
fail:
    //往中心节点发送失败一次后则设置连接not stable，3次则断开
    if(dstaddr == 0 && g_mesh_enable && link_is_build())
    {
        link_set_stable(0);
        g_transmit_fail_count++;
        if(g_transmit_fail_count >= 3)
        {
            g_transmit_fail_count = 0;
            link_disattach();
        }
    }

    return 0;
}

uint8_t nwk_transmit_onehop(uint8_t dstaddr, uint8_t len, uint8_t directly)
{
    uint8_t retries = 1;
    nwk_header_t *nwk = phy_get_trans_payload();

    nwk->sender_addr = g_myaddr;
    nwk->recver_addr = dstaddr;
//  os_log("@@@@@ sender:%d, recver:%d", g_myaddr, dstaddr);
    if(nwk->type == NWK_FRAME_NORMAL && !directly)
    {
        retries = ACK_RETRIES;
    }
    //os_log("retries = %d", retries);
    while(retries--)
    {
        if(nwk->type != NWK_FRAME_ACK_ONEHOP && nwk->type != NWK_FRAME_ACK_LINK)
        {
            nwk_reply_acks();
        }
        //os_log("nwk->type = %x", nwk->type);
        if(phy_transmit(PHY_TYPE_NORMAL, len))
        {
            if(dstaddr == NWK_BROADCAST_ADDR || nwk->type == NWK_FRAME_ACK_ONEHOP)
            {
                return 1;
            }

            g_ack_wait_info.addr = dstaddr;
            g_ack_wait_info.state = ACK_STATE_WAIT_ONEHOP;
            if(!directly)
            {
                hal_wait_ms_cond(ONEHOP_ACK_WAITTIME, g_ack_wait_info.state == ACK_STATE_RECV);
            }

            if(g_ack_wait_info.state == ACK_STATE_RECV)
            {
                return 1;
            }
            if(nwk->type == NWK_FRAME_NORMAL && !directly)
            {
                hal_wait_ms(hal_rnd_range(5, 20));
            }

        }

    }
    return 0;
}

static uint8_t _subcache_list_find(void *item, void *userdata)
{
    subnode_cache_list_item_t *subcache_item = item;
    uint8_t addr = (uint32_t)userdata;

    if(subcache_item->addr == addr)
    {
        return 1;
    }
    return 0;
}

static uint8_t _subcache_list_foreach(void *item, mlist_list_size_t index)
{
    uint8_t *data = nwk_get_trans_payload();

    subnode_cache_list_item_t *subcache_item = (subnode_cache_list_item_t *)item;

    //超过20分钟删除
    if(OS_SYSTIME_BEYOND_SPAN(subcache_item->set_time, 20 * 60 * 1000L))
    {
        mlist_del(&g_subcache_list.list, index);
    }

    return 0;
}

/*void nwk_reply_acks()
{
  //uint8_t i_state;

  //注意要比ack回复的内容大
  uint8_t tmp_buf[20];

  subnode_cache_list_item_t *subcache_item;
  mlist_list_size_t index;
  uint8_t len;
  ack_queue_item_t *ack_item;
  nwk_header_t *nwk = phy_get_trans_payload();

  while(ack_item = mqueue_first(&g_ack_queue.queue))
  {
    //填充ack内容

    memcpy(tmp_buf, nwk, sizeof(tmp_buf));

    nwk->payload[0] = 0;
    len = 1;
    if(ack_item->type == NWK_FRAME_ACK_ONEHOP)
    {
      subcache_item = mlist_find(&g_subcache_list.list,
                                 _subcache_list_find,
                                 &index,
                                 (void *)ack_item->addr);
      if(subcache_item != NULL)
      {
        nwk->payload[0] = subcache_item->len;
        memcpy(nwk->payload + 1, subcache_item->buf, subcache_item->len);
        len += subcache_item->len;
      }
    }

    if(ack_item->type == NWK_FRAME_ACK_ONEHOP)
    {
      len += app_ack_sync_content_set(nwk->payload + len);
    }

    hal_wait_ms(5);
    nwk_transmit(ack_item->type, ack_item->addr, len, 0);
    //os_log("type %d to %d", ack_item->type, ack_item->addr);

    HAL_ENTER_CRITICAL(i_state);
    mqueue_out(&g_ack_queue.queue);
    HAL_EXIT_CRITICAL(i_state);

    memcpy(nwk, tmp_buf, sizeof(tmp_buf));
  }
}*/

void nwk_reply_acks()
{
    //  mlist_list_size_t i;
    uint8_t i_state;
    //注意要比ack回复的内容大
    uint8_t tmp_buf[20];

    subnode_cache_list_item_t *subcache_item;
    mlist_list_size_t index;
    uint8_t len;
    ack_queue_item_t *ack_item;
    nwk_header_t *nwk = phy_get_trans_payload();

    //for(i = 0; i < g_ack_queue_list.queue.size; i++)
    while((ack_item = mqueue_first(&g_ack_queue.queue)) != NULL)
    {
        //填充ack内容

        //      if(!g_ack_queue_list.queue.used[i])
        //      {
        //        continue;
        //      }
        //      ack_item = (ack_queue_item_t *)g_ack_queue_list.queue.data + i *g_ack_queue_list.queue.data_size;
        memcpy(tmp_buf, nwk, sizeof(tmp_buf));

        nwk->payload[0] = 0;
        len = 1;
        if(ack_item->type == NWK_FRAME_ACK_ONEHOP)
        {
            subcache_item = mlist_find(&g_subcache_list.list,
                                       _subcache_list_find,
                                       &index,
                                       (void *)ack_item->addr);
            if(subcache_item != NULL)
            {
                nwk->payload[0] = subcache_item->len;
                memcpy(nwk->payload + 1, subcache_item->buf, subcache_item->len);
                len += subcache_item->len;
            }
        }

        if(ack_item->type == NWK_FRAME_ACK_ONEHOP)
        {
            len += app_ack_sync_content_set(nwk->payload + len);
        }

        hal_wait_ms(5);
//      os_log("reply acks");//+cbl 0207
        nwk_transmit(ack_item->type, ack_item->addr, len, 0);

        HAL_ENTER_CRITICAL(i_state);
        mqueue_out(&g_ack_queue.queue);
        //mlist_del(&g_ack_queue_list.queue,i);
        HAL_EXIT_CRITICAL(i_state);

        memcpy(nwk, tmp_buf, sizeof(tmp_buf));
    }
}


static uint8_t _addto_subcache(uint8_t addr, uint8_t *data, uint8_t len)
{
    mlist_list_size_t subcache_index;
    subnode_cache_list_item_t *subcache_item;

    if(len > SUBNODE_CACHE_DATA_SIZE)
    {
        return 0;
    }
    //printf("_addto_subcache \n");
    subcache_item = mlist_find(&g_subcache_list.list,
                               _subcache_list_find,
                               &subcache_index,
                               (void *)addr);

    if(subcache_item == NULL)
    {
        subcache_index = mlist_add(&g_subcache_list.list, NULL, 0);
        if(subcache_index < 0)
        {
            //随机删除
            mlist_del(&g_subcache_list.list, hal_rnd_range(0, SUBNODE_CACHE_LIST_SIZE - 1));
            subcache_index = mlist_add(&g_subcache_list.list, NULL, 0);
        }
        subcache_item = mlist_get(&g_subcache_list.list, subcache_index);
    }

    if(subcache_item == NULL)
    {
        return 0;
    }

    subcache_item->addr = addr;
    subcache_item->len = len;
    subcache_item->set_time = os_timer_get_systime();
    memcpy(subcache_item->buf, data, len);
    //os_log("%d", addr);
    return 1;
}

void nwk_subcache_del(uint8_t addr)
{
    mlist_list_size_t subcache_index;
    subnode_cache_list_item_t *subcache_item;

    subcache_item = mlist_find(&g_subcache_list.list,
                               _subcache_list_find,
                               &subcache_index,
                               (void *)addr);

    if(subcache_item != NULL)
    {
        mlist_del(&g_subcache_list.list, subcache_index);
    }
}
