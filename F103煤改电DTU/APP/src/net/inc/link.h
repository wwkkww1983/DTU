#ifndef LINK_H
#define LINK_H
#include "os.h"
#include "net.h"

typedef struct
{
    uint8_t dstaddr;
    uint8_t nextaddr;
    uint8_t sleepmode;
} link_record_t;

typedef struct
{
    uint8_t type;
    uint8_t payload[];
} link_header_t;

void link_set_stable(uint8_t is_stable);
void link_init(void);
void link_poll(void);
link_record_t *link_get(uint8_t addr);
uint8_t link_state(void);
void link_request(void);
void link_disattach(void);
uint8_t link_is_build(void);
uint8_t link_is_child(uint8_t addr);
uint8_t link_get_father(void);
void link_add_record(uint8_t dstaddr, uint8_t nextaddr, uint8_t sleepmode);
link_record_t *link_get_record(uint8_t addr);
void link_recv_cb(link_header_t *plink, uint8_t srcaddr, uint8_t lastaddr, uint8_t sleepmode, int8_t rssi);

#endif // _LINK_H
