#include "ModuleTester.h"
#include <string.h>
#include <stdlib.h>
#include "MD5.h"
#include "cloud_helper.h"
#include "cloud_protocol.h"
#include "hal_led.h"
#include "device.h"
#include "hal_wait.h"
#include "property.h"
#include "SDCardRecord.h"


//APP查询标志
#define APP_QURRY_ALL_STATUS    (0X0F)      //查询所有状态
#define APP_QURRY_T             (0X01)      //查询温度
#define APP_QURRY_H             (0X01<<1)   //查询湿度
#define APP_QURRY_PM            (0X01<<2)   //查询门磁开关状态
#define APP_QURRY_CO2           (0X01<<3)   //查询门磁报警状态

#define SET_PRE_SMART_MODE      0x5A    //预进入smart模式状态
#define CLR_PRE_SMART_MODE      0x00    //清除预进入smart状态
#define RESET_TO_DEFAULTS_FLAG  0X5A    //恢复出厂设置标志

//生产测试状态
#define ENTER_PRODUCTION_TEST   0xAA    //进入生产测试状态
#define CLR_PRODUCTION_TEST     0x00    //清除生产测试状态
#define ENTER_FINAL_CHECK_TEST  0xEA    //进入整机测试
#define APP_CMD_TEST_FAIL       0xA5    //测试FAIL
#define APP_CMD_TEST_PASS       0x55    //测试PASS

#define SysTimeBeyond(cur, last, past) ((cur) - (last) > (past))

//版本号存放地址用于
//const uint8_t FwVersion[4] = {0x2, 0x00, 0x03, 0x08};      //版本号信息2.0.3.8

//型号ID与PIN码
//const uint8_t Model_ID[] = "ndbayo";
//const uint8_t Model_PIN[] = "3c215c14d40a464a8d5a99f83bee874f";

typedef struct
{
    SysTime_t lastRequestNettimeTime;
    SysTime_t lastRtcUpdateTime;
    ch_nettime_t RtcTimer;
	int8_t timezone;        //时区
    uint32_t seconds;       //零时区 1970/01/01 00:00:00 至今的秒数
    uint8_t gotNetTime;
} Rtctime_t;

typedef struct UpdateInfo_st
{
    uint32_t lastRequestTime;   //记录请求数据或者固件信息帧的时间
    uint32_t flashUnlockTime;   //记录Flash解锁时间
    uint32_t FwSize;            //获取到的固件信息长度
    uint16_t Parts;             //固件所需要帧数
    uint16_t CurIndex;          //当前请求的固件数据编号
    uint8_t FwMd5[CH_MD5_LEN]; //固件md5值
    uint8_t FwVersion[CH_VERSION_LEN]; //固件版本号
    uint8_t FwFlag;             //标识固件升级还是码库下载 0x55 0xaa
    uint8_t InUpdateFlag;       //处于升级状态标志位
    uint8_t ReqCount;           //升级请求次数
    uint8_t flashUnlockFlag;    //flash解锁标志
} UpdateInfo_t;

#define START_CODE  0XFC

/*******************************************************************************
 * declaration
 ******************************************************************************/
static SysButtonState_t getButtonState(SysButton_t *button);
static uint8_t buttonHandler(SysButton_t *button, uint32_t pressTime, SysButtonState_t state);
static uint8_t uartRecv(const uint8_t *buf, uint32_t len);
static void chEventHandler(ch_event_t event, void *p);
static void chUartWrite(const uint8_t *data, uint16_t len);
static void PowerAndLedCtrl(void);
//static uint8_t CheckPmUartInfo(const uint8_t *buf, uint32_t len);
static void syncProperties_all(void);
static void check_wm_sht_status(void *pt);
static void RtcTimerProcess(void);

//static void get_extenal_sensor_info(void *pt);
static void RomInfoProcess(ch_event_param_rom_info_t *p);
static void RomDataProcess(ch_event_param_rom_data_t *p);

/*******************************************************************************
 * static variable
 ******************************************************************************/

//配置信息
static Rtctime_t g_rtc_time;
static SysButton_t g_ConfigButton;
static ModuleInfo_t g_gw_electInfo;

#if WM_ENABLE
static WM_device_status_t g_wm_device_status[5];
#endif
static SHT_device_status_t g_sht_device_status[5];
static SK_device_status_t g_sk_device_status[5];

//static SHT_device_status_t g_sht_device_status[5];

static SysTime_t g_lastSyncTime;
static uint32_t g_lastSyncMin;
static uint32_t g_startSyncSec;

static MenuconfigInfo_t *g_config = NULL;
static UpdateInfo_t g_updateInfo = {0};
static char g_rmoduleID[32] = {0};
/*******************************************************************************
 * func
 ******************************************************************************/
//初始化
void ModuleTesterInit()
{
    ch_config_t cfg;
	
    HalUartConfig(WIFI_UART, 9600, PARITY_NO, uartRecv);
#if WM_ENABLE
    memset(g_wm_device_status, 0x00, sizeof(g_wm_device_status));
#endif
    memset(g_sht_device_status, 0x00, sizeof(g_sht_device_status));
	memset(g_sk_device_status, 0x00, sizeof(g_sk_device_status));
	g_sk_device_status[0].power = -1;
	g_sk_device_status[0].total_elec= -1;
	g_sk_device_status[1].power = -1;
	g_sk_device_status[1].total_elec = -1;
	
	g_gw_electInfo.sht_online_status = g_sht_device_status;
#if WM_ENABLE
	g_gw_electInfo.wm_online_status = g_wm_device_status;
#endif
#if SK_ENABLE
	g_gw_electInfo.sk_online_status = g_sk_device_status;
#endif

	g_config = GetConfig();
	g_startSyncSec = (GetRTCTime())%60;

    //注册按键
    SysButtonRegister(&g_ConfigButton, buttonHandler, getButtonState);

    cfg.device_type = (const char *)g_config->id;
    cfg.pinCode = (const char *)g_config->pin;
    cfg.uart_write = chUartWrite;
    cfg.event_handler = chEventHandler;
    cfg.version[0] = g_config->version[0];
	cfg.version[1] = g_config->version[1];
	cfg.version[2] = g_config->version[2];
	cfg.version[3] = g_config->version[3];
    ch_init(&cfg);

	PropertyInit();
	
    SysTimerSet(check_wm_sht_status, 5000, SYS_TIMER_REPEAT, NULL);
    //SysTimerSet(get_extenal_sensor_info, 10000, SYS_TIMER_REPEAT, NULL);

	printf("**********************************************************\n");
	printf("HardWare Version V2 \n");
	printf("App Version: %01x.%01x.%01x.%01x\n",g_config->version[0],g_config->version[1],g_config->version[2],g_config->version[3]);
	printf("BuildTime:   %s %s\n",__DATE__,__TIME__);
	printf("ModuleID:    %s\n",g_config->id);
	printf("**********************************************************\n");

	//TODO test
	//post_all_property(1, FRAME_TYPE_FOURCE_SYNC_PROPERTY);
	//record_all_property();
	//RecordReportStart(1514893740-600,1514894040+200);

	//写入需要升级标志
#if 0
{
    uint8_t tempFlagBuffer[4];
	num_to_data(tempFlagBuffer, FW_NEED_UPDATE_FLAG);
	SysUserDataWrite(OTA_UPDATE_INFO_ADDR, tempFlagBuffer, 4);
	HalRestart();    //复位
}
#endif	
	//	
}

void syncProperties()
{
    //判断当前是否在配置模式
    if(ch_wifi_in_config())
    {
        if(SysTimeBeyond(SysTime(), g_gw_electInfo.WifiCfgTimeOut, 120000))
        {
            g_gw_electInfo.WifiCfgTimeOut += 200;
            ch_set_wifi_config_type(CH_WIFI_CONFIG_TYPE_EXIT);
        }
    }

    if(APP_QURRY_ALL_STATUS == g_gw_electInfo.AppQry)
    {
        syncProperties_all();
        g_gw_electInfo.AppQry = 0;
    }
}

static void syncProperties_all(void)
{
	
}

static void syncProperties_all_record(void)
{
	//上电30后，每5s进入一次
    if(SysTimeHasPast(g_lastSyncTime, (5 * 1000ul)) && SysTimeHasRun(30 * 1000ul))
    {
		uint32_t  nowTs = GetRTCTime();
    	uint32_t  nowMin = nowTs/60;

		g_lastSyncTime = SysTime();
		//检测每分钟相同秒数上报
		if((nowMin != 0) && (g_lastSyncMin != nowMin) && (nowTs%60 >= g_startSyncSec))
		{
			g_lastSyncMin = nowMin;
			if(ch_is_online())
			{
				//传感器不在线不上报，属性强制上报
	        	post_all_property(0, FRAME_TYPE_FOURCE_SYNC_PROPERTY, g_lastSyncMin*60+g_startSyncSec);
			}
			else
			{
				SysLog("wifi/2G module offline!")
			}

			record_all_property(g_lastSyncMin*60+g_startSyncSec);
		}
    }
}

//循环
void ModuleTesterPoll()
{
    ch_handle();    //接收与发送函数处理

    syncProperties();   //属性相关处理

    RtcTimerProcess();

    PowerAndLedCtrl();  //LED显示
	
	syncProperties_all_record(); //每分钟上报一次
}

/*******************************************************************************
 * cloud helper
 ******************************************************************************/
//事件处理函数
static void chEventHandler(ch_event_t event, void *p)
{
    ch_event_param_operation_t *operation = (ch_event_param_operation_t *)p;
    ch_event_param_nettime_t *nettime = (ch_event_param_nettime_t *)p;
	ch_event_param_history_t *history = (ch_event_param_history_t *)p;
	ch_event_param_rom_info_t *rom_info = (ch_event_param_rom_info_t *)p;
    ch_event_param_rom_data_t *rom_data = (ch_event_param_rom_data_t *)p;
	char *info = p;	
	uint16_t pid;
	
    switch(event)
    {
        case CH_EVENT_MODULE_STARTUP:
            //模块启动，需同步所有属性
            SysLog("CH_EVENT_MODULE_STARTUP");
			ch_request_module_info();
            break;
        case CH_EVENT_OPERATION:
            //响应App操作，并回复操作结果
            SysLog("CH_EVENT_OPOERATION");
			pid = operation->propertyid[0] * 256 + operation->propertyid[1];
            switch(pid)
            {             
                default:
                    break;
            }
			ch_send_operation_result(true, pid);
            break;

        case CH_EVENT_MODULE_DIE:
            //应复位wifi模块
            SysLog("CH_EVENT_MODULE_DIE");
			HalLedSetStatus(MODULE_LED, 0);
            //HalWifiDeviceReset();
            break;

        case CH_EVENT_NET_TIME_RESPONSE:
            SysLog("got nettime.");
            g_rtc_time.gotNetTime = true;
            g_rtc_time.seconds = nettime->seconds;
			g_rtc_time.timezone = nettime->timezone;
            g_rtc_time.RtcTimer.year = nettime->nettime.year;
            g_rtc_time.RtcTimer.month = nettime->nettime.month;
            g_rtc_time.RtcTimer.day = nettime->nettime.day;
            g_rtc_time.RtcTimer.hour = nettime->nettime.hour;
            g_rtc_time.RtcTimer.min = nettime->nettime.min;
            g_rtc_time.RtcTimer.sec = nettime->nettime.sec;
            g_rtc_time.lastRtcUpdateTime = SysTime(); //记录当前本地时钟

            SysLog("got nettime %04d.%02d.%02d,%02d:%02d:%02d.", g_rtc_time.RtcTimer.year+2000, \
                   nettime->nettime.month, g_rtc_time.RtcTimer.day, g_rtc_time.RtcTimer.hour, \
                   g_rtc_time.RtcTimer.min, g_rtc_time.RtcTimer.sec);

			SetRTCTime(nettime->seconds);
            break;

		case CH_EVENT_MODULE_QUERY:
			SysLog("CH_EVENT_MODULE_QUERY");
			post_all_property(0, FRAME_TYPE_QUERY, GetRTCTime());
			break;

		case CH_EVENT_HISTORY_DATA_QUERY:
			SysLog("CH_EVENT_HISTORY_DATA_QUERY");
			//TODO  start report
			//记录下时间，在poll中上报，没条间隔300ms，不到1024
			RecordReportStart(history->startTime, history->endTime);
			break;
//ota
		case CH_EVENT_ROM_UPGRADE_NOTIFY:
            SysLog("CH_EVENT_ROM_UPGRADE_NOTIFY");
            ch_request_rom_info();
            break;

        case CH_EVENT_ROM_UPGRADE_INFO:
            RomInfoProcess(rom_info);
            break;

        case CH_EVENT_ROM_UPGRADE_DATA:
            SysLog("CH_EVENT_ROM_UPGRADE_DATA %d", rom_data->part_index);
            RomDataProcess(rom_data);
            break;
			
		case CH_EVENT_REQUEST_INFO_RESPONSE:
            SysLog("moduleID:%s", info);
			strcpy(g_rmoduleID, info);
			SDCardWirteModuleInfo(info);
			break;
//
        default:
            break;
    }
}

static void chUartWrite(const uint8_t *data, uint16_t len)
{
    HalUartWrite(WIFI_UART, data, len);
}

static uint8_t uartRecv(const uint8_t *buf, uint32_t len)
{
	ch_uart_data_input(buf, len);
	return len;
}

//按键处理函数   2s-6s松开，添加   按住20s恢复出厂设置
static uint8_t buttonHandler(SysButton_t *button, uint32_t pressTime, SysButtonState_t state)
{
    uint8_t retVal = 0;
    if(button == &g_ConfigButton) //电源开关
    {
        if(state == SYS_BUTTON_STATE_RELEASED)
        {
            if(1)
            {
				SysLog("pressTime=%d", pressTime);
                if((pressTime > 3000) && (pressTime < 8000))
                {
                    startAddDevice();
                    //ble_send_cmd_binding();
                }
            }

            retVal = 1;
        }
        else  //按键未释放
        {
			if((pressTime > 20000) )
			{
				SysLog("恢复出厂设置");
				DeviceSavedData_t sd[20]={0};
				g_gw_electInfo.PreSmartFlag = CLR_PRE_SMART_MODE;
				g_gw_electInfo.ResetToDefaultsTimeOut = SysTime();
				g_gw_electInfo.ResetToDefaultsFlag = RESET_TO_DEFAULTS_FLAG;
			
				memset(sd, 0, sizeof(DeviceSavedData_t)*20);
				SysUserDataWrite(DEVICE_SAVED_DATA_ADDR, (uint8_t *)sd, sizeof(DeviceSavedData_t)*20);//TDO wifi解绑时，一并清除单片机存储的设备信息
				ch_reset_cloud();  //清除云端数据
				//TODO
				HalRestart();
				retVal = 1; 
			}

        }
    }

    return retVal;
}


//判断按键状态
static SysButtonState_t getButtonState(SysButton_t *button)
{
    if(button == &g_ConfigButton)
    {
        return (SysButtonState_t)(!GPIO_ReadInputDataBit(KEY_PORT, KEY_PIN));
    }

    return SYS_BUTTON_STATE_RELEASED;
}

static void PowerAndLedCtrl(void)
{
	if(ch_is_online())
	{
		HalLedON(MODULE_LED);
	}
	else
	{
		HalLedOFF(MODULE_LED);
	}
    return;
}

static void check_wm_sht_status(void *pt)
{
    uint8_t i;
    int16_t diff_temp;
    uint8_t diff_flag = 0;

    for(i = 0; i < 5; i++)
    {
#if 0
        SysLog("wm[%d] %d,%d,%d,%d ", i, gataway_get_wm_info(i)->alloced, \
               gataway_get_wm_info(i)->online, gataway_get_wm_info(i)->voltage, \
               gataway_get_wm_info(i)->alarm_status);

        SysLog("sht[%d] %d,%d,%d,%d ", i, gataway_get_sht_info(i)->alloced, \
               gataway_get_sht_info(i)->online, gataway_get_sht_info(i)->voltage, \
               gataway_get_sht_info(i)->humidity);
#endif
#if WM_ENABLE
		g_wm_device_status[i].online = gataway_get_wm_info(i)->online;
        if((g_wm_device_status[i].voltage != gataway_get_wm_info(i)->voltage) || \
                (g_wm_device_status[i].alarm_status != gataway_get_wm_info(i)->alarm_status))
        {
            g_wm_device_status[i].voltage = gataway_get_wm_info(i)->voltage;
            g_wm_device_status[i].alarm_status = gataway_get_wm_info(i)->alarm_status;

        }
#endif
        /* 温度变化大于1度 */
        diff_temp = g_sht_device_status[i].temperature - gataway_get_sht_info(i)->temperature;
        if(diff_temp > 100 || diff_temp < -100)
        {
            diff_flag = 0xff;
        }
        else
        {
            diff_flag = 0;
        }
		
		g_sht_device_status[i].online= gataway_get_sht_info(i)->online;
        if((g_sht_device_status[i].voltage != gataway_get_sht_info(i)->voltage) || \
                (g_sht_device_status[i].humidity != gataway_get_sht_info(i)->humidity) || diff_flag)
        {
            g_sht_device_status[i].voltage = gataway_get_sht_info(i)->voltage;
            g_sht_device_status[i].humidity = gataway_get_sht_info(i)->humidity;
            g_sht_device_status[i].temperature = gataway_get_sht_info(i)->temperature;
        }


    }
	
	for(i = 0; i < 5; i++)
	{
		
        diff_temp = g_sk_device_status[i].total_elec- gataway_get_sk_info(i)->total_elec;
        if(diff_temp > 100 || diff_temp < -100)
        {
            diff_flag = 0xff;
        }
        else
        {
            diff_flag = 0;
        }

		
		g_sk_device_status[i].online = gataway_get_sk_info(i)->online;
        if((g_sk_device_status[i].power!= gataway_get_sk_info(i)->power) || \
                (g_sk_device_status[i].total_elec!= gataway_get_sk_info(i)->total_elec) /*|| diff_flag*/)
        {
            g_sk_device_status[i].voltage = gataway_get_sk_info(i)->voltage;
            g_sk_device_status[i].power = gataway_get_sk_info(i)->power;
            g_sk_device_status[i].total_elec = gataway_get_sk_info(i)->total_elec;	
        }

	}

}

static void RtcTimerProcess(void)
{
    /* 获取网络时间 */
    if(!g_rtc_time.gotNetTime && ch_is_online() && SysTimeBeyond(SysTime(), g_rtc_time.lastRequestNettimeTime, 5000ul))
    {
        g_rtc_time.lastRequestNettimeTime = SysTime();
        ch_request_nettime();
    }
    else if(ch_is_online() && SysTimeBeyond(SysTime(), g_rtc_time.lastRequestNettimeTime, (60 * 60 * 1000ul)))
    {
        /* 请求更新网络时间,1H更新一次 */
        g_rtc_time.lastRequestNettimeTime = SysTime();
        ch_request_nettime();
    }
}


//固件升级info处理
static void RomInfoProcess(ch_event_param_rom_info_t *p)
{
#if 1
    uint8_t md5NotZero, i;
    uint8_t tempFlagBuffer[4];

    if(g_updateInfo.InUpdateFlag)
    {
        return;
    }

	
	SysLog("Version:%01x.%01x.%01x.%01x -> %01x.%01x.%01x.%01x",g_config->version[0],g_config->version[1],g_config->version[2],g_config->version[3]
		,p->version[0], p->version[1], p->version[2], p->version[3]);
	
    if(p->version[0] != g_config->version[0]
            || p->version[1] != g_config->version[1]
            || p->version[2] != g_config->version[2]
            || p->version[3] != g_config->version[3])
    {
        g_updateInfo.FwFlag = ROM_DOWNLOAD_FLAG;  //固件升级标志
        g_updateInfo.FwSize = p->rom_size;
        g_updateInfo.Parts = (g_updateInfo.FwSize + CH_ROM_PART_SIZE - 1)  / CH_ROM_PART_SIZE;
        memcpy(g_updateInfo.FwMd5, p->md5, CH_MD5_LEN);
        g_updateInfo.CurIndex = 0;
        md5NotZero = 0x00;

        for(i = 0; i < CH_MD5_LEN; i++)
        {
            if(0x00 != g_updateInfo.FwMd5[i])
            {
                md5NotZero = 0xff;
                break;
            }
        }

        if((0x00 != g_updateInfo.FwSize) && (0x00 != md5NotZero))
        {
            g_updateInfo.InUpdateFlag = 0xff;

            //写入需要升级标志
            num_to_data(tempFlagBuffer, FW_NEED_UPDATE_FLAG);
			SysUserDataWrite(OTA_UPDATE_INFO_ADDR, tempFlagBuffer, 4);
			
            HalRestart();    //复位
        }
    }
    else
    {
        ch_request_rom_end(g_config->version);  //升级版本与现在版本一致直接发送升级完成
    }

    return;
#endif
}

//固件升级data处理
static void RomDataProcess(ch_event_param_rom_data_t *p)
{
}

#if 0
//TODO  温度湿度范围不正确，显示0
static void get_extenal_sensor_info(void *pt)
{
    uint32_t co2_val, pm_val, t_val, h_val;
	SensorInfo_t *sensorInfo = NULL;

    sensorInfo = HalGetSensorInfo();

    {
        co2_val = sensorInfo->pm25Co2Info.co2Val;
        pm_val = sensorInfo->pm25Co2Info.pm25Val;
        t_val = sensorInfo->sht20Info.temp_convert;
        h_val = sensorInfo->sht20Info.hum_convert;

		g_gw_electInfo.sensorTHWork = sensorInfo->sht20Info.tempWork;
		g_gw_electInfo.sensorPM25Work= sensorInfo->pm25Co2Info.pm25Work;
		g_gw_electInfo.sensorCO2Work = sensorInfo->pm25Co2Info.co2Work;
		
        SysLog("Temperature:%.02f -> %d", sensorInfo->sht20Info.TEMP_HM, t_val);
        SysLog("Humidity:%.02f -> %d", sensorInfo->sht20Info.HUMI_HM, h_val);
		SysLog("co2_val:%d", co2_val);
		SysLog("pm_val:%d", pm_val);
		
        if(g_gw_electInfo.CO2 != co2_val)
        {
            g_gw_electInfo.CO2 = co2_val;
#if RT_PROPERTY_ENABLE
#endif
        }

        if(g_gw_electInfo.PM25 != pm_val)
        {
            g_gw_electInfo.PM25 = pm_val;
#if RT_PROPERTY_ENABLE
#endif
        }

        if(g_gw_electInfo.CurTemperature != t_val || g_gw_electInfo.CurHumidity != h_val)
        {
            g_gw_electInfo.CurTemperature = t_val;
            g_gw_electInfo.CurHumidity = h_val;
#if RT_PROPERTY_ENABLE
#endif
        }
    }
}
#endif

ModuleInfo_t *GetModuleTesterInfo(void)
{
	return &g_gw_electInfo;
}

char *GetrModuleID(void)
{
	return g_rmoduleID;
}
