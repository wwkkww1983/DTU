#include "userSys.h"
#include "ModuleTester.h"
#include <string.h>
#include <stdlib.h>
#include "cloud_helper.h"
#include "cloud_protocol.h"
#include "device.h"
#include "SDCardRecord.h"
#include "OutdoorPid.h"


 typedef enum
 {
	 GW_PT100_1_T = 25,	
	 GW_PT100_2_T, 			     
	 GW_PT100_3_T,		       
	 GW_PT100_4_T,	
	 GW_PT100_OUT_T,

	 GW_E_METER_VOL,
	 GW_E_METER_ELEC,
	 GW_E_METER_POWER,
	 GW_E_METER_QUANT,
	 GW_E_METER_FACTOR,
	 GW_E_METER_VALLEY,
	 GW_E_METER_PEAK,

	 GW_F_METER_H_FLOW,
	 GW_F_METER_T_FLOW,
	 GW_F_METER_N_FLOW,
	 GW_F_METER_ENTER_HEAT,
	 GW_F_METER_EXIT_HEAT,
	 
	 //无线温湿度
	 GW_SHT20_1_T,
	 GW_SHT20_1_H, 
	 GW_SHT20_1_ONLINE,    
	 GW_SHT20_1_VOL,
	 GW_SHT20_2_T,
	 GW_SHT20_2_H, 
	 GW_SHT20_2_ONLINE,    
	 GW_SHT20_2_VOL,
	 GW_SHT20_3_T,
	 GW_SHT20_3_H, 
	 GW_SHT20_3_ONLINE,    
	 GW_SHT20_3_VOL,
	 GW_SHT20_4_T,
	 GW_SHT20_4_H, 
	 GW_SHT20_4_ONLINE,    
	 GW_SHT20_4_VOL,
	 GW_SHT20_5_T,
	 GW_SHT20_5_H, 
	 GW_SHT20_5_ONLINE,    
	 GW_SHT20_5_VOL,

	 GW_E_METER2_VOL = 101,
	 GW_E_METER2_ELEC,
	 GW_E_METER2_POWER,
	 GW_E_METER2_QUANT,
	 GW_E_METER2_FACTOR,
	 GW_E_METER2_VALLEY,
	 GW_E_METER2_PEAK,
 
	 GW_STATUS_TS = 255,
 } AC_ControlPropertyID;
 
#define sht20(i, type) GW_SHT20_##i##_##type
#define sk(i,type)     GW_SK_##i##_##type
#define wm(i,type)     GW_WM_##i##_##type

static MenuconfigInfo_t *g_config = NULL;
static ModuleInfo_t *g_moduleInfo = NULL;
#if WM_ENABLE
static WM_device_status_t *g_wm_device_status;
#endif
static SHT_device_status_t *g_sht_device_status;
#if SK_ENABLE
static SK_device_status_t *g_sk_device_status;
#endif
static SensorInfo_t *g_sensorInfo = NULL; 

void OutDoorPidInit( void )
{	
	g_config = GetConfig();
	g_moduleInfo = GetModuleTesterInfo();
#if WM_ENABLE
	g_wm_device_status = g_moduleInfo->wm_online_status;
#endif
	g_sht_device_status = g_moduleInfo->sht_online_status;
#if SK_ENABLE
	g_sk_device_status = g_moduleInfo->sk_online_status;
#endif
	g_sensorInfo = HalGetSensorInfo();
}

 static uint16_t Pid(const char *device, uint8_t num, const char *type)
{
	uint16_t id = 0;
	if(device == "sht20")
	{
		switch (num)
		{
			case 1:
			if(type == "online")
			{
				id = sht20(1,ONLINE);
			}
			if(type == "vol")
			{
				id = sht20(1,VOL);
			}
			if(type == "h")
			{
				id = sht20(1,H);
			}
			if(type == "t")
			{
				id = sht20(1,T);
			}
			break;
			
			case 2:
			if(type == "online")
			{
				id = sht20(2,ONLINE);
			}
			if(type == "vol")
			{
				id = sht20(2,VOL);
			}
			if(type == "h")
			{
				id = sht20(2,H);
			}
			if(type == "t")
			{
				id = sht20(2,T);
			}
			break;
			
			case 3:
			if(type == "online")
			{
				id = sht20(3,ONLINE);
			}
			if(type == "vol")
			{
				id = sht20(3,VOL);
			}
			if(type == "h")
			{
				id = sht20(3,H);
			}
			if(type == "t")
			{
				id = sht20(3,T);
			}
			break;
			
			case 4:
			if(type == "online")
			{
				id = sht20(4,ONLINE);
			}
			if(type == "vol")
			{
				id = sht20(4,VOL);
			}
			if(type == "h")
			{
				id = sht20(4,H);
			}
			if(type == "t")
			{
				id = sht20(4,T);
			}
			break;
			
			case 5:
			if(type == "online")
			{
				id = sht20(5,ONLINE);
			}
			if(type == "vol")
			{
				id = sht20(5,VOL);
			}
			if(type == "h")
			{
				id = sht20(5,H);
			}
			if(type == "t")
			{
				id = sht20(5,T);
			}
			break;
		}
	}
#if WM_ENABLE
	else if(device == "wm")
	{
		switch (num)
		{
			case 1:
			if(type == "online")
			{
				id = wm(1,ONLINE);
			}
			if(type == "state")
			{
				id = wm(1,STATE);
			}
			if(type == "vol")
			{
				id = wm(1,VOL);
			}
			break;
			
			case 2:
			if(type == "online")
			{
				id = wm(2,ONLINE);
			}
			if(type == "state")
			{
				id = wm(2,STATE);
			}
			if(type == "vol")
			{
				id = wm(2,VOL);
			}
			break;

			case 3:
			if(type == "online")
			{
				id = wm(3,ONLINE);
			}
			if(type == "state")
			{
				id = wm(3,STATE);
			}
			if(type == "vol")
			{
				id = wm(3,VOL);
			}
			break;

			
			case 4:
			if(type == "online")
			{
				id = wm(4,ONLINE);
			}
			if(type == "state")
			{
				id = wm(4,STATE);
			}
			if(type == "vol")
			{
				id = wm(4,VOL);
			}
			break;
			
			case 5:
			if(type == "online")
			{
				id = wm(5,ONLINE);
			}
			if(type == "state")
			{
				id = wm(5,STATE);
			}
			if(type == "vol")
			{
				id = wm(5,VOL);
			}
			break;

		}	
	}
#endif
#if SK_ENABLE
	else if(device == "sk")
	{
		switch (num)
		{
			case 1:
			if(type == "online")
			{
				id = sk(1,ONLINE);
			}
			if(type == "power")
			{
				id = sk(1,POWER);
			}
			if(type == "t_elec")
			{
				id = sk(1,T_ELEC);
			}
			break;
			
			case 2:
			if(type == "online")
			{
				id = sk(2,ONLINE);
			}
			if(type == "power")
			{
				id = sk(2,POWER);
			}
			if(type == "t_elec")
			{
				id = sk(2,T_ELEC);
			}
			break;
			
			case 3:
			if(type == "online")
			{
				id = sk(3,ONLINE);
			}
			if(type == "power")
			{
				id = sk(3,POWER);
			}
			if(type == "t_elec")
			{
				id = sk(3,T_ELEC);
			}
			break;

			case 4:
			if(type == "online")
			{
				id = sk(4,ONLINE);
			}
			if(type == "power")
			{
				id = sk(4,POWER);
			}
			if(type == "t_elec")
			{
				id = sk(4,T_ELEC);
			}
			break;

			case 5:
			if(type == "online")
			{
				id = sk(5,ONLINE);
			}
			if(type == "power")
			{
				id = sk(5,POWER);
			}
			if(type == "t_elec")
			{
				id = sk(5,T_ELEC);
			}
			break;
		}	
	}
#endif
	if(id == 0)
	{
		SysLog("no such pid:%s-%d-%s",device,num,type);
	}
	//SysLog("%s-%d-%s : %d",device,num,type,id);
	return id;
}



static char* trans_property_by_text(uint8_t pid, void *num)
{
	static char text[30] = {0};
	//uint32_t tempu32 = 0;
	uint16_t tempu16 = 0;
	int32_t  convert = 0;
	uint8_t type = 0;  //1:温度    2:功率 电量
	memset(text, 0 , 30);

	if(pid == GW_PT100_1_T ||pid == GW_PT100_2_T
		|| pid == GW_PT100_3_T ||pid == GW_PT100_4_T
		|| pid == GW_PT100_OUT_T)
	{

	}

	if(pid == GW_SHT20_1_T || GW_SHT20_2_T || GW_SHT20_3_T 
		|| GW_SHT20_4_T || GW_SHT20_5_T)
	{
		type = 1;
		tempu16 = *(uint16_t*)num;
		convert = (int32_t)tempu16;
	}
#if SK_ENABLE
	if(pid == GW_SK_1_POWER || pid == GW_SK_1_T_ELEC
		|| pid == GW_SK_2_POWER || pid == GW_SK_2_T_ELEC
		|| pid == GW_SK_3_POWER || pid == GW_SK_3_T_ELEC
		|| pid == GW_SK_4_POWER || pid == GW_SK_4_T_ELEC
		|| pid == GW_SK_5_POWER || pid == GW_SK_5_T_ELEC)
	{
		type = 2;
		convert = *(int32_t*)num;
	}
#endif


	if(type == 1)
	{
		//温度 超过范围显示0.0
		if(convert && convert < 50000)
		{
			if(abs((convert - 27315) % 100 ) < 10)
			{
				sprintf(text, "%d.0%01d", (convert - 27315) / 100, abs((convert - 27315) % 100));
			}
			else
			{
				sprintf(text, "%d.%01d", (convert - 27315) / 100, abs((convert - 27315) % 100));
			}

		}
		else
		{
			sprintf(text, "%d", 0);
		}

	}
	else if(type == 2)
	{
		if(convert && convert != -1)
		{
			if((convert % 100) < 10)
			{
				sprintf(text, "%d.0%01d", convert / 100, abs(convert % 100));
			}
			else
			{
				sprintf(text, "%d.%01d", convert / 100, abs(convert % 100));
			}
		}
		else if(convert == -1)
		{
			sprintf(text, "%s", "--");
			return "";
		}
		else
		{
			sprintf(text, "%d", 0);
		}
	}

	return text;
}
 
 void Outdoor_post_all_property(uint8_t isAll, uint8_t type, uint32_t ts)
{
  uint8_t i = 0;
  E_MeterInfo_t *e_meterInfo = g_sensorInfo->meterInfo->e_MeterInfo;
  E_MeterInfo_t *e_meterInfo2 = g_sensorInfo->meterInfo->e_MeterInfo2;
  F_MeterInfo_t *f_meterInfo = g_sensorInfo->meterInfo->f_MeterInfo;
  ch_sync_multi_property(GW_STATUS_TS, ts, NULL);

  if(g_sensorInfo->pt100DTUInfo->pt100DTUWork || isAll)
  {
  	if(strlen(g_sensorInfo->pt100DTUInfo->pt1))
  	{
		ch_sync_multi_property(GW_PT100_1_T, 0, g_sensorInfo->pt100DTUInfo->pt1);
  	}
	if(strlen(g_sensorInfo->pt100DTUInfo->pt2))
  	{
		ch_sync_multi_property(GW_PT100_2_T, 0, g_sensorInfo->pt100DTUInfo->pt2);
  	}
	if(strlen(g_sensorInfo->pt100DTUInfo->pt3))
  	{
		ch_sync_multi_property(GW_PT100_3_T, 0, g_sensorInfo->pt100DTUInfo->pt3);
  	}
	if(strlen(g_sensorInfo->pt100DTUInfo->pt4))
  	{
		ch_sync_multi_property(GW_PT100_4_T, 0, g_sensorInfo->pt100DTUInfo->pt4);
  	}
	if(strlen(g_sensorInfo->pt100DTUInfo->pt5Out))
  	{
		ch_sync_multi_property(GW_PT100_OUT_T, 0, g_sensorInfo->pt100DTUInfo->pt5Out);
  	}
  }
  

  if(e_meterInfo && (e_meterInfo->e_meterWork || isAll))
  {
	ch_sync_multi_property(GW_E_METER_VOL, 0, e_meterInfo->voltage);
	ch_sync_multi_property(GW_E_METER_ELEC, 0, e_meterInfo->current);
	ch_sync_multi_property(GW_E_METER_POWER, 0, e_meterInfo->instantPower);
	ch_sync_multi_property(GW_E_METER_QUANT, 0, e_meterInfo->powerCount);
	ch_sync_multi_property(GW_E_METER_FACTOR, 0, e_meterInfo->factor);
	
	if(strlen(e_meterInfo->bottomPower))
	{
		ch_sync_multi_property(GW_E_METER_VALLEY, 0, e_meterInfo->bottomPower);
	}
	
	if(strlen(e_meterInfo->peakPower))
	{
		ch_sync_multi_property(GW_E_METER_PEAK, 0, e_meterInfo->peakPower);
	}
  }

    if(e_meterInfo2 && (e_meterInfo2->e_meterWork || isAll))
  {
	ch_sync_multi_property(GW_E_METER2_VOL, 0, e_meterInfo2->voltage);
	ch_sync_multi_property(GW_E_METER2_ELEC, 0, e_meterInfo2->current);
	ch_sync_multi_property(GW_E_METER2_POWER, 0, e_meterInfo2->instantPower);
	ch_sync_multi_property(GW_E_METER2_QUANT, 0, e_meterInfo2->powerCount);
	ch_sync_multi_property(GW_E_METER2_FACTOR, 0, e_meterInfo2->factor);
	
	if(strlen(e_meterInfo2->bottomPower))
	{
		ch_sync_multi_property(GW_E_METER2_VALLEY, 0, e_meterInfo2->bottomPower);
	}
	
	if(strlen(e_meterInfo2->peakPower))
	{
		ch_sync_multi_property(GW_E_METER2_PEAK, 0, e_meterInfo2->peakPower);
	}
  }

  if(f_meterInfo && (f_meterInfo->f_meterWork || isAll))
  {
	ch_sync_multi_property(GW_F_METER_H_FLOW, 0, f_meterInfo->instantHeat);
	ch_sync_multi_property(GW_F_METER_T_FLOW, 0, f_meterInfo->accHeatDec);
	ch_sync_multi_property(GW_F_METER_N_FLOW, 0, f_meterInfo->flowCount);
	ch_sync_multi_property(GW_F_METER_ENTER_HEAT, 0, f_meterInfo->inputTemp);
	ch_sync_multi_property(GW_F_METER_EXIT_HEAT, 0, f_meterInfo->outputTemp);
  }

  for(i=0; i < g_config->shtNum; i++)
  {
	ch_sync_multi_property(Pid("sht20", i+1, "online"), g_sht_device_status[i].online, NULL);
	if(g_sht_device_status[i].online || isAll)
	{
		  ch_sync_multi_property(Pid("sht20", i+1, "vol"), g_sht_device_status[i].voltage, NULL);
		  ch_sync_multi_property(Pid("sht20", i+1, "t"), 0, trans_property_by_text(Pid("sht20", i+1, "t"), &g_sht_device_status[i].temperature));
		  ch_sync_multi_property(Pid("sht20", i+1, "h"), g_sht_device_status[i].humidity, NULL);
	}
  }

#if WM_ENABLE
  for(i=0; i < g_config->wmNum; i++)
  {
	  ch_sync_multi_property(Pid("wm",i+1, "online"), g_wm_device_status[i].online, NULL);
	  if(g_wm_device_status[i].online || isAll)
	  {
		  ch_sync_multi_property(Pid("wm",i+1, "vol"), g_wm_device_status[i].voltage, NULL);
		  ch_sync_multi_property(Pid("wm",i+1, "state"), g_wm_device_status[i].alarm_status, NULL);
	  }
  }
#endif

#if SK_ENABLE
  for(i=0; i < g_config->skNum; i++)
  {
	  ch_sync_multi_property(Pid("sk",i+1, "online"), g_sk_device_status[i].online, NULL);
	  if(g_sk_device_status[i].online || isAll)
	  {
		ch_sync_multi_property(Pid("sk",i+1, "power"), 0, trans_property_by_text(GW_SK_1_POWER, &g_sk_device_status[i].power)); 
		ch_sync_multi_property(Pid("sk",i+1, "t_elec"), 0, trans_property_by_text(GW_SK_1_T_ELEC, &g_sk_device_status[i].total_elec)); 
	  }
  }
#endif  
  ch_send_multi_property(type);
}

 void Outdoor_record_all_property(uint32_t ts)
{
  uint8_t i = 0;
  uint32_t startTime = RecordBuildStart(ts);
  E_MeterInfo_t *e_meterInfo = g_sensorInfo->meterInfo->e_MeterInfo;
  E_MeterInfo_t *e_meterInfo2 = g_sensorInfo->meterInfo->e_MeterInfo2;
  F_MeterInfo_t *f_meterInfo = g_sensorInfo->meterInfo->f_MeterInfo;
  if(!startTime)
  {
  	return;
  }

  if(g_sensorInfo->pt100DTUInfo->pt100DTUWork)
  {
  	if(strlen(g_sensorInfo->pt100DTUInfo->pt1))
  	{
		RecordBuild(GW_PT100_1_T, 0, g_sensorInfo->pt100DTUInfo->pt1);
  	}
	if(strlen(g_sensorInfo->pt100DTUInfo->pt2))
  	{
		RecordBuild(GW_PT100_2_T, 0, g_sensorInfo->pt100DTUInfo->pt2);
  	}
	if(strlen(g_sensorInfo->pt100DTUInfo->pt3))
  	{
		RecordBuild(GW_PT100_3_T, 0, g_sensorInfo->pt100DTUInfo->pt3);
  	}
	if(strlen(g_sensorInfo->pt100DTUInfo->pt4))
  	{
		RecordBuild(GW_PT100_4_T, 0, g_sensorInfo->pt100DTUInfo->pt4);
  	}
	if(strlen(g_sensorInfo->pt100DTUInfo->pt5Out))
  	{
		RecordBuild(GW_PT100_OUT_T, 0, g_sensorInfo->pt100DTUInfo->pt5Out);
  	}
  }
  

  //emeter 1
  if(e_meterInfo && e_meterInfo->e_meterWork)
  {
	RecordBuild(GW_E_METER_VOL, 0, e_meterInfo->voltage);
	RecordBuild(GW_E_METER_ELEC, 0, e_meterInfo->current);
	RecordBuild(GW_E_METER_POWER, 0, e_meterInfo->instantPower);
	RecordBuild(GW_E_METER_QUANT, 0, e_meterInfo->powerCount);
	RecordBuild(GW_E_METER_FACTOR, 0, e_meterInfo->factor);
	if(strlen(e_meterInfo->bottomPower))
	{
		RecordBuild(GW_E_METER_VALLEY, 0, e_meterInfo->bottomPower);
	}

	if(strlen(e_meterInfo->peakPower))
	{
		RecordBuild(GW_E_METER_PEAK, 0, e_meterInfo->peakPower);
  	}
  }
//emeter2
  if(e_meterInfo2 && e_meterInfo2->e_meterWork)
  {
	RecordBuild(GW_E_METER2_VOL, 0, e_meterInfo2->voltage);
	RecordBuild(GW_E_METER2_ELEC, 0, e_meterInfo2->current);
	RecordBuild(GW_E_METER2_POWER, 0, e_meterInfo2->instantPower);
	RecordBuild(GW_E_METER2_QUANT, 0, e_meterInfo2->powerCount);
	RecordBuild(GW_E_METER2_FACTOR, 0, e_meterInfo2->factor);
	if(strlen(e_meterInfo2->bottomPower))
	{
		RecordBuild(GW_E_METER2_VALLEY, 0, e_meterInfo2->bottomPower);
	}

	if(strlen(e_meterInfo2->peakPower))
	{
		RecordBuild(GW_E_METER2_PEAK, 0, e_meterInfo2->peakPower);
  	}
  }

  //fmeter
  if(f_meterInfo && f_meterInfo->f_meterWork)
  {
	RecordBuild(GW_F_METER_H_FLOW, 0, f_meterInfo->instantHeat);
	RecordBuild(GW_F_METER_T_FLOW, 0, f_meterInfo->accHeatDec);
	RecordBuild(GW_F_METER_N_FLOW, 0, f_meterInfo->flowCount);
	RecordBuild(GW_F_METER_ENTER_HEAT, 0, f_meterInfo->inputTemp);
	RecordBuild(GW_F_METER_EXIT_HEAT, 0, f_meterInfo->outputTemp);
  }

  for(i=0; i < g_config->shtNum; i++)
  {
	RecordBuild(Pid("sht20", i+1, "online"), g_sht_device_status[i].online, NULL);
	if(g_sht_device_status[i].online)
	{
		  RecordBuild(Pid("sht20", i+1, "vol"), g_sht_device_status[i].voltage, NULL);
		  RecordBuild(Pid("sht20", i+1, "t"), 0, trans_property_by_text(Pid("sht20", i+1, "t"), &g_sht_device_status[i].temperature));
		  RecordBuild(Pid("sht20", i+1, "h"), g_sht_device_status[i].humidity, NULL);
	}
  }

#if WM_ENABLE
  for(i=0; i < g_config->wmNum; i++)
  {
	  RecordBuild(Pid("wm",i+1, "online"), g_wm_device_status[i].online, NULL);
	  if(g_wm_device_status[i].online)
	  {
		  RecordBuild(Pid("wm",i+1, "vol"), g_wm_device_status[i].voltage, NULL);
		  RecordBuild(Pid("wm",i+1, "state"), g_wm_device_status[i].alarm_status, NULL);
	  }
  }
#endif

#if SK_ENABLE
  for(i=0; i < g_config->skNum; i++)
  {
	  RecordBuild(Pid("sk",i+1, "online"), g_sk_device_status[i].online, NULL);
	  if(g_sk_device_status[i].online )
	  {
		RecordBuild(Pid("sk",i+1, "power"), 0, trans_property_by_text(GW_SK_1_POWER, &g_sk_device_status[i].power)); 
		RecordBuild(Pid("sk",i+1, "t_elec"), 0, trans_property_by_text(GW_SK_1_T_ELEC, &g_sk_device_status[i].total_elec)); 
	  }
  }
#endif 
  RecordBuildEnd(startTime);
}

