#include "Hal.h"
#include "userSys.h"

static void hal_cal_pm25_val_poll(void);
static void hal_cal_co2_val_poll(void);
static uint8_t HalPmUartRecvData(const uint8_t *data, uint32_t len);
static uint8_t HalCo2UartRecvData(const uint8_t *data, uint32_t len);
static void get_co2_sensor_info(void *pt);

/* PM25 */
static PM25CO2Info_t g_pm25Co2Val = {0};
static uint8_t g_pm25_rcv_count;
static uint8_t g_pm25_rcv_done;
static uint8_t g_pm25_rcv_buf[40];

//CO2
const uint8_t g_cmd_get_co2[] = {0xff, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};
static uint8_t g_co2_rcv_count;
static uint8_t g_co2_rcv_done;
static uint8_t g_co2_rcv_buf[12];
static uint8_t g_co2_data[12];

typedef struct _ST_PMS5003T_DATA_INFO_
{
    uint8_t StartByte[2];
    uint8_t FramLen[2];
    uint8_t Data[13 * 2];
    uint8_t SumCheck[2];
} PmRawDataInfo_st, *pPmRawDataInfo_st;

void HalPM25CO2Init(void)
{
	HalUartConfig(CO2_UART, 9600, PARITY_NO, HalCo2UartRecvData);
	HalUartConfig(PM25_UART, 9600, PARITY_NO, HalPmUartRecvData);
	SysTimerSet(get_co2_sensor_info, 3000, SYS_TIMER_REPEAT | SYS_TIMER_IMME, NULL);
}

void HalPM25CO2Poll(void)
{
	hal_cal_pm25_val_poll();
	hal_cal_co2_val_poll();
}

PM25CO2Info_t HalGetPM25CO2Info(void)
{
	return g_pm25Co2Val;
}


static void HalPmUartRecvByte(uint8_t data)
{
    g_pm25_rcv_buf[g_pm25_rcv_count++] = data;
    if((g_pm25_rcv_count == 1) && (g_pm25_rcv_buf[0] != 0x42))
    {
        g_pm25_rcv_count = 0;
    }
    else if((g_pm25_rcv_buf[1] != 0x4d) && (g_pm25_rcv_count == 2))
    {
        g_pm25_rcv_count = 0;
    }
    else if(g_pm25_rcv_count >= 32)
    {
        g_pm25_rcv_count = 0;
        g_pm25_rcv_done = 0x01;
    }
}

static uint8_t HalPmUartRecvData(const uint8_t *data, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		HalPmUartRecvByte(data[i]);
	}
	return i;
}

static void hal_cal_pm25_val_poll(void)
{
    uint8_t i;

    uint16_t check_sum = 0;
    uint16_t rcv_sum;

    const PmRawDataInfo_st *pPmRawDataInfo = (const PmRawDataInfo_st *)g_pm25_rcv_buf;

    if(g_pm25_rcv_done)
    {
        g_pm25_rcv_done = 0;

        check_sum = 0;

        for(i = 0; i < 30; i++)
        {
            check_sum += g_pm25_rcv_buf[i];
        }

        rcv_sum = pPmRawDataInfo->SumCheck[0] << 8 | pPmRawDataInfo->SumCheck[1];

        if(rcv_sum != check_sum)
        {
            //SysLog("0x04 %04x %04x", rcv_sum, check_sum);
        }
        else
        {
            g_pm25Co2Val.pm25Val  = pPmRawDataInfo->Data[4 * 2] << 8 | pPmRawDataInfo->Data[4 * 2 + 1];
			g_pm25Co2Val.pm25Work = 1;
            //SysLog("pm25 data = %d", g_pm25Co2Val.pm25Val);
        }
    }
}

//CO2
static void HalCo2UartRecvByte(uint8_t data)
{
    g_co2_rcv_buf[g_co2_rcv_count++] = data;
    if((g_co2_rcv_count == 1) && (g_co2_rcv_buf[0] != 0xff))
    {
        g_co2_rcv_count = 0;
    }
    else if((g_co2_rcv_buf[1] != 0x86) && (g_co2_rcv_count == 2))
    {
        g_co2_rcv_count = 0;
    }
    else if(g_co2_rcv_count >= 9)
    {
        g_co2_rcv_count = 0;
        g_co2_rcv_done = 0x01;
    }
}

static uint8_t HalCo2UartRecvData(const uint8_t *data, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		HalCo2UartRecvByte(data[i]);
	}
	return i;
}

static void HalCo2SendBuf(const uint8_t *buf, uint8_t len)
{
	HalUartWrite(CO2_UART, buf, len);
}

static void get_co2_sensor_info(void *pt)
{
    HalCo2SendBuf(g_cmd_get_co2, 9);
}

static void hal_cal_co2_val_poll(void)
{
    uint8_t i;
    uint8_t check_sum;

    if(g_co2_rcv_done)
    {
        for(i = 0; i < 9; i++)
        {
            g_co2_data[i] = g_co2_rcv_buf[i];
        }

        g_co2_rcv_done = 0;

        check_sum = 0;
        for(i = 1; i < 8; i++)
        {
            check_sum += g_co2_data[i];
        }

        check_sum = ~check_sum + 1;

        if(check_sum != g_co2_data[8])
        {
            SysLog("co2 data check sum err!!!!!")
        }
        else
        {
            g_pm25Co2Val.co2Val= g_co2_data[2] << 8 | g_co2_data[3];
			g_pm25Co2Val.co2Work= 1;
            //SysLog("co2 data = %d", g_pm25Co2Val.co2Val);
        }
    }
}

