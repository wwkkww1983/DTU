#include "Hal.h"
#include "userSys.h"

typedef struct 
{
    uint8_t head;
    uint8_t addr[6];
    uint8_t frameHead;
    uint8_t ctrl;
    uint8_t dataLen;
    uint8_t data[];
}ElectricProtcol_t;

static const uint8_t ELECREG_ACTIVE_POWER[4] = {0, 0, 1, 0}; //有功总电能
static const uint8_t ELECREG_VOLTAGE[4] = {0, 1, 1, 2}; //A 相电压
static const uint8_t ELECREG_CURRENT[4] = {0, 1, 2, 2}; //A 相电流
static const uint8_t ELECREG_INST_POWER[4] = {0, 0, 3, 2}; //瞬时总有功功率
static const uint8_t ELECREG_PEAK_POWER[4] = {0, 2, 1, 0}; //正向有功尖电能
static const uint8_t ELECREG_BOTTOM_POWER[4] = {0, 4, 1, 0}; //正向有功谷电能
static const uint8_t ELECREG_FACTOR[4] = {0, 0, 6, 2}; //正向有功谷电能

static uint8_t g_recvBuff[64];
static uint8_t g_elecMeterRecvCount = 0;
static E_MeterInfo_t  g_ddyz747Info = {0};
static uint8_t g_meterWorkAbnormal = 0;

static void DDYZ747DataPrase(uint8_t *data, uint8_t len);
static void DDYZ7474DataSend(uint8_t cmd, uint8_t dlen, const uint8_t *data);
static uint8_t DDYZ7474MeterRecvDate(const uint8_t *data, uint32_t len);
static uint8_t checkSum(const uint8_t *data, uint8_t len);
static void DDYZ747DataPrase(uint8_t *data, uint8_t len);


void DDYZ747Init(void)
{
	memset(&g_ddyz747Info, 0, sizeof(g_ddyz747Info));
	HalUartConfig(METER_UART, 2400 ,PARITY_EVEN, DDYZ7474MeterRecvDate);
	ModbusCtrlInit(METER_CTRL_PORT,METER_CTRL_PIN);
}

uint8_t DDYZ747Query(void)
{
	static uint8_t elecQueryId = 0;
    int ret = 0;

	HalUartConfig(METER_UART, 2400 ,PARITY_EVEN, DDYZ7474MeterRecvDate);
	
    if(elecQueryId == 0)
    {
        DDYZ7474DataSend(0x11, 4, ELECREG_ACTIVE_POWER);
        ret = 1;
    }
    else if(elecQueryId == 1)
    {
        DDYZ7474DataSend(0x11, 4, ELECREG_VOLTAGE);
        ret = 1;
    }
    else if(elecQueryId == 2)
    {
        DDYZ7474DataSend(0x11, 4, ELECREG_CURRENT);
        ret = 1;
    }
    else if(elecQueryId == 3)
    {
        DDYZ7474DataSend(0x11, 4, ELECREG_INST_POWER);
        ret = 1;
    }
	else if(elecQueryId == 4)
	{
		DDYZ7474DataSend(0x11, 4, ELECREG_FACTOR);
        ret = 1;
	}
    else if(elecQueryId == 5)
    {
        DDYZ7474DataSend(0x11, 4, ELECREG_PEAK_POWER);
        ret = 1;
    }
    else
    {
        DDYZ7474DataSend(0x11, 4, ELECREG_BOTTOM_POWER);
    }
    elecQueryId++;

    if(ret == 0)
    {
        elecQueryId = 0;
    }

	//10次未响应，工作异常
	g_meterWorkAbnormal++;
	if(g_meterWorkAbnormal >= 10)
	{
		SysLog("DDZY747 Elec Meter work abnormal!");
		g_meterWorkAbnormal = 0;
		g_ddyz747Info.e_meterWork = 0;
	}

	return ret;
}

E_MeterInfo_t *DDYZ747GetInfo(void)
{
	return &g_ddyz747Info;
}

static void DDYZ7474DataSend(uint8_t cmd, uint8_t dlen, const uint8_t *data)
{
	uint8_t i = 12;
	uint8_t j = 0;
	uint8_t sendData[64]= {0xFE, 0xFE, 0xFE, 0xFE,
                       0x68,
                       0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
                       0x68};
	
	sendData[i++] = cmd;
	sendData[i++] = dlen;

	for(j = 0; j < dlen; j++)
	{
		sendData[i++] = (data[j] + 0x33);
	}

	sendData[i++] = checkSum(sendData+4, i-4);
	sendData[i++] = 0x16;
	
	ModbusCtrlSetMode(METER_CTRL_PORT, METER_CTRL_PIN, 1);	
	//SysDataPrint(sendData, i);
	HalUartWrite(METER_UART, sendData, i);
	ModbusCtrlSetMode(METER_CTRL_PORT, METER_CTRL_PIN, 0);
}

static void DDYZ7474MeterRecvByte(uint8_t byte)
{
    static uint8_t dataLen;
    ElectricProtcol_t *elec;
    uint8_t sum;
    
    g_recvBuff[g_elecMeterRecvCount++] = byte;
    if(g_elecMeterRecvCount >= sizeof(g_recvBuff))
    {
        g_elecMeterRecvCount = 0;
    }
    
    if(g_elecMeterRecvCount == 1)
    {
        if(byte != 0x68)
        {
            g_elecMeterRecvCount = 0;
        }
    }
    else if(g_elecMeterRecvCount == 8)
    {
        if(byte != 0x68)
        {
            g_elecMeterRecvCount = 0;
        }
    }
    else if(g_elecMeterRecvCount == 10)
    {
        dataLen = byte;
    }
    else if(g_elecMeterRecvCount == 12 + dataLen)
    {
#if 0
        uint8_t i;
        SysLog("");
        for(i = 0; i < g_elecMeterRecvCount; i++)
        {
            SysPrintf("%02x ", g_recvBuff[i]);
        }
        SysPrintf("\n");
#endif
        sum = checkSum(g_recvBuff, sizeof(ElectricProtcol_t) + dataLen);
        if(g_recvBuff[sizeof(ElectricProtcol_t) + dataLen] == sum)
        {
            elec = (ElectricProtcol_t *)g_recvBuff;
            DDYZ747DataPrase(elec->data, elec->dataLen);
        }
        else
        {
            SysLog("check sum error!,%02x", sum);
        }
        g_elecMeterRecvCount = 0;
        dataLen = 0;
    }
}

static uint8_t DDYZ7474MeterRecvDate(const uint8_t *data, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		DDYZ7474MeterRecvByte(data[i]);
	}
	return i;
}

static uint8_t checkSum(const uint8_t *data, uint8_t len)
{
	uint8_t i;
	uint8_t sum = 0;
	
	for(i = 0; i < len; i++)
	{
		sum += data[i];
	}
	return sum;
}

static void DDYZ747DataPrase(uint8_t *data, uint8_t len)
{
	uint8_t *value;
    uint8_t i;
    uint16_t ptNum;
    int intNum;
	uint32_t powerCount; //电量
    int16_t voltage;     //电压
    int32_t current;     //电流
    int32_t instantPower; //功率
    int32_t peakPower;    //峰电能
    int32_t bottomPower;  //谷电能
    int32_t factor;


    for(i = 0; i < len; i++)
    {
        data[i] = data[i] - 0x33;
    }
	
	value = &data[4];
	if(memcmp(data, ELECREG_ACTIVE_POWER, 4) == 0)
	{
	    powerCount = (value[3] << 24) \
	                            + (value[2] << 16) \
	                            + (value[1] << 8) \
	                            + value[0];

		ptNum = (uint8_t)(powerCount & 0xff);
	    intNum = powerCount >> 8;
		//SysLog("power num:%x.%02x", intNum, ptNum);
		memset(g_ddyz747Info.powerCount, 0, METER_STR_LEN);
		sprintf(g_ddyz747Info.powerCount, "%x.%02x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_VOLTAGE, 4) == 0)
	{
		voltage = (value[1] << 8) + value[0];
        
        ptNum = (uint8_t)(voltage & 0x000f);
        intNum = voltage >> 4;
        //SysLog("voltage:%x.%01x", intNum, ptNum);
		memset(g_ddyz747Info.voltage, 0, METER_STR_LEN);
		sprintf(g_ddyz747Info.voltage, "%x.%01x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_CURRENT, 4) == 0)
	{
		current = (value[2] << 16) \
                            + (value[1] << 8) \
                            + value[0];

        ptNum = (uint16_t)(current & 0xfff);
        intNum = current >> 12;
        //SysLog("current:%x.%03x", intNum, ptNum);
		memset(g_ddyz747Info.current, 0, METER_STR_LEN);
		sprintf(g_ddyz747Info.current, "%x.%03x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_INST_POWER, 4) == 0)
	{
		instantPower = (value[2] << 16) \
	                            + (value[1] << 8) \
	                            + value[0];
        
        ptNum = (uint16_t)(instantPower & 0xffff);
        intNum = instantPower >> 16;
        //SysLog("instant power:%x.%04x", intNum, ptNum);
		memset(g_ddyz747Info.instantPower, 0, METER_STR_LEN);
		sprintf(g_ddyz747Info.instantPower, "%x.%04x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_PEAK_POWER, 4) == 0)
	{
		peakPower = (value[3] << 24) \
	                            + (value[2] << 16) \
	                            + (value[1] << 8) \
	                            + value[0];
        
        ptNum = (uint8_t)(peakPower & 0xff);
        intNum = peakPower >> 8; 
        //SysLog("peak power: %x.%02x", intNum, ptNum);
		memset(g_ddyz747Info.peakPower, 0, METER_STR_LEN);
		sprintf(g_ddyz747Info.peakPower, "%x.%02x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_BOTTOM_POWER, 4) == 0)
	{
		bottomPower = (value[3] << 24) \
	                            + (value[2] << 16) \
	                            + (value[1] << 8) \
	                            + value[0];
        
        ptNum = (uint8_t)(bottomPower & 0xff);
        intNum = bottomPower >> 8; 
        //SysLog("bottom power: %x.%02x", intNum, ptNum);
		memset(g_ddyz747Info.bottomPower, 0, METER_STR_LEN);
		sprintf(g_ddyz747Info.bottomPower, "%x.%02x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_FACTOR, 4) == 0)
	{
		factor= (value[1] << 8) \
                            + value[0];
		ptNum = (uint16_t)(factor & 0xfff);
        intNum = factor >> 12; 
        //SysLog("factor: %x.%03x", intNum, ptNum);
		memset(g_ddyz747Info.factor, 0, METER_STR_LEN);
		sprintf(g_ddyz747Info.factor, "%x.%03x", intNum, ptNum);
	}

	g_meterWorkAbnormal = 0;
	g_ddyz747Info.e_meterWork = 1;
}

