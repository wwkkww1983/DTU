#include "Hal.h"
#include "userSys.h"

#define DAS_FLOWMETER_MODBUS_ADDR 0x01

static F_MeterInfo_t g_fldInfo = {0};
static uint8_t g_meterWorkAbnormal = 0;

static uint8_t FLDMeterRecvDate(const uint8_t *data, uint32_t len);
static uint8_t FLDMeterDataPrase(ModbusReplyProtcol_t *reply);
static void transTempToText(int16_t temp, char *text);


void FLD_R_LSRInit(void)
{
	memset(&g_fldInfo, 0, sizeof(g_fldInfo));
	HalUartConfig(METER_UART, 9600 ,PARITY_NO, FLDMeterRecvDate);
	ModbusCtrlInit(METER_CTRL_PORT,METER_CTRL_PIN);
}

uint8_t FLD_R_LSRQuery(void)
{
	HalUartConfig(METER_UART, 9600 ,PARITY_NO, FLDMeterRecvDate);
	
	ModbusCtrlSetMode(METER_CTRL_PORT, METER_CTRL_PIN, 1);
	modbusReadRegs(METER_UART, DAS_FLOWMETER_MODBUS_ADDR, 0x04, 0x1010, 36);
	ModbusCtrlSetMode(METER_CTRL_PORT, METER_CTRL_PIN, 0);

	//5次未响应，工作异常
	g_meterWorkAbnormal++;
	if(g_meterWorkAbnormal >= 5)
	{
		SysLog("FLD_R_LSR Flow Meter work abnormal!");
		g_fldInfo.f_meterWork = 0;
		g_meterWorkAbnormal = 0;
	}

	return 0;
}

F_MeterInfo_t *FLD_R_LSRGetInfo(void)
{
	return &g_fldInfo;
}

static uint8_t FLDMeterRecvDate(const uint8_t *data, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		modbusRecvHandle(data[i], DAS_FLOWMETER_MODBUS_ADDR, FLDMeterDataPrase);
		SysPrintf("[%02x]", data[i]);
	}
	return i;
}

static uint8_t FLDMeterDataPrase(ModbusReplyProtcol_t *reply)
{ 
	int32_t inputTemp;
    int32_t outputTemp;
	int32_t flowCount;
    //int32_t flowSpeed;
    int32_t instantHeat;
    int32_t accHeatInt;
    int32_t accHeatDec;
	char checkStr[METER_STR_LEN] = {0};
    if(reply->addr == DAS_FLOWMETER_MODBUS_ADDR)
    {
        flowCount = exchangeValue(&reply->data[0], NORMAL);
        //flowSpeed = exchangeValue(&reply->data[4], NORMAL);
		instantHeat = exchangeValue(&reply->data[44], NORMAL);
		
		accHeatInt = exchangeValue(&reply->data[48], NORMAL);
		accHeatDec = exchangeValue(&reply->data[52], NORMAL);

        inputTemp = reply->data[56];
        inputTemp = (inputTemp << 8) + reply->data[57];
        
        outputTemp = reply->data[58];
        outputTemp = (outputTemp << 8) + reply->data[59];


		memset(checkStr, 0, METER_STR_LEN);
		sprintf(checkStr, "%.4f", intTofloat(instantHeat));
		if(strlen(checkStr) < METER_STR_LEN) //容错，有时会有超长数据
		{
			memset(g_fldInfo.instantHeat, 0, METER_STR_LEN);
			strcpy(g_fldInfo.instantHeat, checkStr);
			SysLog("FLD instantHeat: %s", g_fldInfo.instantHeat);
		}

		
		{
			char point[10] = {0};
			memset(checkStr, 0, METER_STR_LEN);
			sprintf(point, "%.3f", intTofloat(accHeatDec));
			sprintf(checkStr, "%d%s", accHeatInt, &point[1]);

			if(strlen(checkStr) < METER_STR_LEN) //容错，有时会有超长数据
			{
				memset(g_fldInfo.accHeatDec, 0, METER_STR_LEN);
				strcpy(g_fldInfo.accHeatDec, checkStr);
				SysLog("FLD accHeatDec: %s", g_fldInfo.accHeatDec);
			}
			
		}	
		
		memset(checkStr, 0, METER_STR_LEN);
		sprintf(checkStr, "%.4f", intTofloat(flowCount));
		if(strlen(checkStr) < METER_STR_LEN) //容错，有时会有超长数据
		{
			memset(g_fldInfo.flowCount, 0, METER_STR_LEN);
			strcpy(g_fldInfo.flowCount, checkStr);
			SysLog("FLD flowCount: %s", g_fldInfo.flowCount);
		}
		
		
		memset(checkStr, 0, METER_STR_LEN);
		transTempToText(inputTemp, checkStr);
		if(strlen(checkStr) < METER_STR_LEN) //容错，有时会有超长数据
		{
			memset(g_fldInfo.inputTemp, 0, METER_STR_LEN);
			strcpy(g_fldInfo.inputTemp, checkStr);
			SysLog("FLD inputTemp: %s", g_fldInfo.inputTemp);
		}
		
		memset(checkStr, 0, METER_STR_LEN);
		transTempToText(outputTemp, checkStr);
		if(strlen(checkStr) < METER_STR_LEN) //容错，有时会有超长数据
		{
			memset(g_fldInfo.outputTemp, 0, METER_STR_LEN);
			strcpy(g_fldInfo.outputTemp, checkStr);
			SysLog("FLD outputTemp: %s", g_fldInfo.outputTemp);
		}

		g_meterWorkAbnormal = 0;
		g_fldInfo.f_meterWork = 1;
	}
	return 1;
}

//int16强转为float偶尔会出现bug,原因未知
//手动拼字符串
static void transTempToText(int16_t temp, char *text)
{
	uint8_t len = 0;
	//uint8_t i = 0;
	char tempStr[8] = {0};
	if(temp >= 0)
	{
		sprintf(tempStr,"%d",temp);
		len  = strlen(tempStr);
		if(len <= 1)
		{
			sprintf(text,"0.%d",temp);
		}
		else
		{
			tempStr[len] = tempStr[len -1];
			tempStr[len -1] = '.';
			strcpy(text, tempStr);
		}
	}
	else
	{
		sprintf(tempStr,"%d",temp);
		len  = strlen(tempStr);
		if(len == 2)
		{
			tempStr[3] = tempStr[1];
			tempStr[2] = '.';
			tempStr[1] = '0';
			strcpy(text, tempStr);
			
		}
		else
		{
			tempStr[len] = tempStr[len -1];
			tempStr[len -1] = '.';
			strcpy(text, tempStr);
		}
	}
}
