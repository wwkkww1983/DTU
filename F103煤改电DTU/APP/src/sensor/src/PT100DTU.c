#include "Hal.h"
#include "userSys.h"

#define PT100DTU_ADDR 0x01

static uint8_t g_ptRecvBuff[64];
static uint8_t g_ptRecvCount = 0;
static uint8_t g_pt100WorkAbnormal = 0;
static PT100DTUInfo_t g_pt100DTUInfo = {0};

static void queryPT100DTUInfo(void *pt);
static uint8_t PT100DTURecvDate(const uint8_t *data, uint32_t len);
static void pt100modbusRecvHandle(uint8_t byte);
static uint8_t PT100DataPrase(ModbusReplyProtcol_t *reply);
static void transTempToText(int16_t temp, char *text);


void HalPT100DTUInit(void)
{
	HalUartConfig(PT100_UART, 9600, PARITY_NO, PT100DTURecvDate);
	ModbusCtrlInit(PT100_CTRL_PORT, PT100_CTRL_PIN);
	SysTimerSet(queryPT100DTUInfo, 5000, SYS_TIMER_REPEAT, NULL);
}

void HalPT100DTUPoll(void)
{

}

PT100DTUInfo_t* HalGetPT100DTUInfo(void)
{
	return &g_pt100DTUInfo;
}

void EchoPT100DTUInfo(void)
{
	SysPrintf("\nPT100DTU Info:   \n");
	SysPrintf("PT100DTU Status:  %s\n",   g_pt100DTUInfo.pt100DTUWork? "work":"abnormal");
	SysPrintf("    PT1:          %s  \n", (strlen(g_pt100DTUInfo.pt1) && g_pt100DTUInfo.pt100DTUWork) ? g_pt100DTUInfo.pt1 : "--");
	SysPrintf("    PT2:          %s  \n", (strlen(g_pt100DTUInfo.pt2) && g_pt100DTUInfo.pt100DTUWork) ? g_pt100DTUInfo.pt2 : "--");
	SysPrintf("    PT3:          %s  \n", (strlen(g_pt100DTUInfo.pt3) && g_pt100DTUInfo.pt100DTUWork) ? g_pt100DTUInfo.pt3 : "--");
	SysPrintf("    PT4:          %s  \n", (strlen(g_pt100DTUInfo.pt4) && g_pt100DTUInfo.pt100DTUWork)? g_pt100DTUInfo.pt4 : "--");
	SysPrintf("    PT5OUT:       %s  \n", (strlen(g_pt100DTUInfo.pt5Out) && g_pt100DTUInfo.pt100DTUWork) ? g_pt100DTUInfo.pt5Out : "--");

	SysPrintf("\n");
}

static void queryPT100DTUInfo(void *pt)
{
	if(SysTimeHasRun(2000))
	{
		ModbusCtrlSetMode(PT100_CTRL_PORT, PT100_CTRL_PIN, 1);
	    modbusReadRegs(PT100_UART, 0x00, 0x03, 40,8);
		ModbusCtrlSetMode(PT100_CTRL_PORT, PT100_CTRL_PIN, 0);

		g_pt100WorkAbnormal++;
		if(g_pt100WorkAbnormal >= 5)
		{
			g_pt100DTUInfo.pt100DTUWork= 0;
			g_pt100WorkAbnormal = 0;
		}
	}
}

static uint8_t PT100DTURecvDate(const uint8_t *data, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		pt100modbusRecvHandle(data[i]);
		SysPrintf("[%02x]", data[i]);
	}
	return i;
}

static void pt100modbusRecvHandle(uint8_t byte)
{
    static uint8_t dataLen;
    uint16_t crc;

    g_ptRecvBuff[g_ptRecvCount++] = byte;
    
    if(g_ptRecvCount >= sizeof(g_ptRecvBuff))
    {
        g_ptRecvCount = 0;
    }

    if(g_ptRecvCount == 1)
    {
        if(byte != PT100DTU_ADDR)
        {
            g_ptRecvCount = 0;
        }
    }
    else if(g_ptRecvCount == 3)
    {
        dataLen = byte;
    }
    else if(g_ptRecvCount == dataLen + 5)
    {
#if 1
        uint8_t i;
        SysLog("PT100 recv:");
        for(i = 0; i < g_ptRecvCount; i++)
        {
            SysPrintf("%02x ", g_ptRecvBuff[i]);
        }
        SysPrintf("\n");
#endif
        crc = byte;
        crc = (crc << 8) + g_ptRecvBuff[g_ptRecvCount - 2];
        if(crc == crc16(g_ptRecvBuff, g_ptRecvCount - 2))
        {
            PT100DataPrase((ModbusReplyProtcol_t *)g_ptRecvBuff);
        }
        else
        {
            SysLog("crc16 error!");
        }
        memset(g_ptRecvBuff, 0, sizeof(g_ptRecvBuff));
        g_ptRecvCount = 0;
        dataLen = 0;
    }
}

static uint8_t PT100DataPrase(ModbusReplyProtcol_t *reply)
{ 	
	int16_t ptTemp = 0;
	int16_t ptTempNUll = 0;

	ptTempNUll = (0xf8<<8)|0x00;
	if(reply->addr == PT100DTU_ADDR)
	{
		memset(&g_pt100DTUInfo, 0, sizeof(PT100DTUInfo_t));
		
		ptTemp  = reply->data[0];
		ptTemp  = (ptTemp << 8) + reply->data[1];
		
		if(ptTemp != ptTempNUll)
		{
			transTempToText(ptTemp, g_pt100DTUInfo.pt1);
			SysLog("pt1: %s", g_pt100DTUInfo.pt1);
		}
		
		ptTemp  = reply->data[2] <<8;
		ptTemp |= reply->data[3];
		if(ptTemp != ptTempNUll)
		{
			transTempToText(ptTemp, g_pt100DTUInfo.pt2);
			SysLog("pt2: %s", g_pt100DTUInfo.pt2);
		}

		ptTemp  = reply->data[4] <<8;
		ptTemp |= reply->data[5];
		if(ptTemp != ptTempNUll)
		{
			transTempToText(ptTemp, g_pt100DTUInfo.pt3);
			SysLog("pt3: %s", g_pt100DTUInfo.pt3);

		}

		ptTemp  = reply->data[6] <<8;
		ptTemp |= reply->data[7];
		if(ptTemp != ptTempNUll)
		{
			transTempToText(ptTemp, g_pt100DTUInfo.pt4);
			SysLog("pt4: %s", g_pt100DTUInfo.pt4);

		}

		ptTemp  = reply->data[8] <<8;
		ptTemp |= reply->data[9];
		if(ptTemp != ptTempNUll)
		{
			transTempToText(ptTemp, g_pt100DTUInfo.pt5Out);
			SysLog("pt5: %s", g_pt100DTUInfo.pt5Out);
		}

		g_pt100WorkAbnormal = 0;
		g_pt100DTUInfo.pt100DTUWork = 1;
		
		return 1;
	}


	return 0;
}

//int16强转为float偶尔会出现bug,原因未知
//手动拼字符串
static void transTempToText(int16_t temp, char *text)
{
	uint8_t len = 0;
	//uint8_t i = 0;
	char tempStr[8] = {0};
	if(temp >= 0)
	{
		sprintf(tempStr,"%d",temp);
		len  = strlen(tempStr);
		if(len <= 1)
		{
			sprintf(text,"0.%d",temp);
		}
		else
		{
			tempStr[len] = tempStr[len -1];
			tempStr[len -1] = '.';
			strcpy(text, tempStr);
		}
	}
	else
	{
		sprintf(tempStr,"%d",temp);
		len  = strlen(tempStr);
		if(len == 2)
		{
			tempStr[3] = tempStr[1];
			tempStr[2] = '.';
			tempStr[1] = '0';
			strcpy(text, tempStr);
			
		}
		else
		{
			tempStr[len] = tempStr[len -1];
			tempStr[len -1] = '.';
			strcpy(text, tempStr);
		}
	}
}
