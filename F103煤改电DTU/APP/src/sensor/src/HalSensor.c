#include "Hal.h"
#include "userSys.h"

static SensorInfo_t g_sensorInfo;

static void echo_sensor_info(void *pt);

void HalSensorInit(void)
{
	/* PM2.5 CO2 by uart */
	//HalPM25CO2Init();  
	/* sht20  io simulate I2C*/
	//HalSHT20Init();
	/*e-meter f-meter*/
	HalMeterInit();
	HalPT100DTUInit();

    SysTimerSet(echo_sensor_info, 10000, SYS_TIMER_REPEAT, NULL);
}

void HalSensorPoll(void)
{
	//HalPM25CO2Poll();
	//HalSHT20Poll();
	HalMeterPoll();
	HalPT100DTUPoll();
}

SensorInfo_t *HalGetSensorInfo(void)
{
	//g_sensorInfo.pm25Co2Info = HalGetPM25CO2Info();
	//g_sensorInfo.sht20Info   = HalGetSHT20Info();
	g_sensorInfo.meterInfo     = HalGetMeterInfo();
	g_sensorInfo.pt100DTUInfo  = HalGetPT100DTUInfo();
	return &g_sensorInfo;
}

static void echo_sensor_info(void *pt)
{	
	EchoMeterInfo();
	EchoPT100DTUInfo();
}

