#include "Hal.h"
#include "userSys.h"

static MeterInfo_t g_meterInfo = {0};
static SysTime_t g_lastMeterQuery;
static char *g_eMeterStr[E_METER_NUM] = {"无", "DDZY747", "ZWD433B", "DTSF667"};
static char *g_fMeterStr[F_METER_NUM] = {"无", "FLD_R_LSH", "HandHeld Type1"};
static E_MeterReg_t g_eMeterReg[E_METER_NUM] = {0};
static F_MeterReg_t g_fMeterReg[F_METER_NUM] = {0};
static MenuconfigInfo_t *g_config = NULL;

static void ElecMeterInit(void);
static void FlowMeterInit(void);
static uint8_t ElecMeterQuery(void);
static uint8_t FlowMeterQuery(void);
static uint8_t ElecMeter2Query(void);


//TODO  chenlh 考虑加上电表自动检测
void HalMeterInit(void)
{
	g_config = GetConfig();
	memset(&g_meterInfo, 0, sizeof(MeterInfo_t));
	
	//E-meter reg
	HalElecMeterRegister(E_METER_DDY747, DDYZ747Init, DDYZ747Query, DDYZ747GetInfo);
	HalElecMeterRegister(E_METER_ZWD433B, ZWD433BInit, ZWD433BQuery, ZWD433BGetInfo);
	HalElecMeterRegister(E_METER_DTSF667, DTSF667Init, DTSF667Query, DTSF6677GetInfo);

	//F-meter reg
	HalFlowMeterRegister(F_METER_FLD_R_LSH, FLD_R_LSRInit, FLD_R_LSRQuery, FLD_R_LSRGetInfo);
	HalFlowMeterRegister(F_METER_HANDHELD_UNKNOWN_TYPE1, HandHeld1Init, HandHeld1Query, HandHeld1GetInfo);
	
	ElecMeterInit();
	FlowMeterInit();
}

static uint8_t meterQuery = 0;
void HalMeterPoll(void)
{
	//Query ever Meter once
	if(SysTimeHasPast(g_lastMeterQuery, (1 * 1000ul)))
	{
		if(meterQuery == 0)
		{
			if(ElecMeterQuery() == 0)
			{
				meterQuery = 1;
			}
		}
		else if(meterQuery == 1)
		{
			if(FlowMeterQuery() == 0)
			{
				meterQuery = 2;
			}
		}
		else if(meterQuery == 2)
		{
			if(ElecMeter2Query() == 0)
			{
				meterQuery = 0;
			}
		}
		g_lastMeterQuery = SysTime();
	}
}

MeterInfo_t *HalGetMeterInfo(void)
{
	return &g_meterInfo;
}

uint8_t HalElecMeterRegister(uint8_t meterNO, MeterInit_t init, MeterQuery_t query, E_MeterGetInfo_t getInfo)
{
	if(meterNO >= E_METER_NUM)
	{
		return 0;
	}
	g_eMeterReg[meterNO].regStatus = 1;
	g_eMeterReg[meterNO].init = init;
	g_eMeterReg[meterNO].query = query;
	g_eMeterReg[meterNO].getInfo = getInfo;
	return 1;
}

uint8_t HalFlowMeterRegister(uint8_t meterNO, MeterInit_t init, MeterQuery_t query, F_MeterGetInfo_t getInfo)
{
	if(meterNO >= F_METER_NUM)
	{
		return 0;
	}
	g_fMeterReg[meterNO].regStatus = 1;
	g_fMeterReg[meterNO].init = init;
	g_fMeterReg[meterNO].query = query;
	g_fMeterReg[meterNO].getInfo = getInfo;
	return 1;
}

static void ElecMeterInit(void)
{
	LOOP(x, E_METER_NUM)
	{
		if(g_eMeterReg[VAR(x)].regStatus == 1)
		{
			g_eMeterReg[VAR(x)].init();
		}
	}
}

static void FlowMeterInit(void)
{
	LOOP(x, F_METER_NUM)
	{
		if(g_fMeterReg[VAR(x)].regStatus == 1)
		{
			g_fMeterReg[VAR(x)].init();
		}
	}
}

static uint8_t ElecMeterQuery(void)
{
	if(g_config->eMeterType == E_METER_NULL)
	{
		g_meterInfo.e_MeterInfo = NULL;
		return 0;
	}
	if(g_config->eMeterType >= E_METER_COUNT)
	{
		g_meterInfo.e_MeterInfo = NULL;
		SysLog("elecMeter1 config error!");
		return 0;
	}

	if(g_eMeterReg[g_config->eMeterType].regStatus == 0)
	{
		g_meterInfo.e_MeterInfo = NULL;
		SysLog("%s not registe", GetElecMeterType(g_config->eMeterType));
		return 0;
	}
	g_meterInfo.e_MeterInfo = g_eMeterReg[g_config->eMeterType].getInfo();
	return g_eMeterReg[g_config->eMeterType].query();
}

static uint8_t ElecMeter2Query(void)
{
	if(g_config->eMeterType2 == E_METER_NULL)
	{
		g_meterInfo.e_MeterInfo2 = NULL;
		return 0;
	}
	
	if(g_config->eMeterType2 >= E_METER_COUNT)
	{
		g_meterInfo.e_MeterInfo2 = NULL;
		SysLog("elecMeter2 config error!");
		return 0;
	}

	if(g_eMeterReg[g_config->eMeterType2].regStatus == 0)
	{
		g_meterInfo.e_MeterInfo2 = NULL;
		SysLog("%s not registe", GetElecMeterType(g_config->eMeterType2));
		return 0;
	}

	g_meterInfo.e_MeterInfo2 = g_eMeterReg[g_config->eMeterType2].getInfo();
	return g_eMeterReg[g_config->eMeterType2].query();
}

static uint8_t FlowMeterQuery(void)
{
	if(g_config->fMeterType == F_METER_NULL)
	{
		g_meterInfo.f_MeterInfo = NULL;
		return 0;
	}
	
	if(g_config->fMeterType >= F_METER_COUNT)
	{
		g_meterInfo.f_MeterInfo = NULL;
		SysLog("flowcMeter config error!");
		return 0;
	}

	if(g_fMeterReg[g_config->fMeterType].regStatus == 0)
	{
		g_meterInfo.f_MeterInfo = NULL;
		SysLog("%s not registe", GetFlowMeterType(g_config->fMeterType));
		return 0;
	}
	g_meterInfo.f_MeterInfo = g_fMeterReg[g_config->fMeterType].getInfo();
	return g_fMeterReg[g_config->fMeterType].query();
}

char *GetElecMeterType(uint8_t meterIndex)
{
	if(meterIndex >= E_METER_COUNT)
	{
		return "";
	}
	return g_eMeterStr[meterIndex];
}

char *GetFlowMeterType(uint8_t meterIndex)
{
	if(meterIndex >= F_METER_COUNT)
	{
		return "";
	}
	return g_fMeterStr[meterIndex];
}

void EchoMeterInfo(void)
{
	E_MeterInfo_t *e_MeterInfo1 = g_meterInfo.e_MeterInfo;
	E_MeterInfo_t *e_MeterInfo2 = g_meterInfo.e_MeterInfo2;
	F_MeterInfo_t *f_MeterInfo = g_meterInfo.f_MeterInfo;
	SysPrintf("\nElec Meter1 Type:    %s\n", GetElecMeterType(g_config->eMeterType));
	if(g_config->eMeterType != E_METER_NULL)
	{
		SysPrintf("Elec Meter Status:  %s\n", e_MeterInfo1->e_meterWork? "work":"abnormal");
		SysPrintf("    voltage:        %s\n", e_MeterInfo1->e_meterWork?e_MeterInfo1->voltage:"--");
		SysPrintf("    current:        %s\n", e_MeterInfo1->e_meterWork?e_MeterInfo1->current:"--");
		SysPrintf("    instantPower:   %s\n", e_MeterInfo1->e_meterWork?e_MeterInfo1->instantPower:"--");
		SysPrintf("    powerCount:     %s\n", e_MeterInfo1->e_meterWork?e_MeterInfo1->powerCount:"--");
		SysPrintf("    factor:         %s\n", e_MeterInfo1->e_meterWork?e_MeterInfo1->factor:"--"); //部分表无峰电谷电量
		SysPrintf("    bottomPower:    %s\n", (e_MeterInfo1->e_meterWork && strlen(e_MeterInfo1->bottomPower))? e_MeterInfo1->bottomPower : "--");
		SysPrintf("    peakPower:      %s\n", (e_MeterInfo1->e_meterWork && strlen(e_MeterInfo1->peakPower))? e_MeterInfo1->peakPower : "--");
	}
	SysPrintf("\n");
	
	SysPrintf("\nElec Meter2 Type:    %s\n", GetElecMeterType(g_config->eMeterType2));
	if(g_config->eMeterType2 != E_METER_NULL)
	{
		SysPrintf("Elec Meter Status:  %s\n", e_MeterInfo2->e_meterWork? "work":"abnormal");
		SysPrintf("    voltage:        %s\n", e_MeterInfo2->e_meterWork?e_MeterInfo2->voltage:"--");
		SysPrintf("    current:        %s\n", e_MeterInfo2->e_meterWork?e_MeterInfo2->current:"--");
		SysPrintf("    instantPower:   %s\n", e_MeterInfo2->e_meterWork?e_MeterInfo2->instantPower:"--");
		SysPrintf("    powerCount:     %s\n", e_MeterInfo2->e_meterWork?e_MeterInfo2->powerCount:"--");
		SysPrintf("    factor:         %s\n", e_MeterInfo2->e_meterWork?e_MeterInfo2->factor:"--"); //部分表无峰电谷电量
		SysPrintf("    bottomPower:    %s\n", (e_MeterInfo2->e_meterWork && strlen(e_MeterInfo2->bottomPower))? e_MeterInfo2->bottomPower : "--");
		SysPrintf("    peakPower:      %s\n", (e_MeterInfo2->e_meterWork && strlen(e_MeterInfo2->peakPower))? e_MeterInfo2->peakPower : "--");
	}
		SysPrintf("\n");


	SysPrintf("\nFlow Meter Type:    %s\n", GetFlowMeterType(g_config->fMeterType));
	if(g_config->fMeterType!= F_METER_NULL)
	{
		SysPrintf("Flow Meter Status:  %s\n", f_MeterInfo->f_meterWork? "work":"abnormal");
		SysPrintf("    instantHeat:    %s\n", f_MeterInfo->f_meterWork?f_MeterInfo->instantHeat:"--");
		SysPrintf("    accHeatDec:     %s\n", f_MeterInfo->f_meterWork?f_MeterInfo->accHeatDec:"--");
		SysPrintf("    flowCount:      %s\n", f_MeterInfo->f_meterWork?f_MeterInfo->flowCount:"--");
		SysPrintf("    inputTemp:      %s\n", f_MeterInfo->f_meterWork?f_MeterInfo->inputTemp:"--");
		SysPrintf("    outputTemp:     %s\n", f_MeterInfo->f_meterWork?f_MeterInfo->outputTemp:"--");
	}
	SysPrintf("\n");
}




