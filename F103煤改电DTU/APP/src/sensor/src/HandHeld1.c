#include "Hal.h"
#include "userSys.h"

#define HAND1_FLOWMETER_MODBUS_ADDR 0x01

static F_MeterInfo_t g_handInfo = {0};
static uint8_t g_meterWorkAbnormal = 0;

static uint8_t HandHeld1RecvDate(const uint8_t *data, uint32_t len);
static uint8_t HandHeld1DataPrase(ModbusReplyProtcol_t *reply);

void HandHeld1Init(void)
{
	memset(&g_handInfo, 0, sizeof(g_handInfo));
	HalUartConfig(METER_UART, 9600 ,PARITY_NO, HandHeld1RecvDate);
	ModbusCtrlInit(METER_CTRL_PORT,METER_CTRL_PIN);
}

uint8_t HandHeld1Query(void)
{
	HalUartConfig(METER_UART, 9600 ,PARITY_NO, HandHeld1RecvDate);
	
	ModbusCtrlSetMode(METER_CTRL_PORT, METER_CTRL_PIN, 1);
	modbusReadRegs(METER_UART, HAND1_FLOWMETER_MODBUS_ADDR, 0x03, 0x0000, 36);
	ModbusCtrlSetMode(METER_CTRL_PORT, METER_CTRL_PIN, 0);

	//10次未响应，工作异常
	g_meterWorkAbnormal++;
	if(g_meterWorkAbnormal >= 5)
	{
		SysLog("HandHeld1 Flow Meter work abnormal!");
		g_handInfo.f_meterWork = 0;
		g_meterWorkAbnormal = 0;
	}

	return 0;
}

F_MeterInfo_t *HandHeld1GetInfo(void)
{
	return &g_handInfo;
}


static uint8_t HandHeld1RecvDate(const uint8_t *data, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		modbusRecvHandle(data[i], HAND1_FLOWMETER_MODBUS_ADDR, HandHeld1DataPrase);
	}
	return i;
}

static uint8_t HandHeld1DataPrase(ModbusReplyProtcol_t *reply)
{ 
	int32_t inputTemp;
    int32_t outputTemp;
	int32_t flowCount;
    //int32_t flowSpeed;
    int32_t instantHeat;
    int32_t accHeatInt;
    int32_t accHeatDec; 
    if(reply->addr == HAND1_FLOWMETER_MODBUS_ADDR)
    {
        flowCount = exchangeValue(&reply->data[0], PERTICUR);
        //flowSpeed = exchangeValue(&reply->data[8], PERTICUR);
		instantHeat = exchangeValue(&reply->data[4], PERTICUR);
		
		accHeatInt = exchangeValue(&reply->data[32], NORMAL);
		accHeatDec = exchangeValue(&reply->data[36], PERTICUR);

        inputTemp = exchangeValue(&reply->data[64], PERTICUR);
        
        outputTemp = exchangeValue(&reply->data[68], PERTICUR);


		memset(g_handInfo.instantHeat, 0, METER_STR_LEN);
		sprintf(g_handInfo.instantHeat, "%.4f", intTofloat(instantHeat));
		SysLog("handheld instantHeat: %s", g_handInfo.instantHeat);
		
		{
			char point[10] = {0};
			memset(g_handInfo.accHeatDec, 0, METER_STR_LEN);
			sprintf(point, "%.3f", intTofloat(accHeatDec));
			sprintf(g_handInfo.accHeatDec, "%d%s", accHeatInt, &point[1]);
			SysLog("handheld accHeatDec: %s", g_handInfo.accHeatDec);
		}	
		
		memset(g_handInfo.flowCount, 0, METER_STR_LEN);
		sprintf(g_handInfo.flowCount, "%.4f", intTofloat(flowCount));
		SysLog("handheld flowCount: %s", g_handInfo.flowCount);
		
		memset(g_handInfo.inputTemp, 0, METER_STR_LEN);
		sprintf(g_handInfo.inputTemp, "%.1f", intTofloat(inputTemp));
		SysLog("handheld inputTemp: %s", g_handInfo.inputTemp);
		
		memset(g_handInfo.outputTemp, 0, METER_STR_LEN);
		sprintf(g_handInfo.outputTemp, "%.1f", intTofloat(outputTemp));
		SysLog("handheld outputTemp: %s", g_handInfo.outputTemp);

		g_meterWorkAbnormal = 0;
		g_handInfo.f_meterWork = 1;
	}
	return 1;
}

