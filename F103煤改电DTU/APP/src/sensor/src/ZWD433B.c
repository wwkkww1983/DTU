#include "Hal.h"
#include "userSys.h"

#define ZWD433B_MODBUS_ADDR 0x02
#define ZWD433B_MODBUS_ADDR2 0x03
#define ZWD433B_MODBUS_ADDR3 0x04

static E_MeterInfo_t  g_ZWD433BInfo = {0};
static uint8_t g_ZWD433BWorkAbnormal = 0;

static uint8_t ZWD433BRecvDate(const uint8_t *data, uint32_t len);
static uint8_t ZWD433BDataPrase(ModbusReplyProtcol_t *reply);

void ZWD433BInit(void)
{
	memset(&g_ZWD433BInfo, 0, sizeof(g_ZWD433BInfo));
	HalUartConfig(METER_UART, 9600 ,PARITY_NO, ZWD433BRecvDate);
	ModbusCtrlInit(METER_CTRL_PORT,METER_CTRL_PIN);
}

uint8_t ZWD433BQuery(void)
{
	static uint8_t processID = 0;
    int ret = 0;
	
	HalUartConfig(METER_UART, 9600 ,PARITY_NO, ZWD433BRecvDate);
	
	ModbusCtrlSetMode(METER_CTRL_PORT, METER_CTRL_PIN, 1);
    if(processID == 0)// start config
    {
        modbusWriteRegs(METER_UART, ZWD433B_MODBUS_ADDR, 0x10, 0x1F02, 2, 4, 0x42a00000);
        ret = 1;
    }
    else if(processID == 1)
    {
        modbusWriteRegs(METER_UART, ZWD433B_MODBUS_ADDR, 0x10, 0x1F55, 1, 2, 0x0001);
        ret = 1;
    }
	else if(processID == 2)
	{
		modbusWriteRegs(METER_UART, ZWD433B_MODBUS_ADDR2, 0x10, 0x1F02, 2, 4, 0x42a00000);
        ret = 1;
	}
	else if(processID == 3)
    {
        modbusWriteRegs(METER_UART, ZWD433B_MODBUS_ADDR2, 0x10, 0x1F55, 1, 2, 0x0001);
        ret = 1;
    }
	else if(processID == 4)
	{
		modbusWriteRegs(METER_UART, ZWD433B_MODBUS_ADDR3, 0x10, 0x1F02, 2, 4, 0x42a00000);
        ret = 1;
	}
	else if(processID == 5)
    {
        modbusWriteRegs(METER_UART, ZWD433B_MODBUS_ADDR3, 0x10, 0x1F55, 1, 2, 0x0001);
        ret = 1;
    }
    else if(processID == 10) //start query
    {
		modbusReadRegs(METER_UART, ZWD433B_MODBUS_ADDR, 0x03, 0x1300, 18);
		ret = 1;
    }
	else if(processID == 11)
    {
		modbusReadRegs(METER_UART, ZWD433B_MODBUS_ADDR2, 0x03, 0x1300, 18);
		ret = 1;
    }
	else if(processID == 12)
    {
		modbusReadRegs(METER_UART, ZWD433B_MODBUS_ADDR3, 0x03, 0x1300, 18);
		ret = 1;
    }
	else
	{
		
	}
    processID++;

    if(ret == 0)
    {
        processID = 10;//if 'config' end, goto 'query'
    }
	ModbusCtrlSetMode(METER_CTRL_PORT, METER_CTRL_PIN, 0);

	//40次未响应，工作异常
	g_ZWD433BWorkAbnormal++;
	if(g_ZWD433BWorkAbnormal >= 40)
	{
		SysLog("ZWD433B Elec Meter work abnormal!");
		g_ZWD433BInfo.e_meterWork = 0;
		g_ZWD433BWorkAbnormal = 0;
		processID = 0;
	}

	return ret;
}

E_MeterInfo_t *ZWD433BGetInfo(void)
{
	return &g_ZWD433BInfo;
}


static uint8_t ZWD433BRecvDate(const uint8_t *data, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		modbusRecvHandle(data[i], ZWD433B_MODBUS_ADDR, ZWD433BDataPrase);
	}
	return i;
}

static uint8_t ZWD433BDataPrase(ModbusReplyProtcol_t *reply)
{ 
	int32_t powerCount;
    int32_t voltage;
    int32_t current;
    int32_t instantPower;
    int32_t factor;
    if(reply->addr == ZWD433B_MODBUS_ADDR
		|| reply->addr == ZWD433B_MODBUS_ADDR2
		|| reply->addr == ZWD433B_MODBUS_ADDR3)
    {
    	
		//青智平均电压
        voltage = exchangeValue(&reply->data[0], NORMAL);
		memset(g_ZWD433BInfo.voltage, 0, METER_STR_LEN);
		sprintf(g_ZWD433BInfo.voltage, "%.1f", intTofloat(voltage));
		SysLog("ZWD433B voltage: %s", g_ZWD433BInfo.voltage);

        //青智平均电流
        current = exchangeValue(&reply->data[4], NORMAL);
		memset(g_ZWD433BInfo.current, 0, METER_STR_LEN);
        sprintf(g_ZWD433BInfo.current, "%.3f", intTofloat(current));
		SysLog("ZWD433B current: %s", g_ZWD433BInfo.current);
        
        //青智总功率
        instantPower = exchangeValue(&reply->data[8], NORMAL);
		memset(g_ZWD433BInfo.instantPower, 0, METER_STR_LEN);
		sprintf(g_ZWD433BInfo.instantPower, "%.3f", intTofloat(instantPower) / 1000);
		SysLog("ZWD433B instantPower: %s", g_ZWD433BInfo.instantPower);
        
        //青智总电能
        powerCount = exchangeValue(&reply->data[28], NORMAL);
		memset(g_ZWD433BInfo.powerCount, 0, METER_STR_LEN);
		sprintf(g_ZWD433BInfo.powerCount, "%.2f", intTofloat(powerCount)  / 1000);
		SysLog("ZWD433B powerCount: %s", g_ZWD433BInfo.powerCount);
        
        //合计功率因数
        factor = exchangeValue(&reply->data[12], NORMAL);
		memset(g_ZWD433BInfo.factor, 0, METER_STR_LEN);
		sprintf(g_ZWD433BInfo.factor, "%.3f", intTofloat(factor));
		SysLog("ZWD433B factor: %s", g_ZWD433BInfo.factor);

		memset(g_ZWD433BInfo.bottomPower, 0, METER_STR_LEN);
		memset(g_ZWD433BInfo.peakPower, 0, METER_STR_LEN);
		
        	
		g_ZWD433BWorkAbnormal = 0;
		g_ZWD433BInfo.e_meterWork = 1;
	}
	return 1;
}

