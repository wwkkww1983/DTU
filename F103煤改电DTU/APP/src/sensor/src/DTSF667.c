#include "Hal.h"
#include "userSys.h"

typedef struct 
{
    uint8_t head;
    uint8_t addr[6];
    uint8_t frameHead;
    uint8_t ctrl;
    uint8_t dataLen;
    uint8_t data[];
}ElectricProtcol_t;

static const uint8_t ELECREG_ACTIVE_POWER[2] = {0x10, 0x90}; //有功总电能
static const uint8_t ELECREG_VOLTAGE[2] = {0x11, 0xB6}; //A 相电压
static const uint8_t ELECREG_CURRENT[2] = {0x21, 0xB6}; //A 相电流
static const uint8_t ELECREG_INST_POWER[2] = {0x30, 0xB6}; //瞬时总有功功率
static const uint8_t ELECREG_PEAK_POWER[2] = {0x12, 0x90}; //正向有功尖电能
static const uint8_t ELECREG_BOTTOM_POWER[2] = {0x14, 0x90}; //正向有功谷电能
static const uint8_t ELECREG_FACTOR[2] = {0x50, 0xB6}; 

static uint8_t g_recvBuff[64];
static uint8_t g_elecMeterRecvCount = 0;
static E_MeterInfo_t g_DTSF667Info = {0};
static uint8_t g_meterWorkAbnormal = 0;

static void DTSF667DataPrase(uint8_t *data, uint8_t len);
static void DTSF667DataSend(uint8_t cmd, uint8_t dlen, const uint8_t *data);
static uint8_t DTSF667MeterRecvDate(const uint8_t *data, uint32_t len);
static uint8_t checkSum(const uint8_t *data, uint8_t len);
static void DTSF667DataPrase(uint8_t *data, uint8_t len);


void DTSF667Init(void)
{		
	memset(&g_DTSF667Info, 0, sizeof(g_DTSF667Info));
	HalUartConfig(METER_UART, 1200 ,PARITY_EVEN, DTSF667MeterRecvDate);
	ModbusCtrlInit(METER_CTRL_PORT,METER_CTRL_PIN);
}

uint8_t DTSF667Query(void)
{
	static uint8_t elecQueryId = 0;
    int ret = 0;

	HalUartConfig(METER_UART, 1200 ,PARITY_EVEN, DTSF667MeterRecvDate);
	
    if(elecQueryId == 0)
    {
        DTSF667DataSend(0x01, 2, ELECREG_ACTIVE_POWER);
        ret = 1;
    }
    else if(elecQueryId == 1)
    {
        DTSF667DataSend(0x01, 2, ELECREG_VOLTAGE);
        ret = 1;
    }
    else if(elecQueryId == 2)
    {
        DTSF667DataSend(0x01, 2, ELECREG_CURRENT);
        ret = 1;
    }
    else if(elecQueryId == 3)
    {
        DTSF667DataSend(0x01, 2, ELECREG_INST_POWER);
        ret = 1;
    }
	else if(elecQueryId == 4)
	{
		DTSF667DataSend(0x01, 2, ELECREG_FACTOR);
        ret = 1;
	}
    else if(elecQueryId == 5)
    {
        DTSF667DataSend(0x01, 2, ELECREG_PEAK_POWER);
        ret = 1;
    }
    else
    {
        DTSF667DataSend(0x01, 2, ELECREG_BOTTOM_POWER);
    }
    elecQueryId++;

    if(ret == 0)
    {
        elecQueryId = 0;
    }

	//10次未响应，工作异常
	g_meterWorkAbnormal++;
	if(g_meterWorkAbnormal >= 10)
	{
		SysLog("DTSF667 Elec Meter work abnormal!");
		g_meterWorkAbnormal = 0;
		g_DTSF667Info.e_meterWork = 0;
	}

	return ret;
}


E_MeterInfo_t *DTSF6677GetInfo(void)
{
	return &g_DTSF667Info;
}

static void DTSF667DataSend(uint8_t cmd, uint8_t dlen, const uint8_t *data)
{
	uint8_t i = 12;
	uint8_t j = 0;
	uint8_t sendData[64]= {0xFE, 0xFE, 0xFE, 0xFE,
                       0x68,
                       0x99, 0x99, 0x99, 0x99, 0x99, 0x99,
                       0x68};
	
	sendData[i++] = cmd;
	sendData[i++] = dlen;

	for(j = 0; j < dlen; j++)
	{
		sendData[i++] = (data[j] + 0x33);
	}

	sendData[i++] = checkSum(sendData+4, i-4);
	sendData[i++] = 0x16;
	
	ModbusCtrlSetMode(METER_CTRL_PORT, METER_CTRL_PIN, 1);
	HalUartWrite(METER_UART, sendData, i);
	//SysDataPrint(sendData, i);
	ModbusCtrlSetMode(METER_CTRL_PORT, METER_CTRL_PIN, 0);
}

static void DTSF667MeterRecvByte(uint8_t byte)
{
    static uint8_t dataLen;
    ElectricProtcol_t *elec;
    uint8_t sum;
    
    g_recvBuff[g_elecMeterRecvCount++] = byte;
    if(g_elecMeterRecvCount >= sizeof(g_recvBuff))
    {
        g_elecMeterRecvCount = 0;
    }
    
    if(g_elecMeterRecvCount == 1)
    {
        if(byte != 0x68)
        {
            g_elecMeterRecvCount = 0;
        }
    }
    else if(g_elecMeterRecvCount == 8)
    {
        if(byte != 0x68)
        {
            g_elecMeterRecvCount = 0;
        }
    }
    else if(g_elecMeterRecvCount == 10)
    {
        dataLen = byte;
    }
    else if(g_elecMeterRecvCount == 12 + dataLen)
    {
#if 1
        uint8_t i;
        SysLog("");
        for(i = 0; i < g_elecMeterRecvCount; i++)
        {
            SysPrintf("%02x ", g_recvBuff[i]);
        }
        SysPrintf("\n");
#endif
        sum = checkSum(g_recvBuff, sizeof(ElectricProtcol_t) + dataLen);
        if(g_recvBuff[sizeof(ElectricProtcol_t) + dataLen] == sum)
        {
            elec = (ElectricProtcol_t *)g_recvBuff;
            DTSF667DataPrase(elec->data, elec->dataLen);
        }
        else
        {
            SysLog("check sum error!,%02x", sum);
        }
        g_elecMeterRecvCount = 0;
        dataLen = 0;
    }
}

static uint8_t DTSF667MeterRecvDate(const uint8_t *data, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		DTSF667MeterRecvByte(data[i]);
	}
	return i;
}

static uint8_t checkSum(const uint8_t *data, uint8_t len)
{
	uint8_t i;
	uint8_t sum = 0;
	
	for(i = 0; i < len; i++)
	{
		sum += data[i];
	}
	return sum;
}

static void DTSF667DataPrase(uint8_t *data, uint8_t len)
{
	uint8_t *value;
    uint8_t i;
    uint16_t ptNum;
    int intNum;
	uint32_t powerCount; //电量
    int16_t voltage;     //电压
    int32_t current;     //电流
    int32_t instantPower; //功率
    int32_t peakPower;    //峰电能
    int32_t bottomPower;  //谷电能
    int16_t factor;


    for(i = 0; i < len; i++)
    {
        data[i] = data[i] - 0x33;
    }
	
	value = &data[2];
	if(memcmp(data, ELECREG_ACTIVE_POWER, 2) == 0)
	{
	    powerCount = (value[3] << 24) \
	                            + (value[2] << 16) \
	                            + (value[1] << 8) \
	                            + value[0];

		ptNum = (uint8_t)(powerCount & 0xff);
	    intNum = powerCount >> 8;
		memset(g_DTSF667Info.powerCount, 0, METER_STR_LEN);
		sprintf(g_DTSF667Info.powerCount, "%x.%02x", intNum, ptNum);
		SysLog("DTFS667 power num:%x.%02x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_VOLTAGE, 2) == 0)
	{
		voltage = (value[1] << 8) + value[0];
        
        ptNum = 0;
        intNum = voltage;
        SysLog("DTFS667 voltage:%x.%01x", intNum, ptNum);
		memset(g_DTSF667Info.voltage, 0, METER_STR_LEN);
		sprintf(g_DTSF667Info.voltage, "%x.%01x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_CURRENT, 2) == 0)
	{
		current =  (value[1] << 8) \
                            + value[0];

        ptNum = (uint16_t)(current & 0xff);
        intNum = current >> 8;
        SysLog("DTFS667 current:%x.%02x", intNum, ptNum);
		memset(g_DTSF667Info.current, 0, METER_STR_LEN);
		sprintf(g_DTSF667Info.current, "%x.%02x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_INST_POWER, 2) == 0)
	{
		#if 0
		instantPower = (value[2] << 16) \
	                            + (value[1] << 8) \
	                            + value[0];
        
        ptNum = (uint16_t)(instantPower & 0xffff);
        intNum = instantPower >> 16;
        SysLog("DTFS667 instant power:%x.%04x", intNum, ptNum);
		memset(g_DTSF667Info.instantPower, 0, METER_STR_LEN);
		sprintf(g_DTSF667Info.instantPower, "%x.%04x", intNum, ptNum);
		#else
		instantPower = value[0];
        
        ptNum = 0;
        intNum = instantPower;
        SysLog("DTFS667 instant power:%x.%04x", intNum, ptNum);
		memset(g_DTSF667Info.instantPower, 0, METER_STR_LEN);
		sprintf(g_DTSF667Info.instantPower, "%x.%04x", intNum, ptNum);
		#endif
	}
	else if(memcmp(data, ELECREG_PEAK_POWER, 2) == 0)
	{
		peakPower = (value[3] << 24) \
	                            + (value[2] << 16) \
	                            + (value[1] << 8) \
	                            + value[0];
        
        ptNum = (uint8_t)(peakPower & 0xff);
        intNum = peakPower >> 8; 
        SysLog("DTFS667 peak power: %x.%02x", intNum, ptNum);
		memset(g_DTSF667Info.peakPower, 0, METER_STR_LEN);
		sprintf(g_DTSF667Info.peakPower, "%x.%02x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_BOTTOM_POWER, 2) == 0)
	{
		bottomPower = (value[3] << 24) \
	                            + (value[2] << 16) \
	                            + (value[1] << 8) \
	                            + value[0];
        
        ptNum = (uint8_t)(bottomPower & 0xff);
        intNum = bottomPower >> 8; 
        SysLog("DTFS667 bottom power: %x.%02x", intNum, ptNum);
		memset(g_DTSF667Info.bottomPower, 0, METER_STR_LEN);
		sprintf(g_DTSF667Info.bottomPower, "%x.%02x", intNum, ptNum);
	}
	else if(memcmp(data, ELECREG_FACTOR, 2) == 0)
	{
	#if 0
		factor= (value[1] << 8) \
                        + value[0];

		ptNum = (uint16_t)(factor & 0xfff);
        intNum = factor >> 12; 
        SysLog("DTFS667 factor: %x.%03x", intNum, ptNum);
		memset(g_DTSF667Info->factor, 0, METER_STR_LEN);
		sprintf(g_DTSF667Info->factor, "%x.%03x", intNum, ptNum);
	#else
		factor = value[0];

		ptNum = (uint16_t)(factor & 0xfff);
	    intNum = factor >> 12; 
	    SysLog("DTFS667 factor: %x.%03x", intNum, ptNum);
		memset(g_DTSF667Info.factor, 0, METER_STR_LEN);
		sprintf(g_DTSF667Info.factor, "%x.%03x", intNum, ptNum);
	#endif
	}

	g_meterWorkAbnormal = 0;
	g_DTSF667Info.e_meterWork = 1;
}

