#ifndef DTSF667_H
#define DTSF667_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"


void DTSF667Init(void);
uint8_t DTSF667Query(void);
E_MeterInfo_t *DTSF6677GetInfo(void);

#endif
