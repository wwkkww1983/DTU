#ifndef PM25_CO2_H
#define PM25_CO2_H

#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"

typedef struct PM25CO2Info_st
{	
	uint8_t  pm25Work;
	uint16_t pm25Val;
	uint8_t  co2Work;
	uint16_t co2Val;
}PM25CO2Info_t;

void HalPM25CO2Init(void);
void HalPM25CO2Poll(void);
PM25CO2Info_t HalGetPM25CO2Info(void);


#endif
