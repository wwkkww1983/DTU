#ifndef HAND_HELD_1_H
#define HAND_HELD_1_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"

void HandHeld1Init(void);
uint8_t HandHeld1Query(void);
F_MeterInfo_t *HandHeld1GetInfo(void);


#endif
