#ifndef DDZY747_H
#define DDZY747_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"

void DDYZ747Init(void);
uint8_t DDYZ747Query(void);
E_MeterInfo_t *DDYZ747GetInfo(void);


#endif
