#ifndef PT100DTU_H
#define PT100DTU_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"

#define PT100_STR_LEN 16
typedef struct PT100DTUInfo_st
{	
	uint8_t pt100DTUWork;
	char pt1[PT100_STR_LEN];
	char pt2[PT100_STR_LEN];
	char pt3[PT100_STR_LEN];
	char pt4[PT100_STR_LEN];
	char pt5Out[PT100_STR_LEN];
}PT100DTUInfo_t;

void HalPT100DTUInit(void);
void HalPT100DTUPoll(void);
void EchoPT100DTUInfo(void);
PT100DTUInfo_t* HalGetPT100DTUInfo(void);

#endif
