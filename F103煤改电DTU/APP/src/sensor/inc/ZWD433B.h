#ifndef ZWD433B_H
#define ZWD433B_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"

void ZWD433BInit(void);
uint8_t ZWD433BQuery(void);
E_MeterInfo_t *ZWD433BGetInfo(void);

#endif
