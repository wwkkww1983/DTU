#ifndef FLD_R_LSR_H
#define FLD_R_LSR_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"

void FLD_R_LSRInit(void);
uint8_t FLD_R_LSRQuery(void);
F_MeterInfo_t *FLD_R_LSRGetInfo(void);

#endif
