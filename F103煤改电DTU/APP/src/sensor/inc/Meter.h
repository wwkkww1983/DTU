#ifndef METER_H
#define METER_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"

#define NORMAL 1
#define PERTICUR 0
#define E_METER_NUM 6
#define F_METER_NUM 6

typedef enum
{
	E_METER_NULL = 0x00,
	E_METER_DDY747,
	E_METER_ZWD433B,
	E_METER_DTSF667,
	E_METER_COUNT,
}E_METER_TYPE;

typedef enum
{
	F_METER_NULL = 0x00,
	F_METER_FLD_R_LSH,
	F_METER_HANDHELD_UNKNOWN_TYPE1,
	F_METER_COUNT,
}F_METER_TYPE;

#define METER_STR_LEN 16
typedef struct E_MeterInfo_st
{	
	uint8_t e_meterWork;
	char powerCount[METER_STR_LEN]; //电量
    char voltage[METER_STR_LEN];     //电压
    char current[METER_STR_LEN];     //电流
    char instantPower[METER_STR_LEN]; //功率
    char peakPower[METER_STR_LEN];    //峰电能
    char bottomPower[METER_STR_LEN];  //谷电能
    char factor[METER_STR_LEN];	
}E_MeterInfo_t;

typedef struct F_MeterInfo_st
{	
	uint8_t f_meterWork;
	char instantHeat[METER_STR_LEN];
	char accHeatDec[METER_STR_LEN];
	char flowCount[METER_STR_LEN];
	char inputTemp[METER_STR_LEN];
	char outputTemp[METER_STR_LEN];
}F_MeterInfo_t;

typedef struct MeterInfo_st
{	
	E_MeterInfo_t *e_MeterInfo;
	E_MeterInfo_t *e_MeterInfo2;
	F_MeterInfo_t *f_MeterInfo;
}MeterInfo_t;

typedef void(*MeterInit_t)(void);
typedef uint8_t(*MeterQuery_t)(void);
typedef E_MeterInfo_t*(*E_MeterGetInfo_t)(void);
typedef F_MeterInfo_t*(*F_MeterGetInfo_t)(void);


typedef struct E_MeterReg_st
{	
	uint8_t regStatus;
	MeterInit_t init;
	MeterQuery_t query;
	E_MeterGetInfo_t getInfo;
}E_MeterReg_t;

typedef struct F_MeterReg_st
{	
	uint8_t regStatus;
	MeterInit_t init;
	MeterQuery_t query;
	F_MeterGetInfo_t getInfo;
}F_MeterReg_t;

#include "DDZY747.h"
#include "FLD_R_LSH.h"
#include "ZWD433B.h"
#include "DTSF667.h"
#include "HandHeld1.h"

void HalMeterInit(void);
void HalMeterPoll(void);
MeterInfo_t *HalGetMeterInfo(void);
void EchoMeterInfo(void);
char *GetElecMeterType(uint8_t meterIndex);
char *GetFlowMeterType(uint8_t meterIndex);
uint8_t HalElecMeterRegister(uint8_t meterNO, MeterInit_t init, MeterQuery_t query, E_MeterGetInfo_t getInfo);
uint8_t HalFlowMeterRegister(uint8_t meterNO, MeterInit_t init, MeterQuery_t query, F_MeterGetInfo_t getInfo);

#endif
