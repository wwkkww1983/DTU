#ifndef HAL_SENSOR_H
#define HAL_SENSOR_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"
#include "Modbus.h"
#include "PM25CO2.h"
#include "SHT20.h"
#include "Meter.h"
#include "PT100DTU.h"

typedef struct SensorInfo_st
{
	PM25CO2Info_t pm25Co2Info; 
	SHT2x_PARAM   sht20Info;
	MeterInfo_t  *meterInfo;
	PT100DTUInfo_t *pt100DTUInfo;
}SensorInfo_t;

void HalSensorPoll(void);
void HalSensorInit(void);
SensorInfo_t *HalGetSensorInfo(void);

#endif
