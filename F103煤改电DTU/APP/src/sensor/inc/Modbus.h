#ifndef MODBUS_H
#define MODBUS_H
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "misc.h"
#include "os.h"

typedef struct
{
    uint8_t addr;
    uint8_t function;
    uint8_t dataLen;
    uint8_t data[];
}ModbusReplyProtcol_t;

typedef uint8_t(*ModBusRecvCallback_t)(ModbusReplyProtcol_t *reply);

float intTofloat(int i);
unsigned int crc16(unsigned char *buf, unsigned char length);
int exchangeValue(uint8_t *val, uint8_t isNomal);
void ModbusCtrlInit(GPIO_TypeDef* port, uint16_t pin);
void ModbusCtrlSetMode(GPIO_TypeDef* port, uint16_t pin,uint8_t tx);
void modbusRecvHandle(uint8_t byte, uint8_t procode, ModBusRecvCallback_t recvCallBack);
void modbusReadRegs(HalUart_t uart ,uint8_t devAddr, uint8_t cmd, uint16_t startReg, uint16_t length);
void modbusWriteRegs(HalUart_t uart ,uint8_t devAddr, uint8_t cmd, uint16_t startReg, uint16_t regNum, uint8_t length, uint32_t data);



#endif
