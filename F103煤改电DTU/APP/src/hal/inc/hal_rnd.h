#ifndef HAL_RND_H
#define HAL_RND_H
#include "os.h"

void hal_rnd_init(void);
void hal_rnd_seed(void);
void hal_rnd_feed_seed(uint8_t seed);
uint8_t hal_rnd_get(void);
uint8_t hal_rnd_range(uint8_t from, uint8_t to);

#endif // HAL_RND_H

