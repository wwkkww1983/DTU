#ifndef HAL_SPI_H
#define HAL_SPI_H


#define RF_CS_PIN        GPIO_Pin_12
#define RF_CS_PORT       GPIOB
#define RF_CS_PIN_SCK    RCC_APB2Periph_GPIOB

#define RF_SCK_PIN        GPIO_Pin_13
#define RF_SCK_PORT       GPIOB
#define RF_SCK_PIN_SCK    RCC_APB2Periph_GPIOB
#define RF_SCK_SOURCE     GPIO_PinSource13
#define RF_SCK_AF         NULL

#define RF_MISO_PIN       GPIO_Pin_14
#define RF_MISO_PORT      GPIOB
#define RF_MISO_PIN_SCK   RCC_APB2Periph_GPIOB
#define RF_MISO_SOURCE    GPIO_PinSource14
#define RF_MISO_AF        NULL

#define RF_MOSI_PIN       GPIO_Pin_15
#define RF_MOSI_PORT      GPIOB
#define RF_MOSI_PIN_SCK   RCC_APB2Periph_GPIOB
#define RF_MOSI_SOURCE    GPIO_PinSource5
#define RF_MOSI_AF        NULL

/************************************* EXTI ***********************************/
//#define _EXTI
#define RCC_GDO0                            RCC_APB2Periph_GPIOB
#define GPIO_GDO0_PORT                      GPIOB
#define GPIO_GDO0                           GPIO_Pin_9
#define GPIO_GDO0_SOURCE                    GPIO_PinSource9
#define GPIO_GDO0_PORT_SOURCE               GPIO_PortSourceGPIOB
#define GPIO_GDO0_EXTI_LINE                 EXTI_Line9
#define GPIO_GDO0_EXTI_IRQn                 EXTI9_5_IRQn

/* Exported macro ------------------------------------------------------------*/
/* Select SPI FLASH: Chip Select pin low  */
#define RF_SPI_CS_LOW()       GPIO_ResetBits(RF_CS_PORT,RF_CS_PIN)
/* Deselect SPI FLASH: Chip Select pin high */
#define RF_SPI_CS_HIGH()      GPIO_SetBits(RF_CS_PORT,RF_CS_PIN)

//sdcard
#define SD_SPI_CS_LOW()       GPIO_ResetBits(GPIOA, GPIO_Pin_4)
#define SD_SPI_CS_HIGH()      GPIO_SetBits(GPIOA, GPIO_Pin_4)

#define SD_DET()           !GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_4)

#define MAX_TX_TIME       20    //ms
#define MAX_RF_WAIT       20    //ms  

typedef enum
{
    HAL_SPI_1,	/* SDCard */
    HAL_SPI_2,	/* cc1101 */
    HAL_SPI_END,
} HalSPI_n;

void HalSpiInit(HalSPI_n port, uint16_t speed);
unsigned char HalSpiReadWrite(HalSPI_n port, unsigned char byte);

#endif
