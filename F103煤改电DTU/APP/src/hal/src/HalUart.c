#include "Hal.h"
#include <string.h>
#include "userSys.h"
static HalUartRecvCallback_t g_uartCbs[HAL_UART_COUNT] = {0};

#define HAL_CPU_HZ  (8000000UL)  //8MHZ
#define LOG_USART_F  USART2

//加入以下代码,支持printf函数,而不需要选择use MicroLIB	  
#if 1
//#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 
	/* Whatever you require here. If the only file you are using is */ 
	/* standard output using printf() for debugging, no file handling */ 
	/* is required. */ 
}; 
/* FILE is typedef’ d in stdio.h. */ 
FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
void _sys_exit(int x) 
{
	x = x; 
} 
//重定义fputc函数 
int fputc(int ch, FILE *f)
{  
  if(MenuConfigISStart())
  {
  	return (ch);
  }
  while(!((LOG_USART_F->SR)&(1<<7))){}
  LOG_USART_F->DR=ch;
  return (ch);
}
#endif 

void USART1_Configuration(uint32_t rate, uint8_t parity)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;        

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE );
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
			
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = rate;
	
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	if(parity == PARITY_EVEN)
	{
		USART_InitStructure.USART_Parity = USART_Parity_Even ;
		USART_InitStructure.USART_WordLength = USART_WordLength_9b;
	}
	else if(parity == PARITY_ODD)
	{
		USART_InitStructure.USART_Parity = USART_Parity_Odd ;
		USART_InitStructure.USART_WordLength = USART_WordLength_9b;
	}
	else
	{
		USART_InitStructure.USART_Parity = USART_Parity_No ;
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	}
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); 

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn; 
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);
}

void USART1_Send_Byte(u8 Data)
{
	USART_SendData(USART1,Data);
	while( USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET );
}

void USART1_Send_String(u8 *Data)
{
	while(*Data)
	USART1_Send_Byte(*Data++);
}

void USART1_IRQHandler(void)
{
	u8 res;    
	if(USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
	{
		USART_ClearFlag(USART1, USART_IT_RXNE);
		res=USART_ReceiveData(USART1);
		if(g_uartCbs[HAL_UART_1])
		{
			g_uartCbs[HAL_UART_1](&res, 1);
		}
	}  
} 

void USART2_Configuration(uint32_t rate, uint8_t parity)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;        

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA| RCC_APB2Periph_AFIO, ENABLE );
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
			
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = rate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	if(parity == PARITY_EVEN)
	{
		USART_InitStructure.USART_Parity = USART_Parity_Even ;
	}
	else if(parity == PARITY_ODD)
	{
		USART_InitStructure.USART_Parity = USART_Parity_Odd ;
	}
	else
	{
		USART_InitStructure.USART_Parity = USART_Parity_No ;
	}
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	//?????;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	//????;
	USART_Init(USART2, &USART_InitStructure);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART2, ENABLE);
}

void USART2_Send_Byte(u8 Data)
{
	USART_SendData(USART2,Data);
	while( USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET );
}

void USART2_Send_String(u8 *Data)
{
	while(*Data)
	USART2_Send_Byte(*Data++);
}

void USART2_IRQHandler(void)
{
	u8 res;    
	if(USART_GetITStatus(USART2, USART_IT_RXNE) == SET)
	{
		USART_ClearFlag(USART2, USART_IT_RXNE);
		res=USART_ReceiveData(USART2);
		if(g_uartCbs[HAL_UART_2])
		{
			g_uartCbs[HAL_UART_2](&res, 1);
		}
	}  
} 

void USART3_Configuration(uint32_t rate, uint8_t parity)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;        

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE );
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
			
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = rate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	if(parity == PARITY_EVEN)
	{
		USART_InitStructure.USART_Parity = USART_Parity_Even ;
	}
	else if(parity == PARITY_ODD)
	{
		USART_InitStructure.USART_Parity = USART_Parity_Odd ;
	}
	else
	{
		USART_InitStructure.USART_Parity = USART_Parity_No ;
	}
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART3, &USART_InitStructure);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART3, ENABLE);
}

void USART3_Send_Byte(u8 Data)
{
	USART_SendData(USART3,Data);
	while( USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET );
}

void USART3_Send_String(u8 *Data)
{
	while(*Data)
	USART3_Send_Byte(*Data++);
}

void USART3_IRQHandler(void)
{
	u8 res;    
	if(USART_GetITStatus(USART3, USART_IT_RXNE) == SET)
	{
		USART_ClearFlag(USART3, USART_IT_RXNE);
		res=USART_ReceiveData(USART3);
		if(g_uartCbs[HAL_UART_3])
		{
			g_uartCbs[HAL_UART_3](&res, 1);
		}
	}  
} 

void USART4_Configuration(uint32_t rate, uint8_t parity)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;        

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE );
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
			
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = rate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	if(parity == PARITY_EVEN)
	{
		USART_InitStructure.USART_Parity = USART_Parity_Even ;
	}
	else if(parity == PARITY_ODD)
	{
		USART_InitStructure.USART_Parity = USART_Parity_Odd ;
	}
	else
	{
		USART_InitStructure.USART_Parity = USART_Parity_No ;
	}

	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(UART4, &USART_InitStructure);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);
	USART_Cmd(UART4, ENABLE);
}

void USART4_Send_Byte(u8 Data)
{
	USART_SendData(UART4,Data);
	while( USART_GetFlagStatus(UART4, USART_FLAG_TC) == RESET );
}

void USART4_Send_String(u8 *Data)
{
	while(*Data)
	USART4_Send_Byte(*Data++);
}

void UART4_IRQHandler(void)
{
	u8 res;    
	if(USART_GetITStatus(UART4, USART_IT_RXNE) == SET)
	{
		USART_ClearFlag(UART4, USART_IT_RXNE);
		res=USART_ReceiveData(UART4);
		if(g_uartCbs[HAL_UART_4])
		{
			g_uartCbs[HAL_UART_4](&res, 1);
		}
	}  
} 

void USART5_Configuration(uint32_t rate, uint8_t parity)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;        

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC|RCC_APB2Periph_GPIOD|RCC_APB2Periph_AFIO, ENABLE );
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
			
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = rate;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	if(parity == PARITY_EVEN)
	{
		USART_InitStructure.USART_Parity = USART_Parity_Even ;
		USART_InitStructure.USART_WordLength = USART_WordLength_9b;
	}
	else if(parity == PARITY_ODD)
	{
		USART_InitStructure.USART_Parity = USART_Parity_Odd ;
		USART_InitStructure.USART_WordLength = USART_WordLength_9b;
	}
	else
	{
		USART_InitStructure.USART_Parity = USART_Parity_No ;
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	}

	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(UART5, &USART_InitStructure);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);
	USART_Cmd(UART5, ENABLE);
}

void USART5_Send_Byte(u8 Data)
{
	USART_SendData(UART5,Data);
	while( USART_GetFlagStatus(UART5, USART_FLAG_TC) == RESET );
}

void USART5_Send_String(u8 *Data)
{
	while(*Data)
	USART5_Send_Byte(*Data++);
}

void UART5_IRQHandler(void)
{
	u8 res;    
	if(USART_GetITStatus(UART5, USART_IT_RXNE) == SET)
	{
		USART_ClearFlag(UART5, USART_IT_RXNE);
		res=USART_ReceiveData(UART5);

		if(g_uartCbs[HAL_UART_5])
		{
			g_uartCbs[HAL_UART_5](&res, 1);
		}

	}  
} 


void HalUartConfig(HalUart_t uart, uint32_t baudRate, uint8_t parity, HalUartRecvCallback_t cb)
{
	if(uart == HAL_UART_1)
	{
		USART1_Configuration(baudRate, parity);
	}
	else if(uart == HAL_UART_2)
	{
		USART2_Configuration(baudRate, parity);
	}
	else if(uart == HAL_UART_3)
	{
		USART3_Configuration(baudRate, parity);
	}
	else if(uart == HAL_UART_4)
	{
		USART4_Configuration(baudRate, parity);
	}
	else if(uart == HAL_UART_5)
	{
		USART5_Configuration(baudRate, parity);
	}
	g_uartCbs[uart] = cb;
}

uint16_t HalUartWrite(HalUart_t uart, const uint8_t *buf, uint16_t len)
{
    uint16_t i;

    for(i = 0; i < len; i++)
    {
		if(uart == HAL_UART_1)
		{
			USART1_Send_Byte(buf[i]);
		}
		else if(uart == HAL_UART_2)
		{
			USART2_Send_Byte(buf[i]);
		}
		else if(uart == HAL_UART_3)
		{
			USART3_Send_Byte(buf[i]);
		}
		else if(uart == HAL_UART_4)
		{
			USART4_Send_Byte(buf[i]);
		}
		else if(uart == HAL_UART_5)
		{
			USART5_Send_Byte(buf[i]);
		}
    }

    return len;

}

void hal_uart_rcv_poll(void)
{
}

