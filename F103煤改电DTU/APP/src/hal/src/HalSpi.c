#include "Hal.h"



void HalSpiInit(HalSPI_n port, uint16_t speed)
{
	uint16_t spiPre;
	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef  SPI_InitStructure;

	switch(speed)  //设置波特率预分频的值
	{
		case 2:
			spiPre = SPI_BaudRatePrescaler_2;break;
		case 4:
			spiPre = SPI_BaudRatePrescaler_4;break;
		case 8:
			spiPre = SPI_BaudRatePrescaler_8;break;
		case 16:
			spiPre = SPI_BaudRatePrescaler_16;break;
		case 32:
			spiPre = SPI_BaudRatePrescaler_32;break;
		case 64:
			spiPre = SPI_BaudRatePrescaler_64;break;
		case 128:
			spiPre = SPI_BaudRatePrescaler_128;break;
		case 256:
			spiPre = SPI_BaudRatePrescaler_256;break;
		default:
		break ;
	}

    if(HAL_SPI_1 == port) 
    {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOC, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

		//SPI_FLASH_SPI pins: SCK--PA5
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

		//SPI_FLASH_SPI pins: MISO--PA6 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

		//SPI_FLASH_SPI pins: MOSI--PA7 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

		//SPI_FLASH_SPI_CS_PIN pin: CS--PA4
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

		//DET
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
		GPIO_Init(GPIOC, &GPIO_InitStructure);

		SD_SPI_CS_HIGH();
		SPI_I2S_DeInit(SPI1);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
		SPI_Cmd(SPI1, DISABLE );
		SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;//SPI1设置为两线全双工
		SPI_InitStructure.SPI_Mode = SPI_Mode_Master;	  //设置SPI1为主模式
		SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;	//SPI发送接收8位帧结构
		SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;	//串行时钟在不操作时，时钟为高电平
		SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;	//第二个时钟沿开始采样数据
		SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;		//NSS信号由软件（使用SSI位）管理
		SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB; //数据传输从MSB位开始
		SPI_InitStructure.SPI_CRCPolynomial = 7;		  //CRC值计算的多项式
		SPI_InitStructure.SPI_BaudRatePrescaler=spiPre;  //波特率预分频值
		SPI_Init(SPI1, &SPI_InitStructure);
		SPI_Cmd(SPI1, ENABLE);

    }
    else if(HAL_SPI_2 == port)    
    {
		RCC_APB2PeriphClockCmd(RF_CS_PIN_SCK | RF_SCK_PIN_SCK | RF_MISO_PIN_SCK|RF_MOSI_PIN_SCK, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
		/* clk */
		GPIO_InitStructure.GPIO_Pin = RF_SCK_PIN;
		GPIO_Init(RF_SCK_PORT, &GPIO_InitStructure);

		// MISO //
		GPIO_InitStructure.GPIO_Pin = RF_MISO_PIN;
		GPIO_Init(RF_MISO_PORT, &GPIO_InitStructure);

		// MOSI //
		GPIO_InitStructure.GPIO_Pin = RF_MOSI_PIN;
		GPIO_Init(RF_MOSI_PORT, &GPIO_InitStructure);

		// CS pin //
		GPIO_InitStructure.GPIO_Pin = RF_CS_PIN;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
		GPIO_Init(RF_CS_PORT, &GPIO_InitStructure);

		RF_SPI_CS_HIGH();
		// SPI configuration -------------------------------------------------------//
		SPI_I2S_DeInit(SPI2);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2,  ENABLE );
		SPI_Cmd(SPI2, DISABLE );
		SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
		SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
		SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
		SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
		SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
		SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
		SPI_InitStructure.SPI_BaudRatePrescaler = spiPre;
		SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
		SPI_InitStructure.SPI_CRCPolynomial = 7;
		SPI_Init(SPI2, &SPI_InitStructure);

		// Enable SPI  //
		SPI_Cmd(SPI2, ENABLE);	

    }
}

unsigned char HalSpiReadWrite(HalSPI_n port, unsigned char byte)
{
	if(HAL_SPI_1 == port) 
    {
		/* Loop while DR register in not emplty */
		hal_wait_ms_cond(20, !(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET) );
		/* Send byte through the SPI2 peripheral */
		SPI_I2S_SendData(SPI1, byte);

		/* Wait to receive a byte */
		hal_wait_ms_cond(20, !(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET) );
		return SPI_I2S_ReceiveData(SPI1);

    }
    else if(HAL_SPI_2 == port)    
    {
		hal_wait_ms_cond(20, !(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET) );
		SPI_I2S_SendData(SPI2, byte);

		hal_wait_ms_cond(20, !(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET) );
		return SPI_I2S_ReceiveData(SPI2);
    }
	else
	{
		return 0;
	}
}

