#include <stdio.h>
#include "Device.h"
#include "main_module.h"
#include "SysPoll.h"
#include "Hal_wait.h"

#define DEVICE_SAVED_DATA_FLAG_USED 0xfe
#define MAX_SUPPORT_DEVICE_NUM  16

typedef enum
{
    CMD_QUERY = 0x01,         /* 设备查询 */
    CMD_NOTIFY = 0x02,        /* 设备状态改变上报 */
    CMD_CTRL = 0x03,          /* 设置设备状态 */
    CMD_HEARTBEAT = 0x04,     /* 设备心跳 */
    CMD_SETENABLE = 0x05,     /* 设备报警使能 */
    CMD_STARTUP = 0x06,       /*  */
    CMD_START_UPGRADE = 0x07, /* 设备开始升级 */
    CMD_PROPERTY = 0x08,      /* 属性 */
} CmdType_t;

static Device_t g_devices[MAX_SUPPORT_DEVICE_NUM];
#if WM_ENABLE
static WM_device_status_t g_WM_device_status[5];
#endif
static SHT_device_status_t g_SHT_device_status[5];
static SK_device_status_t g_SK_device_status[5];

static void GatewayDeviceRealCountChanged(void);
static void setProperty(Device_t *device, uint8_t propertyid, uint8_t alarm, uint32_t value);
static void cmd_heartbeat_handler(Device_t *device, uint8_t voltage);
static void cmd_notify_handler(Device_t *device, uint8_t *buf, uint8_t len);

uint8_t *DeviceGetKey(Device_t *device)
{
    return device->sd->key;
}

char *DeviceGetId(Device_t *device)
{
    static char id[16];
    sprintf(id, "%c%c%03d%010u", device->sd->id.type[0],
            device->sd->id.type[1],
            device->sd->id.subType,
            device->sd->id.no);
    return id;
}

void DeviceInit(Device_t *device, uint8_t rfAddr)
{
    DeviceSavedData_t *sd = (DeviceSavedData_t *)SPI_ADDR(DEVICE_SAVED_DATA_ADDR + rfAddr * sizeof(DeviceSavedData_t));

    memset(device, 0, sizeof(Device_t));

    device->rfAddr = rfAddr;

    device->sd = sd;

    if(sd->flag == DEVICE_SAVED_DATA_FLAG_USED)
    {
        device->alloced = true;
        device->enabled = device->sd->enabled;
        device->miscData = (DeviceMiscData_t *)malloc(sizeof(DeviceMiscData_t));
        DeviceGetMiscData(device);

        SysLog("%c%c%03d%010u flag=%d, model=%s\n", sd->id.type[0], sd->id.type[1], sd->id.subType, \
               sd->id.no, sd->flag, device->miscData->model);

        SysLog("device->rfAddr = %d", device->rfAddr);
    }

    device->lastOptPropertyid = 0xff;
    device->optResponsed = false;
    device->statusChanged = false;//+cbl 0211
    //device->sd = malloc(sizeof(DeviceSavedData_t));

    //memcpy(device->sd, sd, sizeof(DeviceSavedData_t));
}

bool DeviceIsOldDevice(Device_t *device)
{
    DeviceId_t *id = &device->sd->id;
    if(memcmp(id->type, "BS", 2) == 0)
    {
        if(id->subType == 102)
        {
            return true;
        }
    }
    if(memcmp(id->type, "CL", 2) == 0)
    {
        if(id->subType == 102)
        {
            return true;
        }
    }
    if(memcmp(id->type, "WS", 2) == 0)
    {
        return true;
    }
    return false;
}


/* 状态解析报警 */
static void statusParseToAlarm(Device_t *device)
{
    DeviceId_t *id = &device->sd->id;
    uint32_t rgb = 0, power = 0;

    /* 红外报警器 */
    if(memcmp(id->type, "BS", 2) == 0)
    {
        if(id->subType == 102)
        {
            setProperty(device, DEVICE_STATUS_DVID1, ALARM_LEVEL_6, device->statusData[0]);//alarm
            setProperty(device, DEVICE_STATUS_DVID2, 0, device->statusData[1]);//light
            setProperty(device, DEVICE_STATUS_DVID3, 0, device->statusData[2]);//
        }
    }

    /* 彩灯 */
    if(memcmp(id->type, "CL", 2) == 0)
    {
        if(id->subType == 102)
        {
            rgb = (device->statusData[1] << 16 & 0xff0000) | (device->statusData[2] << 8 & 0xff00) | device->statusData[3];
            //SysLog("R:%x,G:%x,B:%x. RGB:%d", device->statusData[1],device->statusData[2],device->statusData[3], rgb);
            setProperty(device, DEVICE_STATUS_DVID1, 0, device->statusData[0]);//mode
            setProperty(device, DEVICE_STATUS_DVID2, 0, rgb);//rgb
        }
    }

    /* 智能墙体插座 */
    if(memcmp(id->type, "WS", 2) == 0)
    {
        power = (device->statusData[1] << 8 & 0xff00) | device->statusData[2];

        setProperty(device, DEVICE_STATUS_DVID1, 0, device->statusData[0]);//开关
        setProperty(device, DEVICE_STATUS_DVID2, 0, power);//功率
        setProperty(device, DEVICE_STATUS_DVID3, 0, device->statusData[3]);//电压
        setProperty(device, DEVICE_STATUS_DVID4, 0, device->statusData[4]);//电量
    }
}

/* 设备设置状态 */
void DeviceSetStatus(Device_t *device, uint8_t *statusData, uint8_t datalen)
{
    bool changed = false;
    device->statusChanged = false;//+cbl 0203

    if(device->statusData)
    {
        if(memcmp(device->statusData, statusData, datalen) != 0)
        {
            free(device->statusData);
            changed = true;
        }
    }
    else
    {
        changed = true;
    }

    if(changed)
    {
        device->statusChanged = true;
        device->statusData = malloc(datalen);
        memcpy(device->statusData, statusData, datalen);
        device->statusDataLen = datalen;
    }

    if(device->lastOptPropertyid == DEVICE_STATUS_DVID1)
    {
        device->optResponsed = true;
    }

    /* 是否是老设备 */
    if(DeviceIsOldDevice(device))
    {
        statusParseToAlarm(device);
    }
}

static void setProperty(Device_t *device, uint8_t propertyid, uint8_t alarm, uint32_t value)
{
    Property_t *last;
    Property_t *tmp;
    Property_t *current = device->firstProperty;

    while(current)
    {
        if(current->propertyid == propertyid)
        {
            break;
        }
        last = current;
        current = last->next;
    }

    if(current)
    {
        if(current->value != value)
        {
            current->changed = true;
        }
    }
    else
    {
        current = malloc(sizeof(Property_t));
        memset(current, 0, sizeof(Property_t));
        current->changed = true;
        tmp = device->firstProperty;
        device->firstProperty = current;
        current->next = tmp;
    }

    current->propertyid = propertyid;
    current->alarmLevel = alarm;
    current->value = value;
}

void DeviceGetMiscData(Device_t *device)
{
    static DeviceAlarmLevel_t alarmLevel[2] = {{ALARM_LEVEL_6, ALARM_LEVEL_5},
        {ALARM_LEVEL_4, ALARM_LEVEL_3}
    };

    if(device->miscData == NULL)
    {
        return;
    }

    if(memcmp("SK", device->sd->id.type, 2) == 0) //智能插座
    {
        device->miscData->isAlarmDev = false;
        device->miscData->model      = DEVICE_MODEL_SK;
        device->miscData->statusNum  = 1;
    }
    else if(memcmp("WM", device->sd->id.type, 2) == 0) //门窗磁感应器
    {
        device->miscData->isAlarmDev = true;
        device->miscData->level      = &alarmLevel[0];
        device->miscData->model      = DEVICE_MODEL_WM;
        device->miscData->statusNum  = 0;
    }
    else if(memcmp("BS", device->sd->id.type, 2) == 0) //红外人体传感器
    {
        device->miscData->isAlarmDev = true;
        device->miscData->level      = &alarmLevel[0];
        device->miscData->model      = DEVICE_MODEL_BS;
        device->miscData->statusNum  = 2;
    }
    else if(memcmp("SS", device->sd->id.type, 2) == 0) //烟雾传感器
    {
        device->miscData->isAlarmDev = true;
        device->miscData->level      = &alarmLevel[0];
        device->miscData->model      = DEVICE_MODEL_SS;
        device->miscData->statusNum  = 0;
    }
    else if(memcmp("CS", device->sd->id.type, 2) == 0) //可燃气体传感器
    {
        device->miscData->isAlarmDev = true;
        device->miscData->level      = &alarmLevel[0];
        device->miscData->model      = DEVICE_MODEL_CS;
        device->miscData->statusNum  = 0;
    }
    else if(memcmp("CL", device->sd->id.type, 2) == 0) //智能彩灯
    {
        device->miscData->isAlarmDev = false;
        device->miscData->model      = DEVICE_MODEL_CL;
        device->miscData->statusNum  = 2;
    }
    else if(memcmp("LC", device->sd->id.type, 2) == 0) //灯光控制器
    {
        device->miscData->isAlarmDev = false;
        device->miscData->model      = DEVICE_MODEL_LC;
        device->miscData->statusNum  = 1;
    }
    else if(memcmp("CC", device->sd->id.type, 2) == 0) //窗帘控制器
    {
        device->miscData->isAlarmDev = false;
        device->miscData->model      = DEVICE_MODEL_CC;
        device->miscData->statusNum  = 1;
    }
    else if(memcmp("WS", device->sd->id.type, 2) == 0) //智能墙体插座
    {
        device->miscData->isAlarmDev = false;
        device->miscData->model      = DEVICE_MODEL_WS;
        device->miscData->statusNum  = 1;
    }
    else if(memcmp("TS", device->sd->id.type, 2) == 0) //墙体触摸开关
    {
        device->miscData->isAlarmDev = false;
        device->miscData->model      = DEVICE_MODEL_TS;
        device->miscData->statusNum  = 1;
    }
    else if(memcmp("TH", device->sd->id.type, 2) == 0) //无线温湿度传感器
    {
        device->miscData->isAlarmDev = false;
        device->miscData->model      = DEVICE_MODEL_TH;
        device->miscData->statusNum  = 2;
    }
}

#define DEVICE_SIGNAL_DISPLAY_VALUE

void DeviceRFRecvData(Device_t *device, uint8_t *data, uint16_t len, int8_t rssi)
{
    CmdType_t type = (CmdType_t)data[0];
    uint8_t *content = data + 1;
    uint16_t contentLen = len - 1;
    uint8_t enabled, propertyid, alarm, voltage;
    uint32_t value;
    uint8_t buf[2];
    bool  ignore = false;
    int16_t drssi = data[len - 1], signal;

    contentLen--;

    if(drssi >= 128)
    {
        signal = (drssi - 256) / 2 - 74;
    }
    else
    {
        signal = drssi / 2 - 74;
    }
#if defined(TEST_DISPLAY_SIGNAL_VALUE)
    device->sigValue = signal;
#endif
    if(signal >= -78)
    {
        device->signal = 3;
    }
    else if((signal < -78) && (signal > -85))
    {
        device->signal = 2;
    }
    else
    {
        device->signal = 1;
    }

    SysLog("%s RSSI:%ddB\n", DeviceGetId(device), signal);

    if(!device->online)
    {
        device->online = true;
        GatewayDeviceRealCountChanged();

    }
    device->lastHbTime = SysTime();

    switch(type)
    {
        case CMD_QUERY:
            SysLog("CMD_QUERY");
            //SysDataPrint(content, contentLen);
            DeviceSetStatus(device, content, contentLen);
            break;

        case CMD_NOTIFY:
            //if(device->sd->enabled || !DeviceIsAlarmDev(&device->sd->id))//+cbl 0305
            SysLog("CMD_NOTIFY");
            //SysDataPrint(content, contentLen);
            if(device->sd->enabled || !device->miscData->isAlarmDev)
            {
                //DeviceSetStatus(device, content, contentLen);
                cmd_notify_handler(device, content, contentLen);
            }
            break;

        case CMD_STARTUP:
            //memcpy(device->version, content, contentLen);
            break;

        case CMD_SETENABLE:
            break;

        case  CMD_HEARTBEAT:
            SysLog("CMD_HEARTBEAT");
            //SysDataPrint(content, contentLen);
            //enable璁惧涓嶄繚瀛橀渶瑕佸悓姝�
            //if(memcmp("SK", device->sd->id.type, 2)!=0)
            //if(DeviceIsAlarmDev(&device->sd->id))
            if(device->miscData->isAlarmDev)
            {
                enabled = content[contentLen - 3];
                if(enabled != device->sd->enabled)
                {
                    buf[0] = CMD_SETENABLE;
                    buf[1] = device->sd->enabled & 1;
                    main_module_relay_msg_to_device(device->rfAddr, buf, 2, device->signal);
                    enabled = device->sd->enabled;
                }
                device->enabled = enabled;
                if(!device->sd->enabled)
                {
                    ignore = true;
                }
            }
            voltage = content[contentLen - 2];
            if(voltage > 22)
            {
                device->voltage = 2;
            }
            else if(voltage <= 22 && voltage > 20)
            {
                device->voltage = 1;
            }
            else
            {
                device->voltage = 0;
            }

            if(!ignore)
            {
                //DeviceSetStatus(device, content, contentLen - 3);
                cmd_heartbeat_handler(device, device->voltage);
            }

            //SysLog("Heart beat:%s", DeviceGetId(device));//+cbl 0213 test
            break;

        case  CMD_START_UPGRADE:
            device->inUpgrading = true;
            break;

        case CMD_PROPERTY:
            SysLog("CMD_PROPERTY");
            //SysDataPrint(content, contentLen);
            propertyid = content[0];
            if(propertyid == device->lastOptPropertyid)
            {
                device->optResponsed = true;
            }
            alarm = content[1];
            value = 0;
            value |= content[2];
            value <<= 8;
            value |= content[3];
            value <<= 8;
            value |= content[4];
            value <<= 8;
            value |= content[5];
            setProperty(device, propertyid, alarm, value);
            break;

        default:
            //SYS_ASSERT("unsupport cmd", false);
            SysLog("unsupport cmd=%d", type);//+cbl 0127
            break;
    }
}

void DeviceHandleOperation(Device_t *device, uint8_t propertyid, uint32_t val)
{
    uint8_t data[100];
    uint32_t value;

    device->optResponsed = false;
    device->lastOptPropertyid = propertyid;

    value = val;

    if(propertyid == DEVICE_ENABLE_DVID)
    {
        data[0] = CMD_SETENABLE;
        data[1] = value & 1;
        //SysLog("DEVICE_ENABLE_DVID :%02x %02x", data[0], data[1]);
        DeviceSetEnabled(device, value & 1);
        device->optResponsed = true;
        if(device->sd->sleep)
        {
            device->enabled = data[1];
        }
        main_module_relay_msg_to_device(device->rfAddr, data, 2, device->signal);
    }
    else if(propertyid == DEVICE_STATUS_DVID1)
    {
        data[0] = CMD_CTRL;
        data[1] = (uint8_t)value;
        if(strcmp(device->miscData->model, DEVICE_MODEL_CL) == 0)
        {
            memcpy(&data[2], device->statusData + 1, device->statusDataLen - 1); // 3 byte color
        }
        else if(strcmp(device->miscData->model, DEVICE_MODEL_BS) == 0)
        {
            memcpy(&data[2], device->statusData + 1, device->statusDataLen - 1); //夜灯时间（1B）+光照灵敏度（1B）
        }

        if(device->sd->sleep)//休眠设备立即回复
        {
            memcpy(device->statusData, &data[1], device->statusDataLen);
            if(DeviceIsOldDevice(device))
            {
                setProperty(device, DEVICE_STATUS_DVID1, 0, data[1]);
            }
            device->statusChanged = true;
        }
        main_module_relay_msg_to_device(device->rfAddr, data, device->statusDataLen + 1, device->signal);

    }
    else if(propertyid == DEVICE_STATUS_DVID2)
    {
        if(device->miscData->statusNum > 1)
        {
            data[0] = CMD_CTRL;
            //n = strlen(var->value) >> 1;
            data[1] = device->statusData[0];
            data[2] = (uint8_t)value;
            if(strcmp(device->miscData->model, DEVICE_MODEL_BS) == 0)
            {
                data[3] = device->statusData[device->statusDataLen - 1];
            }
            else if(strcmp(device->miscData->model, DEVICE_MODEL_CL) == 0)
            {
                //value = value&0xffffff;
                //memcpy(&data[2], value, 3);//3byte rgb  ???
                data[2] = (uint8_t)(value & 0x00ff0000) >> 16;
                data[3] = (uint8_t)(value & 0x0000ff00) >> 8;
                data[3] = (uint8_t)(value & 0x000000ff);
            }

            SysDataPrint(data, device->statusDataLen + 1);
            if(device->sd->sleep)//休眠设备立即回复
            {
                memcpy(device->statusData, &data[1], device->statusDataLen);
                if(DeviceIsOldDevice(device))
                {
                    setProperty(device, DEVICE_STATUS_DVID2, 0, data[2]);
                }
            }
            main_module_relay_msg_to_device(device->rfAddr, data, device->statusDataLen + 1, device->signal);
        }
    }
    else if(propertyid == DEVICE_STATUS_DVID2)
    {
        if(strcmp(device->miscData->model, DEVICE_MODEL_BS) == 0)
        {
            data[0] = CMD_CTRL;
            data[1] = device->statusData[0];
            data[2] = device->statusData[1];
            data[3] = (uint8_t)value;

            SysDataPrint(data, device->statusDataLen + 1);
            if(device->sd->sleep)//休眠设备立即回复
            {
                memcpy(device->statusData, &data[1], device->statusDataLen);
                if(DeviceIsOldDevice(device))
                {
                    setProperty(device, DEVICE_STATUS_DVID3, 0, data[3]);
                }
            }
            main_module_relay_msg_to_device(device->rfAddr, data, device->statusDataLen + 1, device->signal);
        }
    }
    else
    {
        data[0] = CMD_PROPERTY;
        data[1] = 0;
        data[2] = (value >> 24) & 0xff;
        data[3] = (value >> 16) & 0xff;
        data[4] = (value >> 8) & 0xff;
        data[5] = (value >> 0) & 0xff;
        main_module_relay_msg_to_device(device->rfAddr, data, 6, device->signal);
    }
}

void DeviceDel(Device_t *device)
{
    DeviceSavedData_t sd;
    Property_t *current, *next;
    main_module_del_device(device->rfAddr);
    device->alloced = false;
    memset(&sd, 0, sizeof(DeviceSavedData_t));
    device->online = false;
    if(device->miscData)
    {
        free(device->miscData);
    }

    if(DeviceIsOldDevice(device))
    {
        current = device->firstProperty;
        while(current)
        {
            next = current->next;
            free(current);
            current = next;
        }
        device->firstProperty = NULL;
    }

    SysUserDataWrite(DEVICE_SAVED_DATA_ADDR + device->rfAddr * sizeof(DeviceSavedData_t), \
                     (const uint8_t *)&sd, sizeof(DeviceSavedData_t));//+cbl 0205
    GatewayDeviceRealCountChanged();
}

void DeviceSetEnabled(Device_t *device, bool enabled)
{
    DeviceSavedData_t sd;
    SysUserDataRead(DEVICE_SAVED_DATA_ADDR + device->rfAddr * sizeof(DeviceSavedData_t), \
                    (uint8_t *)&sd, sizeof(DeviceSavedData_t));
    sd.enabled = enabled;
    //device->enabled = enabled;
    SysUserDataWrite(DEVICE_SAVED_DATA_ADDR + device->rfAddr * sizeof(DeviceSavedData_t), \
                     (const uint8_t *)&sd, sizeof(DeviceSavedData_t));//+cbl 0205
}
void delay_ms(uint32_t time)
{
  uint32_t i=8000*time;
  while(i--);
}
void DeviceAdd(Device_t *device, DeviceId_t *id, uint8_t *factoryinfo)
{
    DeviceSavedData_t sd;
    uint32_t idNum;
    long rd = rand();

    device->alloced = true;
    sd.flag = DEVICE_SAVED_DATA_FLAG_USED;
    sd.key[0] = (uint8_t)(rd & 0xff);
    sd.key[1] = (uint8_t)((rd >> 8) & 0xff);
    sd.key[2] = (uint8_t)((rd >> 16) & 0xff);
    memcpy(&sd.id, id, sizeof(DeviceId_t));
    SysLog("key:%d,%d,%d", sd.key[0], sd.key[1], sd.key[2]);
    SysLog("factory info : %s", factoryinfo);

    if(factoryinfo[15] == '1')
    {
        sd.sleep = 1;
        SysLog("sleep device\n");
    }
    else
    {
        sd.sleep = 0;
        SysLog("normal device");
    }
    sd.enabled = true;
    device->enabled = true;
    SysUserDataWrite(DEVICE_SAVED_DATA_ADDR + device->rfAddr * sizeof(DeviceSavedData_t), \
                     (const uint8_t *)&sd, sizeof(DeviceSavedData_t));//+cbl 0205

    if(SysIsLittleEndian())
    {
        idNum = ((id->no & 0xff000000) >> 24)\
                | ((id->no & 0x00ff0000) >>  8)\
                | ((id->no & 0x0000ff00) <<  8)\
                | ((id->no & 0x000000ff) << 24);
        id->no = idNum;
    }
	delay_ms(500);
    main_module_add_device(device->rfAddr, (uint8_t *)id, sd.key);
	delay_ms(500);
	main_module_add_device(device->rfAddr, (uint8_t *)id, sd.key);
	
    device->miscData = (DeviceMiscData_t *)malloc(sizeof(DeviceMiscData_t));
    DeviceGetMiscData(device);
    GatewayDeviceRealCountChanged();
}

static void GatewayDeviceRealCountChanged()
{
    uint8_t count = 0;
    int i = 0;
    for(i = 0; i < MAX_SUPPORT_DEVICE_NUM; i++)
    {
        if(g_devices[i].online)//
        {
            count++;
        }
    }
    main_module_set_device_real_count(count);
}

void GatewayRFFoundDevice(DeviceId_t *id, uint8_t *facInfo)
{
    int i;
    Device_t *device = NULL;
    uint8_t firstEmpty = 0xff;

    uint32_t idNum;

    if(SysIsLittleEndian())
    {
        idNum = ((id->no & 0xff000000) >> 24)\
                | ((id->no & 0x00ff0000) >>  8)\
                | ((id->no & 0x0000ff00) <<  8)\
                | ((id->no & 0x000000ff) << 24);
        id->no = idNum;
    }

    if(memcmp(id->type, "WM", 2) == 0)
    {
        /* 位置预留用作门磁 */
        for(i = 1; i < 6; i++)
        {
            if(!g_devices[i].alloced && (0xff == firstEmpty))
            {
                firstEmpty = i;
                continue;
            }

            if(memcmp(&g_devices[i].sd->id, id, sizeof(DeviceId_t)) == 0)
            {
                device = &g_devices[i];
                device->newAdded = true;//+cbl 0605
                break;
            }
        }
    }
    else if(memcmp(id->type, "TH", 2) == 0)
    {
        /* 位置预留用作温湿度传感器 */
        for(i = 6; i < 11; i++)
        {
            if(!g_devices[i].alloced && (0xff == firstEmpty))
            {
                firstEmpty = i;
                continue;
            }

            if(memcmp(&g_devices[i].sd->id, id, sizeof(DeviceId_t)) == 0)
            {
                device = &g_devices[i];
                device->newAdded = true;//+cbl 0605
                break;
            }
        }
    }
	else if(memcmp(id->type, "SK", 2) == 0)
    {
        /* 位置预留用作温湿度传感器 */
        for(i = 11; i < 16; i++)
        {
            if(!g_devices[i].alloced && (0xff == firstEmpty))
            {
				//TODO 重复的不添加
                firstEmpty = i;
                continue;
            }

            if(memcmp(&g_devices[i].sd->id, id, sizeof(DeviceId_t)) == 0)
            {
                device = &g_devices[i];
                device->newAdded = true;//+cbl 0605
                break;
            }
        }
    }
    else
    {
        return; /* 其余设备不做处理 */
    }

#if 0
    for(i = 0; i < MAX_SUPPORT_DEVICE_NUM; i++)
    {
        if(!g_devices[i].alloced)
        {
            firstEmpty = i;
            continue;
        }

        if(memcmp(&g_devices[i].sd->id, id, sizeof(DeviceId_t)) == 0)
        {
            device = &g_devices[i];
            device->newAdded = true;//+cbl 0605
            break;
        }
    }
#endif

    if(!device && (0xff != firstEmpty))
    {
        device = &g_devices[firstEmpty];
        device->newAdded = true;//+cbl 0203
    }

    //#ifndef COMPILE_WITH_LOG

    //#endif

    if(NULL == device)
    {
        SysLog("no space to add device!");
        return;
    }
    else
    {
        SysLog("Add device %c%c%03d%010u at address %d\n",  id->type[0],
               id->type[1],
               id->subType,
               id->no,
               device->rfAddr);

        DeviceAdd(device, id, facInfo);
    }
}


static uint8_t GatewayDevicesOffline(uint16_t pos, void *args)
{
    Device_t *device;
    SysTime_t outtime;
    uint8_t data_addr;

    if(pos >= MAX_SUPPORT_DEVICE_NUM)
    {
        return 1;
    }

    device = &g_devices[pos];
    if(device->alloced && device->online)
    {
        if(device->sd->sleep)
        {
            outtime = 3600000;// 2*10*60*1000 = 2circle*10min
        }
        else
        {
            //outtime = (main_module_get_device_real_count() + 1) * 100000;
			outtime = (main_module_get_device_real_count() + 5)*60*1000;
        }

        if(SysTimeHasPast(device->lastHbTime, outtime))
        {
            device->online = false;
			SysLog("device offline!!!! addr:%d", device->rfAddr - 11);
        }
    }

    if(memcmp(device->miscData->model, DEVICE_MODEL_WM, 12) == 0)
    {
        /* 门磁设备上报数据处理 */
        data_addr = device->rfAddr - 1;
#if WM_ENABLE
        g_WM_device_status[data_addr].online = device->online;
        g_WM_device_status[data_addr].alloced = device->alloced;
#endif
    }
    else if(memcmp(device->miscData->model, DEVICE_MODEL_TH, 12) == 0)
    {
        /* 温湿度设备 */
        data_addr = device->rfAddr - 6;

        g_SHT_device_status[data_addr].online = device->online;
        g_SHT_device_status[data_addr].alloced = device->alloced;
    }
	 else if(memcmp(device->miscData->model, DEVICE_MODEL_SK, 12) == 0)
    {
        /* 插座设备 */
        data_addr = device->rfAddr - 11;

        g_SK_device_status[data_addr].online = device->online;
        g_SK_device_status[data_addr].alloced = device->alloced;
    }

    return 0;
}


static void initMainModule()
{
    int i;
    int n = 0;
    uint8_t *data;

    for(i = 0; i < MAX_SUPPORT_DEVICE_NUM; i++)
    {
        if(g_devices[i].alloced)
        {
            n++;
        }
    }

    data = malloc(n * 4 + 4);
    //printf("### malloc addr = 0x%08x \r\n",data);
    data[0] = n;
    n = 0;
    for(i = 0; i < MAX_SUPPORT_DEVICE_NUM; i++)
    {
        if(g_devices[i].alloced)
        {
            data[1 + n * 4 + 0] = i;
            memcpy(data + 1 + n * 4 + 1, g_devices[i].sd->key, DEVICE_KEY_LEN); //key
            n++;
        }
    }
    main_module_set_devicelist(data);
    free(data);//+cbl 0213
}

static void initDevices()
{
    int i;

    for(i = 0; i < MAX_SUPPORT_DEVICE_NUM; i++)
    {
        DeviceInit(&g_devices[i], i);
    }

    initMainModule();//+cbl 0411

    sysPeriodPollSet(MAX_SUPPORT_DEVICE_NUM, POLL_ALLWAYS, GatewayDevicesOffline, NULL, 0);
}

void GatewayInit()
{
    initDevices();
}


void GatewayRFVersionSet(uint8_t addr, uint8_t *data, uint8_t len)
{
    Device_t *device = &g_devices[addr];
    DeviceSavedData_t sd;

    if(!device->alloced)
    {
        DeviceDel(device);
        return;
    }
    if(memcmp(device->sd->version, data, DEVICE_VERSION_LEN) == 0)
    {
        return;
    }
    memcpy(&sd, device->sd, sizeof(DeviceSavedData_t));
    memcpy(sd.version, data, DEVICE_VERSION_LEN);
    SysUserDataWrite(DEVICE_SAVED_DATA_ADDR + device->rfAddr * sizeof(DeviceSavedData_t), \
                     (const uint8_t *)&sd, sizeof(DeviceSavedData_t));

}

void GatewayRFRecvData(uint8_t addr, uint8_t *data, uint8_t len, int8_t rssi)
{
    //Device_t oldDevice;
    Device_t *device;

    if(addr >= MAX_SUPPORT_DEVICE_NUM)
    {
        return;
    }

    device = &g_devices[addr];

    if(!device->alloced)
    {
        DeviceDel(device);
        return;
    }

    //oldDevice = *device;
    //SysPrintf("%s %s:",__func__, DeviceGetId(device));//+cbl for test!!
    //SysDataPrint(data, len);//+cbl for test!!
    DeviceRFRecvData(device, data, len, rssi);

}

static SysTimer_t *g_addDevDoneTimer = NULL;

static void addDevDone(void *p)
{
    //uint8_t devNum[MAX_SUPPORT_DEVICE_NUM];
    uint8_t i, count;

    SysTimerUnSet(g_addDevDoneTimer);
    main_module_enter_work();

    for(i = 0; i < MAX_SUPPORT_DEVICE_NUM; i++)
    {
        if(g_devices[i].newAdded)
        {
            g_devices[i].newAdded = false;

            if(g_devices[i].miscData->model)
            {
                //devNum[count] = i;
                g_devices[i].online = true;
                g_devices[i].lastHbTime = SysTime();
                count++;
            }
            else   //not support
            {
                DeviceDel(&g_devices[i]);
            }
        }
    }
	HalLedStopBlink(RF_LED);
}

void startAddDevice(void)
{
	SysLog("start add device!");
    if(g_addDevDoneTimer)
    {
        SysTimerUnSet(g_addDevDoneTimer);
    }

    main_module_enter_buildnet(2);

    g_addDevDoneTimer = SysTimerSet(addDevDone, 30000, SYS_TIMER_NEED_UNSET, NULL); //进入配网20s

	HalLedStartBlink(RF_LED,4,0);
}

static void cmd_notify_handler(Device_t *device, uint8_t *buf, uint8_t len)
{
    uint8_t data_addr;

    if(memcmp(device->miscData->model, DEVICE_MODEL_WM, 12) == 0)
    {
        /* 门磁设备上报数据处理 */
        data_addr = device->rfAddr - 1;
#if WM_ENABLE
        g_WM_device_status[data_addr].online = 1;
        g_WM_device_status[data_addr].alarm_status = buf[0];
#endif

    }
    else if(memcmp(device->miscData->model, DEVICE_MODEL_TH, 12) == 0)
    {
        /* 温湿度设备 */
        data_addr = device->rfAddr - 6;

        g_SHT_device_status[data_addr].online = 1;
        g_SHT_device_status[data_addr].humidity = buf[0];
        g_SHT_device_status[data_addr].temperature = buf[1] << 8 | buf[2];
    }
    else if(memcmp(device->miscData->model, DEVICE_MODEL_SK, 12) == 0)
    {
        
        data_addr = device->rfAddr - 11;

        g_SK_device_status[data_addr].online = 1;
		
		memcpy(&g_SK_device_status[data_addr].power,buf, 4);
		memcpy(&g_SK_device_status[data_addr].total_elec,buf+4, 4);

		SysLog("SK:%d power: %d, total_elec: %d", data_addr, g_SK_device_status[data_addr].power, g_SK_device_status[data_addr].total_elec);
    }

}

static void cmd_heartbeat_handler(Device_t *device, uint8_t voltage)
{
    uint8_t data_addr;

    if(memcmp(device->miscData->model, DEVICE_MODEL_WM, 12) == 0)
    {
        /* 门磁设备上报数据处理 */
        data_addr = device->rfAddr - 1;

        SysLog("wm : data_addr = %d", data_addr);
#if WM_ENABLE
        g_WM_device_status[data_addr].online = 1;
        g_WM_device_status[data_addr].voltage = voltage;
#endif
    }
    else if(memcmp(device->miscData->model, DEVICE_MODEL_TH, 12) == 0)
    {
        /* 温湿度设备 */
        data_addr = device->rfAddr - 6;

        SysLog("sht : data_addr = %d", data_addr);

        g_SHT_device_status[data_addr].online = 1;
        g_SHT_device_status[data_addr].voltage = voltage;
    }
	else if(memcmp(device->miscData->model, DEVICE_MODEL_SK, 12) == 0)
    {
        /* 温湿度设备 */
        data_addr = device->rfAddr - 11;

        SysLog("sk : data_addr = %d", data_addr);

        g_SK_device_status[data_addr].online = 1;
        g_SK_device_status[data_addr].voltage = voltage;
    }

}
#if WM_ENABLE
WM_device_status_t *gataway_get_wm_info(uint8_t num)
{
    return &g_WM_device_status[num];
}
#endif

//TODO 限定温度湿度数值
SHT_device_status_t *gataway_get_sht_info(uint8_t num)
{
    return &g_SHT_device_status[num];
}

SK_device_status_t *gataway_get_sk_info(uint8_t num)
{
	//不在线时传-1
	if(!g_SK_device_status[num].online)
	{
		g_SK_device_status[num].power = -1;
		g_SK_device_status[num].total_elec = -1;
	}

	//状态变为在线时，初始化状态为0
	if(g_SK_device_status[num].online != g_SK_device_status[num].last_online)
	{	
		if(g_SK_device_status[num].online && g_SK_device_status[num].power == -1)
		{
			//TODO 以上线但未收到有效数据，处理待定
			//g_SK_device_status[num].power = 0;
			//g_SK_device_status[num].total_elec = 0;
		}
		g_SK_device_status[num].last_online = g_SK_device_status[num].online;
	}
    return &g_SK_device_status[num];
}


uint8_t GatewayDeviceAllocedCount( void )
{
    uint8_t count = 0;
    int i = 0;
    for(i = 0; i < MAX_SUPPORT_DEVICE_NUM; i++)
    {
        if(g_devices[i].alloced)
        {
            count++;
        }
    }
	return count;
}

uint8_t GatewayDeviceOnlineCount( void )
{
    uint8_t count = 0;
    int i = 0;
    for(i = 0; i < MAX_SUPPORT_DEVICE_NUM; i++)
    {
        if(g_devices[i].online)
        {
            count++;
        }
    }
	return count;
}

