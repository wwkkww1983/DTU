#include "Hal.h"
#include "userSys.h"
#include "ModuleTester.h"
#include "cloud_helper.h"

#define CMD_NUM 16
#define CMD_MAX_LEN 128

typedef enum
{
	CMD_MENUCONFIG = 0x00,
	CMD_QUIT,
	CMD_SET,
	CMD_ECHO,
	CMD_REBOOT,
	CMD_HELP,
	CMD_SUDO,
}CMD_TYPE;

static char *g_cmd[CMD_NUM] = {"menuconfig","quit","set","echo","reboot","help","sudo"};

static bool g_menuconfigStart = false;
static char g_menuconfigRecv[CMD_MAX_LEN] = {0};
static uint8_t g_recvCount = 0;
static uint8_t g_recvd = 0;
static MenuconfigInfo_t g_configInfo = {0};
static SysTime_t g_lastRecvTime = 0;

//TODO
const uint8_t tFwVersion[4] = {0x1, 0x00, 0x02, 0x01};      //版本号信息
#define MODULE_ID  "qmbrud"
#define MODULE_PIN  "3c215c14d40a464a8d5a99f83bee874f"

//
static void MenuConfigHandle(uint8_t cmdID, char *cmd);
static void handleSetConfig(char *cmd);
static void parseSetConfig(char *config);
static void echoConfigInfo(void);
static void echoHelpInfo(void);
static void handleDebug(char *cmd);
static void echoSelectInfo(char *title, uint8_t isSelected);


void MenuConfigInit(void)
{
	uint8_t configCheck = 0;
	//读falsh
	SysUserDataRead(CONFIG_SAVED_DATA_ADDR, (uint8_t *)&g_configInfo, sizeof(MenuconfigInfo_t));
	
	if(g_configInfo.saveFlag != 0x21)
	{
		//初次设定
		g_configInfo.saveFlag = 0x21;
		strcpy(g_configInfo.id, MODULE_ID);
		strcpy(g_configInfo.pin, MODULE_PIN);		
		memcpy(g_configInfo.version, tFwVersion, 4);
		g_configInfo.tm = 1;
		g_configInfo.hum = 1;
		g_configInfo.pm = 1;
		g_configInfo.co2 = 1;
		g_configInfo.shtNum = 5;
		g_configInfo.wmNum = 5;
		g_configInfo.skNum = 5;
		g_configInfo.eMeterType = 1;
		g_configInfo.eMeterType2 = 0;
		g_configInfo.fMeterType = 1;
		
		SysUserDataWrite(CONFIG_SAVED_DATA_ADDR, (const uint8_t *)&g_configInfo, sizeof(MenuconfigInfo_t));
	}

	//check config TODO  封装接口
	
	if(memcmp(g_configInfo.version, tFwVersion, 4) != 0)
	{
		memcpy(g_configInfo.version, tFwVersion, 4);
		configCheck = 1;
	}
	memcpy(g_configInfo.version, tFwVersion, 4);

	if(g_configInfo.tm > 1)
	{
		g_configInfo.tm = 1;
		configCheck = 1;
	}

	if(g_configInfo.hum > 1)
	{
		g_configInfo.hum = 1;
		configCheck = 1;
	}

	if(g_configInfo.pm > 1)
	{
		g_configInfo.pm = 1;
		configCheck = 1;
	}

	if(g_configInfo.co2 > 1)
	{
		g_configInfo.co2 = 1;
		configCheck = 1;
	}

	if(g_configInfo.shtNum > 5)
	{
		g_configInfo.shtNum = 5;
		configCheck = 1;
	}
	
	if(g_configInfo.wmNum > 5)
	{
		g_configInfo.wmNum = 5;
		configCheck = 1;
	}
	
	if(g_configInfo.skNum > 5)
	{
		g_configInfo.skNum = 5;
		configCheck = 1;
	}
	
	if(g_configInfo.eMeterType >= E_METER_COUNT)
	{
		g_configInfo.eMeterType = 1;
		configCheck = 1;
	}
	
	if(g_configInfo.eMeterType2 >= E_METER_COUNT)
	{
		g_configInfo.eMeterType2 = 0;
		configCheck = 1;
	}

	if(g_configInfo.fMeterType >= F_METER_COUNT)
	{
		g_configInfo.fMeterType = 1;
		configCheck = 1;
	}


	if(configCheck == 1)
	{
		SysUserDataWrite(CONFIG_SAVED_DATA_ADDR, (const uint8_t *)&g_configInfo, sizeof(MenuconfigInfo_t));
	}
	
}

void MenuConfigPoll(void)
{
	if(SysTimeHasPast(g_lastRecvTime,1000))
	{
		g_recvCount = 0;
	}

	//收到整条数据后再处理
	if(g_recvd && g_recvCount && SysTimeHasPast(g_lastRecvTime, 50))
	{
		uint8_t i = 0;
		int8_t hasCMD = -1;
		
		g_recvd = 0;
		if(g_recvCount >= CMD_MAX_LEN)
		{
			g_recvCount = 0;
		}
			 
		for(i = 0; i < CMD_NUM; i++)
		{
			//收到的命令包含cmd
			if(g_recvCount >= strlen(g_cmd[i]) && memcmp(g_menuconfigRecv, g_cmd[i], strlen(g_cmd[i])) == 0)
			{
				hasCMD = i;
				break;
			}
		}

		if(hasCMD == -1)
		{
			g_recvCount = 0;
		}
		else
		{
			g_recvCount = 0;
			MenuConfigHandle(hasCMD, g_menuconfigRecv);
		}
	}
}

static void MenuConfigHandle(uint8_t cmdID, char *cmd)
{
	if(cmdID == CMD_MENUCONFIG)
	{
		g_menuconfigStart = 1;
		echoConfigInfo();
	}
	else if(cmdID == CMD_QUIT)
	{
		MenuConfigPrintf("# %s\n",cmd);
		g_menuconfigStart = 0;
		SysUserDataWrite(CONFIG_SAVED_DATA_ADDR, (const uint8_t *)&g_configInfo, sizeof(MenuconfigInfo_t));
		MenuConfigPrintf("saved\n");
	}
	else if(cmdID == CMD_SET)
	{
		MenuConfigPrintf("# %s\n",cmd);
		handleSetConfig(cmd);
		
	}
	else if(cmdID == CMD_ECHO)
	{
		MenuConfigPrintf("# %s\n",cmd);
		echoConfigInfo();
	}
	else if(cmdID == CMD_REBOOT)
	{
		MenuConfigPrintf("# %s\n",cmd);
		HalRestart();
	}
	else if(cmdID == CMD_HELP)
	{
		MenuConfigPrintf("# %s\n",cmd);
		echoHelpInfo();
	}
	else if(cmdID == CMD_SUDO)
	{
		MenuConfigPrintf("# %s waring.\n",cmd);
		handleDebug(cmd);
	}
}

static void handleSetConfig(char *cmd)
{
	uint8_t parseLen = 0;
	char *fchr,*echr = NULL;
	char *parseCmd = cmd;
	char setCmd[64] = {0};
	while(1)
	{
		memset(setCmd, 0, 64);
		fchr = strchr(parseCmd, '-');
		if(fchr == NULL)
		{
			break;
		}
		parseLen += (fchr-parseCmd);
		parseCmd = fchr+1;

		echr = strchr(parseCmd, '-');
		if(echr == NULL)
		{
			memcpy(setCmd, fchr, strlen(cmd) - (fchr - cmd));
			parseSetConfig(setCmd);
			MenuConfigPrintf("saved\n");
			//TODO save 
			SysUserDataWrite(CONFIG_SAVED_DATA_ADDR, (const uint8_t *)&g_configInfo, sizeof(MenuconfigInfo_t));
			//
			echoConfigInfo();
			break;
		}
		else
		{
			parseCmd = echr;
			memcpy(setCmd, fchr, echr-fchr-1);
		}
		parseSetConfig(setCmd);
	}
}

static void parseSetConfig(char *config)
{
	//MenuConfigPrintf("parseSetConfig:%s \n", config);
	uint8_t configNum = 0;
	char *configContent = strchr(config,' ');
	if(configContent == NULL)
	{
		MenuConfigPrintf("format error:%s \n", config);
		return ;
	}	
	configContent += 1;
	while(configContent[0] == ' ' || configContent[0] == '-')
	{
		configContent += 1;
	}
	
	if(strstr(config, "-id"))
	{
		memset(g_configInfo.id, 0, 8);
		memcpy(g_configInfo.id, configContent, 6);
	}
	else if(strstr(config, "-pin"))
	{
		memset(g_configInfo.pin, 0, 64);
		strcpy(g_configInfo.pin, configContent);
	}
	else if(strstr(config, "-tm"))
	{
		configNum = atoi(configContent);
		if(configNum > 1)
		{
			MenuConfigPrintf("error:%s  [0-1]\n", config);
		}
		else
		{
			g_configInfo.tm = configNum;
		}
	}
	else if(strstr(config, "-hum"))
	{
		configNum = atoi(configContent);
		if(configNum > 1)
		{
			MenuConfigPrintf("error:%s [0-1]\n", config);
		}
		else
		{
			g_configInfo.hum = configNum;
		}
	}
	else if(strstr(config, "-pm"))
	{
		configNum = atoi(configContent);
		if(configNum > 1)
		{
			MenuConfigPrintf("error:%s [0-1]\n", config);
		}
		else
		{
			g_configInfo.pm = configNum;
		}
	}
	else if(strstr(config, "-co2"))
	{
		configNum = atoi(configContent);
		if(configNum > 1)
		{
			MenuConfigPrintf("error:%s [0-1]\n", config);
		}
		else
		{
			g_configInfo.co2 = configNum;
		}
	}
	else if(strstr(config, "-sht"))
	{
		configNum = atoi(configContent);
		if(configNum > 5)
		{
			MenuConfigPrintf("error:%s [0-5]\n", config);
		}
		else
		{
			g_configInfo.shtNum = configNum;
		}
	}
	else if(strstr(config, "-wm"))
	{
		configNum = atoi(configContent);
		if(configNum > 5)
		{
			MenuConfigPrintf("error:%s [0-5]\n", config);
		}
		else
		{
			g_configInfo.wmNum = configNum;
		}
	}
	else if(strstr(config, "-sk"))
	{
		configNum = atoi(configContent);
		if(configNum > 5)
		{
			MenuConfigPrintf("error:%s [0-5]\n", config);
		}
		else
		{
			g_configInfo.skNum = configNum;
		}
	}
	else if(strstr(config, "-emeter1"))
	{
		configNum = atoi(configContent);
		if(configNum > E_METER_COUNT)
		{
			MenuConfigPrintf("error:%s [0-%d]\n", config, E_METER_COUNT);
		}
		else
		{
			g_configInfo.eMeterType = configNum;
		}
	}
	else if(strstr(config, "-emeter2"))
	{
		configNum = atoi(configContent);
		if(configNum > E_METER_COUNT)
		{
			MenuConfigPrintf("error:%s [0-%d]\n", config, E_METER_COUNT);
		}
		else
		{
			g_configInfo.eMeterType2 = configNum;
		}
	}
	else if(strstr(config, "-fmeter"))
	{
		configNum = atoi(configContent);
		if(configNum > F_METER_COUNT)
		{
			MenuConfigPrintf("error:%s [0-%d]\n", config, F_METER_COUNT);
		}
		else
		{
			g_configInfo.fMeterType = configNum;
		}
	}
}

static void echoConfigInfo(void)
{
	LOOP(x,5)
	{
		MenuConfigPrintf("\n");
	}

	MenuConfigPrintf("******************************MenuConfig v1.0******************************\n\n");
	MenuConfigPrintf("Auther              :Nero Chen\n");
	MenuConfigPrintf("BuildTime           :%s %s\n",__DATE__,__TIME__);
	MenuConfigPrintf("Version             :%01x.%01x.%01x.%01x\n",g_configInfo.version[0],g_configInfo.version[1],g_configInfo.version[2],g_configInfo.version[3]);
	MenuConfigPrintf("ModuleID            :%s\n", GetrModuleID());
	MenuConfigPrintf("ModuleTypeID(-id)   :%s\n", g_configInfo.id);
	MenuConfigPrintf("ModuleTypePin(-pin) :%s\n", g_configInfo.pin);
	MenuConfigPrintf("Device:  \n");
	MenuConfigPrintf("  无线温湿度(-sht) : %d\n", g_configInfo.shtNum);
	
	MenuConfigPrintf("\n电能表1:(-emeter1)  \n");
	LOOP(a, E_METER_NUM)
	{
		if(strlen(GetElecMeterType(VAR(a))))
		{
			char title[32] = {0};
			uint8_t selected = 0;
			sprintf(title,"  [%d] %s", VAR(a), GetElecMeterType(VAR(a)));
			if(VAR(a) == g_configInfo.eMeterType)
			{
				selected = 1;
			}
			echoSelectInfo(title, selected);
		}
	}

	MenuConfigPrintf("\n电能表2:(-emeter2)  \n");
	LOOP(c, E_METER_NUM)
	{
		if(strlen(GetElecMeterType(VAR(c))))
		{
			char title[32] = {0};
			uint8_t selected = 0;
			sprintf(title,"  [%d] %s", VAR(c), GetElecMeterType(VAR(c)));
			if(VAR(c) == g_configInfo.eMeterType2)
			{
				selected = 1;
			}
			echoSelectInfo(title, selected);
		}
	}

	MenuConfigPrintf("\n流量表:(-fmeter)  \n");
	LOOP(b, F_METER_NUM)
	{
		if(strlen(GetFlowMeterType(VAR(b))))
		{
			char title[32] = {0};
			uint8_t selected = 0;
			sprintf(title,"  [%d] %s", VAR(b), GetFlowMeterType(VAR(b)));
			if(VAR(b) == g_configInfo.fMeterType)
			{
				selected = 1;
			}
			echoSelectInfo(title, selected);
		}
	}
	MenuConfigPrintf("\n*************************************************************************\n");


	LOOP(y,5)
	{
		MenuConfigPrintf("\n");
	}
}

static void echoHelpInfo(void)
{
	LOOP(x, CMD_NUM)
	{
		if(g_cmd[VAR(x)])
		{
			MenuConfigPrintf(g_cmd[VAR(x)]);
			MenuConfigPrintf("   ");
		}
	}
}

static void echoSelectInfo(char *title, uint8_t isSelected)
{
	MenuConfigPrintf(title);
	LOOP(x, 32-strlen(title))
	{
		MenuConfigPrintf(" ");
	}
	if(isSelected)
	{
		MenuConfigPrintf("[*]\n");
	}
	else
	{
		MenuConfigPrintf("[ ]\n");
	}
	
}

static void handleDebug(char *cmd)
{
	if(strstr(cmd, "recovery"))
	{
		MenuConfigPrintf("清除绑定信息..\n清除云端数据..\n恢复默认配置..\n重启\n");
		DeviceSavedData_t sd[20]={0};
		memset(sd, 0, sizeof(DeviceSavedData_t)*20);
		SysUserDataWrite(DEVICE_SAVED_DATA_ADDR, (uint8_t *)sd, sizeof(DeviceSavedData_t)*20); //TDO wifi解绑时，一并清除单片机存储的设备信息
		ch_reset_cloud();  //清除云端数据
		g_configInfo.saveFlag = 0xff;
		SysUserDataWrite(CONFIG_SAVED_DATA_ADDR, (const uint8_t *)&g_configInfo, sizeof(MenuconfigInfo_t));
		HalRestart();
	}
}
static void MenuConfigWrite(const uint8_t *data, uint8_t len)
{
    HalUartWrite(LOG_UART, data, len);
}

//可变参数，引用#include "stdarg.h"
#if 0
#ifndef _STDARG_H
#define _STDARG_H
typedef char *va_list;
/* Amount of space required in an argument list for an arg of type TYPE.
   TYPE may alternatively be an expression whose type is used.  */
#define __va_rounded_size(TYPE)  \
  (((sizeof (TYPE) + sizeof (int) - 1) / sizeof (int)) * sizeof (int))
#ifndef __sparc__
#define va_start(AP, LASTARG)       \
 (AP = ((char *) &(LASTARG) + __va_rounded_size (LASTARG)))
#else
#define va_start(AP, LASTARG)       \
 (__builtin_saveregs (),      \
  AP = ((char *) &(LASTARG) + __va_rounded_size (LASTARG)))
#endif
void va_end (va_list);  /* Defined in gnulib */
#define va_end(AP)
#define va_arg(AP, TYPE)      \
 (AP += __va_rounded_size (TYPE),     \
  *((TYPE *) (AP - __va_rounded_size (TYPE))))
#endif 
#endif

void MenuConfigPrintf(char *fmt,...)
{
	char str[256] = {0};
	va_list args;
	va_start(args,fmt);
	vsprintf(str,fmt,args);//sprintf不能用
	va_end(args);
	MenuConfigWrite((const uint8_t *)str, strlen(str));
}

uint8_t MenuConfigRecv(const uint8_t *buf, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		g_menuconfigRecv[g_recvCount++] = buf[i];
		g_menuconfigRecv[g_recvCount] = 0;
	}
	g_lastRecvTime = SysTime();
	g_recvd = 1;
	return 1;
}

uint8_t MenuConfigISStart(void)
{
	return g_menuconfigStart;
}


MenuconfigInfo_t *GetConfig(void)
{
	return &g_configInfo;
}

