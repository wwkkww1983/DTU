#ifndef MAIN_MODULE_H
#define MAIN_MODULE_H
#include "os.h"
#include "app.h"
#include "net.h"

#define UART_HEARTBEAT_SPAN 10000 //ms

void main_module_setaddr(uint8_t *addr);
void main_module_init(void);
void main_module_poll(void);
void main_module_recv_deal(uint8_t *data, uint8_t len, uint8_t *segaddr, uint8_t srcaddr, uint8_t dstaddr, uint8_t rssi);
void main_module_enter_work(void);
void _send_searchdevice_broadcast(void *p);
void main_module_enter_buildnet(uint8_t broadcast_times);
void main_module_set_devicelist(uint8_t *data);
void main_module_add_device(uint8_t addr, uint8_t *uid, uint8_t *key);
void main_module_del_device(uint8_t addr);
void main_module_relay_msg_to_device(uint8_t addr, uint8_t *data, uint8_t len, uint8_t signal);
void main_module_set_device_real_count(uint8_t count);
uint8_t main_module_get_device_real_count(void);
uint8_t *main_module_get_key(uint8_t addr);
void main_module_ack_sync_content_cb(uint8_t *data, uint8_t len);
uint8_t main_module_ack_sync_content_set(uint8_t *data);
void main_module_add_led_blink_count(uint8_t n);
void main_module_enter_upgrade(void);
void main_module_reset_heartbeat_time(void);
void main_module_wakeup_sleep_device(void);
void main_module_set_args(uint8_t addr, uint8_t value);
void main_module_set_check_mode(bool flag);
uint8_t GetWorkChannel(void);


#endif // MAIN_MODULE_H

